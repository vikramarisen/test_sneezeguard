<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_PASSWORD);

  if (isset($HTTP_POST_VARS['action']) && ($HTTP_POST_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    $password_current = tep_db_prepare_input($HTTP_POST_VARS['password_current']);
    $password_new = tep_db_prepare_input($HTTP_POST_VARS['password_new']);
    $password_confirmation = tep_db_prepare_input($HTTP_POST_VARS['password_confirmation']);

    $error = false;

    if (strlen($password_new) < ENTRY_PASSWORD_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR);
    } elseif ($password_new != $password_confirmation) {
      $error = true;

      $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
    }

    if ($error == false) {
      $check_customer_query = tep_db_query("select customers_password from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
      $check_customer = tep_db_fetch_array($check_customer_query);

      if (tep_validate_password($password_current, $check_customer['customers_password'])) {
        tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_encrypt_password($password_new) . "' where customers_id = '" . (int)$customer_id . "'");

        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

        $messageStack->add_session('account', SUCCESS_PASSWORD_UPDATED, 'success');

        tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
      } else {
        $error = true;

        $messageStack->add('account_password', ERROR_CURRENT_PASSWORD_NOT_MATCHING);
      }
    }
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL'));

  require(DIR_WS_INCLUDES . 'template_top.php');
  require('includes/form_check.js.php');
?>



<?
  if (!$detect->isMobile())
{
?>


<style>
  .infoBoxsss tr{height:40px; text-align:center;}
  .infoBoxsss input[type=text] {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
    .infoBoxsss input[type=password] {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
    .infoBoxsss select {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
  .inputRequirement {
    color: #FF0000;
    font-family: Verdana,Arial,sans-serif;
    font-size: 16px;
}




  </style>



<style>

       .button224 {
    background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 19px;
    cursor: pointer;
    width: 136px;
    height: 41px;
}



.button4 {
    border-radius: 10px;
    padding-top: 0px;
}


</style>



<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2" class="infoBoxsss">
<tr>

<td valign="top" >
<h2 style=" height:26px;"><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('account_password') > 0) {
    echo $messageStack->output('account_password');
  }
?>

<?php echo tep_draw_form('account_password', tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL'), 'post', 'onsubmit="return check_form(account_password);"', true) . tep_draw_hidden_field('action', 'process'); ?>

<div class="contentContainer">
  <div>
    <span class="inputRequirement" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></span>
    <h2><?php //echo MY_PASSWORD_TITLE; ?></h2>
  </div>

  <div class="contentText">
    <table border="0" width="100%" cellspacing="2" cellpadding="2">
      <tr>
        <td class="fieldKey" width="125"><?php //echo ENTRY_PASSWORD_CURRENT; ?></td>
        <td class="fieldValue"><?php echo tep_draw_password_field('password_current', '', 'placeholder="Current Password"') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_CURRENT_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_CURRENT_TEXT . '</span>': ''); ?></td>
      </tr>
      <tr> 
        <td class="fieldKey"><?php //echo ENTRY_PASSWORD_NEW; ?></td>
        <td class="fieldValue"><?php echo tep_draw_password_field('password_new', '', 'placeholder="New Password"') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_NEW_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_NEW_TEXT . '</span>': ''); ?></td>
      </tr>
      <tr> 
        <td class="fieldKey"><?php //echo ENTRY_PASSWORD_CONFIRMATION; ?></td>
        <td class="fieldValue"><?php echo tep_draw_password_field('password_confirmation', '', 'placeholder="Password Confirmation"') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_CONFIRMATION_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_CONFIRMATION_TEXT . '</span>': ''); ?></td>
      </tr>
	  
	  <tr>
      	<td><a href="<?php echo tep_href_link(FILENAME_ACCOUNT, '', 'SSL') ?>"><img src="img/new_icons/back_button.png" alt="Back" title=" Back " style="height: 56px;width: 146px;"></a></td>
		<td align="right"><input type="image" class="updatebutton" style="width: 192px;" src="img/new_icons/continue.png" alt="Continue" title=" Continue " primary="" onclick="javascript:document.forms['checkout_payment'].submit();"></td>
      </tr>
     <!-- <tr>
      	<td><?php echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">'.tep_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>';?></td>
		
		<td align="right"><?php echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONTINUE, 'primary'); ?></td>
      </tr>-->
    </table>
  </div>
</div>
</form>
</td></tr></table></td></tr></table>










<?
}

else{
?>


<style>
  .infoBoxsss tr{height:103px; text-align:center;}
  .infoBoxsss input[type=text] {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
    .infoBoxsss input[type=password] {
    width: 492px;
    height: 73px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
    font-size: 30px;
}
  
    .infoBoxsss select {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
  .inputRequirement {
    color: #FF0000;
    font-family: Verdana,Arial,sans-serif;
    font-size: 16px;
}




  </style>



<style>

       .button224 {
    background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 19px;
    cursor: pointer;
    width: 136px;
    height: 41px;
}



.button4 {
    border-radius: 10px;
    padding-top: 0px;
}
.inputRequirement {
    color: #FF0000;
    font-family: Verdana,Arial,sans-serif;
    font-size: 28px;
}

</style>


<td id="ex1" align=center width="190" valign="top">
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2" class="infoBoxsss">
<tr>

<td valign="top" >
<h2 style=" height:26px;font-size: 34px;"><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('account_password') > 0) {
    echo $messageStack->output('account_password');
  }
?>

<?php echo tep_draw_form('account_password', tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL'), 'post', 'onsubmit="return check_form(account_password);"', true) . tep_draw_hidden_field('action', 'process'); ?>

<div class="contentContainer">
  <div>
    <span class="inputRequirement" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></span>
    <h2><?php //echo MY_PASSWORD_TITLE; ?></h2>
  </div>

  <div class="contentText">
    <table border="0" width="100%" cellspacing="2" cellpadding="2">
      <tr>
        <td class="fieldKey" width="125"><?php //echo ENTRY_PASSWORD_CURRENT; ?></td>
        <td class="fieldValue"><?php echo tep_draw_password_field('password_current', '', 'placeholder="Current Password"') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_CURRENT_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_CURRENT_TEXT . '</span>': ''); ?></td>
      </tr>
      <tr> 
        <td class="fieldKey"><?php //echo ENTRY_PASSWORD_NEW; ?></td>
        <td class="fieldValue"><?php echo tep_draw_password_field('password_new', '', 'placeholder="New Password"') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_NEW_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_NEW_TEXT . '</span>': ''); ?></td>
      </tr>
      <tr> 
        <td class="fieldKey"><?php //echo ENTRY_PASSWORD_CONFIRMATION; ?></td>
        <td class="fieldValue"><?php echo tep_draw_password_field('password_confirmation', '', 'placeholder="Password Confirmation"') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_CONFIRMATION_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_CONFIRMATION_TEXT . '</span>': ''); ?></td>
      </tr>
	  </table>
	  <table>
	  <tr>
      	<td style="width:50%"><a href="<?php echo tep_href_link(FILENAME_ACCOUNT, '', 'SSL') ?>"><img src="img/new_icons/back_button.png" alt="Back" title=" Back " style="height: 118px;width: 222px;"></a></td>
		<td style="width:50%" align="right"><input type="image" class="updatebutton" style="width: 361px;" src="img/new_icons/continue.png" alt="Continue" title=" Continue " primary="" onclick="javascript:document.forms['checkout_payment'].submit();"></td>
      </tr>
     <!-- <tr>
      	<td><?php echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">'.tep_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>';?></td>
		
		<td align="right"><?php echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONTINUE, 'primary'); ?></td>
      </tr>-->
    </table>
  </div>
</div>
</form>
</td></tr></table></td></tr></table>









<?php
    }
?>



<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
