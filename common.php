<?php
/*
  $Id: index.php,v 1.1 2003/06/11 17:37:59 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  
  Released under the GNU General Public License
*/



  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);
  
// ini_set('display_errors', '1');
// ini_set('display_startup_errors', '1');
// error_reporting(E_ALL);
// the following cPath references come from application_top.php
  $category_depth = 'top';
  if (isset($cPath) && tep_not_null($cPath)) {
    $categories_products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$current_category_id . "'");
    $cateqories_products = tep_db_fetch_array($categories_products_query);
    if ($cateqories_products['total'] > 0) {
      $category_depth = 'products'; // display products
    } else {
      $category_parent_query = tep_db_query("select count(*) as total from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$current_category_id . "'");
      $category_parent = tep_db_fetch_array($category_parent_query);
      if ($category_parent['total'] > 0) {
        $category_depth = 'nested'; // navigate through the categories
      } else {
        $category_depth = 'products'; // category has no products, but display the 'no products' message
      }
    }
  }

  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
		
?>

 
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>


<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-1072651700');
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>


<title>ADM Sneezeguards - Contact Us | Sneeze Guard Online Questions</title>
<!-- End Google Add Conversion -->




<link rel="preconnect" href="https://www.sneezeguard.com">
<link rel="dns-prefetch" href="https://www.sneezeguard.com">


<link rel="preload" as="script" href="jquery-latest.js">
<link rel="preload" as="script" href="thickbox.js">
<link rel="preload" as="script" href="ext/jquery/jquery-1.4.2.min.js">
<link rel="preload" as="script" href="ext/jquery/ui/jquery-ui-1.8.6.min.js">
<link rel="preload" as="script" href="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js">
<link rel="preload" as="script" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js">
<link rel="preload" as="script" href="jquery.colorbox3.js">
<link rel="preload" as="script" href="./dist/html2canvas.js">
<link rel="preload" as="script" href="./dist/canvas2image.js">
<link rel="preload" as="script" href="https://platform.twitter.com/widgets.js">
<link rel="preload" as="script" href="jquery.confirm/jquery.confirm.js">





<link rel="preload" as="style" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css">
<link rel="preload" as="style" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css">
<link rel="preload" as="style" href="ext/960gs/960_24_col.css">
<link rel="preload" as="style" href="stylesheet.css">
<link rel="preload" as="style" href="colorbox3.css">
<link rel="preload" as="style" href="jquery/jquery.simplyscroll.css">



<link rel="preload" as="image" href="https://www.sneezeguard.com/images/new_logo_main.png">
<link rel="preload" as="image" href="images/wishlist_icon.png">
<link rel="preload" as="image" href="image_new/office.png">
<link rel="preload" as="image" href="image_new/banking.png">
<link rel="preload" as="image" href="image_new/grossery.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/1.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/3.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/5.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/16.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/21.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/22.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/23.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/24.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/25.png">


<link rel="preload" as="image" href="img/close.png">
<link rel="preload" as="image" href="img/mail/mail_img.png">
<link rel="preload" as="image" href="social_icons/rss-icon.png">
<link rel="preload" as="image" href="img/social/facebook.png">
<link rel="preload" as="image" href="img/social/twittee.png">
<link rel="preload" as="image" href="img/social/linklind.png">
<link rel="preload" as="image" href="img/social/pintrest.png">
<link rel="preload" as="image" href="img/social/instagram.png">
<link rel="preload" as="image" href="img/social/youtube.png">




<link rel="preload" as="image" href="images/bulletpt.jpg">
<link rel="preload" as="image" href="images/logo.jpg">
<link rel="preload" as="image" href="images/navbar/homeup.jpg">
<link rel="preload" as="image" href="images/navbar/instockup.jpg">
<link rel="preload" as="image" href="images/navbar/customup.jpg">
<link rel="preload" as="image" href="images/navbar/adjustableup.jpg">
<link rel="preload" as="image" href="images/navbar/portableup.jpg">
<link rel="preload" as="image" href="images/navbar/orbitup.jpg">
<link rel="preload" as="image" href="images/navbar/shelvingup.jpg">
<link rel="preload" as="image" href="background.jpg">
<link rel="preload" as="image" href="images/middleback.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/In-Stock.jpg">
<link rel="preload" as="image" href="images/shortVertSepIS.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/Custom.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/Adjustable.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/Portables.jpg">
<link rel="preload" as="image" href="img/social/new_img_bestprice.jpg">
<link rel="preload" as="image" href="images/backBottom.jpg">
<link rel="preload" as="image" href="images/comstr.jpg">
<link rel="preload" as="image" href="images/nsflogo.jpg">










<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<meta name="description" content="Custom sneeze guard are available sneezeguard.com Here you will find various shapes, styles and sizes; all with numerous options and made-to-order specific requirements.">
<meta name="keywords" content="ADM Sneezeguards contact us, Sneeze Guard Questions, Common questions Sneeze Guards, Sneeze Guard Manufacturer company, sneeze posts">


<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com/<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>




<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<!--<title><?php echo TITLE; ?></title>-->

<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">

<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>

<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">

<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />
<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />







<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

  <!-- header_eof //-->
  
  <!-- body //-->
  
  
  <?php
	if (!$detect->isMobile())
	{
	?>	
	
	

<?
}

else{
?>


<td id="ex1" align=center width="190" valign="top">

	
	
<?
}
?>
  <div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 
<tr>
<td colspan="3" style=" height:26px; border:1px solid #ccc;">&nbsp;</td>
</tr>
<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
</p>
<p align="center" class="style1 style7">Common Questions</p>
<p class="style4">Where do you fabricate/get your parts from?</p>
<p class="style2">All our parts are fabricated here in Concord, California and we are proud to say "Made in USA" for all of our Sneezeguard lines.</p>
<p class="style4">Do you offer dealer discounts?</p>
<p class="style2"> Yes, please call for information on our dealer discount program. (1-800-690-0002)</p>
<p class="style2">_________________________________________________ </p>
<p class="style2 style3"><strong>What are the available finishes?</strong></p>
<div align=center><p class="style2"><img alt="sneeze guard" title="sneeze guard for office" src="images/Finishes_old.jpg" width="640" height="270"></p>
  <p class="style8"><strong>PDF Flange Layout: </strong><a href="https://www.sneezeguard.com/PDF/flange.pdf"><img alt="sneeze guard" title="sneeze guard for office" src="https://www.sneezeguard.com/images/pdf_icon.gif" width="50" height="50"></a></p>
</div>
<p class="style2">_________________________________________________ </p>
<p class="style4">Do I have to order over       the internet?</p>
<p class="style2"> No&hellip; Placing       orders over the internet is only for your convenience. </p>
<p class="style2"> If you       prefer, our knowledgeable and friendly staff is here to assist you. (1-800-690-0002)</p>
<p class="style2">_________________________________________________ </p>
<p class="style4">Is custom Glass Sizing offered?</p>
<p class="style2"> Yes&hellip; we can custom order glass to any length, lead time on Custom Glass is 4-5 Business days. (1-800-690-0002)</p>
<p class="style2">See below for our sizing recommendations.</p>
<p class="style2">_________________________________________________ </p>
<p class="style4">How do I determine Glass Length?</p>
<p class="style2"> To detetmine glass length, take the center to center dimension of your posts and subtract 1 7/8&quot; inches.</p>
<p class="style2">Additional Glass sizing information is located under the &quot;Post Dimension&quot; button in each product category.</p>
<p class="style2">_________________________________________________ </p>
<p class="style2"><span class="style5">How far can I       span each sneezeguard post?</span><br>
  <br>
Using: 1/4'' glass, don&rsquo;t exceed 42''<br>
<br>
Using: 3/8'' glass, don&rsquo;t exceed 54'' </p>
<p class="style2"> &nbsp;Any length       beyond the above noted sizes will exceed the physical glass       limitations. To span further apart, may cause the glass       panels to bow &amp; even explode!</p>
<p class="style2"> <strong> Caution:</strong> Companies that offer &ldquo;Cross-supports       to span long openings&rdquo; are improperly&nbsp;exceeding the glass panel/s       tensile-strength and posing a danger to you and your clients.</p>
<p class="style2">_________________________________________________ </p>
<p class="style4">How       can we offer such low prices?</p>
<p class="style2"><strong>&nbsp;We&nbsp;have       the ability to manufacture all of our own products in house through the use       of :</strong></p>
<p class="style2"> ..... CNC       mandrel bending machines</p>
<p class="style2"> ..... CNC       machining stations</p>
<p class="style2"> ..... CNC       turning centers</p>
<p class="style2"> ..... CNC       Swiss screw machines</p>
<p class="style2"> ..... Full       Powder-Coating facility</p>
<p class="style2"> ..... Full       finishing / polishing facility</p>
<p class="style2"> .....       Automated order processing and confirmation system</p>
<p class="style2"> &nbsp;<strong>All       of these above capabilities under one roof; plus .....</strong></p>
<p class="style2"> ..... Every       part and process manufactured to its absolute highest quality</p>
<p class="style2"> .....       Large ready-to-ship inventories</p>
<p class="style2"> We take       pride in&nbsp;passing all savings that comes with being an industry leader. <em> In-fact,&nbsp;we thank every&nbsp;order we receive by shipping it on time. </u>&nbsp;&nbsp;</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p class="style2"><br>
  _________________________________________________</p>
<p class="style2"><strong><br>
    <span class="style6">Can my Express       Food-Guard be modified for my application?</span></strong><br>
  <br>
  Yes&hellip; We do limit our modifications to certain models, call for assistance.</p>
<p class="style2"><br>
  _________________________________________________<br>
  <br>
  <span class="style6"><strong>Do you have any       additional models or custom units available?</strong></span><br>
  <br>
  Our Express Food-Guard line of sneezeguards are designed to be fast-track,&nbsp;&nbsp;&nbsp;       in-stock &amp; ready for immediate shipment program. In order to maintain this       program, we must restrict the number of models and modifications to this       line. </p>
<p class="style2"> <strong><u>Custom       sneezeguards</u>:</strong> are available from our parent company at: <a href="https://www.sneezeguard.com/index.html"> www.sneezeguard.com </a>Here you will find       various shapes, styles and sizes; all with numerous options and       made-to-order specific requirements. </p>
<p class="style2">Custom Dept. Telephone: 1-800-805-1114</p>
<p class="style2">Email: <a href="mailto:sales@sneezeguards.com">sales@sneezeguards.com</a></p>
<p class="style1">&nbsp;</p>
</td></tr></table></td></tr></table></div>

<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
