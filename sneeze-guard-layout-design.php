<?php
ob_start();

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
  require('includes/application_top.php');
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADVANCED_SEARCH);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ADVANCED_SEARCH));

  //require(DIR_WS_INCLUDES . 'template_top.php');
// insert your mysql connection code here

?>


 <?php
 
 
 /*Replace Titles start ob_start(); is on top
 //ob_start();
    $buffer=ob_get_contents();
    ob_end_clean();
 
 
 $keyword = 'Sneeze Guard, Sneeze Guard Glass, Sneeze Guard Buffet, Sneeze Guard Frames, Vertical Sneeze Guard';
  //add anew desc and author
$buffer = str_replace('name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment"', 'name="keywords" content="'.$keyword.'"', $buffer);

 $titlessss = 'Sneeze Guard Portable | Buffet Guards - ADM Sneezeguards';
    $buffer = preg_replace('/(<title>)(.*?)(<\/title>)/i', '$1' . $titlessss . '$3', $buffer);

    echo $buffer;
    
    
    */
    
    
    


  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard Layout Design Portable - ADM Sneezeguards</title>
<!-- End Google Add Conversion -->

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="Maintain social distance and physical separation in Hospital, Grocery, Office, Counter while protecting from virus and germs with our sneeze guard layout design model">
<meta name="keywords" content="Sneeze Guard Layout Design, Sneeze Guard Glass, Sneeze Guard Buffet, Sneeze Guard Frames, Vertical Sneeze Guard">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">

<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>


<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />
<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />






<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

	
 


<!--
<script>
$( document ).ready(function() {
 window.location.href = "https://sneezeguard.com";
});
</script>
<body onload=window.location='https://sneezeguard.com'>
-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<?php

        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        $no_of_records_per_page = 30;
        $offset = ($pageno-1) * $no_of_records_per_page;

        //$conn=mysqli_connect("localhost","my_user","my_password","my_db");
		//$conn=mysqli_connect("localhost","root","","esneezeg_new_sneezeguard") or die(mysqli_connect_error());
		
		$servername = DB_SERVER;
		$username = DB_SERVER_USERNAME;
		$password = DB_SERVER_PASSWORD;
		$dbname = DB_DATABASE;
		$conn=mysqli_connect($servername,$username,$password,$dbname) or die(mysqli_connect_error());
		
		
        // Check connection
        if (mysqli_connect_errno()){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            die();
        }

        $total_pages_sql = "SELECT COUNT(*) FROM orders";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result);
        $total_pages = 700;

        $sql = "SELECT * FROM orders WHERE  `orders_id` > 905300 ORDER BY orders_id DESC LIMIT $offset, $no_of_records_per_page";
        $res_data = mysqli_query($conn,$sql);
		$srno=1;
        while($row = mysqli_fetch_array($res_data)){
            //here goes the data
		$orders_id=$row[0];

		$sql22 = "SELECT * FROM `screen_table` where order_no ='".$orders_id."'";
        $res_data22 = mysqli_query($conn,$sql22);
        while($row22 = mysqli_fetch_array($res_data22)){
			echo'<h3>'.$row22['category'].'</h3>';
			echo'<center><img src="includes/img/'.$row22['osc_id'].'_'.$row22["img_id"].'.jpg" title="Sneeze Guard" alt="Sneeze Guard" style="width: 640px;"></center>';
			echo'<h3>Sneeze guard</h3>';
			echo'<hr />
			
			
			';
			//echo'order'.$l.''.$orders_id.'<br />';
			
			$srno++;
		}
	

		
			
			 // echo'order'.$l.''.$hdgsgfs.'<br />';
        }
        mysqli_close($conn);
    ?>
    <ul class="pagination" style="white-space:nowrap; text-align: center;">
        <li style="display: inline;"><a href="sneezeguard-layout-design.php?pageno=1">&bull; First</a></li>
        <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>" style="display: inline;margin-left: 10px;">
            <a href="sneezeguard-layout-design.php<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">&bull; Prev</a>
        </li>
        <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>" style="display: inline;margin-left: 10px;">
            <a href="sneezeguard-layout-design.php<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">&bull; Next</a>
        </li>
        <!--         <li style="display: inline;margin-left: 10px;"><a href="sneezeguard-layout-design.php?pageno=<?php echo $total_pages; ?>">&bull; Last</a></li> -->
    </ul>

<?php


/*
$perPage = 10;
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$startAt = $perPage * ($page - 1);













$query =tep_db_query("SELECT COUNT(*) as total FROM orders");
$r =tep_db_fetch_array($query);

$totalPages = ceil($r['total'] / $perPage);

$links = "";
for ($i = 1; $i <= $totalPages; $i++) {
  $links .= ($i != $page ) 
            ? "<a href='sneezeguard-layout-design.php?page=$i'>Page $i</a> "
            : "$page ";
}





 $res=tep_db_query("select * from orders where `orders_id` > 905300 order by orders_id desc");
                $l=1;
                $sssss=0;
                while($row=tep_db_fetch_array($res)){
                    $orders_id=$row['orders_id'];
					//echo'<br />';
					
				$res_img=tep_db_query("SELECT * FROM `screen_table` where order_no ='".$orders_id."'");	
				
				$ct=0;
          while($row_img=tep_db_fetch_array($res_img)){
			  
			  $hdgsgfs=$row_img[0];
			  echo'order'.$l.''.$hdgsgfs.'<br />';
		  }
		  $l++;
				}



*/

/*

$r = mysql_query($query);

$query = "SELECT * FROM 'redirect'
WHERE 'user_id'= \''.$_SESSION['user_id'].' \' 
ORDER BY 'timestamp' LIMIT $startAt, $perPage";

$r = mysql_query($query);

// display results here the way you want

echo $links; // show links to other pages

*/

?>


<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>