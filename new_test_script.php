	<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="jquery.table2excel.js"></script>
<script>
$(document).ready(function() {

  function exportTableToCSV($table, filename) {

    var $rows = $table.find('tr:has(td)'),

      // Temporary delimiter characters unlikely to be typed by keyboard
      // This is to avoid accidentally splitting the actual contents
      tmpColDelim = String.fromCharCode(11), // vertical tab character
      tmpRowDelim = String.fromCharCode(0), // null character

      // actual delimiter characters for CSV format
      colDelim = '","',
      rowDelim = '"\r\n"',

      // Grab text from table into CSV formatted string
      csv = '"' + $rows.map(function(i, row) {
        var $row = $(row),
          $cols = $row.find('td');

        return $cols.map(function(j, col) {
          var $col = $(col),
            text = $col.text();

          return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

      }).get().join(tmpRowDelim)
      .split(tmpRowDelim).join(rowDelim)
      .split(tmpColDelim).join(colDelim) + '"';

    // Deliberate 'false', see comment below
    if (false && window.navigator.msSaveBlob) {

      var blob = new Blob([decodeURIComponent(csv)], {
        type: 'text/csv;charset=utf8'
      });

      // Crashes in IE 10, IE 11 and Microsoft Edge
      // See MS Edge Issue #10396033
      // Hence, the deliberate 'false'
      // This is here just for completeness
      // Remove the 'false' at your own risk
      window.navigator.msSaveBlob(blob, filename);

    } else if (window.Blob && window.URL) {
      // HTML5 Blob        
      var blob = new Blob([csv], {
        type: 'text/csv;charset=utf-8'
      });
      var csvUrl = URL.createObjectURL(blob);

      $(this)
        .attr({
          'download': filename,
          'href': csvUrl
        });
    } else {
      // Data URI
      var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

      $(this)
        .attr({
          'download': filename,
          'href': csvData,
          'target': '_blank'
        });
    }
  }

  // This must be a hyperlink
  $(".export").on('click', function(event) {
    // CSV
    var args = [$('#dvData>table'), 'EP15_new_right_end_products.csv'];

    exportTableToCSV.apply(this, args);

    // If CSV, don't do event.preventDefault() or return false
    // We actually need this to be a typical hyperlink
  });
});

//Warning: file_get_contents(D:\xampp\htdocs\magento\magmi\state\): failed to open stream: No such file or directory in D:\xampp\htdocs\magento\magmi\web\progress_parser.php on line 25
</script>


</head>
<body>
	<style>
	a.export, a.export:visited {
    text-decoration: none;
    color:#000;
    background-color:#ddd;
    border: 1px solid #ccc;
    padding:8px;
}
</style>	

<a href="#" class="export">Export Table data into Excel</a><!--
<div class="search-inner" id="dataTableDiv" ng-class="searchFilter ? 'results-on' : ''" ng-show="!emptyAgreements">

<table width="auto" border=1 id="dataTable" class="table table-bordered ag-table">
-->

<div id="dvData">
<table width="auto" border=1 id="dataTable" class="table table-bordered">
<?php

//Header for excel file

print "<tr>";print"<th>";print "sku";print"</th>";print"<th>";print "_attribute_set";print"</th>";print"<th>";print"_type";print"</th>";
print"<th>";print"_category";print"</th>";print"<th>";print"_root_category";print"</th>";print"<th>";print"_product_websites";print"</th>";
print"<th>";print "name";print"</th>";print"<th>";print"description";print"</th>";print"<th>";print "price";print"</th>";
print"<th>";print "weight";print"</th>";print"<th>";print "height";print"</th>";print"<th>";print "width";print"</th>";
print"<th>";print "status";print"</th>";print"<th>";print "tax_class_id";print"</th>";print"<th>";print "visibility";print"</th>";
print"<th>";print "short_description";print"</th>";print"<th>";print "qty";print"</th>";print"<th>";print "is_in_stock";print"</th>";
print"<th>";print "image";print"</th>";print"<th>";print "small_image";print"</th>";print"<th>";print "thumbnail";print"</th>";
print"<th>";print "media_gallery";print"</th>";print"<th>";print "_media_attribute_id";print"</th>";print"<th>";print "_media_image";print"</th>";
print"<tr>";

// Squared corners


$k=301075;




// Decimal to fraction conversion

function decimal_to_fraction($x)
{
	$x=$x-(int)$x;
	$heightfraction=$x;
	
		if($heightfraction==0.25)
		{
			print '-1/4';
		}
		elseif($heightfraction==0.50)
		{
			print '-1/2';
		}
		elseif($heightfraction==0.75) 
		{
			print '-3/4';
	    }
		elseif($heightfraction==0.125)
		{
			print '-1/8';
		}
		elseif($heightfraction==0.375)
		{
			print '-3/8';
		}
		elseif($heightfraction==0.625)
		{
			print '-5/8';
		}
		elseif($heightfraction==0.875)
		{
			print '-7/8';
		}
	
	
	}
	
	
	
	// Custom product squared 
	
	
	function custom_check($a,$b)
{   $flag=0;
	//old
	//$noncustom_height=array(18,22);
	$noncustom_height=array(18);
	$noncustom_width=array(12,18,24,30,36,42);
	
  for($i=0;$i<=1;$i++)
{
	for($j=0;$j<=5;$j++)
	{
		if($a==$noncustom_height[$i] && $b==$noncustom_width[$j])
		{
			$flag=1;
			break;
		
		}
	}
}
		if($flag==0)
		{
			print "custom products**";
		}
			
		
}



 
 	


// Right End Glass

for($height=8;$height<=30;$height=$height+0.25)
{
	for($size=8;$size<=42;$size=$size+0.25)
	{
		
 
print "";
print "EP15 ";
print (int)$height;
print decimal_to_fraction($height);
print '" ';
print (int)$size;
print decimal_to_fraction($size);
print '" ';
print "Right End Glass(Squared Corners)";
custom_check($height,$size);
print "<br />";


	}
}


	



	
	
	
	
?>

</table>
</body>
</html>