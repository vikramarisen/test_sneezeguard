<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
<?php 
ini_set('display_errors', 0);
require("includes/application_top.php");
include('includes/header_new_design.php'); 
?>
<style>
    body{
        background-color: white;
    }
    .container .row{
        border: none ; 
        margin-top: 0px ; 
    }
    
    .form-group input {
            border: 0px !important;
            border-radius: 0px !important;
            outline: 2px solid #9a9a9b !important;
            height: 40px !important;
            font-size: 20px !important;
        }
        .form-group label{
            font-size: 13px;
        }
        
        .form-group .btn-outline-dark {
            border: 0px !important;
            border-radius: 0px !important;
            outline: 2px solid #000000c7 !important;
            font-size: 15px !important;
        }

        .cbtn .btn-outline-dark {
            border: 0px !important;
            border-radius: 0px !important;
            outline: 2px solid #000000c7 !important;
            font-size: 15px;
        }
        .cbtn input{
            height: 40px !important;
        }
        .cbtn{
                width: 70%;
            }
        
        .form-group a:hover,
        .form-group a:focus {
            color: #d46405 !important;
        }
        
        .form-group a.color {
            color: #ed7006 !important;
            font-size: medium !important;
        }
        @media screen and (max-width: 576px) {
            .col-md-6{
                border-right: none !important;
            }
            .cbtn{
                width: 100%;
            }
        }
    </style>
<body>
<section>
    <div class="mt-5">
        <h1>&nbsp;</h1>
    </div>
    <h1 class="text-center p-2 font-weight-bold">LOGIN</h1>
    <h3 class="text-center">LOG IN OR SIGN UP TO SNEEZEGUARD</h3>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 border-right pl-5 pr-5 pb-5">
                <form>
                    <div>
                        <h3 class="text-uppercase font-weight-bold">Returning Customers</h3>
                    </div>
                    <div class="mt-2 mb-1">
                        <h5 class="text-dark font-weight-normal">I am a returning customer.</h5>
                    </div>
                    <div class="form-group text-secondary">
                        <label for="email" class="text-uppercase">Email</label>
                        <input type="email" class="form-control" name="email" id="email">
                    </div>
                    <div class="form-group text-secondary">
                        <label for="password" class="text-uppercase">Password</label>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>
                    <div class="form-group p-3 text-center">
                        <a href="#" target="" rel="" class="text-danger"><h4>Forgot your password?</h4></a>
                    </div>
                    <div class="form-group">
                        <?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?>
                        <div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_PUBLIC_KEY; ?>"></div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-outline-dark form-control p-2" value="SIGN IN">
                    </div>
                </form>
            </div>
            <div class="col-md-6 col-sm-6 pl-5 pr-5">
                <div>
                    <h3 class="text-uppercase font-weight-bold">Check out as a guest</h3>
                </div>
                <div class="mt-2 mb-1">
                    <h5 class="text-dark font-weight-normal">Proceed to checkout and create a Login ID with us .</h5>
                </div>
                <div class="mt-4 text-uppercase cbtn">
                    <input type="submit" class="btn btn-outline-dark form-control p-2" value="CONTINUE AS GUEST">
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
<?php include('includes/footer_new_design.php');?>