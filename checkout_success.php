<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
//-----------------------------------------------


//----------------------------------------------------
  
  
  
  
  require('includes/application_top.php');
  


//echo $tishu;
//exit;

// if the customer is not logged on, redirect them to the shopping cart page
  if (!tep_session_is_registered('customer_id')) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  }

  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'update')) {
    $notify_string = '';

    if (isset($HTTP_POST_VARS['notify']) && !empty($HTTP_POST_VARS['notify'])) {
      $notify = $HTTP_POST_VARS['notify'];

      if (!is_array($notify)) {
        $notify = array($notify);
      }

      for ($i=0, $n=sizeof($notify); $i<$n; $i++) {
        if (is_numeric($notify[$i])) {
          $notify_string .= 'notify[]=' . $notify[$i] . '&';
        }
      }

      if (!empty($notify_string)) {
        $notify_string = 'action=notify&' . substr($notify_string, 0, -1);
      }
    }

    tep_redirect(tep_href_link(FILENAME_DEFAULT, $notify_string));
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SUCCESS);

  $breadcrumb->add(NAVBAR_TITLE_1);
  $breadcrumb->add(NAVBAR_TITLE_2);

  $global_query = tep_db_query("select global_product_notifications from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customer_id . "'");
  $global = tep_db_fetch_array($global_query);

  if ($global['global_product_notifications'] != '1') {
    $orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where customers_id = '" . (int)$customer_id . "' order by date_purchased desc limit 1");
    $orders = tep_db_fetch_array($orders_query);

    $products_array = array();
    $products_query = tep_db_query("select products_id, products_name from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$orders['orders_id'] . "' order by products_name");
    while ($products = tep_db_fetch_array($products_query)) {
      $products_array[] = array('id' => $products['products_id'],
                                'text' => $products['products_name']);
    }
  }

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script>
  $(document).ready(function(){
    if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
        $("#ex1").css('width',"100%");
        $(".price_table").css("text-align","left");
    }
    });

</script>

<?
  if (!$detect->isMobile())
{
?>

<style>

.processccc{color:#605b5b; font-size:12px;}

p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 17px;
    /* padding-left: 35px; */
}
</style>
<div class="" style="background-color:white;" >
<table width="100%" border="0" style="font:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bolder;  ">
  <tr>
    <td class="processccc">1. Log In	</td>
    <td class="processccc">2. Address Information</td>
    <td class="processccc">3. Shipping & Delivery</td>
    <td class="processccc">4. Payment Options</td>
    <td class="processccc">5. Order Review </td>
    <td class="processccc"><b style="color:Red;">6. Order Receipt</b></td>
  </tr>
</table>
<div class="form_white" style="height:auto !important;   margin-bottom:40px; padding-bottom:70px;" >
<h3  align="left" style="padding:5px;font-size: 16px; font-family:Arial, Helvetica, sans-serif; color:#000000; ">Thank you for your Order.</h3> 
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc; padding:20px;" p>

<h2  align="left" style="padding:5px; font-family:Arial, Helvetica, sans-serif; ">Order Receipt</h2> 
<h1 style="font-size: 13px;"><?php echo HEADING_TITLE; ?></h1>
<strong  style="font-size: 14px;">Order Number :  &nbsp;<?php echo $orders[orders_id];?></strong>
<?php echo tep_draw_form('order', tep_href_link(FILENAME_CHECKOUT_SUCCESS, 'action=update', 'SSL')); ?>

<div class="contentContainer" style="text-align: left;">
  <div class="contentText">
    <?php echo TEXT_SUCCESS; ?>
	
  </div>

  <div class="contentText">
  
  
  
  
  
  
 <b> 

<?php
  if ($global['global_product_notifications'] != '1') {
    echo TEXT_NOTIFY_PRODUCTS . '<br /><p class="productsNotifications">';

    $products_displayed = array();
    for ($i=0, $n=sizeof($products_array); $i<$n; $i++) {
      if (!in_array($products_array[$i]['id'], $products_displayed)) {
       //echo tep_draw_checkbox_field('notify[]', $products_array[$i]['id']) ;
	    echo $products_array[$i]['text'] . '<br />';
         $products_displayed[] = $products_array[$i]['id'];
      }
    }

    echo '</p>';
  }
?>
</b>
<?php
  echo TEXT_SEE_ORDERS . '<br /><br />' . TEXT_CONTACT_STORE_OWNER;
?>

  </div>

  <div class="contentText">
    <h3><?php echo TEXT_THANKS_FOR_SHOPPING; ?></h3>
  </div>

<?php
  if (DOWNLOAD_ENABLED == 'true') {
    include(DIR_WS_MODULES . 'downloads.php');
  }
?>

  <!--<div class="buttonSet">
    <span class="buttonAction"><?php echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONTINUE, 'button'); ?></span>
	
  </div>-->
  <div class="buttonSet">
    <span class="buttonAction"><input type="image" class="updatebutton" style="width:202px;" src="img/new_icons/continue.png" alt="Continue" title=" Continue " button="" onclick="javascript:document.forms['checkout_payment'].submit();"></span>
	
  </div>
  
  
</div>
</td></tr></table></div></td></tr></table>

</form>
</div>

<?
}
  else
{
?>
<style>

.processccc{color:#605b5b; font-size:22px;}

p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 13px;
    /* padding-left: 35px; */
}

.contentText, .contentText table {
    /* padding: 5px 0 5px 0; */
    font-size: 12px;
    line-height: 1.5;
}
</style>
<td id="ex1" align=center width="190" valign="top">
<div class="" style="background-color:white;" >
<table width="100%" border="0" style="font:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bolder;  ">
  <tr>
    <td class="processccc">1. Log In	</td>
    <td class="processccc">2. Address Information</td>
    <td class="processccc">3. Shipping & Delivery</td>
	</tr>
	<tr>
    <td class="processccc">4. Payment Options</td>
    <td class="processccc">5. Order Review </td>
    <td class="processccc"><<b style="color:Red;">>6. Order Receipt</b></td>
  </tr>
</table>
<div class="form_white" style="height:auto !important;   margin-bottom:40px; padding-bottom:70px;" >
<h3  align="left" style="padding:5px;font-size: 28px; font-family:Arial, Helvetica, sans-serif; color:#000000; ">Thank you for your Order.</h3> 
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc; padding:20px;" p>

<h2  align="left" style="padding:5px; font-family:Arial, Helvetica, sans-serif; ">Order Receipt</h2> 
<h1 style="font-size: 12px;"><?php echo HEADING_TITLE; ?></h1>
<strong  style="font-size: 12px;">Order Number :  &nbsp;<?php echo $orders[orders_id];?></strong>
<?php echo tep_draw_form('order', tep_href_link(FILENAME_CHECKOUT_SUCCESS, 'action=update', 'SSL')); ?>

<div class="contentContainer" style="text-align: left;">
  <div class="contentText">
    <?php echo TEXT_SUCCESS; ?>
	
  </div>

  <div class="contentText">
  
  
  
  
  
  
 <b> 

<?php
  if ($global['global_product_notifications'] != '1') {
    echo TEXT_NOTIFY_PRODUCTS . '<br /><p class="productsNotifications">';

    $products_displayed = array();
    for ($i=0, $n=sizeof($products_array); $i<$n; $i++) {
      if (!in_array($products_array[$i]['id'], $products_displayed)) {
       //echo tep_draw_checkbox_field('notify[]', $products_array[$i]['id']) ;
	    echo $products_array[$i]['text'] . '<br />';
         $products_displayed[] = $products_array[$i]['id'];
      }
    }

    echo '</p>';
  }
?>
</b>
<?php
  echo TEXT_SEE_ORDERS . '<br /><br />' . TEXT_CONTACT_STORE_OWNER;
?>

  </div>

  <div class="contentText">
    <h3 style="font-family:Arial, Helvetica, sans-serif; color:#000000;"><?php echo TEXT_THANKS_FOR_SHOPPING; ?></h3>
  </div>

<?php
  if (DOWNLOAD_ENABLED == 'true') {
    include(DIR_WS_MODULES . 'downloads.php');
  }
?>

  <!--<div class="buttonSet">
    <span class="buttonAction"><?php echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONTINUE, 'button'); ?></span>
	
  </div>-->
  <div>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <span ><input type="image" class="updatebutton" style="width:626px;height: 122px;" src="img/new_icons/continue.png" alt="Continue" title=" Continue " button="" onclick="javascript:document.forms['checkout_payment'].submit();"></span>
	
  </div>
  
  
</div>
</td></tr></table></div></td></tr></table>

</form>
</div>


<?
}
?>
<script type="text/javascript">
  $(document).ready(function() {
    console.log('run...');
    news_feed_call();

  function news_feed_call(){
  $.ajax({
    type:'POST',
    url:'https://www.sneezeguard.com/news_rss.php',
    async: false,
  success: function(){
    console.log('success');
    //         setTimeout(function(){
    //   news_feed_call();
    // }, 300000);
  }
    });
}
});
</script>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
