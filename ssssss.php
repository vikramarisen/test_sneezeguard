
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<script src="js.js"></script>

	 
	<link rel="stylesheet" type="text/css" href="css.css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="google-site-verification" content="xsRWXo9DojaFZHaVbn2EYFEHHUoC8bhu0IaMtcYkz6Y" />


<link rel="alternate" href="" hreflang="en-US" />
<meta name="viewport" content="width=device-width">

<meta property="og:url" content="http://admglass.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Custom Glasses, Table Top, Replacement - ADM Glass"/>
<meta property="og:description" content="Order online ADM Glass for all kinds of glasses, mirror, table top, replacement, tempered, self, beveled, oval mirror, square mirror, custom and mirrors." />
<meta property="og:image" content="http://admglass.com/img/pic2.jpg" />
<meta property="og:site_name" content="ADM Glass"/>
<meta property="fb:app_id" content="415324555227963"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="http://admglass.com/img/pic2.jpg"/>
<meta name="twitter:site" content="@ADMGlass"/>
<meta name="twitter:url" content="https://twitter.com/ADMGlass"/>
<meta name="twitter:description" content="ADM GLASS provides you with CUSTOM CUT GLASS of desired shape and size according to customer needs. Order online a variety of glass products in our glass shop."/>
<meta name="twitter:title" content="Custom Shape Dimension ADM Glass Showerdoor" />



<link rel="canonical" href="https://www.admglass.com">
<meta name="description" content="Order online ADM Glass for all kinds of glasses, mirror, table top, replacement, tempered, self, beveled, oval mirror, square mirror, custom and mirrors." />


<title>Custom Glasses, Table Top, Replacement - ADM Glass</title>
<meta name="keywords" content="Custom Glass, Tempered Glass, Glass Replacement, ADM Glass, Custom Shape Glass in California" />











<style type="text/css">
<!--

body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color:#171717;
}
.style1 {
	font-size: 16px;
	font-style: italic;
	font-weight: bold;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #DEDEDE;
}
.style3 {
	font-size: 12px;
	color: #DEDEDE;
	font-style: italic;
}
.style4 {
	font-size: 16px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
.style5 {
	font-size: 18px;
	font-weight: bold;
}
.style6 {padding-left:20px}
.style9 {color: #CECED0}
.style10 {font-size: 18px; color: #CECED0; }
.style12 {font-size: 14px;
padding-left:10px;}
.style16 {padding-left: 10px; color: #ABAD95; font-size: 14px;}
.style18 {color: #ABAD95}
.style19 {padding-left:80px}
.style21 {
	font-size: 16px;
	color: #FF0000;
	font-weight: bold;
}
.style23 {
	font-size: 16px;
	color: #636766;
	font-weight: bold;
}
.style24 {
	color: #636766;
	font-size: 14px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}

-->

A:link {text-decoration: none; color: #FFFFFF;}
A:visited {text-decoration: none; color: #FFFFFF;}
A:active {text-decoration: none; color: #d8d8d8;}
A:hover {text-decoration: underline; color: #d8d8d8;}

.ProjListCSS {
  background: #FFFFFF;
  }
  
.toplink {
 font-size: 10px;
}

body {
  background: #171717;
  font-family: Tahoma;
}

.modName {
  font-size: 13px;
  color: #c38731;
  font-weight: bold;
  line-height: 22px;
  text-align: left;
}

.projName {
  font-size: 20px;
  color: #c38731;
  font-weight: bold;
  text-align: left;
  text-indent: 20px;
}

.bulletList {
  font-size: 12px;
  list-style-type: square;
  text-align: left;
  line-height: 16px;
  padding-left: 80px;
  color: #CCCCCC;
  }

.modText {
  font-size: 12px;
  color: #CCCCCC;
  line-height: 16px;
  text-align: left;
}

.modTextFrontPage {
  font-size: 13px;
  color: #CCCCCC;
  line-height: 18px;
  text-align: left;
  height: 55px;
  margin-left: 15px;
  text-indent: 15px;
}

.modTextFrontPageAddl {
  font-size: 13px;
  color: #FFFFFF;
  line-height: 18px;
  text-align: center;
  font-weight: bold;
}

.ProdTime {
  font-size: 13px;
  color: #FFFFFF;
  font-weight: bold;
}



.linkClass A:link {font-weight: bold; font-size: 13px; line-height: 22px; text-decoration: none; color: #c38731;}
.linkClass A:visited {font-weight: bold; font-size: 13px; line-height: 22px; text-decoration: none; color: #c38731;}
.linkClass A:hover {font-weight: bold; font-size: 13px; line-height: 22px; text-decoration: none; color: #c39f31; text-decoration: underline;}
.linkClass {text-align: left;}

.linkClassFront A:link {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #c38731;}
.linkClassFront A:visited {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #c38731;}
.linkClassFront A:hover {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #c39f31; text-decoration: underline;}
.linkClassFront {text-align: left;}

.linkClassFrontStock A:link {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #ccf906;}
.linkClassFrontStock A:visited {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #ccf906;}
.linkClassFrontStock A:hover {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #ccf906; text-decoration: underline;}
.linkClassFrontStock {text-align: left;}

.linkClassOrbit A:link {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #ccf906;}
.linkClassOrbit A:visited {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #ccf906;}
.linkClassOrbit:hover {font-weight: bold; font-size: 16px; line-height: 22px; text-decoration: none; color: #ccf906; text-decoration: underline;}
.linkClassOrbit {text-align: left;}

.centerDiv {
  margin-left: auto;
  margin-right: auto;
  border-collaps: collapse;
}
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38653380-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>




<body itemscope="" itemtype="http://schema.org/WebPage">
<script type="application/ld+json">
{
"@context" : "http://schema.org",
"@type" : "WebSite",
"name" : "ADM Glass",
"url" : "http://admglass.com/",
"potentialAction" :
{
"@type" : "SearchAction",
"target" : "http://admglass.com/index.php{search_term}.html",
"query-input" : "required name=search_term"
}}
</script>
<script type="application/ld+json">
{
"@context" : "http://schema.org",
"@type" : "Organization",
"legalName" : "ADM Glass",
"name" : "ADM Glass",
"url" : "http://admglass.com/",
"logo" : "http://admglass.com/img/pic2.jpg",
"sameAs" :[
"http://www.facebook.com/pages/ADM-Glass/415324555227963",
"http://twitter.com/ADMGlass"
]
}
</script>












<table width="864" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="864"><table width="861" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="45" ><img src="img/pic2.jpg" alt="ADM Glass LOGO Online shop" width="48" height="92" /></td>
       	 <td width="816">
         <table width="816" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            	<td><table width="726" border="0" cellspacing="0" cellpadding="0">
             		 <tr>
               
               		 <td width="372" bgcolor="#171717">
                     	<table width="730" border="0" cellpadding="0" cellspacing="0">
                  		  <tr>
                			<td width="72" ><a href="index.php"><img src="img/pic21.jpg" alt="ADM Glass LOGO" width="75" height="48" border="0" /></a></td>
                            <td colspan="2"><img src="img/pic22.jpg" alt="Online Orde ADM Glass" width="358" height="17" /></td>
                    	</tr>
                         <tr>
            					<td bgcolor="#171717" colspan="3" style="padding-left:2px;"><img src="img/pic20.jpg" alt="ADM Glass in California" width="699" height="2" /></td>
         				 </tr>
              			<tr>
                			<td width="310" height="42" align="left" valign="top" bgcolor="#171717"><span class="style1"><img src="images/g_title.jpg" alt="ADM Glass Title" /></span> 		</td>
                			<td width="144" bgcolor="#171717">&nbsp;</td>
               				 <td align="left" valign="top" bgcolor="#171717"><span class="style3"> </span></td>
              			</tr>
            			</table>
                     </td>
                	<td width="60" valign="top"><div  style="background:url(images/bascket.gif) no-repeat; width:97px; height:92px;"><span style="font-size:12px; color:#D8D8D8; padding-left:14px;"><a href="shopping_cart.php" style="text-decoration:none; font-size:12px; color:#D8D8D8; ">
					0 Items</a></span></div></td>
             	 	</tr>
           		 </table>
               </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#171717"><table width="861" border="0" cellspacing="0" cellpadding="0" align="left">
      <tr>
      <td width="503" colspan="2">&nbsp;</td>
        <td width="75"><a href="shape.php" style="text-decoration:none;"><img src="images/red_shape.gif" alt="ADM Glass shape Rectangle Square" width="75" height="24" /></a></td>
        <td width="85"><img src="images/dimension.gif" alt="ADM Glass Sizes for Restaurant Industry" width="85" height="24"/></td>
    <td width="65"><img src="images/type.gif" alt="ADM Glass types for Business" width="65" height="24"/></td>
        <td width="75"><img src="images/thickness.gif" alt="Select ADM Glass thickness "  width="75" height="24"/></td>
           <td width="65"><img src="images/tint.gif" alt="Choose online Glass style" width="65" height="24"/></td>
        <td width="85"><img src="images/edge.gif" alt="Online online adm glass edge" width="95" height="24"/></td>
     
      
    
              <td width="75"><img src="images/option.gif" alt="Select ADM Glass toption" width="75" height="24"/></td>
                  <td width="75"><img src="images/summary.gif" alt="Read ADM Glass product summary" width="75" height="24" /></td>
        <td width="75"><img src="images/order.gif" width="75" alt="online order adm glass in USA" height="24" /></td>
      </tr>    
        <tr>
     
        <td colspan="2" align="left"><img src="images/ten.jpg" alt="adm glass online order process" width="180" height="49" /></td><td colspan="9"><img src="images/progress.jpg" alt="online order adm glass process score " width="700" height="50" /></td></tr>

    </table></td> 
  </tr>
  <tr>
    <td><table width="864" border="0" cellspacing="0" cellpadding="0">
      <tr>
      
        <td width="680" valign="top" colspan="2" align="center" >
        
                 
       
      
<!-- body //-->
<table border="0" width="864" cellspacing="3" cellpadding="3">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="0" cellpadding="2">
<!-- left_navigation //-->
<?php //require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo sprintf(HEADING_TITLE, $HTTP_GET_VARS['error_id']); ?></td>
            <td class="pageHeading" align="right"><?php echo tep_image(DIR_WS_IMAGES . 'table_background_password_forgotten.gif', HEADING_TITLE, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td><br><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo sprintf(TEXT_INFORMATION, $error_text)?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="right" class="main"><br><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT, '', 'NONSSL') . '">' . tep_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE) . '</a>'; ?></td>
      </tr>
    </table>
 <?php //this is where I added the advanced Search
  echo tep_draw_form('advanced_search', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL', false), 'get', 'onSubmit="return check_form(this);"') . tep_hide_session_id(); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
	      <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE_1; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
<?php
  if ($messageStack->size('search') > 0) {
?>
      <tr>
        <td><?php echo $messageStack->output('search'); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
<?php
  }
?>
      <tr>
        <td>
<?php
  $info_box_contents = array();
  $info_box_contents[] = array('text' => HEADING_SEARCH_CRITERIA);

  new infoBoxHeading($info_box_contents, true, true);

  $info_box_contents = array();
  $info_box_contents[] = array('text' => tep_draw_input_field('keywords', '', 'style="width: 100%"'));
  $info_box_contents[] = array('align' => 'right', 'text' => tep_draw_checkbox_field('search_in_description', '1') . ' ' . TEXT_SEARCH_IN_DESCRIPTION);

  new infoBox($info_box_contents);
?>
        </td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText"><?php echo '<a href="javascript:popupWindow(\'' . tep_href_link(FILENAME_POPUP_SEARCH_HELP) . '\')">' . TEXT_SEARCH_HELP_LINK . '</a>'; ?></td>
            <td class="smallText" align="right"><?php echo tep_image_submit('button_search.gif', IMAGE_BUTTON_SEARCH); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2" class="infoBox">
          <tr class="infoBoxContents">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="fieldKey"><?php echo ENTRY_CATEGORIES; ?></td>
                <td class="fieldValue"><?php echo tep_draw_pull_down_menu('categories_id', tep_get_categories(array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES)))); ?></td>
              </tr>
              <tr>
                <td class="fieldKey">&nbsp;</td>
                <td class="smallText"><?php echo tep_draw_checkbox_field('inc_subcat', '1', true) . ' ' . ENTRY_INCLUDE_SUBCATEGORIES; ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td class="fieldKey"><?php echo ENTRY_MANUFACTURERS; ?></td>
                <td class="fieldValue"><?php echo tep_draw_pull_down_menu('manufacturers_id', tep_get_manufacturers(array(array('id' => '', 'text' => TEXT_ALL_MANUFACTURERS)))); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td class="fieldKey"><?php echo ENTRY_PRICE_FROM; ?></td>
                <td class="fieldValue"><?php echo tep_draw_input_field('pfrom'); ?></td>
              </tr>
              <tr>
                <td class="fieldKey"><?php echo ENTRY_PRICE_TO; ?></td>
                <td class="fieldValue"><?php echo tep_draw_input_field('pto'); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td class="fieldKey"><?php echo ENTRY_DATE_FROM; ?></td>
                <td class="fieldValue"><?php echo tep_draw_input_field('dfrom', DOB_FORMAT_STRING, 'onFocus="RemoveFormatString(this, \'' . DOB_FORMAT_STRING . '\')"'); ?></td>
              </tr>
              <tr>
                <td class="fieldKey"><?php echo ENTRY_DATE_TO; ?></td>
                <td class="fieldValue"><?php echo tep_draw_input_field('dto', DOB_FORMAT_STRING, 'onFocus="RemoveFormatString(this, \'' . DOB_FORMAT_STRING . '\')"'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></form></td>
<!-- body_text_eof //-->
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="0" cellpadding="2">
<!-- right_navigation //-->
<?php //require(DIR_WS_INCLUDES . 'column_right.php'); ?>
<!-- right_navigation_eof //-->
    </table></td>
  </tr>
</table>
<!-- body_eof //-->






           
             
                 
             
             
             </td>
    
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="861"  bgcolor="#171717">
      <p style="width:850px; padding-left:20px; font:Arial, Helvetica, sans-serif; color:#FFFFFF" align="justify">
     <!-- <i>
   ADM GLASS provides you with CUSTOM CUT, CUSTOM GLASS of desired shape and size according to customer needs. We have a variety of glass products in our glass shop such as Glass Table Top, Tempered Glass, Glass Replacement, Glass Shelf, Beveled Glass which are best in quality .You can easily place orders online through our website. </i>
-->    </p>
    
    </td>
  </tr>
    
 <tr>
    <td bgcolor="#171717" class="style6"><img src="img/pic16.jpg" alt="ADM GLASSS Custom for showe door" width="829" height="3" align="baseline" /></td>
  </tr>
  <tr>
    <td height="31" ><table width="854" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
       
        
         <td  align="left" valign="bottom" bgcolor="#171717" class="style10" ><a href="index.php?page=shipping" class="style10" style="font-size:14px; font-weight:bold; font:Arial, Helvetica, sans-serif; text-decoration:none;">Shipping and Returns</a> </td>
        <td  align="left" valign="bottom" bgcolor="#171717" class="style10" ><a href="index.php?page=contact" class="style10" style="font-size:14px; font-weight:bold; font:Arial, Helvetica, sans-serif; text-decoration:none;">Contact Us </a> </td>
        <td  align="left" valign="bottom" bgcolor="#171717" class="style10" ><a href="index.php?page=term_payment" class="style10" style="font-size:14px; font-weight:bold; font:Arial, Helvetica, sans-serif; text-decoration:none;">Terms of use </a> </td>
        <td  align="left" valign="bottom" bgcolor="#171717" class="style10" ><a href="index.php?page=privacy" class="style10" style="font-size:14px; font-weight:bold; font:Arial, Helvetica, sans-serif; text-decoration:none;">Privacy Notice</a> </td>
        <td  align="left" valign="bottom" bgcolor="#171717" class="style10" ><a href="index.php?page=projects" class="style10" style="font-size:14px; font-weight:bold;  color:#00CC33; font:Arial, Helvetica, sans-serif; text-decoration:none;">Previous Projects</a> </td>

  <td class="style10" style="font-size:16px; font-weight:bold; font:Arial, Helvetica, sans-serif; text-decoration:none;"> Phone: 800-690-0002 </td>
  
  <td class="style10" style="font-size:16px; font-weight:bold; font:Arial, Helvetica, sans-serif; text-decoration:none;"> Fax: 925-680-7252 </td>
  <td  align="left" valign="bottom" bgcolor="#171717" class="style10" ><a href="https://admglass.com/login.php" class="style10" style="font-size:14px; font-weight:bold; font:Arial, Helvetica, sans-serif; text-decoration:none;">Log In</a> </td>
        <td width="80" bgcolor="#171717">&nbsp;</td>
      </tr>
       <tr>
        
        <td  align="center" colspan="6"  valign="bottom" bgcolor="#171717">&nbsp;</td>
      </tr>
      
    
          <tr>
        
        <td  align="center" colspan="6"  valign="bottom" bgcolor="#171717"><span class="style10" style="font-size:14px; font-weight:bold;">
        
        <a href="http://www.facebook.com/pages/ADM-Glass/415324555227963" target="_blank"><img src="icon_facebook.png" alt="Facebook" border="0" /></a> <a href=" http://twitter.com/ADMGlass" target="_blank"><img src="icon_twitter.png" alt="Twiier" border="0" /></a> <a href="http://www.linkedin.com/pub/adm-glass/66/b32/159/" alt="Linkedin" target="_blank"><img src="icon_linkedin.png"  alt="Linkedin" border="0" /></a>  </span></td>
      </tr> 
      
      
      
      <tr>
        
        <td  align="center" colspan="6"  valign="bottom" bgcolor="#171717"><span class="style10" style="font-size:14px; font-weight:bold;">Copyright &copy; 2018 ADM glass </span></td>
      </tr>
    </table></td>
  </tr>
   <tr>
    <td bgcolor="#171717" color="#171717">
	<h1 style="color:#171717;">ADM Glass - Custom Glasses, Table Top, Replacement in California</h1>
	<h2 style="color:#171717;">ADM Glass Shapes Square, Rectangle, Shower door, Oval, Custom for Industry and Restaurant Supply</h2>
	<h3 style="color:#171717;">Tempered Glass, Glasses Replacement, Custom Shape Glass in USA</h3>
	</td>
  </tr>
  <tr>
    <td bgcolor="#171717">&nbsp;</td>
  </tr>
  <tr>
    <td  bgcolor="#171717">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
