<?php
// Error Redirects from many different states of Error, and handles them in one file.
// Records the output in the database for error trapping and also hacking attempts.
// You will recieve many 404 errors during a hacking attempt and i expect you would be very surprised if you knew how many attempts your site gets in a day!
// Also recorded Errors will show up any scripting errors, missing images, malformed code etc... giving you a much better web site
// Written by FImble for osCommerce
// osCommerce Forums URL -  http://forums.oscommerce.com/index.php?showuser=15542
// Add on URL http://addons.oscommerce.com/info/
// to hire me ... linuxux.group@gmail.com
// If you have concerns about security please get in touch with me for fast expert service,
// hacked? I am more than capable of clearing malicious code from your site, and securing it to prevent further attempts.

  require('includes/application_top.php');
  // Perform some maintenance on the table by clearing out old entries
 $maintain=tep_db_query("DELETE FROM linuxuk_error_log WHERE linuxuk_error_log_date < FROM_UNIXTIME(UNIX_TIMESTAMP()- ".LINUX_ERROR_DELETE_RECORD. "*24*60*60)");


      $action = (isset($_GET['action']) ? $_GET['action'] : '');
      if (tep_not_null($action)) {
      switch ($action) {
      case 'Ban':
            $IP_IP = tep_db_prepare_input($_GET['hID']);
            $result = tep_db_query("SELECT * FROM  " .TABLE_IPTRAP ." WHERE IP_Number =  '" . $IP_IP . "'") ;
            $number_of_rows = tep_db_num_rows($result);
            if ($number_of_rows == 0){
            tep_db_query("INSERT INTO " .TABLE_IPTRAP ." (date,IP_Number,UserAgent,BAN,BY_ADMIN)values (now(),'" .$IP_IP. "','" . 'Banned from HTTP Error Page' . "','BLACK',1)");
            tep_redirect(tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG));
              }
            else
             {
              tep_redirect(tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG));
             }
      break;

       case 'UnBanned':
            $IP_IP = tep_db_prepare_input($_GET['hID']);
            tep_db_query("DELETE FROM " . TABLE_IPTRAP ." WHERE IP_Number =  '" . $IP_IP . "'") ;
            tep_redirect(tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG));
       break;

           case 'Banned':
            $IP_IP = tep_db_prepare_input($_GET['hID']);
            $result = tep_db_query("SELECT * FROM  " .TABLE_IPTRAP ." WHERE IP_Number =  '" . $IP_IP . "'") ;
            $number_of_rows = tep_db_num_rows($result);
            if ($number_of_rows == 0){
            tep_db_query("INSERT INTO " .TABLE_IPTRAP ." (date,IP_Number,UserAgent,BAN,BY_ADMIN)values (now(),'" .$IP_IP. "','" . 'Banned from HTTP Error Page' . "','BLACK',1)");
            tep_redirect(tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG));
            }
      else
            {
              tep_redirect(tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG, 'page=' . $_GET[FILENAME_LINUXUK_HTTP_ERROR_LOG]));
            }
      break;
      case 'Delete':
            $IP_IP = tep_db_prepare_input($_GET['hID']);
            if(LINUXUK_IP_TRAP_EXCESSIVE_UNBAN == "True") {
            tep_db_query("DELETE FROM " . TABLE_LINUXUK_HTTP_ERROR ." WHERE linuxuk_error_log_address =  '" . $IP_IP . "'") ;
            tep_db_query("DELETE FROM " . TABLE_IPTRAP ." WHERE IP_Number =  '" . $IP_IP . "'") ;
            }
            else
            {
            tep_db_query("DELETE FROM " . TABLE_LINUXUK_HTTP_ERROR ." WHERE linuxuk_error_log_address =  '" . $IP_IP . "'") ;
            }
            tep_redirect(tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG));
      break;
    }
  }
  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="pageHeading"  colspan="2"><?php echo LINUXUK_HTTP_ERROR_PAGE_TITLE  .  LINUXUK_HTTP_IPTRAP_VERSION; ?></td>
                                </tr>
                                  <tr><td class="main" colspan="2"><?php echo LINUXUK_HTTP_ERROR_PAGE_DESCRIPTION;?></td></tr>
                                    <tr><td align="left" width="211px" ><?php echo tep_image(DIR_WS_IMAGES . 'explain.png');?></td>
                                     <td rowspan="2" align="left" class="main"><?php echo LINUXUK_HTTP_ERROR_PAGE_DESCRIPTION1 ;?></td>
                                 </tr>
                          </table></td>
                          </tr>
                          <tr>
                          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                          <tr>
                          <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                          <tr class="dataTableHeadingRow">
                          <td class="dataTableHeadingContent" width="130px">&nbsp;<?php echo LINUXUK_HTTP_ERROR_DATE;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_REQUEST;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_REFERRER;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_ADDRESS;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_USER_AGENT;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_CODE;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_IP_BANNED;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_IP_UNBAN;?></td>
                          <td class="dataTableHeadingContent">&nbsp;<?php echo LINUXUK_HTTP_ERROR_DELETE;?></td>

<?php
            $http_query_raw = ("select linuxuk_error_log_id, linuxuk_error_log_date,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error,linuxuk_error_log_referrer,linuxuk_error_log_request from " . TABLE_LINUXUK_HTTP_ERROR . " order by linuxuk_error_log_request desc") ;
            $http_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $http_query_raw, $http_query_numrows);
            $http_query = tep_db_query($http_query_raw);
            while ($http = tep_db_fetch_array($http_query)) {


              if ((!isset($_GET['hID']) || (isset($_GET['hID']) && ($_GET['hID'] == $http['linuxuk_error_log_id']))) && !isset($trInfo) && (substr($action, 0, 3) != 'new')) {
                $trInfo = new objectInfo($http);
              }

              if (isset($trInfo) && is_object($trInfo) && ($http['linuxuk_error_log_id'] == $trInfo->linuxuk_error_log_id)) {
                echo ' <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG, 'page=' . $_GET['page'] . '&hID=' . $trInfo->linuxuk_error_log_id . '&action=edit') . '\'">' . "\n";
              } else {
                echo ' <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG, 'page=' . $_GET['page'] . '&hID=' . $http['linuxuk_error_log_id']) . '\'">' . "\n";
              }
?>
                       	  <td class="dataTableContent"><?php echo $http['linuxuk_error_log_date'] ;?> </td>
                          <td class="dataTableContent"><?php echo $http['linuxuk_error_log_request'] ;?>   </td>
                          <td class="dataTableContent"><?php echo $http['linuxuk_error_log_referrer'] ;?>   </td>
                          <td class="dataTableContent"><?php echo '<a href="http://www.projecthoneypot.org/ip_' . $http['linuxuk_error_log_address'] . '" target="_blank"class="Ban">' . $http['linuxuk_error_log_address'] . '<a/>';?>    </td>
                          <?php if (preg_match("/google|MSN|Yahoo|bing/i", $http['linuxuk_error_log_agent'])) {    ?>
                          <td class="dataTableContent"><?php echo '<font color ="#F6005F"><b>' .  $http['linuxuk_error_log_agent'] . '</b></font>' ;?></td>
<?php
                          }
                          else
                          {
?>
                          <td class="dataTableContent"><?php echo   $http['linuxuk_error_log_agent']  ;?></td>
<?php
                          }
?>


                        <td class="dataTableContent"><?php echo '<a href="http://www.w3schools.com/tags/ref_httpmessages.asp" target="_blank" class="Ban">' . $http['linuxuk_error_log_error'] . '<a/>';?></td>
<?php
                           $IP_IP = $http['linuxuk_error_log_address'];
                           $result = tep_db_query("SELECT * FROM  " .TABLE_IPTRAP ." WHERE IP_Number =  '" . $IP_IP . "'") ;
                           $number_of_rows = tep_db_num_rows($result);
                           if (preg_match("/google|MSN|Yahoo|bing/i", $http['linuxuk_error_log_agent'])) {    ?>
                          <td class="dataTableContent"><?php echo tep_image_button('button_not_banned.png', IMAGE_NOT_BANNED);?></td>
                          <td class="dataTableContent"><?php echo tep_image_button('button_not_banned.png', IMAGE_NOT_BANNED);?></td>
<?php
                          }
                          else
                          {
?>
                          <td class="dataTableContent"><?php echo (($number_of_rows == 0)?  '<a href="' . tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG, 'page=' . $_GET['page']  . '&hID=' . $http['linuxuk_error_log_address'] . '&action=Banned') . '">'. tep_image_button('button_ban.gif', IMAGE_BAN) . '</a>' : tep_image_button('button_banned.gif', IMAGE_NOT_BANNED) ); ?></td>
                          <td class="dataTableContent"><?php echo (($number_of_rows == 1)?  '<a href="' . tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG, 'page=' . $_GET['page']  . '&hID=' . $http['linuxuk_error_log_address'] . '&action=UnBanned') . '">'. tep_image_button('button_free.gif', IMAGE_FREE) . '</a>' : tep_image_button('button_not_banned.png', IMAGE_NOT_BANNED) ); ?></td>
<?php
                          }
?>
                          <td class="dataTableContent"><?php echo  '<a href="' . tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG, 'page=' . $_GET['page']  . '&hID=' . $http['linuxuk_error_log_address'] . '&action=Delete') . '">'. tep_image_button('button_delete.gif', IMAGE_FREE) . '</a>'; ?></td>
                          </tr>
<?php
                          }
?>
                        <tr>
                        <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                          <tr>
                            <td class="smallText" valign="top"><?php echo $http_split->display_count($http_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_HTTP_ERRORS); ?></td>
                            <td class="smallText" align="right"><?php echo $http_split->display_links($http_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                          </td>
                      </tr>
                         <tr>
                          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr>
                              <td><p class="smallText"><?php echo LINUXUK_AUTHOR_MESSAGE; ?></p></td>
                            </tr>
                          </table></td>
                        </tr>
                       </table>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>