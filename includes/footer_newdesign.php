



<div class="main"  onmouseover="openCity(event, 'Hide');hide_cart_data()">
<footer class="ct-footer">
  <div class="container" >
    
    <ul class="ct-footer-list text-center-sm">
	<li class="footer-main-list1">
       <div class="footer-logo-img"  >
		  <img src="img/logo-new.png" ></div>
	</li>
	
	
	<li class="footer-main-list1">
        <h2 class="ct-footer-list-header" onclick="show_list_footer1(this)">SUPPORT <i class="fa fa-caret-down" id="down-button"></i></h2>
			<ul class="footerlist1">
          <li >
            <a href="" >Rider Care</a>
          </li>
			<li >
            <a href="" >Contact Us</a>
          </li>
			<li >
            <a href="" >FAQ</a>
          </li>
		  <li >
            <a href="" >Returns</a>
          </li>
		  <li >
            <a href="" >Warranty</a>
          </li>
		  <li >
            <a href="" >Bike Registration</a>
          </li>

		  </ul>
	</li>
	<li class="footer-main-list2">
        <h2 class="ct-footer-list-header" onclick="show_list_footer2(this)">RESOURCES <i class="fa fa-caret-down" id="down-button"></i></h2>
			<ul class="footerlist2">
          <li >
            <a href="" >Bike Archive</a>
          </li>
			<li >
            <a href="" >Demo and Events</a>
          </li>
			<li >
            <a href="" >Safety Notifications</a>
          </li>
		  <li >
            <a href="" >Suspension Calculator</a>
          </li>
		  <li >
            <a href="" >Counterfeit Awareness</a>
          </li>
		 
		  </ul>
	</li>
	
	
	<li class="footer-main-list3">
        <h2 class="ct-footer-list-header" onclick="show_list_footer3(this)">ABOUT <i class="fa fa-caret-down" id="down-button"></i></h2>
		
			<ul  class="footerlist3">
          <li >
            <a href="" >Our Story</a>
          </li>
			<li >
            <a href="" >Careers</a>
          </li>
			<li >
            <a href="" >Innovation</a>
          </li>
		  <li >
            <a href="" >Stories</a>
          </li>
		  <li >
            <a href="" >Sustainability</a>
          </li>
		 
		  <li >
            <a href="" >Retül</a>
          </li>
		 
		  <li >
            <a href="" >Outride</a>
          </li>
		 
		  </ul>
	</li>
	
	  
	  
	  
      <li class="footer-main-list4">
        
		
		<ul>
	
		
			<li>&nbsp;</li>	
			<li>&nbsp;</li>
			<li>&nbsp;</li>	
			<li>
			<div style="width:100%;" align="left" class="footer-social-icon">
			<a href="#" class="fa fa-facebook"></a>
			<a href="#" class="fa fa-twitter"></a>
			<a href="#" class="fa fa-linkedin"></a>
			<a href="#" class="fa fa-youtube"></a>
			<a href="#" class="fa fa-pinterest"></a>
			</div>
			</li>	
				
        </ul>
      </li>
  
      <li class="footer-main-list5">
        <h2 class="ct-footer-list-header">NEWSLETTER</h2>
        <ul>
       	<li>
		<div align="left" class="newsletter">
			<input type="text" placeholder="Email Address" name="name" >
			<button type="submit">JOIN</button>
			<br /><br />
			<span class="newsletter-terms">* By submitting your email address you agree to the Terms & Conditions</span>
			
			</div>
		</li>
		<li>&nbsp;</li>
			<li>
			<div class="dealer-button"><button >Dealer Inquiries</button> </div>
			
		</li>
		
        </ul>
      </li>
    </ul>
    
  </div>
  <div class="ct-footer-post">
    <div class="container">
	
	<div class="ct-footer-left">
	<img src="img/nsf-logo.png">
	</div>
	
	
	<div class="ct-footer-right">
	<img src="images/SSL.gif">
	</div>
	
      <div class="inner-left">
        <ul>
          <li>
            <a href="">FAQ</a>
          </li>
          <li>
            <a href="">Blog</a>
          </li> 
          <li>
            <a href=""> Helpful Terminology</a>
          </li>
          <li>
            <a href="">Report an Issue</a>
          </li>
           
		    <li>
            <a href="">Privacy Policy</a>
          </li>
           
        </ul>
      </div>
	  
      <div class="inner-right">
        <p>
Copyright©2020 ADM Sneezeguards A division of Advanced Design Manufacturing L.L.C. </p>
        
      </div>
    </div>
  </div>
</footer>
</div>
</body>
</html> 
