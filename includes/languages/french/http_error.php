<?php
/*
  $Id: http_error.php,v 1.5 2004/06/30 20:55:17 chaicka Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2004 osCommerce

  Released under the GNU General Public License
azer 31oct05 Custom HTTP Error Page v1.4 (http://www.oscommerce.com/community/contributions,933)

*/

define('HEADING_TITLE', '%s ERREUR');
define('TEXT_INFORMATION', 'Nous sommes desol� mais la page que vous avez demand�e � gen�r� cette erreur :
<br><br><b>%s</b><br><br>Veuillez tenter une autre page de notre boutique ou rechercher dans notre formulaire de recherche ci-dessous pour trouver l\'article que vous cherchez. <br> Une alerte � �t� automatiquement envoy�e pour informer le webmaster. Nous nous excusons pour le d�sagr�ment caus�.');

define('EMAIL_BODY', 
'------------------------------------------------------' . "\n" .
'Web Site: <font color="blue">%s</font>' . "\n" .
'Code d\'erreur: <font color="red">%s - %s</font>' . "\n" .
'Apparue: <font color="green">%s</font>' . "\n" .
'URL demand�e: <font color="blue">%s</font>' . "\n" .
'Addresse du visiteur: <font color="red">%s</font>' . "\n" .
'Navigateur: <font color="purple">%s</font>' . "\n" .
'Referant: %s' . "\n" .
'------------------------------------------------------'
);

define('EMAIL_TEXT_SUBJECT', 'Un visiteur � eu une errreur HTTP');

//Client Error Codes 
define('ERROR_400_DESC', 'Mauvaise demande');
define('ERROR_401_DESC', 'Authorisation requise');
define('ERROR_403_DESC', 'Non autoris�');
define('ERROR_404_DESC', '404 - Page non trouv�e');
define('ERROR_405_DESC', 'Methode non authoris�e');
define('ERROR_408_DESC', 'Delai de demande depass�');
define('ERROR_415_DESC', 'Type de media non support�');
define('ERROR_416_DESC', 'Requested Range Not Satisfiable');
define('ERROR_417_DESC', 'Expectation Failed');

//Server Error Codes
define('ERROR_500_DESC', 'Erreur interne de serveur ');
define('ERROR_501_DESC', 'Not Implemented');
define('ERROR_502_DESC', 'Bad Gateway');
define('ERROR_503_DESC', 'Service non disponible');
define('ERROR_504_DESC', 'Gateway Timeout');
define('ERROR_505_DESC', 'Version HTTP non support�e');
define('UNKNOWN_ERROR_DESC', 'Erreur non definie');
?>