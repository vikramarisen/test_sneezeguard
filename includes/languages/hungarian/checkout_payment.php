<?php
/*
  $Id: checkout_payment.php,v 1.14 2003/02/06 17:38:16 thomasamoulton Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Fizet�s');
define('NAVBAR_TITLE_2', 'Sz�ml�z�si m�d');

define('HEADING_TITLE', 'Sz�ml�z�s');

define('TABLE_HEADING_BILLING_ADDRESS', 'Sz�ml�z�si c�mem:');
define('TEXT_SELECTED_BILLING_DESTINATION', '');
define('TITLE_BILLING_ADDRESS', 'Erre a c�mre kapom a sz�ml�t:');

define('TABLE_HEADING_PAYMENT_METHOD', 'Fizet�si m�dja:');
define('TEXT_SELECT_PAYMENT_METHOD', 'V�lassz fizet�si m�dot.');
define('TITLE_PLEASE_SELECT', 'K�rj�k, v�lassz');
define('TEXT_ENTER_PAYMENT_INFORMATION', 'Jelenleg ez az egyetlen el�rhet� fizet�si m�d.');

define('TABLE_HEADING_COMMENTS', 'Megjegyz�sem:');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', '');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'Megrendel�sem �ttekint�se.');

//kgt - discount coupons
define('TABLE_HEADING_COUPON', 'Itt add meg pr�m�ci�s, kedvezm�nyre jogos�t� kuponod k�dj�t!<br />
(Pontosan add meg, �s ellen�rizd a k�vetkez� oldalon. Ha valami nem ok� add meg itt �jra!)' );
//end kgt - discount coupons
?>
