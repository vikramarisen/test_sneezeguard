<?php
/*
  $Id: http_error.php,v 1.4 2004/06/30 20:55:17 chaicka Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2004 osCommerce

  Released under the GNU General Public License
  //azer 31oct05 Custom HTTP Error Page v1.5 (http://www.oscommerce.com/community/contributions,933)
*/

define('HEADING_TITLE', 'ERROR  %s');
define('TEXT_INFORMATION', 'Sorry, aber der von Ihnen aufgerufene Seite hat die folgenden Fehler aufgetreten:
<br><br><b><font color="red">%s</font></b><br><br>Man kann durch den Rest unseres speichern<BR>Sie k�nnen auch die "Erweiterte Suche" unten, um ein Produkt zu suchen<BR>Wir entschuldigen uns f�r m�gliche Unannehmlichkeiten');

define('EMAIL_BODY', 
'------------------------------------------------------' . "\n" .
'Web Site: <font color="blue">%s</font>' . "\n" .
'Error Code: <font color="red">%s - %s</font>' . "\n" .
'Happened: <font color="green">%s</font>' . "\n" .
'Angeforderte URL: <font color="blue">%s</font>' . "\n" .
'Benutzer-Adresse: <font color="red">%s</font>' . "\n" .
'User-Browser: <font color="purple">%s</font>' . "\n" .
'Referenz: %s' . "\n" .
'------------------------------------------------------'
);

define('EMAIL_TEXT_SUBJECT', 'Ein Kunde hat ein HTTP-Fehler erhalten');

//Client Error Codes 
define('ERROR_400_DESC', 'Bad Request');
define('ERROR_401_DESC', 'Authorization Required');
define('ERROR_403_DESC', 'Forbidden');
define('ERROR_404_DESC', 'Not Found');
define('ERROR_405_DESC', 'Method Not Allowed');
define('ERROR_408_DESC', 'Zeiteinstellung');
define('ERROR_415_DESC', 'Typ nicht unterst�tzt');
define('ERROR_416_DESC', 'Range fragte nicht erf�llbar');
define('ERROR_417_DESC', 'Erwartung Mi�erfolg');

//Server Error Codes
define('ERROR_500_DESC', 'Internal Server Error');
define('ERROR_501_DESC', 'Nicht implementiert');
define('ERROR_502_DESC', 'Bad Gateway');
define('ERROR_503_DESC', 'Service Unavailable');
define('ERROR_504_DESC', 'Gateway Timeout');
define('ERROR_505_DESC', 'HTTP-Version wird nicht unterst�tzt');
define('UNKNOWN_ERROR_DESC', 'Unbefristet Scheitern');
?>