<?php
/*
  $Id: http_error.php,v 1.4 2004/06/30 20:55:17 chaicka Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2004 osCommerce

  Released under the GNU General Public License
  //azer 31oct05 Custom HTTP Error Page v1.5 (http://www.oscommerce.com/community/contributions,933)
*/

define('HEADING_TITLE', 'ERROR  %s');
define('TEXT_INFORMATION', 'Lo sentimos pero la pagina que ha solicitado ha detectado el siguiente error:
<br><br><b><font color="red">%s</font></b><br><br>Puede navegar por el resto de nuestra tienda<BR>Tambien puede utilizar la funcion "Busqueda avanzada" a continuacion para encontrar el articulo que busca<BR>Pedimos disculpas por cualquier inconveniente causado');

define('EMAIL_BODY', 
'------------------------------------------------------' . "\n" .
'Sitio web: <font color="blue">%s</font>' . "\n" .
'Codigo de error: <font color="red">%s - %s</font>' . "\n" .
'Fecha y hora de la incidencia: <font color="green">%s</font>' . "\n" .
'URL solicitada: <font color="blue">%s</font>' . "\n" .
'IP del usuario: <font color="red">%s</font>' . "\n" .
'Navegador del usuario: <font color="purple">%s</font>' . "\n" .
'Referencia: %s' . "\n" .
'------------------------------------------------------'
);

define('EMAIL_TEXT_SUBJECT', 'Un cliente ha recibido un Error HTTP');

//Client Error Codes 
define('ERROR_400_DESC', 'Solicitud incorrecta');
define('ERROR_401_DESC', 'Autorizacion Requerida');
define('ERROR_403_DESC', 'Prohibida');
define('ERROR_404_DESC', 'No encontrada');
define('ERROR_405_DESC', 'Metodo no permitido');
define('ERROR_408_DESC', 'Tiempo de espera agotado');
define('ERROR_415_DESC', 'Tipo no soportado');
define('ERROR_416_DESC', 'No pidio Rango Satisfiable');
define('ERROR_417_DESC', 'Error expectativa');

//Server Error Codes
define('ERROR_500_DESC', 'Error interno del servidor');
define('ERROR_501_DESC', 'No Implementado');
define('ERROR_502_DESC', 'Gateway mala');
define('ERROR_503_DESC', 'Servicio no disponible');
define('ERROR_504_DESC', 'Gateway fuera de tiempo');
define('ERROR_505_DESC', 'Version de protocolo HTTP no soportado');
define('UNKNOWN_ERROR_DESC', 'Error indefinido');
?>