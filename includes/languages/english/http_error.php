<?php
/*
  $Id: http_error.php,v 1.4 2004/06/30 20:55:17 chaicka Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2004 osCommerce

  Released under the GNU General Public License
  //azer 31oct05 Custom HTTP Error Page v1.5 (http://www.oscommerce.com/community/contributions,933)
*/

define('HEADING_TITLE', '%s ERROR');
define('TEXT_INFORMATION', 'We\'re sorry but the page you have requested has encountered the following error:
<br><br><b><font color="red">%s</font></b><br><br>Please feel free to browse the rest of our store<br>You may also use the "Advanced Search" feature provided below to find the product you are looking for<br>We apologize for any inconvenience caused.');

define('EMAIL_BODY', 
'------------------------------------------------------' . "\n" .
'Web Site: <font color="blue">%s</font>' . "\n" .
'Error Code: <font color="red">%s - %s</font>' . "\n" .
'Occurred: <font color="green">%s</font>' . "\n" .
'Requested URL: <font color="blue">%s</font>' . "\n" .
'User Address: <font color="red">%s</font>' . "\n" .
'User Agent: <font color="purple">%s</font>' . "\n" .
'Referer: %s' . "\n" .
'------------------------------------------------------'
);

define('EMAIL_TEXT_SUBJECT', 'A Customer has received an HTTP Error');

//Client Error Codes 
define('ERROR_400_DESC', 'Bad Request');
define('ERROR_401_DESC', 'Authorization Required');
define('ERROR_403_DESC', 'Forbidden');
define('ERROR_404_DESC', 'Not Found');
define('ERROR_405_DESC', 'Method Not Allowed');
define('ERROR_408_DESC', 'Request Timed Out');
define('ERROR_415_DESC', 'Unsupported Media Type');
define('ERROR_416_DESC', 'Requested Range Not Satisfiable');
define('ERROR_417_DESC', 'Expectation Failed');

//Server Error Codes
define('ERROR_500_DESC', 'Internal Server Error');
define('ERROR_501_DESC', 'Not Implemented');
define('ERROR_502_DESC', 'Bad Gateway');
define('ERROR_503_DESC', 'Service Unavailable');
define('ERROR_504_DESC', 'Gateway Timeout');
define('ERROR_505_DESC', 'HTTP Version Not Supported');
define('UNKNOWN_ERROR_DESC', 'Undefined Error');
?>