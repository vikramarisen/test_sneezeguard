<?php
// Error Redirects from many different states of Error, and handles them in one file.
// Records the output in the database for error trapping and also hacking attempts.
// You will recieve many 404 errors during a hacking attempt and i expect you would be very surprised if you knew how many attempts your site gets in a day!
// Also recorded Errors will show up any scripting errors, missing images, malformed code etc... giving you a much better web site
// Written by FImble for osCommerce
// osCommerce Forums URL -  http://forums.oscommerce.com/index.php?showuser=15542
// Add on URL http://addons.oscommerce.com/info/
// to hire me ... linuxux.group@gmail.com
// If you have concerns about security please get in touch with me for fast expert service,
// hacked? I am more than capable of clearing malicious code from your site, and securing it to prevent further attempts.

define('LINUXUK_HTTP_ERROR_TITLE', 'Ooops! an Error has occurred.');
define('LINUXUK_HTTP_HREF_LINK_TEXT', ' Click to go back to our Home page');
// Error Code Headers
define('LINUXUK_ERROR_400_HTTP_ERROR', '400 - Bad Request');
define('LINUXUK_ERROR_401_HTTP_ERROR', '401 - Authorization Required');
define('LINUXUK_ERROR_403_HTTP_ERROR', '403 - Forbidden');
define('LINUXUK_ERROR_404_HTTP_ERROR', '404 - Not Found');
define('LINUXUK_ERROR_405_HTTP_ERROR', '405 - Method Not Allowed');
define('LINUXUK_ERROR_408_HTTP_ERROR', '408 - Request Timed Out');
define('LINUXUK_ERROR_415_HTTP_ERROR', '415 - Unsupported Media Type');
define('LINUXUK_ERROR_416_HTTP_ERROR', '416 - Requested Range Not Satisfiable');
define('LINUXUK_ERROR_417_HTTP_ERROR', '417 - Expectation Failed');
define('LINUXUK_ERROR_500_HTTP_ERROR', '500 - Internal Server Error');
define('LINUXUK_ERROR_501_HTTP_ERROR', '501 - Not Implemented');
define('LINUXUK_ERROR_502_HTTP_ERROR', '502 - Bad Gateway');
define('LINUXUK_ERROR_503_HTTP_ERROR', '503 - Service Unavailable');
define('LINUXUK_ERROR_504_HTTP_ERROR', '504 - Gateway Timeout');
define('LINUXUK_ERROR_505_HTTP_ERROR', '505 - HTTP Version Not Supported');
define('LINUXUK_UNKNOWN_HTTP_ERROR', 'Undefined Error');
// Error Code Short Description
define('LINUXUK_ERROR_400_HTTP_ERROR_MESSAGE', '400 - Bad Request - please try again.');
define('LINUXUK_ERROR_401_HTTP_ERROR_MESSAGE', '401 - Authorization Required - please try again.');
define('LINUXUK_ERROR_403_HTTP_ERROR_MESSAGE', '403 - Forbidden - please try again.');
define('LINUXUK_ERROR_404_HTTP_ERROR_MESSAGE', '404 - Not Found - please try again.');
define('LINUXUK_ERROR_405_HTTP_ERROR_MESSAGE', '405 - Method Not Allowed - please try again.');
define('LINUXUK_ERROR_408_HTTP_ERROR_MESSAGE', '408 - Request Timed Out - please try again.');
define('LINUXUK_ERROR_415_HTTP_ERROR_MESSAGE', '415 - Unsupported Media Type - please try again.');
define('LINUXUK_ERROR_416_HTTP_ERROR_MESSAGE', '416 - Requested Range Not Satisfiable - please try again.');
define('LINUXUK_ERROR_417_HTTP_ERROR_MESSAGE', '417 - Expectation Failed - please try again.');
define('LINUXUK_ERROR_500_HTTP_ERROR_MESSAGE', '500 - Internal Server Error - please try again.');
define('LINUXUK_ERROR_501_HTTP_ERROR_MESSAGE', '501 - Not Implemented - please try again.');
define('LINUXUK_ERROR_502_HTTP_ERROR_MESSAGE', '502 - Bad Gateway - please try again.');
define('LINUXUK_ERROR_503_HTTP_ERROR_MESSAGE', '503 - Service Unavailable - please try again.');
define('LINUXUK_ERROR_504_HTTP_ERROR_MESSAGE', '504 - Gateway Timeout - please try again.');
define('LINUXUK_ERROR_505_HTTP_ERROR_MESSAGE', '505 - HTTP Version Not Supported - please try again.');
define('LINUXUK_UNKNOWN_HTTP_ERROR_MESSAGE', 'Undefined Error - please try again.');
// Error Code Descriptions
define('LINUXUK_HTTP_EXPLAINATION_400', 'There is a syntax error in the request, and it is denied.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_401', 'The request header did not contain the necessary authentication codes, and the client is denied access.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_403', 'The client is not allowed to see a certain file. This is also returned at times when the server doesn\'t want any more visitors.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_404', 'The requested file was not found on the server. Possibly because it was deleted, or never existed before. Often caused by misspellings of URLs.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_405', 'The method you are using to access the file is not allowed.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_408', 'The server took longer than its allowed time to process the request. Often caused by heavy net traffic.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_415', 'The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_416', 'A server SHOULD return a response with this status code if a request included a Range request-header field, and none of the range-specifier values in this field overlap the current extent of the selected resource, and the request did not include an If-Range request-header field. (For byte-ranges, this means that the first- byte-pos of all of the byte-range-spec values were greater than the current length of the selected resource.)<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_417', 'The expectation given in an Expect request-header field could not be met by this server, or, if the server is a proxy, the server has unambiguous evidence that the request could not be met by the next-hop server.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_500', 'The server encountered an unexpected condition which prevented it from fulfilling the request.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_501', 'The request cannot be carried out by the server.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_502', 'The server you\'re trying to reach is sending back errors.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_503', 'The service or file that is being requested is not currently available.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_504', 'The gateway has timed out. Like the 408 timeout error, but this one occurs at the gateway of the server.<br /> ');
define('LINUXUK_HTTP_EXPLAINATION_505', 'The HTTP protocol you are asking for is not supported.<br /> ');
define('LINUXUK_NOTIFY_EMAIL_ERROR_CREDIT', 'Another Great security script brought to you by Linuxuk.co.uk -- <br />') ;
define('LINUXUK_UNKNOWN_HTTP_ERROR_TEXT', 'An unknown Error has occurred; we will be looking into this ASAP.<br /> ');
define('LINUXUK_NOTIFY_EMAIL_SUBJECT', 'User Banned for Excessive HTTP errors');
define('LINUXUK_NOTIFY_EMAIL_TEXT_1', 'A user has been banned from the site for excessive HTTP errors, this can be the result of someone probing your site, please go to your admin area and check out the HTTP error page') ;
define('LINUXUK_NOTIFY_EMAIL_TEXT_2', 'HTTP error Alert ');
define('LINUXUK_NOTIFY_EMAIL_IP', 'Users IP Number ');
define('LINUXUK_NOTIFY_EMAIL_USER_NAME', 'Users user agent ');
define('LINUXUK_NOTIFY_EMAIL_ERROR_NUMBER', 'Error Number ');
define('LINUXUK_NOTIFY_EMAIL_TEXT_4','Banned for Continued HTTP Errors');
?>