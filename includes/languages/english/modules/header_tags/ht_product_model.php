<?php
/*
 $Id: ht_product_model.php v1.0 20101204 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_PRODUCT_MODEL_TITLE', 'Product Title - Model' );
  define( 'MODULE_HEADER_TAGS_PRODUCT_MODEL_DESCRIPTION', 'Show the model number of the current product in the product head title.' );
?>
