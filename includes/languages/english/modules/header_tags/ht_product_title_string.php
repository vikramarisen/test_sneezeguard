<?php
/*
  $Id: ht_product_title_string.php v1.0 20101122 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_STRING_TITLE', 'Product Title - String' );
  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_STRING_DESCRIPTION', 'Show a string of characters in every product page title.' );

?>
