<?php
/*
  $Id: ht_front_title.php v1.0 20101122 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_FRONT_TITLE_TITLE', 'Front Page - Title' );
  define( 'MODULE_HEADER_TAGS_FRONT_TITLE_DESCRIPTION', 'Add a head title to the store front page.' );

?>
