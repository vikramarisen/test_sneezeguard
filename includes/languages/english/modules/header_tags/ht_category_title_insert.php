<?php
/*
  $Id: ht_category_title_insert.php v1.0 20101122 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_CATEGORY_TITLE_INSERT_NEW_TITLE', 'Category Title - Insert New' );
  define( 'MODULE_HEADER_TAGS_CATEGORY_TITLE_INSERT_NEW_DESCRIPTION', 'Insert a new title for each category in the head title.' );
?>
