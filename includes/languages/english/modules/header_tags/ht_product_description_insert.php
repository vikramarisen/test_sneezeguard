<?php
/*
  $Id: ht_product_description_insert.php v1.0 20101126 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_PRODUCT_DESCRIPTION_INSERT_NEW_TITLE', 'Product Meta Description - Insert' );
  define( 'MODULE_HEADER_TAGS_PRODUCT_DESCRIPTION_INSERT_NEW_DESCRIPTION', 'Insert a new description for each product in the meta description.' );
?>
