<?php
/*
  $Id: ht_language.php v1.0 20110103 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2011 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_LANGUAGE_TITLE', 'Language' );
  define( 'MODULE_HEADER_TAGS_LANGUAGE_DESCRIPTION', 'Add the language tag to all pages.' );

?>
