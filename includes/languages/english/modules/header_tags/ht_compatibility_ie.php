<?php
/*
  $Id: ht_compatibility_ie.php v1.0 20110201 Gergely $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2011 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_COMPATIBILITY_IE_TITLE', 'IE Compatibility' );
  define( 'MODULE_HEADER_TAGS_COMPATIBILITY_IE_DESCRIPTION', 'Add the Internet Explorer compatibility tag to all pages.' );

?>