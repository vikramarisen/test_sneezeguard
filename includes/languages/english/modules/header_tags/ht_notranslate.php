<?php
/*
  $Id: ht_notranslate.php v1.0 20101128 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_NOTRANSLATE_TITLE', 'Google Notranslate Tag' );
  define( 'MODULE_HEADER_TAGS_NOTRANSLATE_DESCRIPTION', 'Add the Google-specific notranslate tag to all pages.' );

?>
