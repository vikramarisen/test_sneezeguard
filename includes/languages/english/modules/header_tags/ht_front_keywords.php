<?php
/*
  $Id: ht_front_keywords.php v1.0 20110415 Kymation $
  $Loc: catalog/includes/languages/english/modules/header_tags/ $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_FRONT_KEYWORDS_TITLE', 'Front Page - Keywords' );
  define( 'MODULE_HEADER_TAGS_FRONT_KEYWORDS_DESCRIPTION', 'Add meta keywords to the store front page.' );

?>
