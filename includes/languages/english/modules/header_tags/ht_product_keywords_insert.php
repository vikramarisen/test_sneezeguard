<?php
/*
  $Id: ht_product_keywords_insert.php v1.0 20110415 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2011 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_PRODUCT_KEYWORDS_INSERT_NEW_TITLE', 'Product Meta Keywords - Insert' );
  define( 'MODULE_HEADER_TAGS_PRODUCT_KEYWORDS_INSERT_NEW_DESCRIPTION', 'Insert new keywords for each product in the meta keywords tag.' );
?>
