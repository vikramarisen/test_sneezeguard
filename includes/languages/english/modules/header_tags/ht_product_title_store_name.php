<?php
/*
  $Id: ht_product_title_store_name.php v1.0 20101122 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_STORE_NAME_TITLE', 'Product Title - Store Name' );
  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_STORE_NAME_DESCRIPTION', 'Show the store name as part of the produt page title.' );

?>
