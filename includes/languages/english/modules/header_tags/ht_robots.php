<?php
/*
  $Id: ht_notranslate.php v1.0 20101128 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_ROBOTS_TITLE', 'Robots Meta Tag' );
  define( 'MODULE_HEADER_TAGS_ROBOTS_DESCRIPTION', 'Add a robots meta tag to all pages.' );

?>
