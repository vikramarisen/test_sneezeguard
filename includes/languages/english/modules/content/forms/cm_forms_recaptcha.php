<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2016 osCommerce

  Released under the GNU General Public License
*/

  const MODULE_CONTENT_RECAPTCHA_TITLE       = 'reCAPTCHA-2 Form Validation';
  const MODULE_CONTENT_RECAPTCHA_DESCRIPTION = 'Shows the "reCAPTCHA-2 Form Validation" module on your pages that contain a form.';
