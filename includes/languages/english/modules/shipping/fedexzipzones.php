<?php
/*
  $Id: zones.php,v 1.2 2001/10/15 07:07:45 mabosch Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Copyright (c) 2000,2001 The Exchange Project

  Released under the GNU General Public License
*/

define('MODULE_SHIPPING_FEDEXZIPZONES_TEXT_TITLE', 'Federal Express');
define('MODULE_SHIPPING_FEDEXZIPZONES_TEXT_DESCRIPTION', 'Fedex Zipcode Zone Based Rates');
define('MODULE_SHIPPING_FEDEXZIPZONES_TEXT_WAY', 'Shipping to');
define('MODULE_SHIPPING_FEDEXZIPZONES_TEXT_UNITS', 'lbs');
define('MODULE_SHIPPING_FEDEXZIPZONES_INVALID_ZONE', 'No shipping available to the selected Zip Code');
define('MODULE_SHIPPING_FEDEXZIPZONES_NO_ZIPCODE_FOUND', 'No Zip Code available in the order data');
define('MODULE_SHIPPING_FEDEXZIPZONES_NO_ZONE_FOUND', 'Could not find a zone for the given Zip Code');
define('MODULE_SHIPPING_FEDEXZIPZONES_UNDEFINED_RATE', 'The shipping rate cannot be determined at this time');
?>
