<?php
/*
  Released under the GNU General Public License
*/

  class fedexzipzones {
    var $code, $title, $description, $enabled, $sort_order, $fuel_surcharge, $free_shipping, $bundled;

// class constructor

    function fedexzipzones() {
      $this->code = 'fedexzipzones';
      $this->title = MODULE_SHIPPING_FEDEXZIPZONES_TEXT_TITLE;
      $this->description = MODULE_SHIPPING_FEDEXZIPZONES_TEXT_DESCRIPTION;
      $this->icon = DIR_WS_ICONS . 'shipping_fedex.gif';
      $this->enabled = ((MODULE_SHIPPING_FEDEXZIPZONES_STATUS == 'True') ? true : false);
	  $this->free_shipping = ((MODULE_SHIPPING_FEDEXZIPZONES_FREE == 'True') ? true : false);
	  $this->bundled = ((MODULE_SHIPPING_FEDEXZIPZONES_BUNDLED == 'True') ? true : false);
      $this->sort_order = MODULE_SHIPPING_FEDEXZIPZONES_SORT_ORDER;

      $this->types = array(
      		'Standard Overnight' => 'std',
			'Priority Overnight' => 'pri',
			'Ground Service'     => 'gnd',
			'Home Delivery'      => 'hom',
			'First Overnight'    => 'frs',
			'Express Saver'      => 'sav',
			'2nd Day'            => 'two',
			);

      $this->ground_fuel_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_GROUND_FUEL_SURCHARGE;
      $this->express_fuel_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_EXPRESS_FUEL_SURCHARGE;
    }

// class methods

    function quote($method = '')
	{
      global $order, $shipping_weight, $shipping_num_boxes, $cart;

      if ($this->bundled) { 
      	  //If you bundle your packages into 1 before shipping, it will result in lower shipping rates
	  	  $shipping_weight_custom = $shipping_weight;
	  	  $shipping_num_boxes_custom = $shipping_num_boxes;
  	  } else {
		  //However, if you ship by individual packages, this will result in more accurate shipping rates
		  $shipping_num_boxes_custom = $cart->count_contents();
		  $shipping_weight_custom = $shipping_weight/$shipping_num_boxes_custom;
	  }

	  $rounded_weight = round($shipping_weight_custom,0);
      
      // Allow only USA otherwise other country can be given local rates if zip codes matches
      if ($order->delivery['country']['iso_code_2'] != "US") {
	    return $this->quotes;
      }

      // first get the dest zip and check the db for our dest zone
      $zip = $order->delivery['postcode'];
      if ( $zip == '' ){
        // something is wrong, we didn't find any zone
	    $this->quotes['error'] = MODULE_SHIPPING_FEDEXZIPZONES_NO_ZIPCODE_FOUND;
      	return $this->quotes;
      }

      $sql = "SELECT *
      		FROM fedex_pcode_to_zone_xref
		WHERE
			$zip >= pcode_from and
			$zip <= pcode_to";
      $qResult = tep_db_query($sql); // run the query
      $rec = tep_db_fetch_array($qResult); // get the first row of the result

      $zone_id = $rec['zone_id'];

      if ( $zone_id == '' ){
      	// something is wrong, we didn't find any zone
	    $this->quotes['error'] = MODULE_SHIPPING_FEDEXZIPZONES_NO_ZONE_FOUND;
      	return $this->quotes;
      }
      
      // Check for Hawaii, Puerto Rico and Alaska zones as FedEx assigns them 2 different zones rates
	  if ($order->delivery['zone_id'] == '2' || $order->delivery['zone_id'] == '21' || $order->delivery['zone_id'] == '52') {
		  $zone_id_ground = $rec['zone_id_ground'];  //non-continental USA has different zone id for Ground and Express
 	  } else {
	 	  $zone_id_ground = '';
 	  }

      $sql = "SELECT
      		fedex_zone_rates.zone_id,
      		fedex_zone_rates.shiptype_id,
			fedex_shiptype.shiptype_name,
			fedex_zones.zone_name,
			fedex_zone_rates.zonerate_id,
			fedex_zone_rates.zone_cost
      		FROM
			fedex_zone_rates,
			fedex_zones,
			fedex_shiptype
		WHERE ";

	if (tep_not_null($method)){
		$sql .= "fedex_shiptype.shiptype_id = '$method' AND ";
	}

	$sql .=	"fedex_zone_rates.zone_id = fedex_zones.zone_id 
			AND
			fedex_zone_rates.shiptype_id = fedex_shiptype.shiptype_id
			AND
			fedex_zone_rates.weight = $rounded_weight
			AND ";
			
	if (tep_not_null($zone_id_ground)){
		$sql .= "(fedex_zone_rates.zone_id = $zone_id OR fedex_zone_rates.zone_id = $zone_id_ground)";
	} else {
		$sql .= "(fedex_zone_rates.zone_id = $zone_id)";
	}
	
	$sql .= " ORDER BY
			fedex_zone_rates.zone_cost";
			
    $qResult = tep_db_query($sql); // run the query

    while($rec = tep_db_fetch_array($qResult)) {
        $retArr[] = $rec;
    }
    
	foreach( $retArr as $aquote ){
		$cost = $aquote['zone_cost'];
		$title = $aquote['shiptype_name'];
		$zoneid = $aquote['zone_id'];
		$this->fuel_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_EXPRESS_FUEL_SURCHARGE;
		$service_surcharge = 0;
		$service_discount = 0;

		//Offer only Ground or Home Delivery option depending on the Company field
		if ($title == 'Home Delivery') {
			if ($order->delivery['company'] != '') {
				continue; //skip to next for loop
			}
			if ((tep_not_null($zone_id_ground)) && ($zone_id != $zone_id_ground)) {     //For Hawaii, PR, Alaska
				if ($zoneid == $zone_id) {        //Skip for Express zones
					continue;
				}
			}
			$this->fuel_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_GROUND_FUEL_SURCHARGE;
			$service_discount = MODULE_SHIPPING_FEDEXZIPZONES_GROUND_DISCOUNT;
	    	$service_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_GROUND_SURCHARGE;
		}
		if ($title == 'Ground Service') {
			if ($order->delivery['company'] == '') {
				continue; //skip to next for loop
			}
			if ((tep_not_null($zone_id_ground)) && ($zone_id != $zone_id_ground)) {     //For Hawaii, PR, Alaska
				if ($zoneid == $zone_id) {        //Skip for Express zones
					continue;
				}
			}
			$this->fuel_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_GROUND_FUEL_SURCHARGE;
			$service_discount = MODULE_SHIPPING_FEDEXZIPZONES_GROUND_DISCOUNT;
	    	$service_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_GROUND_SURCHARGE;
		}
		
		if ($title == 'Standard Overnight - by 3PM') {
			if ((tep_not_null($zone_id_ground)) && ($zone_id != $zone_id_ground)) {     //For Hawaii, PR, Alaska
				if ($zoneid == $zone_id_ground) {   //Skip for Ground zones
					continue;
				}
			}
	    	$service_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_SURCHARGE;
			$service_discount = MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_DISCOUNT;
		}		
		if ($title == '2nd Day') {
			if ((tep_not_null($zone_id_ground)) && ($zone_id != $zone_id_ground)) {     //For Hawaii, PR, Alaska
				if ($zoneid == $zone_id_ground) {   //Skip for Ground zones
					continue;
				}
			}
	    	$service_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_2DAY_SURCHARGE;
			$service_discount = MODULE_SHIPPING_FEDEXZIPZONES_2DAY_DISCOUNT;
		}		
		if ($title == 'Express Saver - 3 days') {
			if ((tep_not_null($zone_id_ground)) && ($zone_id != $zone_id_ground)) {     //For Hawaii, PR, Alaska
				if ($zoneid == $zone_id_ground) {   //Skip for Ground zones
					continue;
				}
			}
	    	$service_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_ES_SURCHARGE;
			$service_discount = MODULE_SHIPPING_FEDEXZIPZONES_ES_DISCOUNT;
		}
		if ($title == 'Priority Overnight') {
			if ((tep_not_null($zone_id_ground)) && ($zone_id != $zone_id_ground)) {     //For Hawaii, PR, Alaska
				if ($zoneid == $zone_id_ground) {   //Skip for Ground zones
					continue;
				}
			}
	    	$service_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_SURCHARGE;
			$service_discount = MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_DISCOUNT;
		}		
		if ($title == 'First Overnight') {
			if ((tep_not_null($zone_id_ground)) && ($zone_id != $zone_id_ground)) {     //For Hawaii, PR, Alaska
				if ($zoneid == $zone_id_ground) {   //Skip for Ground zones
					continue;
				}
			}
	    	$service_surcharge = MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_SURCHARGE;
			$service_discount = MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_DISCOUNT;
		}
		
		//*****FedEx Rate Calculations****
		if (($this->free_shipping) && ($title == 'Ground Service' || $title == 'Home Delivery') && ($zone_id_ground == '')) {
				$cost = 0;
		} else {
			//Standard FedEx formula
 	    	$formula = $cost * (($this->fuel_surcharge + 100) / 100);  //add fuel surcharge
 	    	$formula = $formula * ((100 - $service_discount) / 100);   //FedEx discounts
 
			//Customise your own shipping formula here
	    	if ($this->bundled == false) {$formula = $formula * $shipping_num_boxes_custom;}         //multiply by number of packages if this is not bundled packaging
	    	$formula += $service_surcharge;                           //add individual service surcharge based on type, if any.  e.g. You might want to charge more for customers requested Overnight services
	    	$formula += MODULE_SHIPPING_FEDEXZIPZONES_HANDLING;       //add one time handling and packaging fee, if any
	    	$formula = round($formula, 2);                            //round to 2 decimal places
			$cost = $formula;
    	}			

   		$methods[] = array(
				'id' => $aquote['shiptype_id'],
				'title' => $title,
				'cost' => $cost);
	} //end for
	  			
    $this->quotes = array('id' => $this->code,
      					  'module' => MODULE_SHIPPING_FEDEXZIPZONES_TEXT_TITLE ,
//					' (' . $shipping_num_boxes_custom . ' x ' . round($shipping_weight, 1) . ' ' . MODULE_SHIPPING_FEDEXZIPZONES_TEXT_UNITS .')' ,
   		                        #'module' => MODULE_SHIPPING_FEDEXZIPZONES_TEXT_TITLE,
                		   'methods' => $methods);
    if (tep_not_null($this->icon)) $this->quotes['icon'] = tep_image($this->icon, $this->title);
    if ($error == true) $this->quotes['error'] = MODULE_SHIPPING_FEDEXZIPZONES_INVALID_ZONE;
    return $this->quotes;
    } //end function quote

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_FEDEXZIPZONES_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Fedex Zipcode Zones Method', 'MODULE_SHIPPING_FEDEXZIPZONES_STATUS', 'False', 'Do you want to offer fedex zip zone rate shipping?', '6', '0', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Bundle packages?', 'MODULE_SHIPPING_FEDEXZIPZONES_BUNDLED', 'True', 'Do you ship multiple items as 1 package?', '6', '10', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Handling Fee ($)', 'MODULE_SHIPPING_FEDEXZIPZONES_HANDLING', '0', 'Handling Fee for each order', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_FEDEXZIPZONES_SORT_ORDER', '0', 'Sort order of display.', '6', '24', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ground Fuel surcharge (%)', 'MODULE_SHIPPING_FEDEXZIPZONES_GROUND_FUEL_SURCHARGE', '2.5', 'Current ground fuel surcharge', '6', '22', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Express Fuel surcharge (%)', 'MODULE_SHIPPING_FEDEXZIPZONES_EXPRESS_FUEL_SURCHARGE', '10.5', 'Current express fuel surcharge', '6', '23', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Free Ground Shipping?', 'MODULE_SHIPPING_FEDEXZIPZONES_FREE', 'False', 'Free shipping for store promotion', '6', '30', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Surcharge - Overnight ($)', 'MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_SURCHARGE', '0', 'Additional handling charge for this service', '6', '40', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Surcharge - 2nd Day ($)', 'MODULE_SHIPPING_FEDEXZIPZONES_2DAY_SURCHARGE', '0', 'Additional handling charge for this service', '6', '41', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Surcharge - Express Saver ($)', 'MODULE_SHIPPING_FEDEXZIPZONES_ES_SURCHARGE', '0', 'Additional handling charge for this service', '6', '42', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Surcharge - Ground ($)', 'MODULE_SHIPPING_FEDEXZIPZONES_GROUND_SURCHARGE', '0', 'Additional handling charge for this service', '6', '43', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Discount - Overnight (%)', 'MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_DISCOUNT', '0', 'Put your FedEx discount here', '6', '51', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Discount - 2nd Day (%)', 'MODULE_SHIPPING_FEDEXZIPZONES_2DAY_DISCOUNT', '0', 'Put your FedEx discount here', '6', '52', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Discount - Express Saver (%)', 'MODULE_SHIPPING_FEDEXZIPZONES_ES_DISCOUNT', '0', 'Put your FedEx discount here', '6', '53', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Discount - Ground (%)', 'MODULE_SHIPPING_FEDEXZIPZONES_GROUND_DISCOUNT', '0', 'Put your FedEx discount here', '6', '55', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      $keys = array(
      'MODULE_SHIPPING_FEDEXZIPZONES_STATUS',
	  'MODULE_SHIPPING_FEDEXZIPZONES_HANDLING',
      'MODULE_SHIPPING_FEDEXZIPZONES_BUNDLED',
	  'MODULE_SHIPPING_FEDEXZIPZONES_SORT_ORDER',
	  'MODULE_SHIPPING_FEDEXZIPZONES_GROUND_FUEL_SURCHARGE',
	  'MODULE_SHIPPING_FEDEXZIPZONES_EXPRESS_FUEL_SURCHARGE',
	  'MODULE_SHIPPING_FEDEXZIPZONES_FREE',
	  'MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_SURCHARGE',
	  'MODULE_SHIPPING_FEDEXZIPZONES_2DAY_SURCHARGE',
	  'MODULE_SHIPPING_FEDEXZIPZONES_ES_SURCHARGE',
	  'MODULE_SHIPPING_FEDEXZIPZONES_GROUND_SURCHARGE',
	  'MODULE_SHIPPING_FEDEXZIPZONES_OVERNIGHT_DISCOUNT',
	  'MODULE_SHIPPING_FEDEXZIPZONES_2DAY_DISCOUNT',
	  'MODULE_SHIPPING_FEDEXZIPZONES_ES_DISCOUNT',
	  'MODULE_SHIPPING_FEDEXZIPZONES_GROUND_DISCOUNT',
	);

      return $keys;
    }
  }
?>
