<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2016 osCommerce

  Released under the GNU General Public License
*/

  class cm_forms_recaptcha {
	var $version = '1.0';
	var $code;
    var $group;
    var $title;
    var $description;
    var $enabled = false;

    function __construct() {
      $this->code = get_class($this);
      $this->group = basename(dirname(__FILE__));

      $this->title = MODULE_CONTENT_RECAPTCHA_TITLE;
      $this->description = MODULE_CONTENT_RECAPTCHA_DESCRIPTION;
      $this->description .= '<div class="secWarning">' . MODULE_CONTENT_BOOTSTRAP_ROW_DESCRIPTION . '</div>';
						   
      if ( defined('MODULE_CONTENT_RECAPTCHA_STATUS') ) {
        $this->enabled = (MODULE_CONTENT_RECAPTCHA_STATUS == 'True');		
      }
    }

    function execute() {
      global $oscTemplate;
	  
	  $content_width = MODULE_CONTENT_RECAPTCHA_CONTENT_WIDTH;

	  // remove any leading and trailing spaces from entered API keys
	  str_replace(' ', '', MODULE_CONTENT_RECAPTCHA_PUBLIC_KEY);
	  str_replace(' ', '', MODULE_CONTENT_RECAPTCHA_PRIVATE_KEY);
      	  
      ob_start();
      include('includes/modules/content/' . $this->group . '/templates/recaptcha.php');
      $template = ob_get_clean(); 
        
      $oscTemplate->addContent($template, $this->group);
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_CONTENT_RECAPTCHA_STATUS');
    }

    function install() {
	  if ( !defined('RECAPTCHA_MODULE') ) {
		  define('MODULE_CONTENT_RECAPTCHA_PUBLIC_KEY', 'Enter your public key');
		  define('MODULE_CONTENT_RECAPTCHA_PRIVATE_KEY', 'Enter your private key');

	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Current Version', 'RECAPTCHA_MODULE', 'reCAPTCHA-2 BS v" . $this->version . "', 'Creates a reCAPTCHA-2 validation for forms on the pages you select.', '6', '0', 'tep_version_readonly(', now())");
      tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable reCAPTCHA-2 Validation Module', 'MODULE_CONTENT_RECAPTCHA_STATUS', 'True', 'Do you want to enable this module? For this module to work, you must first get your API keys from https://www.google.com/recaptcha/admin/', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) values ('Public Key', 'MODULE_CONTENT_RECAPTCHA_PUBLIC_KEY', '" . tep_db_input(MODULE_CONTENT_RECAPTCHA_PUBLIC_KEY) . "', 'Enter your reCAPTCHA Public Key.', '6', '2', NOW(), null, null)");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) values ('Private Key', 'MODULE_CONTENT_RECAPTCHA_PRIVATE_KEY', '" . tep_db_input(MODULE_CONTENT_RECAPTCHA_PRIVATE_KEY) . "', 'Enter your reCAPTCHA Private Key.', '6', '3', NOW(), null, null)");
	  // pages start
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Login', 'MODULE_CONTENT_RECAPTCHA_PAGE_LOGIN', 'True', 'Do you want to enable reCAPTCHA on login.php?', '6', '4', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Create Account', 'MODULE_CONTENT_RECAPTCHA_PAGE_CREATE_ACCOUNT', 'True', 'Do you want to enable reCAPTCHA on create_account.php?', '6', '5', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Account PWA (Purchase Without Account)', 'MODULE_CONTENT_RECAPTCHA_PAGE_ACCOUNT_PWA', 'True', 'Do you want to enable reCAPTCHA on account_pwa.php?', '6', '6', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Account Edit', 'MODULE_CONTENT_RECAPTCHA_PAGE_ACCOUNT_EDIT', 'False', 'Do you want to enable reCAPTCHA on account_edit.php?', '6', '7', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Account Password', 'MODULE_CONTENT_RECAPTCHA_PAGE_ACCOUNT_PASSWORD', 'False', 'Do you want to enable reCAPTCHA on account_password.php?', '6', '8', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Password Forgotten', 'MODULE_CONTENT_RECAPTCHA_PAGE_PASSWORD_FORGOTTEN', 'False', 'Do you want to enable reCAPTCHA on password_forgotten.php? (NOTE: this page is already secured via Action Recorder, adding reCAPTCHA is not needed.)', '6', '9', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Contact Us', 'MODULE_CONTENT_RECAPTCHA_PAGE_CONTACT_US', 'False', 'Do you want to enable reCAPTCHA on contact_us.php? (NOTE: this page is already secured via Action Recorder, adding reCAPTCHA is not needed.)', '6', '10', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Tell A Friend', 'MODULE_CONTENT_RECAPTCHA_PAGE_TELL_A_FRIEND', 'True', 'Do you want to enable reCAPTCHA on tell_a_friend.php? (NOTE: this page is already secured via Action Recorder, adding reCAPTCHA is not needed.)', '6', '11', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Product Reviews Write', 'MODULE_CONTENT_RECAPTCHA_PAGE_PRODUCT_REVIEWS_WRITE', 'False', 'Do you want to enable reCAPTCHA on product_reviews_write.php?', '6', '12', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Page: Product Reviews Write PWA', 'MODULE_CONTENT_RECAPTCHA_PAGE_PRODUCT_REVIEWS_WRITE_PWA', 'True', 'Do you want to enable reCAPTCHA on product_reviews_write_pwa.php?', '6', '13', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
	  // pages end
      tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Theme', 'MODULE_CONTENT_RECAPTCHA_THEME', 'light', 'Please select the theme for your reCAPTCHA.', '6', '15', 'tep_cfg_select_option(array(\'light\', \'dark\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Size', 'MODULE_CONTENT_RECAPTCHA_SIZE', 'normal', 'Please select the size of your reCAPTCHA box.', '6', '16', 'tep_cfg_select_option(array(\'normal\', \'compact\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Position', 'MODULE_CONTENT_RECAPTCHA_POSITION', 'pull-right', 'Where would you like the reCAPTCHA displayed?', '6', '17', 'tep_cfg_select_option(array(\'pull-left\', \'center-block\', \'pull-right\'), ', now())");
	  tep_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Content Width', 'MODULE_CONTENT_RECAPTCHA_CONTENT_WIDTH', '12', 'What width container should the content be shown in? (12 = full width, 6 = half width).', '6', '18', 'tep_cfg_select_option(array(\'12\', \'11\', \'10\', \'9\', \'8\', \'7\', \'6\', \'5\', \'4\', \'3\', \'2\', \'1\'), ', now())");
	  } // if ( !defined('RECAPTCHA_MODULE') )
    }

    function remove() {
      tep_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('RECAPTCHA_MODULE', 
	  			   'MODULE_CONTENT_RECAPTCHA_STATUS', 
				   'MODULE_CONTENT_RECAPTCHA_PUBLIC_KEY', 
				   'MODULE_CONTENT_RECAPTCHA_PRIVATE_KEY', 
				   // pages start
				   'MODULE_CONTENT_RECAPTCHA_PAGE_LOGIN', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_CREATE_ACCOUNT', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_ACCOUNT_PWA', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_ACCOUNT_EDIT', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_ACCOUNT_PASSWORD', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_PASSWORD_FORGOTTEN', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_CONTACT_US', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_TELL_A_FRIEND', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_PRODUCT_REVIEWS_WRITE', 
				   'MODULE_CONTENT_RECAPTCHA_PAGE_PRODUCT_REVIEWS_WRITE_PWA', 
				   // pages end
				   'MODULE_CONTENT_RECAPTCHA_THEME', 
				   'MODULE_CONTENT_RECAPTCHA_SIZE', 
				   'MODULE_CONTENT_RECAPTCHA_POSITION', 
				   'MODULE_CONTENT_RECAPTCHA_CONTENT_WIDTH');
    }
  }
  