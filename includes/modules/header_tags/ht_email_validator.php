<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  class ht_email_validator {
    var $code = 'ht_email_validator';
    var $group = 'header_tags';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function ht_email_validator() {
      $this->title = MODULE_HEADER_TAGS_EMAIL_VALIDATOR_TITLE;
      $this->description = MODULE_HEADER_TAGS_EMAIL_VALIDATOR_DESCRIPTION;

      if ( defined('MODULE_HEADER_TAGS_EMAIL_VALIDATOR_STATUS') ) {
        $this->sort_order = MODULE_HEADER_TAGS_EMAIL_VALIDATOR_SORT_ORDER;
        $this->enabled = (MODULE_HEADER_TAGS_EMAIL_VALIDATOR_STATUS == 'True');
      }
    }

    function execute() {
      global $PHP_SELF, $oscTemplate;
      
      if (basename($PHP_SELF) == FILENAME_CREATE_ACCOUNT) {
        
            if (MODULE_HEADER_TAGS_EMAIL_VALIDATOR_JS_PLACEMENT != 'Header') {
                $this->group = 'footer_scripts';
            }
        
            $javascript = '<script type="text/javascript" src="ext/jquery/email_validator/email_validator.js"></script>' . "\n";
            $javascript .= '<script type="text/javascript">
                            $(function(){//BOF doc ready
                                $("#email").on("blur", function() {
                                    clearHide();
                                    $(this).mailcheck({
                                        suggested: function(element, suggestion) {
                                            $(".email_suggestion").show();
                                            $("#suggested_email").text(suggestion.full);
                                    
                                        },
                                        empty: function(element) {
                                            clearHide();
                                        }
                                    });
                                });
                                $(".suggested-email").on("click", function(e) {
                                    $("#email").val($("#suggested_email").text());
                                    clearHide();
                                    e.preventDefault();
                                });
                                $("#email").on("focus", function() {
                                    clearHide();
                                });
                            });//eof doc ready
                            </script>' . "\n";

            $oscTemplate->addBlock($javascript, $this->group);
      }
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_HEADER_TAGS_EMAIL_VALIDATOR_STATUS');
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Email Validation  Module', 'MODULE_HEADER_TAGS_EMAIL_VALIDATOR_STATUS', 'True', 'Do you want to add live create account Email Validation  to your shop?', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Javascript Placement', 'MODULE_HEADER_TAGS_EMAIL_VALIDATOR_JS_PLACEMENT', 'Header', 'Should the Email Validation  javascript be loaded in the header or footer?', '6', '1', 'tep_cfg_select_option(array(\'Header\', \'Footer\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_HEADER_TAGS_EMAIL_VALIDATOR_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_HEADER_TAGS_EMAIL_VALIDATOR_STATUS', 'MODULE_HEADER_TAGS_EMAIL_VALIDATOR_JS_PLACEMENT', 'MODULE_HEADER_TAGS_EMAIL_VALIDATOR_SORT_ORDER');
    }
  }
?>