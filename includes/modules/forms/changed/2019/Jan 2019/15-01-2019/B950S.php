 <?php 
    require_once('Mobile_Detect.php');
    $detect = new Mobile_Detect();
    $videoname = 'B-950-SWIVEL';
    if($category_name == 'B950') {
        $videoname = 'B-950';
    }
?>
<script type="text/javascript">
var tot1=osc=im_id=img_ajx="";
<?php
        if(isset($HTTP_GET_VARS['id'])){
            $product="select count(*) as total from ".TABLE_PRODUCTS." as p, ".TABLE_PRODUCTS_DESCRIPTION." as pd, ".TABLE_PRODUCTS_TO_CATEGORIES." as pc where p.products_id=pd.products_id and pd.products_id=pc.products_id and pc.categories_id=".$HTTP_GET_VARS['id']." and pd.language_id=".(int)$languages_id;
            $product=tep_db_query($product);
            $products=tep_db_fetch_array($product);  
        }
    ?>
    arr_len=<?=$products['total']?>;
    <? if($category_name!='EP5'){ ?>
    arr_len=parseInt(arr_len)+7;
    <? } ?>
var product_name_price=new Array(arr_len);
    <?php
        if(isset($HTTP_GET_VARS['id'])){
            $product="select p.products_id as id, pd.products_name as name, p.products_price as price from ".TABLE_PRODUCTS." as p, ".TABLE_PRODUCTS_DESCRIPTION." as pd, ".TABLE_PRODUCTS_TO_CATEGORIES." as pc where p.products_id=pd.products_id and pd.products_id=pc.products_id and pc.categories_id=".$HTTP_GET_VARS['id']." and pd.language_id=".(int)$languages_id;
            $product=tep_db_query($product);
            while($products=tep_db_fetch_array($product)){?>
                    product_name_price['<?=$products['name']?>']=new Array("<?=$products['id']?>", "<?=$products['price']?>");
            <?php } 
        if($HTTP_GET_VARS['id']==81){
            $product="select p.products_id as id, pd.products_name as name, p.products_price as price from ".TABLE_PRODUCTS." as p, ".TABLE_PRODUCTS_DESCRIPTION." as pd, ".TABLE_PRODUCTS_TO_CATEGORIES." as pc where p.products_id=pd.products_id and pd.products_id=pc.products_id and pc.categories_id=80 and pd.language_id=".(int)$languages_id;
            $product=tep_db_query($product);
            while($products=tep_db_fetch_array($product)){?>
                    product_name_price['<?=$products['name']?>']=new Array("<?=$products['id']?>", "<?=$products['price']?>");
            <?}
         }
      }
    ?>
</script>
<script src="jquery.confirm/jquery.confirm.js"></script>
<style type="text/css">
    .message_w {
    border: 2px solid #ff0000;
}
	.item{
	/*background: url("img/shadow_wide.png") no-repeat center bottom;*/
	padding-bottom: 6px;
	display: inline-block;
	margin-bottom: 30px;
	position:relative;
}
	.heading_all{
        color:white; 
        margin-bottom:0px;
        margin-top:5px;
        font-size:16px;
        text-shadow: 1px 1px black,1px 1px black,1px 1px black,1px 1px black;
    }

.item .delete{
	/*background:url('img/delete_icon.png') no-repeat;*/
	width:37px;
	height:38px;
	position:absolute;
	cursor:pointer;
	top:10px;
	right:-80px
}

.item a{
	background-color: #FAFAFA;
	border: none;
	display: block;
	padding: 10px;
	text-decoration: none;
}

.item:first-child .delete:before{
	background:url('img/tooltip.png') no-repeat;
	content:'.';
	text-indent:-9999px;
	overflow:hidden;
	width:145px;
	height:90px;
	position:absolute;
	right:-110px;
	top:-95px;
}

.item a img{
	display:block;
	border:none;
}
.c_msg{background:none;}
#confirmOverlay{
	width:100%;
	height:100%;
	position:fixed;
	top:0;
	left:0;
	background:url('jquery.confirm/ie.png');
	background: -moz-linear-gradient(rgba(11,11,11,0.1), rgba(11,11,11,0.6)) repeat-x rgba(11,11,11,0.2);
	background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(11,11,11,0.1)), to(rgba(11,11,11,0.6))) repeat-x rgba(11,11,11,0.2);
	z-index:100000;
}

#confirmBox{
	background:url('jquery.confirm/body_bg.jpg') repeat-x left bottom #e5e5e5;
	width:500px;
	position:fixed;
	left:50%;
	top:20%;
	margin:-130px 0 0 -230px;
	border: 1px solid rgba(33, 33, 33, 0.6);
	
	-moz-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
	-webkit-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
	box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
}

#confirmBox h1,
#confirmBox p{
	font:26px/1 'Cuprum','Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	background:url('jquery.confirm/header_bg.jpg') repeat-x left bottom #f5f5f5;
	padding: 0px 25px;
	text-shadow: 1px 1px 0 rgba(255, 255, 255, 0.6);
	color:#666;
}

#confirmBox h1{
	letter-spacing:0.3px;
	color:#888;
	font:26px/1 'Cuprum','Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	background:url('jquery.confirm/header_bg.jpg') repeat-x left bottom #f5f5f5;
	padding: 18px 25px;
	text-shadow: 1px 1px 0 rgba(255, 255, 255, 0.6);
	color:#666;
}

#confirmBox p{
	background:none;
	font-size:16px;
	line-height:1.4;
	padding-top: 0px;
}

#confirmButtons{
	padding:15px 0 25px;
	text-align:center;
}

#confirmBox .button{
	display:inline-block;
	background:url('jquery.confirm/buttons.png') no-repeat;
	color:white;
	position:relative;
	height: 33px;
	
	font:17px/33px 'Cuprum','Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	
	margin-right: 15px;
	padding: 0 35px 0 40px;
	text-decoration:none;
	border:none;
}

#confirmBox .button:last-child{	margin-right:0;}

#confirmBox .button span{
	position:absolute;
	top:0;
	right:-5px;
	background:url('jquery.confirm/buttons.png') no-repeat;
	width:5px;
	height:33px
}

#confirmBox .blue{				background-position:left top;text-shadow:1px 1px 0 #5889a2;}
#confirmBox .blue span{			background-position:-195px 0;}
#confirmBox .blue:hover{		background-position:left bottom;}
#confirmBox .blue:hover span{	background-position:-195px bottom;}

#confirmBox .gray{				background-position:-200px top;text-shadow:1px 1px 0 #707070;}
#confirmBox .gray span{			background-position:-395px 0;}
#confirmBox .gray:hover{		background-position:-200px bottom;}
#confirmBox .gray:hover span{	background-position:-395px bottom;}
</style>
<script>
	$(document).ready(function(){
		zero=true;
        $("tr#right_lenght").css('display','none');
        $("tr#left_lenght").css('display','none');
        $("input#glass-face").val(4);
		getPriceOfProduct(document.forms['cart_quantity']);
        $("#end_options").change(function(){
            if($("#end_options").val()!="select"){
                $("select").removeAttr("disabled");
                if($(this).val()=="Both Closed End Panels"){
                    $("input#glass-face").val(1);//calling the image of both closed end panels
                    $("#left_length").removeAttr("disabled");
                    $("tr#right_lenght").css('display','');
                    $("tr#left_lenght").css('display','');
                    $("#right_length").removeAttr("disabled");
                }else if($(this).val()=="Right Closed End Panel"){
                    $("input#glass-face").val(2);//calling the image according to the above click
                    $("#left_length").attr("disabled","disabled");
                    $("#right_length").removeAttr("disabled");
                    $("tr#right_lenght").css('display','');
                    $("tr#left_lenght").css('display','none');
                }else if($(this).val()=="Left Closed End Panel"){
                    $("#left_length").removeAttr("disabled");
                    $("#right_length").attr("disabled","disabled");
                    $("tr#right_lenght").css('display','none');
                    $("tr#left_lenght").css('display','');
                    $("input#glass-face").val(3);//showing the image of left closed panel
                }else if($(this).val()=="No Closed End Panels"){
                    $("#left_length").attr('disabled', 'disabled');
                    $("#right_length").attr('disabled', 'disabled');
                    $("tr#right_lenght").css('display','none');
                    $("tr#left_lenght").css('display','none');
                    $("input#glass-face").val(4);//showing the image
                }
                if($(".makeadjustablecheck31").val()!="select"){
                    //$("#round_check").attr("disabled",true);//making disable the checkbox.. .. ..
                }
                $("#endpan_err").attr("src","img/iconCheckOn.gif");
                zero=true;
                getPriceOfProduct(document.forms['cart_quantity']);
            }else{
                $("#endpan_err").attr("src","img/iconCheckOff.gif");
                $("tr#right_lenght").css('display','none');
				$("tr#left_lenght").css('display','none');
				$("input#glass-face").val(4);
                $("#left_length").removeAttr("disabled");
                $("#right_length").removeAttr("disabled");
                zero=false;
                getPriceOfProduct(document.forms['cart_quantity']);
            }
        });
        

    // });

        

    });

</script>
<script type="text/javascript">

		var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6
if(isFirefox==true){
	var width_one=23;
	var width_two=27;
	width_three=30;
	right_next=-44;
	width_review=21;  
	redlinebrowser=26;
	redlinebrowser1=26;
	redln=85;        /*red line width for price */
} else if(isChrome==true){
	var width_one=19;
	var width_two=25;
	width_three=28;
	right_next=-36;
	width_review=19; 
	redlinebrowser=0;
	redlinebrowser1=0;
	redln=85;
}
else if(isSafari==true){
	var width_one=19;
	var width_two=25;
	width_three=28;
	right_next=-40;
	width_review=19; 
	redlinebrowser=0;
	redlinebrowser1=0;
	redln=85;
}else if(isIE==true){
	if (document.all && !document.querySelector) {
        width_two=27;
		redln=85;
    }else{
       	var width_two=32;
       	redln=87;
    }
	var width_one=19;
	//var width_two=28;
	width_three=28;
	right_next=-40;
	width_review=19; 
	redlinebrowser=-40;
	redlinebrowser1=-40;
	//redln=87;
}else if(isOpera==true){
	var width_one=19;
	var width_two=25;
	width_three=28;
	right_next=-40;
	width_review=19; 
	redlinebrowser=0;
	redlinebrowser1=0;
	redln=85;
}else{
	var width_one=19;
	var width_two=29;	
	width_three=28;
	width_review=19;
	redln=85;
}
	one=two=three=four=five=false;
    choseOption=0;
    choselength=0;
    choseRounded=0;
    choseFlang=0;
    choseBracket=0;
    priceOption=0;
    h=100;
    h1=128;
    h2=153;
    h3=200;
    h8=0;
    t8=0;
    //image_string1='<img src="images/B950'+<?=$HTTP_GET_VARS['type']?>+'/START.jpg" style="width:100%">'; 
    //document.getElementById('additional_image').innerHTML=image_string1;
    leftstr='<td class="test-lenght1bay" ><a class="thickbox" href="images/EP5/1bay_faceA.jpg" ><h1 style="margin-left:20px;">Right End</h1></a></td><td><span id="left_length_span"><select name="left_length" onchange="getPriceOfProduct(this.form)"> <option value="12">12"</option><option value="18">18"</option><option value="24">24"</option><option value="custom">Custom</option></select></span></td><td><span id="errormsgfirstname"><img id="glass_a_err" src="img/iconCheckOff.gif"></span></td>';
    rightstr='<td class="test-lenght2baya"><a class="thickbox" href="images/EP5/2bay_faceA.jpg"><h1 style="margin-left:20px;">Left End</h1></a></td><td><span id="right_length_span"><select name="right_length" onchange="getPriceOfProduct(this.form)"> <option value="12">12"</option><option value="18">18"</option><option value="24">24"</option><option value="custom">Custom</option></select></span></td><td><span id="errormsgfirstname"><img id="glass_b_err" src="img/iconCheckOff.gif"></span></td>';
    $(document).ready(function(){
    	var rght=lft=bth=noe=false;
        $("#1").click(function(){
            rght=lft=bth=noe=false;
            bth=true;
        });
        $("#2").click(function(){
            rght=lft=bth=noe=false;
            rght=true;
        });
        $("#3").click(function(){
            rght=lft=bth=noe=false;
            lft=true;
        });
        $("#4").click(function(){
            rght=lft=bth=noe=false;
            noe=true;
        });
        $("ul.option li").click(function(){
            i=$("ul.option").children().length;
            j=0;
            while(j<i){
                $("ul.option li").removeClass('selected');
                j++;
            }
            $(this).addClass('selected');
			if($(this).text()=="Both Closed End Panels"||bth){
				$("input#glass-face").val(1);
                $("tr#right_lenght").html(rightstr);
                $("tr#left_lenght").html(leftstr);
			}
			else if($(this).text()=="Right Closed End Panel"||rght){
				$("input#glass-face").val(2);
                $("tr#left_lenght").html("<td height='22'></td>");
                 $("tr#right_lenght").html(rightstr);
			}
			else if($(this).text()=="Left Closed End Panel"||lft){
  	             $("tr#right_lenght").html("<td height='22'></td>");
                $("tr#left_lenght").html(leftstr);
				$("input#glass-face").val(3);
			}
			else if($(this).text()=="No Closed End Panels"||noe){
			     $("tr#right_lenght").html("<td height='22'></td>");
                $("tr#left_lenght").html("<td height='22'></td>");
				$("input#glass-face").val(4);
			}
            $("select").removeAttr("disabled");
            $("input").removeAttr("disabled");
           // $("#msg").remove();
            $("table tr td#option-panel").css("background","none");
            action_event(".test-warsi")
			getPriceOfProduct(document.forms['cart_quantity']);
        });
        
        $('input[type="checkbox"]').click(function(){
			if($(this).is(':checked')){
				$(this).val(1);
                getPriceOfProduct(document.forms['cart_quantity']);
			}
			else{
				$(this).val(0);
                getPriceOfProduct(document.forms['cart_quantity']);
			}            
        });
    });
</script>
<!--Coading for custom popup-->
<?php
    $msg="";
    $id=$_GET['id'];
    $tp=$_GET['type'];
    $rs=tep_db_query("select * from custom_popup where id='".$id."'and bay='".$tp."'");
    $rw=tep_db_fetch_array($rs);
    $ms=$rw['message'];
    $ms_option=$rw['option_popup'];
    $ms_option1=$rw['opiton1_popup'];
    $ms_post=$rw['post_popup'];
    $ms_left=$rw['left_popup'];
    $ms_right=$rw['right_popup'];
    $ms_face=$rw['face_popup'];
    $ms_adjustable=$rw['adjustable_popup'];
    $ms_cart=$rw['cart_popup'];
    $im_id=rand();
    if(isset($_SESSION["scr"])){
        $_SESSION['scr']=$_SESSION['scr']."-".$im_id;
    }else{
        $_SESSION['scr']=$im_id;
    }
    //echo $ms.'<br>'.$ms_adjustable.'<br>'.$ms_cart.'<br>'.$ms_face.'<br>'.$ms_left.'<br>'.$ms_option.'<br>'.$ms_option1.'<br>'.$ms_post.'<br>'.$ms_right;
?>
<script type="text/javascript">
	
	function finishImage(form,image){
         category_name="<?=$category_name?>";
        foldername=getProductFolderName("<?=$category_name?>");
        if(image!=""){
            imageName=image;
        }

        cross = '<input type="button" onclick="getPriceOfProduct(this.form);" style="margin: 0 4px;position: absolute;right: -615px;top: -160px;width: 20px;z-index: 1000000;" value="X" class="rounded-corner-image">'
      <?php 
		if (!$detect->isMobile())
		{
		?>
		
        image_string='<img src="images/'+imageName+'" style="width:568px;height:453px">';
		<?php 
		}
		else{
		?>
        image_string='<img src="images/'+imageName+'" style="width:828px;height:583px;">'; 

		<?php
		}
		?>
//        alert(image_string);
        
        document.getElementById('additional_image').innerHTML=image_string;
        document.getElementById('rott').innerHTML=cross;
    }
	
 function getProductFolderName(productname){
        foldername="";
        switch(productname){
            case 'B950':{
                foldername="B950";
                break;
            }
            case 'B950 SWIVEL':{
                foldername="B950-Swivel";
                break;
            }
        }
        return foldername;
    }
    
    function getVedio(){
        str='<video id="example_video_1" class="video-js" width="600" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay ><source src="images/flang.mp4"'+" type='video/mp4; codecs="+'"avc1.42E01E, mp4a.40.2"'+' /><source src="images/flang.webm"'+" type='video/webm; codecs="+'"vp8, vorbis"'+' /><source src="images/flang.ogv"'+" type='video/ogg; codecs="+'"theora, vorbis"'+' /><object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf"><param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" /><param name="allowfullscreen" value="true" /><param name="flashvars" value='+"config={"+'"playlist":["pic.jpg", {"url": "images/flang.mp4","autoPlay":false,"autoBuffering":true}]}'+' /><img src="pic.jpg" width="640" height="480" alt="Poster Image" title="No video playback capabilities." /></object></video>';
        document.getElementById('additional_image').innerHTML=str;
    }
    
	function getPriceOfProduct(form){
	
	
	
	
	$('#product_type').val($('.product-title').text());
	
	if(!$('select[name="right_length"]').length){
			$('#c_glass_right_val').val('');
			$('#c_glass_right').val('');
		}
		if(!$('select[name="left_length"]').length){
			$('#c_glass_left_val').val('');
			$('#c_glass_left').val('');
		}
		if(!$('select[name="post_height"]').length){
			$('#c_glass_post_val').val('');
		}
		if(!$('select[name="face_length"]').length){
			$('#c_glass_face_val').val('');
			$('#c_glass_face').val('');
		}
		if(!$('select[name="face_length_a"]').length){
			$('#c_glass_a_val').val('');
			$('#c_glass_a').val('');
		}
		if(!$('select[name="face_length_b"]').length){
			$('#c_glass_b_val').val('');
			$('#c_glass_b').val('');
		}
		if(!$('select[name="face_length_c"]').length){
			$('#c_glass_c_val').val('');
			$('#c_glass_c').val('');
		}
	
		//alert("warsi");
		number_of_end_post=2;
		number_glass=1;
		number_center=1;
        flag=1;
		foldername="";
        imageName="";
        osc="<?=$_REQUEST['osCsid']; ?>";
        im_id="<?=$im_id; ?>";
		glassName="";
		glassName_l="";
		glassName_r="";
		glassName_a="";
		glassName_b="";
		glassName_c="";
		glassName_d="";
		leftEndPost="";
		rightEndPost="";
		centerPost="";
		
		centerPost1="";
		centerPost2="";
		centerPost3="";
		
		leftEndPanel="";
		rightEndPanel="";
		flangeCovers="";//Use For Light
        flageCovers2="";//Use For Light Bracket
        light_a="";
        light_b="";
        light_c="";
		light_d="";
       
        facePrice=0;
        facePrice_a=0;
        facePrice_b=0;
        facePrice_c=0;
		facePrice_d=0;
        facePrice_l=0;
        facePrice_r=0;
        leftPostPrice=0;
        rightPostPrice=0;
        leftEndPanelPrice=0;
        rightEndPanelPrice=0;
        centerPostPrice=0;
		
			//corner post
			centerPostPrice1=0;
			centerPostPrice2=0;
			centerPostPrice3=0;
			
        anglePostPrice=0;
        flangeCoversPrice=0;//Use for Light
        flangeCoversPrice2=0;//Use for Light Bracket
        str="";        
		
		category_name="<?=$category_name?>";
		right_lenght_obj=form.right_length;
		left_lenght_obj=form.left_length;
		post_height_obj=form.post_height;
		face_lenght_obj=form.face_length;
		face_lenght_a_obj=form.face_length_a;
		face_lenght_b_obj=form.face_length_b;
		face_lenght_c_obj=form.face_length_c;
        face_lenght_d_obj=form.face_length_d;		
		type_obj=form.type;
        
		glass_face_obj=form.glass_face;
		corner_obj=form.rounded_corners;
		flange_covers_obj=form.flange_covers;//use Form Light
        flange_covers_obj2=form.flange_covers_2;//use Form Light Bracket
		choose_finish_obj=form.choose_finish;
		
		
			degree_obj=form.degree;
			posttype_obj=form.posttype;
	
	
	
	if(type_obj.value=="2BAY"||type_obj.value=="3BAY"||type_obj.value=="4BAY"){
		if(form.posttype.value=='outer')
		{
			var posttypegl='Outer';
		}
		else if(form.posttype.value=='inner')
		{
			var posttypegl='Inner';
		}
		
		if(form.degree.value=='90degre')
		{
			var postdegreegl="90 Degree";
		}
		else if(form.degree.value=='135degre')
		{
			var postdegreegl="135 Degree";
		}
	}
	
	
			var gotocornerpostss=$("input[name='gotocornerpostcheck']:checked").val();
		var cornerPosition=$("input[name='corner_post']:checked").val();
					//alert(cornerPosition);
        foldername=getProductFolderName(category_name)+type_obj.value;
         
         ///alert(type_obj.value);
         
         
         //Code Start Here
         //posts
         if(category_name=="B950 SWIVEL"){
            leftEndPost="B950SLP"+choose_finish_obj.value;
         }
         else{
            leftEndPost="B950LP"+choose_finish_obj.value;
         }
        
        str+='<input type="hidden" name="products_id[]" value="'+product_name_price[leftEndPost][0]+'" />';
        leftPostPrice=parseFloat(product_name_price[leftEndPost][1]);
        
        if(category_name=="B950 SWIVEL"){
            rightEndPost="B950SRP"+choose_finish_obj.value;
        }
        else{
            rightEndPost="B950RP"+choose_finish_obj.value;
        }
        
        str+='<input type="hidden" name="products_id[]" value="'+product_name_price[rightEndPost][0]+'" />';
        rightPostPrice=parseFloat(product_name_price[rightEndPost][1]);
        if(type_obj.value!="1BAY"){
            if(category_name=="B950 SWIVEL"){
				
				
				
                //centerPost="B950SCP"+choose_finish_obj.value;
				//B950S "+posttypegl+" Corner "+postdegreegl+" Post PC
				if(gotocornerpostss=="1"){
				if(cornerPosition=="1st Center Post from Left"){
				centerPost1="B950S "+posttypegl+" Corner "+postdegreegl+" Post "+choose_finish_obj.value;
				centerPost2="B950SCP"+choose_finish_obj.value;
				centerPost3="B950SCP"+choose_finish_obj.value;
				}
				if(cornerPosition=="2nd Center Post from Left"){
					
				centerPost2="B950S "+posttypegl+" Corner "+postdegreegl+" Post "+choose_finish_obj.value;
				centerPost1="B950SCP"+choose_finish_obj.value;
				centerPost3="B950SCP"+choose_finish_obj.value;
				}
				if(cornerPosition=="3rd Center Post from Left"){
				centerPost3="B950S "+posttypegl+" Corner "+postdegreegl+" Post "+choose_finish_obj.value;
				centerPost1="B950SCP"+choose_finish_obj.value;
				centerPost2="B950SCP"+choose_finish_obj.value;
				}
				else{
					centerPost="B950SCP"+choose_finish_obj.value;
				}
				}
				else{
					centerPost="B950SCP"+choose_finish_obj.value;
				}
				
				
            }
            else{
                centerPost="B950CP"+choose_finish_obj.value;
            }
        }
        
        
        //endpanels
        if(glass_face_obj.value==1){
        	if(left_lenght_obj.value!="select"){
        		leftEndPanel="B950-g"+left_lenght_obj.value+"LEP";
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[leftEndPanel][0]+'" />';
        		leftEndPanelPrice=parseFloat(product_name_price[leftEndPanel][1]);
        	}
        	if(right_lenght_obj.value!="select"){
        		rightEndPanel="B950-g"+right_lenght_obj.value+"REP";
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[rightEndPanel][0]+'" />';
        		rightEndPanelPrice=parseFloat(product_name_price[rightEndPanel][1]);
        	}
            imageName="BOTHENDS";
        }
        else if(glass_face_obj.value==2){
        	if(left_lenght_obj.value!="select"){

        	}
        	if(right_lenght_obj.value!="select"){
        		rightEndPanel="B950-g"+right_lenght_obj.value+"REP";
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[rightEndPanel][0]+'" />';
        		rightEndPanelPrice=parseFloat(product_name_price[rightEndPanel][1]);
        	}
            leftEndPanel="";
            //alert("warsi");
            leftEndPanelPrice=0;
            imageName="RIGHTEND";
        }
        else if(glass_face_obj.value==3){
        	if(left_lenght_obj.value!="select"){
        		leftEndPanel="B950-g"+left_lenght_obj.value+"LEP";
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[leftEndPanel][0]+'" />';
        		leftEndPanelPrice=parseFloat(product_name_price[leftEndPanel][1]);
        	}
        	if(right_lenght_obj.value!="select"){
        		
        	}
            rightEndPanel="";
            imageName="LEFTEND";
            rightEndPanelPrice=0;
        }
        else{
            leftEndPanel="";
            rightEndPanel="";
            imageName="NOENDS";
            leftEndPanelPrice=0;
            rightEndPanelPrice=0;
        }
       //"B950S "+posttypegl+" Corner "+postdegreegl+" Post"+choose_finish_obj.value;
        //glasses
		 if(type_obj.value=="4BAY"){
		 	if(face_lenght_a_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")){
					glassName_a="B950S-"+face_lenght_a_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_a="B950-"+face_lenght_a_obj.value+"GL";	
					}
				}
				else{
				glassName_a="B950-"+face_lenght_a_obj.value+"GL";	
				}
				
		 		
				
				
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_a][0]+'" />';
		 		facePrice_a=parseFloat(product_name_price[glassName_a][1]);
		 	}
		 	if(face_lenght_b_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")||(cornerPosition=="2nd Center Post from Left"))){
					glassName_b="B950S-"+face_lenght_b_obj.value+" "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_b="B950-"+face_lenght_b_obj.value+"GL";	
					}
				}
				else{
				glassName_b="B950-"+face_lenght_b_obj.value+"GL";	
				}
				
		 		
				
				
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_b][0]+'" />';
		 		facePrice_b=parseFloat(product_name_price[glassName_b][1]);
		 	}
		 	if(face_lenght_c_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="2nd Center Post from Left")||(cornerPosition=="3rd Center Post from Left"))){
					glassName_c="B950S-"+face_lenght_c_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_c="B950-"+face_lenght_c_obj.value+"GL";	
					}
				}
				else{
				glassName_c="B950-"+face_lenght_c_obj.value+"GL";	
				}
		 		
				
				
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_c][0]+'" />';
		 		facePrice_c=parseFloat(product_name_price[glassName_c][1]);
		 	}
		 	if(face_lenght_d_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if((form.posttype.value!="select")&&(cornerPosition=="3rd Center Post from Left")){
					glassName_d="B950S-"+face_lenght_d_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_d="B950-"+face_lenght_d_obj.value+"GL";	
					}
				}
				else{
				glassName_d="B950-"+face_lenght_d_obj.value+"GL";	
				}
		 		
				
				
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_d][0]+'" />';
				facePrice_d=parseFloat(product_name_price[glassName_d][1]); 		
		 	}
			
			if(gotocornerpostss=="1"){
			str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost1][0]+'" />';
            str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost2][0]+'" />';
			str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost3][0]+'" />';
            centerPostPrice=parseFloat(product_name_price[centerPost1][1])+parseFloat(product_name_price[centerPost2][1])+parseFloat(product_name_price[centerPost3][1]);	
			}
			else{
            str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
            str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
			str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
            centerPostPrice=parseFloat(product_name_price[centerPost][1])+parseFloat(product_name_price[centerPost][1])+parseFloat(product_name_price[centerPost][1]);
			//alert(facePrice_d);
			}
			
        }
        else if(type_obj.value=="3BAY"){
        	if(face_lenght_a_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")){
					glassName_a="B950S-"+face_lenght_a_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_a="B950-"+face_lenght_a_obj.value+"GL";	
					}
				}
				else{
				glassName_a="B950-"+face_lenght_a_obj.value+"GL";	
				}
        		
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_a][0]+'" />';
        		facePrice_a=parseFloat(product_name_price[glassName_a][1]);
        	}
        	if(face_lenght_b_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")||(cornerPosition=="2nd Center Post from Left"))){
					glassName_b="B950S-"+face_lenght_b_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_b="B950-"+face_lenght_b_obj.value+"GL";	
					}
				}
				else{
				glassName_b="B950-"+face_lenght_b_obj.value+"GL";	
				}
        		
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_b][0]+'" />';
        		facePrice_b=parseFloat(product_name_price[glassName_b][1]);
        	}
        	if(face_lenght_c_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="2nd Center Post from Left")||(cornerPosition=="3rd Center Post from Left"))){
					glassName_c="B950S-"+face_lenght_c_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_c="B950-"+face_lenght_c_obj.value+"GL";	
					}
				}
				else{
				glassName_c="B950-"+face_lenght_c_obj.value+"GL";	
				}
        		
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_c][0]+'" />';
        		facePrice_c=parseFloat(product_name_price[glassName_c][1]);
        	}
			glassName_d="";
			facePrice_d=0;
			
			if(gotocornerpostss=="1"){
			 str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost1][0]+'" />';
            str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost2][0]+'" />';
            centerPostPrice=parseFloat(product_name_price[centerPost1][1])+parseFloat(product_name_price[centerPost2][1]);	
			}
			else{
             str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
            str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
            centerPostPrice=parseFloat(product_name_price[centerPost][1])+parseFloat(product_name_price[centerPost][1]);
			//alert(facePrice_d);
			}
			
            //str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
            //str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
            //centerPostPrice=parseFloat(product_name_price[centerPost][1])+parseFloat(product_name_price[centerPost][1]);
        }
		
        else if(type_obj.value=="2BAY"){
        	if(face_lenght_a_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")){
					glassName_a="B950S-"+face_lenght_a_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_a="B950-"+face_lenght_a_obj.value+"GL";	
					}
				}
				else{
				glassName_a="B950-"+face_lenght_a_obj.value+"GL";	
				}
        		
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_a][0]+'" />';
        		facePrice_a=parseFloat(product_name_price[glassName_a][1]);
        	}
        	if(face_lenght_b_obj.value!="select"){
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")||(cornerPosition=="2nd Center Post from Left"))){
					glassName_b="B950S-"+face_lenght_b_obj.value+"GL "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					glassName_b="B950-"+face_lenght_b_obj.value+"GL";	
					}
				}
				else{
					glassName_b="B950-"+face_lenght_b_obj.value+"GL";
				}
        		
        		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_b][0]+'" />';
        		facePrice_b=parseFloat(product_name_price[glassName_b][1]);
        	}
            glassName_c="";
			glassName_d="";
            facePrice_c=0;
			facePrice_d=0;
			
			if(gotocornerpostss=="1"){
			 str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost1][0]+'" />';
            centerPostPrice=parseFloat(product_name_price[centerPost1][1]);	
			}
			else{
             str+='<input type="hidden" name="products_id[]" value="'+product_name_price[centerPost][0]+'" />';
            centerPostPrice=parseFloat(product_name_price[centerPost][1]);
			//alert(facePrice_d);
			}
			
            
        }
        else if(type_obj.value=="1BAY"){
        	if(face_lenght_obj.value!="select"){
	            glassName_a="B950-"+face_lenght_obj.value+"GL";
	            glassName_b="";
	            glassName_c="";
				glassName_d="";
	            facePrice_a=parseFloat(product_name_price[glassName_a][1]);
	            facePrice_b=0;
	            facePrice_c=0;
				facePrice_d=0;
	            
	            
	            str+='<input type="hidden" name="products_id[]" value="'+product_name_price[glassName_a][0]+'" />';
	        }
        }
        
        //lights
         if(flange_covers_obj.value=="yes"){
		 if(type_obj.value=="4BAY"){
		 	if(face_lenght_a_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left"))){
					light_a="B950S-"+face_lenght_a_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					light_a="B950-"+face_lenght_a_obj.value+"LYT";	
					}
				}
				else{
					light_a="B950-"+face_lenght_a_obj.value+"LYT";
				}
				
				
		 		
				
				
				
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_a][0]+'" />';
		 		flangeCoversPrice+=parseFloat(product_name_price[light_a][1])
		 	}
		 	if(face_lenght_b_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")||(cornerPosition=="2nd Center Post from Left"))){
					light_b="B950S-"+face_lenght_b_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
						light_b="B950-"+face_lenght_b_obj.value+"LYT";
					}
				}
				else{
					light_b="B950-"+face_lenght_b_obj.value+"LYT";
				}
				
				
		 		
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_b][0]+'" />';
		 		flangeCoversPrice+=parseFloat(product_name_price[light_b][1])
		 	}
		 	if(face_lenght_c_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="3rd Center Post from Left")||(cornerPosition=="2nd Center Post from Left"))){
					light_c="B950S-"+face_lenght_c_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					light_c="B950-"+face_lenght_c_obj.value+"LYT";	
					}
				}
				else{
					light_c="B950-"+face_lenght_c_obj.value+"LYT";
				}
				
				
		 		
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_c][0]+'" />';
		 		flangeCoversPrice+=parseFloat(product_name_price[light_c][1])
		 	}
		 	if(face_lenght_d_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="3rd Center Post from Left"))){
						light_d="B950S-"+face_lenght_d_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";
					}
					else{
					light_d="B950-"+face_lenght_d_obj.value+"LYT";	
					}
				}
				else{
					light_d="B950-"+face_lenght_d_obj.value+"LYT";
				}
				
				
		 		
		 		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_d][0]+'" />';
		 		flangeCoversPrice+=parseFloat(product_name_price[light_d][1]);
		 	}
                // flangeCoversPrice=parseFloat(product_name_price[light_a][1])+parseFloat(product_name_price[light_b][1])+parseFloat(product_name_price[light_c][1])+parseFloat(product_name_price[light_d][1]);
            }
           else if(type_obj.value=="3BAY"){
           		if(face_lenght_a_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left"))){
						light_a="B950S-"+face_lenght_a_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";
					}
					else{
						light_a="B950-"+face_lenght_a_obj.value+"LYT";
					}
				}
				else{
					light_a="B950-"+face_lenght_a_obj.value+"LYT";
				}
				
				
           			
           			str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_a][0]+'" />';
           			flangeCoversPrice+=parseFloat(product_name_price[light_a][1])
           		}
           		if(face_lenght_b_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left")||(cornerPosition=="2nd Center Post from Left"))){
					light_b="B950S-"+face_lenght_b_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
						light_b="B950-"+face_lenght_b_obj.value+"LYT";
					}
				}
				else{
					light_b="B950-"+face_lenght_b_obj.value+"LYT";
				}
				
				
           			
           			str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_b][0]+'" />';
           			flangeCoversPrice+=parseFloat(product_name_price[light_b][1])
           		}
           		if(face_lenght_c_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="2nd Center Post from Left"))){
						light_c="B950S-"+face_lenght_c_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";
					}
					else{
					light_c="B950-"+face_lenght_c_obj.value+"LYT";	
					}
				}
				else{
					light_c="B950-"+face_lenght_c_obj.value+"LYT";
				}
				
				
           			
           			str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_c][0]+'" />';
           			flangeCoversPrice+=parseFloat(product_name_price[light_c][1]);
           		}
            }
            else if(type_obj.value=="2BAY"){
            	if(face_lenght_a_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left"))){
					light_a="B950S-"+face_lenght_a_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";	
					}
					else{
					light_a="B950-"+face_lenght_a_obj.value+"LYT";	
					}
				}
				else{
					light_a="B950-"+face_lenght_a_obj.value+"LYT";
				}
				
				
            		
            		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_a][0]+'" />';
            		flangeCoversPrice+=parseFloat(product_name_price[light_a][1])
            	}
            	if(face_lenght_b_obj.value!="select"){
				
				if(gotocornerpostss=="1"){
					if(((form.posttype.value!="select")&&(cornerPosition=="1st Center Post from Left"))){
						light_b="B950S-"+face_lenght_b_obj.value+"LYT "+posttypegl+" Corner "+postdegreegl+"";
					}
					else{
						light_b="B950-"+face_lenght_b_obj.value+"LYT";
					}
				}
				else{
					light_b="B950-"+face_lenght_b_obj.value+"LYT";
				}
				
				
            		
            		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_b][0]+'" />';
            		flangeCoversPrice+=parseFloat(product_name_price[light_b][1])+0;
            	}
                
                
                light_c="";
                
                
                
            }
            else{
            	if(face_lenght_obj.value!="select"){
            		light_a="B950-"+face_lenght_obj.value+"LYT";
            		str+='<input type="hidden" name="products_id[]" value="'+product_name_price[light_a][0]+'" />';
                	flangeCoversPrice=parseFloat(product_name_price[light_a][1]);
            	}
                
                light_b="";
                light_c="";
                
            }
         }
         if(flange_covers_obj2.value!=0&&flange_covers_obj2.value!="select"){
            flageCovers2="Light Bracket";
            for(i=1;i<=flange_covers_obj2.value;i++){
                str+='<input type="hidden" name="products_id[]" value="'+product_name_price[flageCovers2][0]+'" />';
            }
            if(flange_covers_obj2.value!="select"){
            	flangeCoversPrice2=parseFloat(product_name_price[flageCovers2][1])*flange_covers_obj2.value;
            }
         }
         //images
         if(choose_finish_obj.value=="SS"){
            if(flange_covers_obj.value=="yes"){
                imageName=imageName;
            }
            else{
                imageName="NORAD"+imageName;
            }
         }
         else{
            if(flange_covers_obj.value=="yes"){
                imageName="BLACK"+imageName;
            }
            else{
                imageName="BLACKNORAD"+imageName;
            }
         }
         
         // Code End Here
        glassPrice=facePrice_a+facePrice_b+facePrice_c+facePrice_d;
        t_post_price=centerPostPrice+anglePostPrice;
        totalPrice=glassPrice+leftPostPrice+rightPostPrice+leftEndPanelPrice+rightEndPanelPrice+t_post_price+flangeCoversPrice+flangeCoversPrice2;
		
		
		
		var rad1=$("input[name='corner_post']:checked").val();
		//alert(rad1);
		
		
		
		
		
		//gotocornerpostcheck	 backtostraightpostcheck
		var gotocornerpost=$("input[name='gotocornerpostcheck']:checked").val();
		//alert(gotocornerpost);
			if(gotocornerpost=="1"){
				//form.flange_covers.value="no";
               // form.flange_covers.selected="No";
				//form.flange_covers_2.value="0";
                //form.flange_covers_2.selected="0";
				
			}
		
	
			
			if(type_obj.value=="2BAY"||type_obj.value=="3BAY"||type_obj.value=="4BAY"){
			
			if(rad1=="1st Center Post from Left"){//adding image of Post place!!
//              alert(imageName);
				if(gotocornerpost=="1"){
                imageName="1ST"+imageName;
				}
				else{
                imageName=""+imageName;

				}
            
            }
			
			if(rad1=="2nd Center Post from Left"){//adding image of Post place!!
//              alert(imageName);
                
				if(gotocornerpost=="1"){
                imageName="2ND"+imageName;
				}
				else{
                imageName=""+imageName;

				}
            
            }
			
			if(rad1=="3rd Center Post from Left"){//adding image of Post place!!
//              alert(imageName);
                
				if(gotocornerpost=="1"){
                imageName="3RD"+imageName;
				}
				else{
                imageName=""+imageName;

				}
            
            }
			
			
			if(form.degree.value=="90degre"){//adding image of Post degree!!
//              alert(imageName);
                
				if(gotocornerpost=="1"){
                imageName="90D"+imageName;
				}
				else{
                imageName=""+imageName;

				}
            
            }
			
			if(form.degree.value=="135degre"){//adding image of Post degree!!
//              alert(imageName);
                
				if(gotocornerpost=="1"){
                imageName="135D"+imageName;
				}
				else{
                imageName=""+imageName;

				}
            
            }
			
			if(form.posttype.value=="inner"){//adding image of Post Inside!!
//              alert(imageName);
                
				if(gotocornerpost=="1"){
                imageName="INNER"+imageName;
				}
				else{
                imageName=""+imageName;

				}
            
            }
			
			if(form.posttype.value=="outer"){//adding image of Post Outside!!
//              alert(imageName);
                
				if(gotocornerpost=="1"){
                imageName="OUTER"+imageName;
				}
				else{
                imageName=""+imageName;

				}
            
            }
			
			
	}
		
		
        img_ajx=imageName;
		
		
		<?php 
		if (!$detect->isMobile())
		{
		?>
		
        image_string='<img src="images/'+foldername+'/'+imageName+'.jpg" style="width:568px;height:453px;">'; 
		<?php 
		}
		else{
		?>
        image_string='<img src="images/'+foldername+'/'+imageName+'.jpg" style="width:828px;height:583px;">'; 

		<?php
		}
		?>
        image_string+='<div class="left">12"</div><div class="right">12"</div>';
		image_string+='<div class="msgtishu"></div>';
			image_string+='<div class="msgtishu1"></div>';
			image_string+='<div class="msgtishu2"><hr color="red" size="6px"   width="'+width_three+'"> </div>';
        

        image_string+='<div class="glass">12"</div><div class="glass_a">12"</div><div class="glass_b">12"</div><div class="glass_c">12"</div><div class="glass_d">12"</div><div class="total">38"</div>';
        
        document.getElementById('additional_image').innerHTML=image_string;
		
		
		//for shopping cart degree
		if(gotocornerpost=="1"){
			
		$('#post_type_val').val($('[name="posttype"]').find('option:selected').text());
		$('#post_degree_val').val($('[name="degree"]').find('option:selected').text());
		
		//$('#quotetext span').text('goes inside the span');
		//$('#quotetext span').html('&nbsp;&nbsp;4)');
		
		}
		
		if(type_obj.value=="2BAY"||type_obj.value=="3BAY"||type_obj.value=="4BAY"){
		
			if(form.posttype.value=="inner")
			{
			if(form.degree.value=="90degre"){
			
			var imgpsorname1="INNER90D1STPOST.jpg";
			var imgpsorname2="INNER90D2NDPOST.jpg";
			var imgpsorname3="INNER90D3RDPOST.jpg";
	
			}	
			if(form.degree.value=="135degre"){
			
			var imgpsorname1="INNER135D1STPOST.jpg";
			var imgpsorname2="INNER135D2NDPOST.jpg";
			var imgpsorname3="INNER135D3RDPOST.jpg";
	
			}
			}
			if(form.posttype.value=="outer")
			{
			if(form.degree.value=="90degre"){
			
			var imgpsorname1="OUTER90D1STPOST.jpg";
			var imgpsorname2="OUTER90D2NDPOST.jpg";
			var imgpsorname3="OUTER90D3RDPOST.jpg";
	
			}	
			if(form.degree.value=="135degre"){
			
			var imgpsorname1="OUTER135D1STPOST.jpg";
			var imgpsorname2="OUTER135D2NDPOST.jpg";
			var imgpsorname3="OUTER135D3RDPOST.jpg";
	
			}
			}
		if(type_obj.value=="4BAY"){
		document.getElementById("postimg1").src = "images/B950S/4BAY/"+imgpsorname1+"";
		document.getElementById("postimg2").src = "images/B950S/4BAY/"+imgpsorname2+"";
		document.getElementById("postimg3").src = "images/B950S/4BAY/"+imgpsorname3+"";
		}
		if(type_obj.value=="3BAY"){
		document.getElementById("postimg1").src = "images/B950S/3BAY/"+imgpsorname1+"";
		document.getElementById("postimg2").src = "images/B950S/3BAY/"+imgpsorname2+"";
		}
		if(type_obj.value=="2BAY"){
		document.getElementById("postimg1").src = "images/B950S/2BAY/"+imgpsorname1+"";
		}
		
	}
		
		
		
		
		
        
        if(glass_face_obj.value==1){
			if(left_lenght_obj.value!="select"){
				$("div.left").text(left_lenght_obj.options[left_lenght_obj.selectedIndex].text);
				$('#c_glass_left_val').val(left_lenght_obj.options[left_lenght_obj.selectedIndex].text);
				if(leftEndPanel!=''){
					// $('#c_glass_right').val(product_name_price[rightEndPanel][0]);
					$('#c_glass_left').val(product_name_price[leftEndPanel][0]);		
				}
			}else{
				$("div.left").text("Left");
			}
			if(right_lenght_obj.value!="select"){
				$("div.right").text(right_lenght_obj.options[right_lenght_obj.selectedIndex].text);
				$('#c_glass_right_val').val(right_lenght_obj.options[right_lenght_obj.selectedIndex].text);
				if(rightEndPanel!=''){
					$('#c_glass_right').val(product_name_price[rightEndPanel][0]);
					// $('#c_glass_left').val(product_name_price[leftEndPanel][0]);		
				}
			}else{
				$("div.right").text("Right");
			}
          	   
                
				<!-- ani code-->
				
				
				
				
				
        }
        else if(glass_face_obj.value==2){
        	if(left_lenght_obj.value!="select"){

			}
			if(right_lenght_obj.value!="select"){
				$("div.right").text(right_lenght_obj.options[right_lenght_obj.selectedIndex].text);
				if(rightEndPanel!=''){
					$('#c_glass_right').val(product_name_price[rightEndPanel][0]);
				}
				$('#c_glass_right_val').val(right_lenght_obj.options[right_lenght_obj.selectedIndex].text);
			}else{
				$("div.right").text("Right");
			}
           		 $("div.left").css("display","none");
                 
				 <!-- ani code -->
				 
				 
        }
        else if(glass_face_obj.value==3){
        	if(left_lenght_obj.value!="select"){
        		$("div.left").text(left_lenght_obj.options[left_lenght_obj.selectedIndex].text);
        		if(leftEndPanel!=''){
					$('#c_glass_left').val(product_name_price[leftEndPanel][0]);
				}
				$('#c_glass_left_val').val(left_lenght_obj.options[left_lenght_obj.selectedIndex].text);
			}else{
				$("div.left").text("Left");
			}
			if(right_lenght_obj.value!="select"){
				$("div.right").css("display","none");
			}
             $("div.right").css("display","none");
                
				 <!-- ani code -->
				 		
				
				 <!-- ani code -->
        }
        else if(glass_face_obj.value==4){
        	if(left_lenght_obj.value!="select"){

			}
			if(right_lenght_obj.value!="select"){
				
			}
            $("div.left").css("display","none");
            $("div.right").css("display","none");
        }
       
        if(type_obj.value=="1BAY"){
            <!-- ani code -->
			//for custom face set value to hidden fileds
			if(face_lenght_obj.value!="select"){
				if(flange_covers_obj.value=="yes"){
					$('#c_glass_a_light').val(product_name_price[light_a][0]);
	           		$('#c_glass_a_val_light').val(face_lenght_obj.options[face_lenght_obj.selectedIndex].text);
	           	}
	           	$('#c_glass_a_val').val($('[name="face_length"]').find('option:selected').text());
	           	if(glassName_a!=''){
					$('#c_glass_a').val(product_name_price[glassName_a][0]);
				}
				$("div.glass").text($('[name="face_length"]').find('option:selected').text());
			}else{
				$("div.glass").text("A");
			}
			
			
			
			<!-- ani code -->
            
			var n1=getBeforeChar($('[name="face_length"]').find('option:selected').text())-0;
			if(getAfterChar($('[name="face_length"]').find('option:selected').text())!=""){
			n1=(n1+2)+'-'+getAfterChar($('[name="face_length"]').find('option:selected').text())+'"';
			}else { n1=(n1+2)+'"';}
			if(n1=='2"'){
				$("div.total").text("Total");
			}else{
				$("div.total").text(n1);
                tot1=n1;
			}
             
			 if(face_lenght_obj.value == 'No Glass'){
				noGlass()
			}
        }
        if(type_obj.value=="2BAY"){
            <!-- ani code -->
			if(face_lenght_a_obj.value!="select"){
				$('#c_glass_a_val').val($('[name="face_length_a"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
		 			$('#c_glass_a_light').val(product_name_price[light_a][0]);
		           $('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
		           // $('#c_glass_b_light').val(product_name_price[light_b][0]);
		           // $('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
			    }
			    if(glassName_a!=''){
					$('#c_glass_a').val(product_name_price[glassName_a][0]);
					// $('#c_glass_b').val(product_name_price[glassName_b][0]);
				}
				$("div.glass_a").text($('[name="face_length_a"]').find('option:selected').text());
			}else{
				$("div.glass_a").text("A");
			}
			if(face_lenght_b_obj.value!="select"){
				$('#c_glass_b_val').val($('[name="face_length_b"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
		 			// $('#c_glass_a_light').val(product_name_price[light_a][0]);
		    //        $('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
		           $('#c_glass_b_light').val(product_name_price[light_b][0]);
		           $('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
				}
		           if(glassName_b!=''){
						// $('#c_glass_a').val(product_name_price[glassName_a][0]);
						$('#c_glass_b').val(product_name_price[glassName_b][0]);
					}
			   
			   $("div.glass_b").text($('[name="face_length_b"]').find('option:selected').text());
			}else{
				$("div.glass_b").text("B");
			}
		
				
				
				    
								
				
				
			
				
				
				var n1=getBeforeChar($('[name="face_length_a"]').find('option:selected').text())-0;
				var n2=getBeforeChar($('[name="face_length_b"]').find('option:selected').text())-0;
				var f_n1=getAfterChar($('[name="face_length_a"]').find('option:selected').text());
				var f_n2=getAfterChar($('[name="face_length_b"]').find('option:selected').text());
				var total= getTotal(n1,n2,f_n1,f_n2);
				if(total=='2"'){
					$("div.total").text("Total");
				}else{
					$("div.total").text(total);
                    tot1=total;	
				}
				
				<!-- ani code -->
				if(face_lenght_a_obj.value == 'No Glass' || face_lenght_b_obj.value == 'No Glass'){
					noGlass()
				}
        }
        if(type_obj.value=="3BAY"){
            <!-- ani code -->
			if(face_lenght_a_obj.value!="select"){
				$('#c_glass_a_val').val($('[name="face_length_a"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
					$('#c_glass_a_light').val(product_name_price[light_a][0]);
           			$('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
        //    			$('#c_glass_b_light').val(product_name_price[light_b][0]);
        //    			$('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
		   			// $('#c_glass_c_light').val(product_name_price[light_c][0]);
        //    			$('#c_glass_c_val_light').val(face_lenght_c_obj.options[face_lenght_c_obj.selectedIndex].text);
				}
				if(glassName_a!=''){
					$('#c_glass_a').val(product_name_price[glassName_a][0]);
					// $('#c_glass_b').val(product_name_price[glassName_b][0]);
					// $('#c_glass_c').val(product_name_price[glassName_c][0]);	
				}
				$("div.glass_a").text($('[name="face_length_a"]').find('option:selected').text());
			}else{
				$("div.glass_a").text("A");
			}
			if(face_lenght_b_obj.value!="select"){
				$('#c_glass_b_val').val($('[name="face_length_b"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
					// $('#c_glass_a_light').val(product_name_price[light_a][0]);
     //       			$('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
           			$('#c_glass_b_light').val(product_name_price[light_b][0]);
           			$('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
		   			// $('#c_glass_c_light').val(product_name_price[light_c][0]);
        //    			$('#c_glass_c_val_light').val(face_lenght_c_obj.options[face_lenght_c_obj.selectedIndex].text);
				}
				if(glassName_b!=''){
					// $('#c_glass_a').val(product_name_price[glassName_a][0]);
					$('#c_glass_b').val(product_name_price[glassName_b][0]);
					// $('#c_glass_c').val(product_name_price[glassName_c][0]);	
				}
				$("div.glass_b").text($('[name="face_length_b"]').find('option:selected').text());
			}else{
				$("div.glass_b").text("B");
			}
			if(face_lenght_c_obj.value!="select"){
				$('#c_glass_c_val').val($('[name="face_length_c"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
					// $('#c_glass_a_light').val(product_name_price[light_a][0]);
     //       			$('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
     //       			$('#c_glass_b_light').val(product_name_price[light_b][0]);
     //       			$('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
		   			$('#c_glass_c_light').val(product_name_price[light_c][0]);
           			$('#c_glass_c_val_light').val(face_lenght_c_obj.options[face_lenght_c_obj.selectedIndex].text);
				}
				if(glassName_c!=''){
					// $('#c_glass_a').val(product_name_price[glassName_a][0]);
					// $('#c_glass_b').val(product_name_price[glassName_b][0]);
					$('#c_glass_c').val(product_name_price[glassName_c][0]);	
				}
				$("div.glass_c").text($('[name="face_length_c"]').find('option:selected').text());
			}else{
				$("div.glass_c").text("C");
			}
			
			
			
			 
			
			
			
            
            
            
			var n1=getBeforeChar($('[name="face_length_a"]').find('option:selected').text())-0;
			var n2=getBeforeChar($('[name="face_length_b"]').find('option:selected').text())-0;
			var n3=getBeforeChar($('[name="face_length_c"]').find('option:selected').text())-0;
			var f_n1=getAfterChar($('[name="face_length_a"]').find('option:selected').text());
			var f_n2=getAfterChar($('[name="face_length_b"]').find('option:selected').text());
			var f_n3=getAfterChar($('[name="face_length_c"]').find('option:selected').text());	
			//this function not working properly		
			var total=getTotal3Bay(n1,n2,n3,f_n1,f_n2,f_n3);
			if(total=='2"'){
				$("div.total").text("Total");
			}else{
				$("div.total").text(total);
                tot1=total;
			}
            
        }if(type_obj.value=="4BAY"){
            <!-- ani code -->
			
			if(face_lenght_a_obj.value!="select"){
				$('#c_glass_a_val').val($('[name="face_length_a"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
					$('#c_glass_a_light').val(product_name_price[light_a][0]);
           			$('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
		      //      	$('#c_glass_b_light').val(product_name_price[light_b][0]);
		      //      	$('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
				   	// $('#c_glass_c_light').val(product_name_price[light_c][0]);
		      //      	$('#c_glass_c_val_light').val(face_lenght_c_obj.options[face_lenght_c_obj.selectedIndex].text);
				   	// $('#c_glass_d_light').val(product_name_price[light_d][0]);
		      //      	$('#c_glass_d_val_light').val(face_lenght_d_obj.options[face_lenght_d_obj.selectedIndex].text);
				}
				if(glassName_a!=''){
					$('#c_glass_a').val(product_name_price[glassName_a][0]);
					// $('#c_glass_b').val(product_name_price[glassName_b][0]);
					// $('#c_glass_c').val(product_name_price[glassName_c][0]);
		   //          $('#c_glass_d').val(product_name_price[glassName_d][0]);			
				}
				$("div.glass_a").text($('[name="face_length_a"]').find('option:selected').text());
			}else{
				$("div.glass_a").text("A");
			}
			if(face_lenght_b_obj.value!="select"){
				$('#c_glass_b_val').val($('[name="face_length_b"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
					// $('#c_glass_a_light').val(product_name_price[light_a][0]);
     //       			$('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
           			$('#c_glass_b_light').val(product_name_price[light_b][0]);
           			$('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
		   			// $('#c_glass_c_light').val(product_name_price[light_c][0]);
        //    			$('#c_glass_c_val_light').val(face_lenght_c_obj.options[face_lenght_c_obj.selectedIndex].text);
		   			// $('#c_glass_d_light').val(product_name_price[light_d][0]);
        //    			$('#c_glass_d_val_light').val(face_lenght_d_obj.options[face_lenght_d_obj.selectedIndex].text);
				}
				if(glassName_b!=''){
					// $('#c_glass_a').val(product_name_price[glassName_a][0]);
					$('#c_glass_b').val(product_name_price[glassName_b][0]);
					// $('#c_glass_c').val(product_name_price[glassName_c][0]);
		   //          $('#c_glass_d').val(product_name_price[glassName_d][0]);			
				}
				$("div.glass_b").text($('[name="face_length_b"]').find('option:selected').text());
			}else{
				$("div.glass_b").text("B");
			}
			if(face_lenght_c_obj.value!="select"){
				$('#c_glass_c_val').val($('[name="face_length_c"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
					// $('#c_glass_a_light').val(product_name_price[light_a][0]);
     //       			$('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
     //       			$('#c_glass_b_light').val(product_name_price[light_b][0]);
     //       			$('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
		   			$('#c_glass_c_light').val(product_name_price[light_c][0]);
           			$('#c_glass_c_val_light').val(face_lenght_c_obj.options[face_lenght_c_obj.selectedIndex].text);
		   			// $('#c_glass_d_light').val(product_name_price[light_d][0]);
        //    			$('#c_glass_d_val_light').val(face_lenght_d_obj.options[face_lenght_d_obj.selectedIndex].text);
				}
				if(glassName_c!=''){
					// $('#c_glass_a').val(product_name_price[glassName_a][0]);
					// $('#c_glass_b').val(product_name_price[glassName_b][0]);
					$('#c_glass_c').val(product_name_price[glassName_c][0]);
		            // $('#c_glass_d').val(product_name_price[glassName_d][0]);			
				}
				$("div.glass_c").text($('[name="face_length_c"]').find('option:selected').text());
			}else{
				$("div.glass_c").text("C");
			}
			if(face_lenght_d_obj.value!="select"){
				$('#c_glass_d_val').val($('[name="face_length_d"]').find('option:selected').text());
				if(flange_covers_obj.value=="yes"){
					// $('#c_glass_a_light').val(product_name_price[light_a][0]);
     //       			$('#c_glass_a_val_light').val(face_lenght_a_obj.options[face_lenght_a_obj.selectedIndex].text);
     //       			$('#c_glass_b_light').val(product_name_price[light_b][0]);
     //       			$('#c_glass_b_val_light').val(face_lenght_b_obj.options[face_lenght_b_obj.selectedIndex].text);
		   // 			$('#c_glass_c_light').val(product_name_price[light_c][0]);
     //       			$('#c_glass_c_val_light').val(face_lenght_c_obj.options[face_lenght_c_obj.selectedIndex].text);
		   			$('#c_glass_d_light').val(product_name_price[light_d][0]);
           			$('#c_glass_d_val_light').val(face_lenght_d_obj.options[face_lenght_d_obj.selectedIndex].text);
				}
				if(glassName_d!=''){
					// $('#c_glass_a').val(product_name_price[glassName_a][0]);
					// $('#c_glass_b').val(product_name_price[glassName_b][0]);
					// $('#c_glass_c').val(product_name_price[glassName_c][0]);
		            $('#c_glass_d').val(product_name_price[glassName_d][0]);			
				}
				$("div.glass_d").text($('[name="face_length_d"]').find('option:selected').text());
			}else{
				$("div.glass_d").text("D");
			}
			var n1=getBeforeChar($('[name="face_length_a"]').find('option:selected').text())-0;
			var n2=getBeforeChar($('[name="face_length_b"]').find('option:selected').text())-0;
			var n3=getBeforeChar($('[name="face_length_c"]').find('option:selected').text())-0;
			var n4=getBeforeChar($('[name="face_length_d"]').find('option:selected').text())-0;
			var f_n1=getAfterChar($('[name="face_length_a"]').find('option:selected').text());
			var f_n2=getAfterChar($('[name="face_length_b"]').find('option:selected').text());
			var f_n3=getAfterChar($('[name="face_length_c"]').find('option:selected').text());	
			var f_n4=getAfterChar($('[name="face_length_d"]').find('option:selected').text());	
			//this function not working properly		
			var total=getTotal4Bay(n1,n2,n3,n4,f_n1,f_n2,f_n3,f_n4);
			if(total=='2"'){
				$("div.total").text("Total");
			}else{
				$("div.total").text(total);
                tot1=total;
			}
			

            
        }
         <?php if($detect->isMobile()||$detect->isTablet()){?>
         if(type_obj.value=="1BAY"){
                 h=286+27;
                h1=220+27;
                h2=189+7-11;
                h3=204+27;
                h8=191+27-5;
                t8=251+5;
                t1=156;
                t2=222;
                t3=253+31;
                t4=239;
				right_next=-40;
				redlineheight=97;
		redlineheight1=180;
		redlineheight2=177;
		redlineheight3=206;
		redlineheight4=402;
		redlineverticle=308;
            }
            else if(type_obj.value=="2BAY"){
                 h=286+27;
                h1=196+27;
                h2=163+7-11;
                h3=175+27;
                h8=167+27-5;
                t8=275+5;
                t1=156;
                t2=246;
                t3=279+31;
                t4=267;
				right_next=-40;
				redlineheight=97;
		redlineheight1=204;
		redlineheight2=205;
		redlineheight3=230;
		redlineheight4=427;
		redlineverticle=333;
            }
            else if(type_obj.value=="3BAY"){
                 h=286+27;
                h1=168+27;
                h2=135+7-11;
                h3=150+27;
                h8=143+27-5;
                t8=299+5;
                t1=156;
                t2=274;
                t3=307+31;
                t4=291;
				right_next=-40;
				redlineheight=97;
		redlineheight1=228;
		redlineheight2=225;
		redlineheight3=255;
		redlineheight4=450;
		redlineverticle=356;
            }else if(type_obj.value=="4BAY"){
                 h=286+27;
                h1=168+27;
                h2=135+7-11;
                h3=150+27;
                h8=143+27-5;
                t8=299+5;
                t1=156;
                t2=274;
                t3=307+31;
                t4=291;
				right_next=-40;
				redlineheight=97;
		redlineheight1=252;
		redlineheight2=255;
		redlineheight3=280;
		redlineheight4=475;
		redlineverticle=381;
            }
         <?php } else {?> 
		  if(category_name=="B950"){
        if(type_obj.value=="1BAY"){
                 h=290+27;
                h1=224+27;
                h2=162+27;
                h3=207+27;
                h8=191+27;
                t8=251;
                t1=152;
                t2=218;
                t3=280;
                t4=235;
				redlineheight=97;
		redlineheight1=180;
		redlineheight2=177;
		redlineheight3=206;
		redlineheight4=402+redlinebrowser1;
		redlineverticle=308+redlinebrowser;
            }
            else if(type_obj.value=="2BAY"){
                 h=290+27;
                h1=200+27;
                h2=138+27;
                h3=183+27;
                h8=167+27;
                t8=275;
                t1=152;
                t2=242;
                t3=304;
                t4=259;
				redlineheight=97;
		redlineheight1=204;
		redlineheight2=205;
		redlineheight3=230;
		redlineheight4=427+redlinebrowser1;
		redlineverticle=333+redlinebrowser;
            }
            else if(type_obj.value=="3BAY"){
                 h=290+27;
                h1=172+27;
                h2=114+27;
                h3=159+27;
                h8=143+27;
                t8=299;
                t1=152;
                t2=270;
                t3=328;
                t4=283;
				lh1=452;
				lh2=513;
				lh=248;
				redlineheight=97;
		redlineheight1=228;
		redlineheight2=225;
		redlineheight3=255;
		redlineheight4=450+redlinebrowser1;
		redlineverticle=356+redlinebrowser;
            }else if(type_obj.value=="4BAY"){
                 h=290+27;
                h1=172+27;
                h2=114+27;
                h3=159+27;
                h8=143+27;
                t8=299;
                t1=152;
                t2=270;
                t3=328;
                t4=283;
				redlineheight=97;
		redlineheight1=252;
		redlineheight2=255;
		redlineheight3=280;
		redlineheight4=475+redlinebrowser1;
		redlineverticle=381+redlinebrowser;
            }
            } else{
			
			
			
			   if(type_obj.value=="1BAY"){
                 h=290+27;
                h1=224+27;
                h2=162+27;
                h3=207+27;
                h8=191+27;
                t8=251;
                t1=152;
                t2=218;
                t3=280;
                t4=235;
				redlineheight=97;
		redlineheight1=180;
		redlineheight2=212;
		redlineheight3=242;
		redlineheight4=454+redlinebrowser1;
		redlineverticle=360+redlinebrowser;
            }
            else if(type_obj.value=="2BAY"){
                 h=290+27;
                h1=200+27;
                h2=138+27;
                h3=183+27;
                h8=167+27;
                t8=275;
                t1=152;
                t2=242;
                t3=304;
                t4=259;
				redlineheight=97;
		redlineheight1=204;
		redlineheight2=240;
		redlineheight3=264;
		redlineheight4=478+redlinebrowser1;
		redlineverticle=384+redlinebrowser;
            }
            else if(type_obj.value=="3BAY"){
                 h=290+27;
                h1=172+27;
                h2=114+27;
                h3=159+27;
                h8=143+27;
                t8=299;
                t1=152;
                t2=270;
                t3=328;
                t4=283;
				redlineheight=97;
		redlineheight1=228;
		redlineheight2=262;
		redlineheight3=290;
		redlineheight4=502+redlinebrowser1;
		redlineverticle=408+redlinebrowser;
            }else if(type_obj.value=="4BAY"){
                 h=290+27;
                h1=172+27;
                h2=114+27;
                h3=159+27;
                h8=143+27;
                t8=299;
                t1=152;
                t2=270;
                t3=328;
                t4=283;
				redlineheight=97;
		redlineheight1=252;
		redlineheight2=286;
		redlineheight3=312;
		redlineheight4=528+redlinebrowser1;
		redlineverticle=434+redlinebrowser;
            }
			
			
			
			
			
			}
        <?php }?>
        <?php if($category_name=="B950"){?>
                  t3= t3-31;
                  h2=h2+30
            <?}?>
			
			
			
		
		
		<?php 
		if (!$detect->isMobile())
		{
		?>
		
		
		
		
		
		
		//form.posttype.value
		//form.degree.value
		var cornerPosition=$("input[name='corner_post']:checked").val();
					//alert(cornerPosition);
					
		var gotocornerpostss=$("input[name='gotocornerpostcheck']:checked").val();
		

		if(type_obj.value=="2BAY"){
			
		if(gotocornerpostss=='1')
		{
		if(form.posttype.value=="inner")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			
			$('.left').css("top","152px");
            $('.left').css("left","82px");
			
			$('.right').css("top","141px");
            $('.right').css("left","467px");
			
			$('.glass_a').css("top","265px");
            $('.glass_a').css("left","226px");
			
			$('.glass_b').css("top","272px");
            $('.glass_b').css("left","326px");

			
			$('.total').css("display","none");
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
				
			$('.left').css("top","107px");
            $('.left').css("left","36px");
			
			$('.right').css("top","92px");
            $('.right').css("left","491px");
			
			
			$('.glass_a').css("top","257px");
            $('.glass_a').css("left","207px");
			
			$('.glass_b').css("top","259px");
            $('.glass_b').css("left","375px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		else if(form.posttype.value=="outer")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","68px");
            $('.left').css("left","109px");
			
			$('.right').css("top","54px");
            $('.right').css("left","420px");
			
			
			$('.glass_a').css("top","327px");
            $('.glass_a').css("left","156px");
			
			$('.glass_b').css("top","307px");
            $('.glass_b').css("left","453px");
			
			$('.total').css("display","none");
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			$('.left').css("top","106px");
            $('.left').css("left","58px");
			
			$('.right').css("top","76px");
            $('.right').css("left","496px");
			
			
			$('.glass_a').css("top","330px");
            $('.glass_a').css("left","154px");
			
			$('.glass_b').css("top","303px");
            $('.glass_b').css("left","444px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		}
			
			
		}
		if(type_obj.value=="3BAY"){
			
			
		if(gotocornerpostss=='1')
		{
		if(form.posttype.value=="inner")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","136px");
            $('.left').css("left","57px");
			
			$('.right').css("top","206px");
            $('.right').css("left","482px");
			
			
			$('.glass_a').css("top","226px");
            $('.glass_a').css("left","185px");
			
			$('.glass_b').css("top","226px");
            $('.glass_b').css("left","267px");
			
			$('.glass_c').css("top","301px");
            $('.glass_c').css("left","372px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
				
			$('.left').css("top","200px");
            $('.left').css("left","53px");
			
			$('.right').css("top","111px");
            $('.right').css("left","500px");
			
			
			$('.glass_a').css("top","299px");
            $('.glass_a').css("left","198px");
			
			$('.glass_b').css("top","223px");
            $('.glass_b').css("left","312px");
			
			$('.glass_c').css("top","216px");
            $('.glass_c').css("left","399px");
			
			$('.total').css("display","none");	
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","196px");
            $('.left').css("left","33px");
			
			$('.right').css("top","111px");
            $('.right').css("left","496px");
			
			
			$('.glass_a').css("top","304px");
            $('.glass_a').css("left","165px");
			
			$('.glass_b').css("top","271px");
            $('.glass_b').css("left","289px");
			
			$('.glass_c').css("top","261px");
            $('.glass_c').css("left","446px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
				
			$('.left').css("top","215px");
            $('.left').css("left","53px");
			
			$('.right').css("top","73px");
            $('.right').css("left","481px");
			
			
			$('.glass_a').css("top","323px");
            $('.glass_a').css("left","196px");
			
			$('.glass_b').css("top","255px");
            $('.glass_b').css("left","301px");
			
			$('.glass_c').css("top","225px");
            $('.glass_c').css("left","417px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		else if(form.posttype.value=="outer")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
				
			$('.left').css("top","98px");
            $('.left').css("left","106px");
			
			$('.right').css("top","45px");
            $('.right').css("left","460px");
			
			
			$('.glass_a').css("top","313px");
            $('.glass_a').css("left","124px");
			
			$('.glass_b').css("top","303px");
            $('.glass_b').css("left","378px");
			
			$('.glass_c').css("top","220px");
            $('.glass_c').css("left","488px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			$('.left').css("top","65px");
            $('.left').css("left","85px");
			
			$('.right').css("top","98px");
            $('.right').css("left","470px");
			
			
			$('.glass_a').css("top","254px");
            $('.glass_a').css("left","97px");
			
			$('.glass_b').css("top","324px");
            $('.glass_b').css("left","238px");
			
			$('.glass_c').css("top","317px");
            $('.glass_c').css("left","483px");
			
			$('.total').css("display","none");
			
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
				
			$('.left').css("top","138px");
            $('.left').css("left","63px");
			
			$('.right').css("top","126px");
            $('.right').css("left","512px");
			
			
			$('.glass_a').css("top","306px");
            $('.glass_a').css("left","102px");
			
			$('.glass_b').css("top","308px");
            $('.glass_b').css("left","328px");
			
			$('.glass_c').css("top","271px");
            $('.glass_c').css("left","481px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			$('.left').css("top","126px");
            $('.left').css("left","62px");
			
			$('.right').css("top","154px");
            $('.right').css("left","505px");
			
			
			$('.glass_a').css("top","273px");
            $('.glass_a').css("left","93px");
			
			$('.glass_b').css("top","315px");
            $('.glass_b').css("left","231px");
			
			$('.glass_c').css("top","314px");
            $('.glass_c').css("left","459px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		}
			
			
		}
		if(type_obj.value=="4BAY"){
			
		if(gotocornerpostss=='1')
		{
		if(form.posttype.value=="inner")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","107px");
            $('.left').css("left","31px");
			
			$('.right').css("top","218px");
            $('.right').css("left","497px");
			
			
			$('.glass_a').css("top","180px");
            $('.glass_a').css("left","149px");
			
			$('.glass_b').css("top","179px");
            $('.glass_b').css("left","215px");
			
			$('.glass_c').css("top","236px");
            $('.glass_c').css("left","291px");
			
			$('.glass_d').css("top","302px");
            $('.glass_d').css("left","386px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
				
			$('.left').css("top","152px");
            $('.left').css("left","30px");
			
			$('.right').css("top","151px");
            $('.right').css("left","509px");
			
			
			$('.glass_a').css("top","236px");
            $('.glass_a').css("left","156px");
			
			$('.glass_b').css("top","183px");
            $('.glass_b').css("left","263px");
			
			$('.glass_c').css("top","185px");
            $('.glass_c').css("left","326px");
			
			$('.glass_d').css("top","238px");
            $('.glass_d').css("left","405px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
			
			$('.left').css("top","225px");
            $('.left').css("left","32px");
			
			$('.right').css("top","122px");
            $('.right').css("left","523px");
			
			
			$('.glass_a').css("top","315px");
            $('.glass_a').css("left","168px");
			
			$('.glass_b').css("top","253px");
            $('.glass_b').css("left","287px");
			
			$('.glass_c').css("top","204px");
            $('.glass_c').css("left","382px");
			
			$('.glass_d').css("top","207px");
            $('.glass_d').css("left","445px");
			
			$('.total').css("display","none");
		}
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","193px");
            $('.left').css("left","31px");
			
			$('.right').css("top","110px");
            $('.right').css("left","510px");
			
			
			$('.glass_a').css("top","274px");
            $('.glass_a').css("left","139px");
			
			$('.glass_b').css("top","240px");
            $('.glass_b').css("left","235px");
			
			$('.glass_c').css("top","233px");
            $('.glass_c').css("left","352px");
			
			$('.glass_d').css("top","223px");
            $('.glass_d').css("left","473px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			$('.left').css("top","202px");
            $('.left').css("left","45px");
			
			$('.right').css("top","66px");
            $('.right').css("left","490px");
			
			
			$('.glass_a').css("top","280px");
            $('.glass_a').css("left","164px");
			
			$('.glass_b').css("top","220px");
            $('.glass_b').css("left","242px");
			
			$('.glass_c').css("top","197px");
            $('.glass_c').css("left","325px");
			
			$('.glass_d').css("top","187px");
            $('.glass_d').css("left","445px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
				
			$('.left').css("top","240px");
            $('.left').css("left","35px");
			
			$('.right').css("top","47px");
            $('.right').css("left","503px");
			
			
			$('.glass_a').css("top","338px");
            $('.glass_a').css("left","164px");
			
			$('.glass_b').css("top","268px");
            $('.glass_b').css("left","262px");
			
			$('.glass_c').css("top","206px");
            $('.glass_c').css("left","349px");
			
			$('.glass_d').css("top","175px");
            $('.glass_d').css("left","453px");
			
			$('.total').css("display","none");
		}
		}
			
		}
		else if(form.posttype.value=="outer")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","143px");
            $('.left').css("left","98px");
			
			$('.right').css("top","53px");
            $('.right').css("left","487px");
			
			
			$('.glass_a').css("top","333px");
            $('.glass_a').css("left","109px");
			
			$('.glass_b').css("top","323px");
            $('.glass_b').css("left","334px");
			
			$('.glass_c').css("top","251px");
            $('.glass_c').css("left","439px");
			
			$('.glass_d').css("top","194px");
            $('.glass_d').css("left","510px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			$('.left').css("top","99px");
            $('.left').css("left","82px");
			
			$('.right').css("top","88px");
            $('.right').css("left","491px");
			
			
			$('.glass_a').css("top","266px");
            $('.glass_a').css("left","98px");
			
			$('.glass_b').css("top","330px");
            $('.glass_b').css("left","213px");
			
			$('.glass_c').css("top","320px");
            $('.glass_c').css("left","433px");
			
			$('.glass_d').css("top","247px");
            $('.glass_d').css("left","521px");
			
			$('.total').css("display","none");
			
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
			
			$('.left').css("top","80px");
            $('.left').css("left","66px");
			
			$('.right').css("top","149px");
            $('.right').css("left","491px");
			
			
			$('.glass_a').css("top","233px");
            $('.glass_a').css("left","73px");
			
			$('.glass_b').css("top","285px");
            $('.glass_b').css("left","180px");
			
			$('.glass_c').css("top","350px");
            $('.glass_c').css("left","304px");
			
			$('.glass_d').css("top","337px");
            $('.glass_d').css("left","515px");
			
			$('.total').css("display","none");
		}
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","150px");
            $('.left').css("left","43px");
			
			$('.right').css("top","126px");
            $('.right').css("left","538px");
			
			
			$('.glass_a').css("top","288px");
            $('.glass_a').css("left","85px");
			
			$('.glass_b').css("top","290px");
            $('.glass_b').css("left","269px");
			
			$('.glass_c').css("top","260px");
            $('.glass_c').css("left","410px");
			
			$('.glass_d').css("top","241px");
            $('.glass_d').css("left","507px");
			
			$('.total').css("display","none");	
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			$('.left').css("top","137px");
            $('.left').css("left","46px");
			
			$('.right').css("top","149px");
            $('.right').css("left","515px");
			
			
			$('.glass_a').css("top","265px");
            $('.glass_a').css("left","65px");
			
			$('.glass_b').css("top","299px");
            $('.glass_b').css("left","182px");
			
			$('.glass_c').css("top","301px");
            $('.glass_c').css("left","367px");
			
			$('.glass_d').css("top","270px");
            $('.glass_d').css("left","496px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
			$('.left').css("top","124px");
            $('.left').css("left","47px");
			
			$('.right').css("top","168px");
            $('.right').css("left","528px");
			
			
			$('.glass_a').css("top","241px");
            $('.glass_a').css("left","73px");
			
			$('.glass_b').css("top","270px");
            $('.glass_b').css("left","172px");
			
			$('.glass_c').css("top","305px");
            $('.glass_c').css("left","303px");
			
			$('.glass_d').css("top","312px");
            $('.glass_d').css("left","486px");
			
			$('.total').css("display","none");
		}
		}
			
		}
		}
			
			
		}
		
		
		<?php
		}
		else
		{
		?>
		
		
		
		
		
		
		
		//form.posttype.value
		//form.degree.value
		var cornerPosition=$("input[name='corner_post']:checked").val();
					//alert(cornerPosition);
					
		var gotocornerpostss=$("input[name='gotocornerpostcheck']:checked").val();
		

		if(type_obj.value=="2BAY"){
			
		if(gotocornerpostss=='1')
		{
		if(form.posttype.value=="inner")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","202px");
            $('.left').css("left","114px");
			
			$('.right').css("top","186px");
            $('.right').css("left","670px");
			
			
			$('.glass_a').css("top","337px");
            $('.glass_a').css("left","340px");
			
			$('.glass_b').css("top","350px");
            $('.glass_b').css("left","486px");
			
			$('.total').css("display","none");
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
				
			$('.left').css("top","144px");
            $('.left').css("left","56px");
			
			$('.right').css("top","119px");
            $('.right').css("left","719px");
			
			
			$('.glass_a').css("top","339px");
            $('.glass_a').css("left","293px");
			
			$('.glass_b').css("top","327px");
            $('.glass_b').css("left","531px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		else if(form.posttype.value=="outer")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","92px");
            $('.left').css("left","167px");
			
			$('.right').css("top","71px");
            $('.right').css("left","612px");
			
			
			$('.glass_a').css("top","425px");
            $('.glass_a').css("left","228px");
			
			$('.glass_b').css("top","400px");
            $('.glass_b').css("left","661px");
			
			$('.total').css("display","none");
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			$('.left').css("top","138px");
            $('.left').css("left","82px");
			
			$('.right').css("top","95px");
            $('.right').css("left","721px");
			
			
			$('.glass_a').css("top","435px");
            $('.glass_a').css("left","256px");
			
			$('.glass_b').css("top","397px");
            $('.glass_b').css("left","659px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		}
			
			
		}
		if(type_obj.value=="3BAY"){
			
			
		if(gotocornerpostss=='1')
		{
		if(form.posttype.value=="inner")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			$('.left').css("top","180px");
            $('.left').css("left","82px");
			
			$('.right').css("top","269px");
            $('.right').css("left","705px");
			
			
			$('.glass_a').css("top","286px");
            $('.glass_a').css("left","268px");
			
			$('.glass_b').css("top","291px");
            $('.glass_b').css("left","380px");
			
			$('.glass_c').css("top","388px");
            $('.glass_c').css("left","534px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
				
			$('.left').css("top","260px");
            $('.left').css("left","82px");
			
			$('.right').css("top","141px");
            $('.right').css("left","726px");
			
			
			$('.glass_a').css("top","378px");
            $('.glass_a').css("left","301px");
			
			$('.glass_b').css("top","280px");
            $('.glass_b').css("left","465px");
			
			$('.glass_c').css("top","282px");
            $('.glass_c').css("left","575px");
			
			$('.total').css("display","none");	
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			
			$('.left').css("top","248px");
            $('.left').css("left","49px");
			
			$('.right').css("top","154px");
            $('.right').css("left","735px");
			
			$('.glass_a').css("top","386px");
            $('.glass_a').css("left","245px");
			
			$('.glass_b').css("top","350px");
            $('.glass_b').css("left","424px");
			
			$('.glass_c').css("top","337px");
            $('.glass_c').css("left","651px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
				
			
			$('.left').css("top","278px");
            $('.left').css("left","82px");
			
			$('.right').css("top","100px");
            $('.right').css("left","708px");
			
			$('.glass_a').css("top","423px");
            $('.glass_a').css("left","282px");
			
			$('.glass_b').css("top","325px");
            $('.glass_b').css("left","433px");
			
			$('.glass_c').css("top","295px");
            $('.glass_c').css("left","612px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		else if(form.posttype.value=="outer")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
				
			
			$('.left').css("top","128px");
            $('.left').css("left","160px");
			
			$('.right').css("top","58px");
            $('.right').css("left","668px");
			
			$('.glass_a').css("top","408px");
            $('.glass_a').css("left","183px");
			
			$('.glass_b').css("top","397px");
            $('.glass_b').css("left","545px");
			
			$('.glass_c').css("top","285px");
            $('.glass_c').css("left","713px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			
			$('.left').css("top","88px");
            $('.left').css("left","126px");
			
			$('.right').css("top","122px");
            $('.right').css("left","686px");
			
			$('.glass_a').css("top","328px");
            $('.glass_a').css("left","159px");
			
			$('.glass_b').css("top","421px");
            $('.glass_b').css("left","358px");
			
			$('.glass_c').css("top","411px");
            $('.glass_c').css("left","704px");
			
			$('.total').css("display","none");
			
		}
		
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
				
			
			$('.left').css("top","178px");
            $('.left').css("left","82px");
			
			$('.right').css("top","168px");
            $('.right').css("left","750px");
			
			$('.glass_a').css("top","402px");
            $('.glass_a').css("left","158px");
			
			$('.glass_b').css("top","398px");
            $('.glass_b').css("left","482px");
			
			$('.glass_c').css("top","349px");
            $('.glass_c').css("left","703px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			$('.left').css("top","166px");
            $('.left').css("left","82px");
			
			$('.right').css("top","200px");
            $('.right').css("left","736px");
			
			$('.glass_a').css("top","358px");
            $('.glass_a').css("left","128px");
			
			$('.glass_b').css("top","412px");
            $('.glass_b').css("left","335px");
			
			$('.glass_c').css("top","415px");
            $('.glass_c').css("left","663px");
			
			$('.total').css("display","none");
		}
		
		}
			
		}
		}
			
			
		}
		if(type_obj.value=="4BAY"){
			
		if(gotocornerpostss=='1')
		{
		if(form.posttype.value=="inner")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			
			$('.left').css("top","137px");
            $('.left').css("left","50px");
			
			$('.right').css("top","281px");
            $('.right').css("left","738px");
			
			$('.glass_a').css("top","231px");
            $('.glass_a').css("left","206px");
			
			$('.glass_b').css("top","236px");
            $('.glass_b').css("left","309px");
			
			$('.glass_c').css("top","300px");
            $('.glass_c').css("left","427px");
			
			$('.glass_d').css("top","391px ");
            $('.glass_d').css("left","574px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
				
			
			$('.left').css("top","196px");
            $('.left').css("left","44px");
			
			$('.right').css("top","193px");
            $('.right').css("left","749px");
			
			$('.glass_a').css("top","310px");
            $('.glass_a').css("left","225px");
			
			$('.glass_b').css("top","236px");
            $('.glass_b').css("left","379px");
			
			$('.glass_c').css("top","238px");
            $('.glass_c').css("left","472px");
			
			$('.glass_d').css("top","315px");
            $('.glass_d').css("left","592px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
			
			
			$('.left').css("top","285px");
            $('.left').css("left","54px");
			
			$('.right').css("top","156px");
            $('.right').css("left","773px");
			
			$('.glass_a').css("top","411px");
            $('.glass_a').css("left","236px");
			
			$('.glass_b').css("top","331px");
            $('.glass_b').css("left","427px");
			
			$('.glass_c').css("top","267px");
            $('.glass_c').css("left","556px");
			
			$('.glass_d').css("top","273px");
            $('.glass_d').css("left","656px");
			
			$('.total').css("display","none");
		}
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			
			$('.left').css("top","250px");
            $('.left').css("left","44px");
			
			$('.right').css("top","150px");
            $('.right').css("left","755px");
			
			$('.glass_a').css("top","361px");
            $('.glass_a').css("left","200px");
			
			$('.glass_b').css("top","311px");
            $('.glass_b').css("left","333px");
			
			$('.glass_c').css("top","304px");
            $('.glass_c').css("left","514px");
			
			$('.glass_d').css("top","290px");
            $('.glass_d').css("left","677px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			
			$('.left').css("top","258px");
            $('.left').css("left","69px");
			
			$('.right').css("top","90px");
            $('.right').css("left","722px");
			
			$('.glass_a').css("top","373px");
            $('.glass_a').css("left","225px");
			
			$('.glass_b').css("top","291px");
            $('.glass_b').css("left","345px");
			
			$('.glass_c').css("top","254px");
            $('.glass_c').css("left","493px");
			
			$('.glass_d').css("top","239px");
            $('.glass_d').css("left","650px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
				
			
			$('.left').css("top","311px");
            $('.left').css("left","56px");
			
			$('.right').css("top","72px");
            $('.right').css("left","750px");
			
			$('.glass_a').css("top","438px");
            $('.glass_a').css("left","231px");
			
			$('.glass_b').css("top","351px");
            $('.glass_b').css("left","379px");
			
			$('.glass_c').css("top","269px");
            $('.glass_c').css("left","501px");
			
			$('.glass_d').css("top","229px");
            $('.glass_d').css("left","662px");
			
			$('.total').css("display","none");
		}
		}
			
		}
		else if(form.posttype.value=="outer")
		{
		if(form.degree.value=="90degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			
			$('.left').css("top","188px");
            $('.left').css("left","143px");
			
			$('.right').css("top","71px");
            $('.right').css("left","708px");
			
			$('.glass_a').css("top","432px");
            $('.glass_a').css("left","166px");
			
			$('.glass_b').css("top","422px");
            $('.glass_b').css("left","489px");
			
			$('.glass_c').css("top","327px");
            $('.glass_c').css("left","636px");
			
			$('.glass_d').css("top","253px");
            $('.glass_d').css("left","742px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			
			$('.left').css("top","132px");
            $('.left').css("left","125px");
			
			$('.right').css("top","116px");
            $('.right').css("left","716px");
			
			$('.glass_a').css("top","343px");
            $('.glass_a').css("left","147px");
			
			$('.glass_b').css("top","422px");
            $('.glass_b').css("left","302px");
			
			$('.glass_c').css("top","411px");
            $('.glass_c').css("left","634px");
			
			$('.glass_d').css("top","321px");
            $('.glass_d').css("left","761px");
			
			$('.total').css("display","none");
			
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
			
			
			$('.left').css("top","106px");
            $('.left').css("left","98px");
			
			$('.right').css("top","194px");
            $('.right').css("left","720px");
			
			$('.glass_a').css("top","297px");
            $('.glass_a').css("left","110px");
			
			$('.glass_b').css("top","371px");
            $('.glass_b').css("left","253px");
			
			$('.glass_c').css("top","452px");
            $('.glass_c').css("left","444px");
			
			$('.glass_d').css("top","434px");
            $('.glass_d').css("left","750px");
			
			$('.total').css("display","none");
		}
		}
		else if(form.degree.value=="135degre")
		{
		if(cornerPosition=="1st Center Post from Left")
		{
			
			
			$('.left').css("top","189px");
            $('.left').css("left","70px");
			
			$('.right').css("top","161px");
            $('.right').css("left","787px");
			
			$('.glass_a').css("top","375px");
            $('.glass_a').css("left","119px");
			
			$('.glass_b').css("top","378px");
            $('.glass_b').css("left","393px");
			
			$('.glass_c').css("top","349px");
            $('.glass_c').css("left","590px");
			
			$('.glass_d').css("top","315px");
            $('.glass_d').css("left","755px");
			
			$('.total').css("display","none");	
		}
		else if(cornerPosition=="2nd Center Post from Left")
		{
			
			
			$('.left').css("top","179px");
            $('.left').css("left","65px");
			
			$('.right').css("top","196px");
            $('.right').css("left","764px");
			
			$('.glass_a').css("top","343px");
            $('.glass_a').css("left","91px");
			
			$('.glass_b').css("top","390px");
            $('.glass_b').css("left","257px");
			
			$('.glass_c').css("top","390px");
            $('.glass_c').css("left","525px");
			
			$('.glass_d').css("top","356px");
            $('.glass_d').css("left","720px");
			
			$('.total').css("display","none");
		}
		else if(cornerPosition=="3rd Center Post from Left")
		{
			
			$('.left').css("top","162px");
            $('.left').css("left","67px");
			
			$('.right').css("top","216px");
            $('.right').css("left","784px");
			
			$('.glass_a').css("top","321px");
            $('.glass_a').css("left","94px");
			
			$('.glass_b').css("top","351px");
            $('.glass_b').css("left","236px");
			
			$('.glass_c').css("top","401px");
            $('.glass_c').css("left","426px");
			
			$('.glass_d').css("top","398px");
            $('.glass_d').css("left","704px");
			
			$('.total').css("display","none");
		}
		}
			
		}
		}
			
			
		}
		
			<?php
		}
		
		?>
		
		
		
			
		
		
			
			
			
			
			
			
        document.getElementById("products_ids").innerHTML=str;        
        document.getElementById("left-post").innerHTML=leftPostPrice+".00";
        document.getElementById("right-post").innerHTML=rightPostPrice+".00";
        document.getElementById("trasition-post").innerHTML=t_post_price+".00";
        document.getElementById("face-glass").innerHTML=glassPrice+".00";
        document.getElementById("total").innerHTML=totalPrice+".00";
        document.getElementById("flange-cover").innerHTML=flangeCoversPrice+".00";   
        document.getElementById("flange-cover2").innerHTML=flangeCoversPrice2+".00"; 
        document.getElementById("left-Panel").innerHTML=leftEndPanelPrice+".00";
        document.getElementById("right-panel").innerHTML=rightEndPanelPrice+".00";
        if($("#end_options").val()!="select"){
			$("#endpan_err").attr("src","img/iconCheckOn.gif");
		}else{
		    $("#endpan_err").attr("src","img/iconCheckOff.gif");
		}
        if(right_lenght_obj.value=="select"){
            $("#right_err").attr("src","img/iconCheckOff.gif");
            one=false;
        }else{
            $("#right_err").attr("src","img/iconCheckOn.gif");
            one=true;
        } 
        if(left_lenght_obj.value=="select"){
            two=false;
            $("#left_err").attr("src","img/iconCheckOff.gif");
        }else{
            $("#left_err").attr("src","img/iconCheckOn.gif");
            two=true;
        }
        if(type_obj.value=="1BAY"){
        	var foura=fourb=fourc=fourd=false;
            if(face_lenght_obj!=null && face_lenght_obj.value=="select"){
                $("#glass_a_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_a_err").attr("src","img/iconCheckOn.gif");
                four=true;
            }
			if(one&&two&&three&&four){
                //$("#froast").css("display","");
            }else{
                //$("#froast").css("display","none");
            }
        }else if(type_obj.value=="2BAY"){
        	var foura=fourb=fourc=fourd=false;
            if(face_lenght_a_obj!=null && face_lenght_a_obj.value=="select"){
                $("#glass_a_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_a_err").attr("src","img/iconCheckOn.gif");
                foura=true;
            }
            if(face_lenght_b_obj!=null && face_lenght_b_obj.value=="select"){
                $("#glass_b_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_b_err").attr("src","img/iconCheckOn.gif");
                fourb=true;
            }
			if(foura&&fourb){
				four=true;
                //$("#froast").css("display","");
            }else{
                //$("#froast").css("display","none");
            }
        }else if(type_obj.value=="3BAY"){
        	var foura=fourb=fourc=fourd=false;
            if(face_lenght_a_obj!=null && face_lenght_a_obj.value=="select"){
                $("#glass_a_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_a_err").attr("src","img/iconCheckOn.gif");
                foura=true;
            }
            if(face_lenght_b_obj!=null && face_lenght_b_obj.value=="select"){
                $("#glass_b_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_b_err").attr("src","img/iconCheckOn.gif");
                fourb=true;
            }
            if(face_lenght_c_obj!=null && face_lenght_c_obj.value=="select"){
                $("#glass_c_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_c_err").attr("src","img/iconCheckOn.gif");
                fourc=true;
            }
			if(foura&&fourb&&fourc){
                four=true;
                //$("#froast").css("display","");
            }else{
                //$("#froast").css("display","none");
            }
        }else if (type_obj.value=="4BAY"){
        	var foura=fourb=fourc=fourd=false;
            if(face_lenght_a_obj!=null && face_lenght_a_obj.value=="select"){
                $("#glass_a_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_a_err").attr("src","img/iconCheckOn.gif");
                foura=true;
            }
            if(face_lenght_b_obj!=null && face_lenght_b_obj.value=="select"){
                $("#glass_b_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_b_err").attr("src","img/iconCheckOn.gif");
                fourb=true;
            }
            if(face_lenght_c_obj!=null && face_lenght_c_obj.value=="select"){
                $("#glass_c_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_c_err").attr("src","img/iconCheckOn.gif");
                fourc=true;
            }
            if(face_lenght_d_obj!=null && face_lenght_d_obj.value=="select"){
                $("#glass_d_err").attr("src","img/iconCheckOff.gif");
                four=false;
            }else{
                $("#glass_d_err").attr("src","img/iconCheckOn.gif");
                fourd=true;
            }
			if(foura&&fourb&&fourc&&fourd){
                //$("#froast").css("display","");
                four=true;
            }else{
                //$("#froast").css("display","none");
            }
        }
		
		
		if(type_obj.value=="2BAY"||type_obj.value=="3BAY"||type_obj.value=="4BAY"){
		if(degree_obj.value=="select"){
            $("#post_angle_err").attr("src","img/iconCheckOff.gif");
            eight=false;
        }else{
            $("#post_angle_err").attr("src","img/iconCheckOn.gif");
            eight=true;
        }
		
		if(posttype_obj.value=="select"){
            $("#post_type_err").attr("src","img/iconCheckOff.gif");
            eight=false;
        }else{
            $("#post_type_err").attr("src","img/iconCheckOn.gif");
            eight=true;
        }
		}
		
		
		
        if(flange_covers_obj.value=="select"){
            $("#light_err").attr("src","img/iconCheckOff.gif");
            five=false;
        }else{
            $("#light_err").attr("src","img/iconCheckOn.gif");
            five=true;
        }
        if(flange_covers_obj2.value=="select"){
            $("#light_br_err").attr("src","img/iconCheckOff.gif");
            six=false;
        }else{
            $("#light_br_err").attr("src","img/iconCheckOn.gif");
            six=true;
        }
        if(choose_finish_obj.value=="select"){
            $("#finish_err").attr("src","img/iconCheckOff.gif");
            eight=false;
        }else{
            $("#finish_err").attr("src","img/iconCheckOn.gif");
            eight=true;
        }
        if(glass_face_obj.value==2){
            $("#left_err").attr("src","img/iconCheckOn.gif");
            two=true;
            if(!zero){
                $("#left_err").attr("src","img/iconCheckOff.gif");
            }
        }else if(glass_face_obj.value==3){
            $("#right_err").attr("src","img/iconCheckOn.gif");
            one=true;
            if(!zero){
                $("#right_err").attr("src","img/iconCheckOff.gif");
            }
        }else if(glass_face_obj.value==4){
            $("#left_err").attr("src","img/iconCheckOn.gif");
            $("#right_err").attr("src","img/iconCheckOn.gif");
            one=true;
            two=true;
            if(!zero){
                $("#left_err").attr("src","img/iconCheckOff.gif");
                $("#right_err").attr("src","img/iconCheckOff.gif");
            }
        }
        if(category_name=="B950"){
        	six=true;
        }
        if(zero&&one&&two&&four&&five&&six){
            // $("#add").removeAttr("disabled");
            // $("#add").css("opacity","1");
        }else{
            // $("#add").css("opacity","0.3");
            // $("#add").attr("disabled","disabled");
        }
	} 
 
</script>
<script type="text/javascript">

        function setHideShow(selector, msg){
            setShowEvent(selector, msg)
            //setHideEvent(selector);
        }
		 function setHideShow1(selector, msg){
            setShowEventmsg(selector, msg)
			//$(".msgtishu1").remove();
		
            //setHideEvent(selector);
        }
		 function setHideShow2(selector, msg){
            setShowEventmsg2(selector, msg)
            //setHideEvent(selector);
        }
        function setShowEvent(selector, msg){
           // $("#additional_image").css("opacity","0.5");
           // $(".test-hide").css("opacity","0.5");
            var cssObj={
                    "background-color":"#111",
                    "border-style":"solid",
                    "border-width":"2px",
                    "border-color":"#ff0000"};
            $(selector).css(cssObj);
            $("#message_w").html(msg);
        }
		function setShowEventverticle(selector, msg){
           // $("#additional_image").css("opacity","0.5");
           // $(".test-hide").css("opacity","0.5");
            var cssObj={
                    "background-color":"#111",
                    "border-style":"solid",
                    "border-width":"2px",
                    "border-color":"#ff0000"};
            $(selector).css(cssObj);
            $("#message_wp").html(msg);
        }
		function setShowEventhori(selector, msg){
           // $("#additional_image").css("opacity","0.5");
           // $(".test-hide").css("opacity","0.5");
            var cssObj={
                    "background-color":"#111",
                    "border-style":"solid",
                    "border-width":"2px",
                    "border-color":"#ff0000"};
            $(selector).css(cssObj);
            $("#message_wp1").html(msg);
        }
		function setShowEventhori1(selector, msg){
           // $("#additional_image").css("opacity","0.5");
           // $(".test-hide").css("opacity","0.5");
            var cssObj={
                    "background-color":"#111",
                    "border-style":"solid",
                    "border-width":"2px",
                    "border-color":"#ff0000"};
            $(selector).css(cssObj);
            $("#message_wp2").html(msg);
        }
		 function setShowEventmsg(selector, msg){
           // $("#additional_image").css("opacity","0.5");
           // $(".test-hide").css("opacity","0.5");
            var cssObj={
                    "background-color":"#111",
                    "border-style":"solid",
                    "border-width":"2px",
                    "border-color":"#ff0000"};
            $(selector).css(cssObj);
            $(".msgtishu").html(msg);
			
        }
		 function setShowEventmsg2(selector, msg){
           // $("#additional_image").css("opacity","0.5");
           // $(".test-hide").css("opacity","0.5");
            var cssObj={
                    "background-color":"#111",
                    "border-style":"solid",
                    "border-width":"2px",
                    "border-color":"#ff0000"};
            $(selector).css(cssObj);
            $(".msgtishu1").html(msg);
        }
        function setHideEvent(selector){
           action_event(selector);
        }
        var action_event = function(selector){
                $("#additional_image").css("opacity","1.0");
                 var cssObj={
                        "background":"none",
                        "border":"none",
                        "box-shadow":"none"};
                $(selector).css(cssObj);
                $(".test-hide").css("opacity","1.0");
                $("#message_w").html("");
				 $("#message_wp").html("");
				 $("#message_wp1").html("");
				 $("#message_wp2").html("");
            };

</script>
<script type="text/javascript">
	$(document).ready(function(){
	
	
		$('[name="face_length_a"]').live('change',function(){
		if($(this).val()=='custom'){
			custom=$(this);post='';
			$msg='<?php echo $ms_face;?>';
			$('.delete').click();
			
		}
	})
	$('[name="face_length_b"]').live('change',function(){
		if($(this).val()=='custom'){
			$msg='<?php echo $ms_face;?>';
			custom=$(this);post='';
			$('.delete').click();
		}
	})
	$('[name="face_length_c"]').live('change',function(){
		if($(this).val()=='custom'){
			custom=$(this);post='';
			$msg='<?php echo $ms_face;?>';
			$('.delete').click();
		}
	})
	
	$('[name="face_length"]').live('change',function(){
		if($(this).val()=='custom'){
			$msg='<?php echo $ms_face;?>';
			custom=$(this);post='';
			$('.delete').click();
		}
	})
	
	$('[name="face_length_d"]').live('change',function(){
		if($(this).val()=='custom'){
			custom=$(this);post='';
			$msg='<?php echo $ms_face;?>';
			$('.delete').click();
		}
	})
	
	
	$('[name="right_length"]').live('change', function(){
		if($(this).val()=='custom'){
			$msg='<?php echo $ms_right?>';
			custom=$(this);post='';
			$('.delete').click();
		}
	})
	$('[name="left_length"]').live('change', function(){
		if($(this).val()=='custom'){
			$msg='<?php echo $ms_left?>';
			custom=$(this);
			post='';
			$('.delete').click();
		}
	})
	
	
	
	
	$('.item .delete').click(function(){
		
		var elem = $(this).closest('.item');
		
		$.confirm({
		
	
			'title'		: 'Confirmation',
			'message'	:$msg,
			'buttons'	: {
				'Proceed'	: {
					'class'	: 'blue',
					'action': function(){
						
						
						
						my_facelengt_select="";
						my_facelengt_select+='<select name="'+custom.attr("name")+'" onchange="getPriceOfProduct(this.form)" >';
						//	my_facelengt_simple_select+='<select name="face_length" onchange="getPriceOfProduct(this.form)" >';
						var myArray = new Array();
						var i=1;
						$('[name="'+custom.attr("name")+'"] > option') .each(function() {
														
							if($(this).val()!='custom'){
								myArray[i]=$(this).val();
								i++;
							}
													
						});
						/*for ep 11 category post height*/
						
											
						
						var j=0;
						for (i=8;i<myArray[2];i++){
						
							my_facelengt_select+='<option value="'+myArray[2]+'">'+i+'"</option>';
							my_facelengt_select+='<option value="'+myArray[2]+'">'+i+'-1/4'+'"</option>';
							my_facelengt_select+='<option value="'+myArray[2]+'">'+i+'-1/2'+'"</option>';
							my_facelengt_select+='<option value="'+myArray[2]+'">'+i+'-3/4'+'"</option>';
							j=i;
						}
						
						for(i=1;i< myArray.length-1;i++){
							for(j=myArray[i];j<myArray[i+1];j++){
								if(j>myArray[i]){
									my_facelengt_select+='<option value="'+myArray[i+1]+'">'+j+'"</option>';
								}else
								{
									my_facelengt_select+='<option value="'+myArray[i]+'">'+j+'"</option>';	
								}
								my_facelengt_select+='<option value="'+myArray[i+1]+'">'+j+'-1/4'+'"</option>';
								my_facelengt_select+='<option value="'+myArray[i+1]+'">'+j+'-1/2'+'"</option>';
								my_facelengt_select+='<option value="'+myArray[i+1]+'">'+j+'-3/4'+'"</option>';
							}
						}
						
					 
						my_facelengt_select+='<option value="'+myArray[i]+'">'+myArray[i]+'"</option>';
						
						

					$('#'+custom.attr("name")+'_span').html(my_facelengt_select);
						
						$('#is_custom').val('Yes');
						getPriceOfProduct(document.forms['cart_quantity']);
						
					}
				},
				'Cancel'	: {
					'class'	: 'gray',
					'action': function(){
						var str=custom.attr("name");
                        $('select[name='+str+']').val("select");
                        getPriceOfProduct(document.forms['cart_quantity']);
                        return false;

					}	// Nothing to do in this case. You can as well omit the action property.
					
				}
			}
		});
		
	});

})
</script>
<style type="text/css">
    .message_p{
        position:relative;
        z-index: 102;
    }
    .message_w{
        /*position:absolute;
        color:#C7F900;
        text-shadow:2px 2px 3px #111;
        font-size: 18px;
        right:15px;
        bottom:-460px;
        background: url('images/login-bg.png');
        padding:5px;
        border-radius:10px;
        font-weight: bold;
        text-align: center;
        width: 400px;*/
         color:#C7F900 !important;
        text-shadow:2px 2px 3px #111;
        font-size: 18px;
        border: 2px solid #ff0000;
        background: url('images/login-bg.png');
        padding:5px;
        border-radius:4px;
        font-weight: bold;
        width: 585px;
        height:67px    
    }
  table#cart-form tr td .next_button{
        background-color:green !important;
        box-shadow: none;
        font-weight: bold;
    }
</style>


<?php
if (!$detect->isMobile())
{
?>

<table id="cart-form"> 
<?php
    // echo '<tr>
    //          <td id="option-panel">
    //          <div class="test-warsi" style="position:relative;">
    //             <h1>Options:</h1><br style="clear:both">
    //             <ul class="option-panel">
    //                 <li class="images">
    //                     <ul class="option-images">
    //                         <li><img width="12" height="12" src="images/linkbullet.gif"></li>
    //                         <li><img width="12" height="12" src="images/linkbullet.gif"></li>
    //                         <li><img width="12" height="12" src="images/linkbullet.gif"></li>
    //                         <li><img width="12" height="12" src="images/linkbullet.gif"></li>
    //                     </ul>
    //                 </li>
    //                 <li class="option-list">
    //                     <ul class="option">
    //                         <li id=1>Both Closed End Panels</li>
    //                         <li id=2>Right Closed End Panel</li>
    //                         <li id=3>Left Closed End Panel</li>
    //                         <li id=4 class="last">No Closed End Panels</li>
    //                     </ul>
    //                 </li>
    //             </ul>
    //             <!--div id="line" style="background: none repeat scroll 0 0 #ff0000;height: 4px;position: absolute;right: -33px;top: 45px;width: 33px;display:none;z-index: 102;"></div-->
    //            <div id="line" style="width:42px;border: 1px solid #FF0000; background-color: green !important; top: 0px; left: -55px; padding: 5px; position: absolute;">Step-1</div>
    //             <div class="cilck_button" id="cilck_button" style="position: absolute;right: -44px;top: 0;z-index: 102;display:none;"></div>
    //             <div style="clear:both"></div>
    //             </div>
    //         </td>     
    //     </tr>';
?>
    <tr>
        <td>
            <table id="hidetable1" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">
                <tr>
                        <td colspan=3><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp1)</span>Measurements</center></h2></td>
                </tr>
                 <?php 
                    $fn="";
                    if($category_name=="B950 SWIVEL"){
                        $fn="B-950-SWIVEL";
                    }else{
                        $fn="B950";
                    }

                    if($_REQUEST['type']=='1BAY') {
                    echo    '<tr>
                                <td class="test-lenght1baya"><a class="thickbox" href="images/'.$fn.'/1bay_faceA.jpg"><h1>Face Length A</h1></td>
                                <td>
								<span id="face_length_span">
                                    <select name="face_length" onchange="getPriceOfProduct(this.form)">
                                        <option value="select">Select</option>
                                        <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                    </select>
									</span>
                                </td>
                                <td>
                                	<span id="errormsgfirstname">
                            			<img id="glass_a_err" src="img/iconCheckOff.gif">
                        			</span>
                                </td>
                            </tr>'; 
                }
                if($_REQUEST['type']=='2BAY'){
                    echo '<tr>
                            <td class="test-lenght2baya"><a class="thickbox" href="images/'.$fn.'/2bay_faceA.jpg"><h1>Face Length A</h1></a></td>
                            <td>
								<span id="face_length_a_span">
                                <select name="face_length_a" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
                                <span id="errormsgfirstname">
                            		<img id="glass_a_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayb"><a class="thickbox" href="images/'.$fn.'/2bay_faceB.jpg" ><h1>Face Length B</h1></a></td>
                            <td>
								<span id="face_length_b_span">
                                <select name="face_length_b" onchange="getPriceOfProduct(this.form)">
                                   <option value="select">Select</option>
                                   <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_b_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>';
                }
                if($_REQUEST['type']=='3BAY'){
                    echo '<tr>
                            <td class="test-lenght3baya"><a class="thickbox" href="images/'.$fn.'/3bay_faceA.jpg"><h1>Face Length A</h1></a></td>
                            <td>
							<span id="face_length_a_span">
                                <select name="face_length_a" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
                                <span id="errormsgfirstname">
                            		<img id="glass_a_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayb"><a class="thickbox" href="images/'.$fn.'/3bay_faceB.jpg" ><h1>Face Length B</h1></a></td>
                            <td>
								<span id="face_length_b_span">
                                <select name="face_length_b" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_b_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayc"><a class="thickbox" href="images/'.$fn.'/3bay_faceC.jpg" ><h1>Face Length C</h1></a></td>
                            <td>
							<span id="face_length_c_span">
                                <select name="face_length_c" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_c_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>';
                } if($_REQUEST['type']=='4BAY'){
                    echo '<tr>
                            <td class="test-lenght3baya"><a class="thickbox" href="images/'.$fn.'/4bay_faceA.jpg"><h1>Face Length A</h1></a></td>
                            <td>
							<span id="face_length_a_span">
                                <select name="face_length_a" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
                                <span id="errormsgfirstname">
                            		<img id="glass_a_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayb"><a class="thickbox" href="images/'.$fn.'/4bay_faceB.jpg"><h1>Face Length B</h1></a></td>
                            <td>
								<span id="face_length_b_span">
                                <select name="face_length_b" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_b_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayc"><a class="thickbox" href="images/'.$fn.'/4bay_faceC.jpg" ><h1>Face Length C</h1></a></td>
                            <td>
							<span id="face_length_c_span">
                                <select name="face_length_c" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_c_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr><tr>
                            <td class="test-lenght4bayd"><a class="thickbox" href="images/'.$fn.'/4bay_faceD.jpg"><h1>Face Length D</h1></a></td>
                            <td>
							<span id="face_length_d_span">
                                <select name="face_length_d" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_d_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>';
                }?>
                </table>
            </td>
        </tr>
        <tr>
        	<td>
                <table id="hidetable2" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">
                	<tr>
                        <td colspan=3><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp2)</span>Options</center></h2></td>
                    </tr>
                    <tr>
                        <td>
                            <a class="thickbox" href="images/<?php echo $fn?>/End_panels.jpg"><h1>End Panels</h1></a>
                        </td>
                        <td>
                            <select class="option" id="end_options">
                                <option value="select">Select</option>
                                <option value="Both Closed End Panels">Both Ends</option>
                                <option value="Right Closed End Panel">Right End</option>
                                <option value="Left Closed End Panel">Left End</option>
                                <option value="No Closed End Panels">No Ends</option>
                            </select>
                        </td>
                        <td>
                            <span id="errormsgfirstname">
                                <img id="endpan_err" src="img/iconCheckOff.gif">
                            </span>
                        </td>
                    </tr>
                    <tr id="right_lenght">
                    <td class="test-lenght1bay" ><a class="thickbox" href='images/<?php echo $fn?>/Right_length.jpg' ><h1 style="margin-left:20px;">Right End</h1></a></td>
                    <td>
						<span id="right_length_span">
                        <select name="right_length" onchange="getPriceOfProduct(this.form)"> 
                            <option value="select">Select</option>
                            <option value="12">12"</option>
                            <option value="18">18"</option>
                            <option value="24">24"</option>
							<option value="custom">Custom</option>
                        </select>
						</span>
                    </td>
                    <td>
                    	<span id="errormsgfirstname">
                    		<img id="right_err" src="img/iconCheckOff.gif">
                		</span>
                    </td>
                </tr>
                <tr id="left_lenght">
                    <td class="test-lenght2baya"><a class="thickbox" href='images/<?php echo $fn?>/Left_length.jpg'><h1 style="margin-left:20px;">Left End</h1></a></td>
                    <td>
						<span id="left_length_span">
                        <select name="left_length" onchange="getPriceOfProduct(this.form)">
                            <option value="select">Select</option>
                            <option value="12">12"</option>
                            <option value="18">18"</option>
                            <option value="24">24"</option>
							<option value="custom">Custom</option>
                        </select>
						</span>
                    </td>
                    <td>
                    	<span id="errormsgfirstname">
                            <img id="left_err" src="img/iconCheckOff.gif">
                        </span>
                    </td>
                </tr>

                <tr>
                    <td class="test-light"><a class="thickbox" href="light.php?name=<?php echo $videoname;?>&type=adj&KeepThis=true&TB_iframe=true&height=480&width=640"><h1>Light Bar</h1></a></td>
                    <td>
                    	<select name="flange_covers" style="margin:4px;" onchange="getPriceOfProduct(this.form);"> 
                            <option value="select">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                        <!--input type="button" class="flange-covers-image thickbox" value="?" style="width:20px;margin: 0 4px;" onclick="javascript:window.location.href='flang.php?type=adj&KeepThis=true&TB_iframe=true&height=480&width=640'" disabled="disabled"/-->
                        <!-- <a style="width:20px;margin: 0 4px;float: right;" class="thickbox" href='light.php?type=adj&KeepThis=true&TB_iframe=true&height=480&width=640' disabled="disabled"><img src="images/flang.jpg" ></a>   -->
                        <!-- <input type="checkbox" name="flange_covers" value="0" style="margin:4px;" onclick="getPriceOfProduct(this.form);" disabled="disabled"/>     -->
                    </td>
                    <td>
                    	<span id="errormsgfirstname">
                            <img id="light_err" src="img/iconCheckOff.gif">
                        </span>
                    </td>
                </tr>
                <?php if($category_name=="B950"){
                        		$dsp="display:none";
                        	}else{
                        		$dsp="";
                        	}?>
                        <tr style="<?php echo $dsp;?>">
                        	
                            <td class="test-light"><a class="thickbox" href='light_bracket.php?type=adj&KeepThis=true&TB_iframe=true&height=480&width=640'><h1>Light Bracket</h1></a></td>
                            <td>
                                <select name="flange_covers_2" onchange="getPriceOfProduct(this.form)" style="width: 68px;">
                                	<option value="select">Select</option>
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>    
                            </td>
                            <td>
                            	<span id="errormsgfirstname">
                            		<img id="light_br_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>

               <tr>
                <td colspan=2><a class="thickbox" href='images/Finishes.jpg'><h1>Post Finish</h1></a>
					<select name="choose_finish" style="width:130px;margin:0" onchange="getPriceOfProduct(this.form)" >
                        <option value="SS">Brushed Stainless</option>
                        <option value="PC">Coated Black</option>
                    </select>
                </td>
                <td>
                	<span id="errormsgfirstname">
                        <img id="finish_err" src="img/iconCheckOff.gif">
                    </span>
                </td>
            </tr>
            </table>
				
				
			<?php
if($_REQUEST['type']=='2BAY'||$_REQUEST['type']=='3BAY'||$_REQUEST['type']=='4BAY') {
?>
				<div id="cart-form">
				<table id="showtable1" align="center" style="display:none" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px;">
                    <tr>
                        <td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;1)</span>Choose Option</center></h2></td>
                    </tr>
                   
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
				   <tr>
                        <td><a class="thickbox" href='light_bracket.php?type=adj&KeepThis=true&TB_iframe=true&height=480&width=640'><h1 style="text-decoration: none;">Corner Post Type</h1></a></td>
                        <td>
						<select name="posttype" style="width:70px;margin:0" onchange="getPriceOfProduct(this.form)" >
                            
                                <option value="inner">Inner</option>
                                <option value="outer">Outer</option>
                            </select>
                        </td>
						
						<td style="width:46px;">
                    <span id="errormsgfirstname">
                        <img id="post_type_err" src="img/iconCheckOff.gif">
                    </span>
                </td>
                    </tr>
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
				   <tr>
                        <td><a class="thickbox" href='light_bracket.php?type=adj&KeepThis=true&TB_iframe=true&height=480&width=640'><h1 style="text-decoration: none;">Corner Post Angle</h1></a></td>
                        <td>
                            
							<select name="degree" style="width:70px;margin:0" onchange="getPriceOfProduct(this.form)" >
                            
                                <option value="90degre">90 Degree</option>
                                <option value="135degre">135 Degree</option>
                            </select>
                        </td>
						
						<td style="width:46px;">
                    <span id="errormsgfirstname">
                        <img id="post_angle_err" src="img/iconCheckOff.gif">
                    </span>
                </td>
                    </tr>
					
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
					
					
					
					
                </table>
				</div>
				
				
				
				
				
				<div id="cart-form">
				<table id="showtable2" align="center" style="display:none" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px;">
                    <tr>
                        <td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;2)</span>Choose Corner Post</center></h2></td>
                    </tr>
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
					<?php
					if($_REQUEST['type']=='2BAY') {
					?>
                   <tr>
				  
				   <td colspan=2>
				   <input type="radio" name="corner_post" id="1stcenter" onchange="getPriceOfProduct(this.form)" value="1st Center Post from Left" checked>
				   <img src="images/B950S/2BAY/INNER90D1STPOST.jpg" style="width:90%;" id="postimg1" /></a></label></td>
				  
				   </tr>
				   
				   
				   
				   
				   <?
					}
				   ?>
				   
				   
					<?php
					if($_REQUEST['type']=='3BAY') {
					?>
                   <tr>
				  
				   <td>
				   <label>
				   <input type="radio" name="corner_post" id="1stcenter" onchange="getPriceOfProduct(this.form)" value="1st Center Post from Left" checked>
				   <img src="images/B950S/3BAY/INNER90D1STPOST.jpg" style="width:90%;" id="postimg1" /></a></label></td>
				   <td style="padding-left:5px;"><label>
				   <input type="radio" name="corner_post" id="2ndcenter" onchange="getPriceOfProduct(this.form)" value="2nd Center Post from Left">
				   <img src="images/B950S/3BAY/INNER90D2NDPOST.jpg" id="postimg2" style="width:90%;" /></a></label></td>
				   </tr>
				   <tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
				    
				   
				
				   
				   <?
					}
				   ?>
				   
				   
					<?php
					if($_REQUEST['type']=='4BAY') {
					?>
                   <tr>
				  
				   <td>
				   <label>
				   <input type="radio" name="corner_post" id="1stcenter" onchange="getPriceOfProduct(this.form)" value="1st Center Post from Left" checked>
				   <img src="images/B950S/4BAY/INNER90D1STPOST.jpg" style="width:90%;" id="postimg1" /></a></label></td>
				   <td style="padding-left:5px;"><label>
				   <input type="radio" name="corner_post" id="2ndcenter" onchange="getPriceOfProduct(this.form)" value="2nd Center Post from Left">
				   <img src="images/B950S/4BAY/INNER90D2NDPOST.jpg" id="postimg2" style="width:90%;" /></a></label></td>
				   </tr>
				   
				   
				   <tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
				  
				   
				   <td colspan=2><label>
				   <input type="radio" name="corner_post" id="3rdcenter" onchange="getPriceOfProduct(this.form)" value="3rd Center Post from Left">
				   <img src="images/B950S/4BAY/INNER90D3RDPOST.jpg" id="postimg3" style="width:50%;" /></label></td>
				   
				   </tr>
				
				
				   <?
					}
				   ?>
				   
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
					
                </table>
				</div>
				
	<style>				
	/* HIDE RADIO */
[type=radio] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
}

/* IMAGE STYLES */
[type=radio] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=radio]:checked + img {
  outline: 2px solid #d84747;
}
.heading_all {
    
    font-size: 15px !important;
    
}				
	</style>			
				
			<div id="cart-form">
				
				<table id="forgotot" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px;">
                    <tr>
                        <td colspan=2><h3 style="text-align:left !important;"><a >
						<input type="checkbox" id="gotocornerpostcheck" onchange="getPriceOfProduct(this.form)" name="gotocornerpostcheck" value="Go To Corner Post" style="display:none;" >
						<span style="color:white; text-align:left;font-size:15px;">&nbsp;&nbsp;3)</span>
						<label for="gotocornerpostcheck" id="gotocornerpost" > Go To Corner Post <span style="color:#e0946f; text-align:left !important;font-size: 16px;"><blink>New!</blink></span></label>
						
						</a>
						</h3></td>
                    </tr>
					</table>
					
				<table id="forstarightpost" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px; display:none;">
                    <tr>
                        <td colspan=2><center><h3 style="text-decoration: none;"><a id="backtostraightpost">
						<span style="color:white;font-size:16px;">&nbsp;&nbsp;3)</span>&nbsp;
						<label for="gotocornerpostcheck" > Back To Straight Post</label></a></h3></center><br /></td>
                    </tr>
					</table>
					
				</div>
				
				<style>
		
				
				
				table#cart-form h3 {
    text-decoration: none;
    padding-left: 0;
    font-size: 14px;
	text-align:center;
}

				table#forstarightpost h3 {
    text-decoration: none;
    padding-left: 0;
    font-size: 14px;
	text-align:center;
}
				h3 {
    color: #C7F900;
    font-size: 13px;
    padding: 4px;
    padding-left: 30px;
    text-align: left;
    /* padding-top: 10px; */
}
				table#showtable1 {
    border: 1px solid white;
    padding: 2px 0 2px 2px;
    width: 250px;
    background: #484844;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

table#showtable2 {
    border: 1px solid white;
    padding: 2px 0 2px 2px;
    width: 250px;
    background: #484844;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}
				</style>
				
<script>
$(document).ready(function() {
     
    $("[data-labelfor]").click(function() {
        $('#' + $(this).attr("data-labelfor")).prop('checked',
       function(i, oldVal) { return !oldVal; });
    });
   
});



$(document).ready(function() {
    $('#gotocornerpost').click(function() {
    $('#showtable1').toggle('slow');
    $('#showtable2').toggle('slow');
    $('#forstarightpost').toggle('slow');
	$('#hidetable1').toggle('hide');
	$('#hidetable2').toggle('hide');
	$('#hidetable3').toggle('hide');
	$('#forgotot').toggle('hide');
	$('#quotetext span').html('&nbsp;&nbsp;4)');
    });
})


$(document).ready(function() {
    $('#backtostraightpost').click(function() {
  $('#showtable1').toggle('slow');
    $('#showtable2').toggle('slow');
    $('#forstarightpost').toggle('slow');
	$('#hidetable1').toggle('hide');
	$('#hidetable2').toggle('hide');
	$('#hidetable3').toggle('hide');
	$('#forgotot').toggle('hide');
	$('#quotetext span').html('&nbsp;&nbsp;5)');
    });
})
</script>

<?
}
?>
	
			
			
			
			
			
        </td>
    </tr>
    
</table>
<br />
<style>
.test-Price td{font-size: 80% !important;}
</style>
<div class="test-Price" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">
    <table id="cart-form" class="price"> 
    	<tr>
		
			<?php
			if($_REQUEST['type']=='1BAY') {
			?>
			<td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;3)</span>Quote</h2></center></td>
			
			<?php
			}
			else{
			?>
			
			<td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;4)</span>Quote</h2></center></td>
			<?php
			}
			?>
        	
			
    	</tr>
        <tr>
            <td align="left">Left Post:</td><td id="left-post" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Right Post:</td><td id="right-post" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Transistions Post:</td><td id="trasition-post" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Light:</td><td id="flange-cover" align="right">0.00</td>
        </tr>
		<?php if($category_name=="B950") {?>
         <tr style="position: absolute;right: -44px;top: 0;z-index: 102;display:none;">
            <td align="left">Light Bracket:</td><td id="flange-cover2" align="right">0.00</td>
        </tr><?php }else{?><tr>
            <td align="left">Light Bracket:</td><td id="flange-cover2" align="right">0.00</td>
        </tr><?}?>
        <tr>
            <td align="left">Face Glass:</td><td id="face-glass" align="right">0.00</td>
        </tr>
        <?php if($category_name!="EP5" && $category_name!="EP15") {?>
        <tr>
            <td align="left">Left End Glass:</td><td id="left-Panel" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Right End Glass:</td><td id="right-panel" align="right">0.00</td>
        </tr>
        <?php }?>
        <tr>
            <td colspan="2" style="padding:0px !important;background: #f4f4f4; height:1px"></td>       
        </tr>
        <tr>
            <td align="left">Total:</td><td id="total" align="right">0.00</td>
        </tr>
    </table>
</div>
<br />
<table id="cart-form" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">

	<input type="hidden" id="c_glass_face" name="c_glass_face" value=""  />
        <input type="hidden" id="c_glass_face_val" name="c_glass_face_val" value=""  />
        
        <input type="hidden" id="c_glass_right" name="c_glass_right" value=""  />
        <input type="hidden" id="c_glass_right_val" name="c_glass_right_val" value=""  />
        
        <input type="hidden" id="c_glass_left" name="c_glass_left" value=""  />
        <input type="hidden" id="c_glass_left_val" name="c_glass_left_val" value=""  />
		
		<input type="hidden" id="c_glass_a" name="c_glass_a" value=""  />
        <input type="hidden" id="c_glass_a_val" name="c_glass_a_val" value=""  />
		
		<input type="hidden" id="c_glass_b" name="c_glass_b" value=""  />
        <input type="hidden" id="c_glass_b_val" name="c_glass_b_val" value=""  />
		
		<input type="hidden" id="c_glass_c" name="c_glass_c" value=""  />
        <input type="hidden" id="c_glass_c_val" name="c_glass_c_val" value=""  />
		
		<input type="hidden" id="c_glass_d" name="c_glass_d" value=""  />
        <input type="hidden" id="c_glass_d_val" name="c_glass_d_val" value=""  />
		
		
		 
		<input type="hidden" id="c_glass_a_light" name="c_glass_a_light" value=""  />
		<input type="hidden" id="c_glass_a_val_light" name="c_glass_a_val_light" value=""  />
		<input type="hidden" id="c_glass_b_light" name="c_glass_b_light" value=""  />
		<input type="hidden" id="c_glass_b_val_light" name="c_glass_b_val_light" value=""  />
		<input type="hidden"id="c_glass_c_light" name="c_glass_c_light" value=""  />
		<input type="hidden"id="c_glass_c_val_light" name="c_glass_c_val_light" value=""  />
		<input type="hidden"id="c_glass_d_light" name="c_glass_d_light" value=""  />
		<input type="hidden"id="c_glass_d_val_light" name="c_glass_d_val_light" value=""  />
		<input type="hidden" id="is_custom" name="is_custom" value=""  />
		
		<input type="hidden" id="post_type_val" name="post_type_val" value=""  />
		<input type="hidden" id="post_degree_val" name="post_degree_val" value=""  />
		
		
		<input type="hidden" id="product_type" name="product_type" value=""  />
    <tr>
        <!--td><h1>Add to Cart</h1></td-->
        <td colspan="2" align="center"><input type="hidden" name="type" value="<?=$_REQUEST['type']?>" />
			<input type="hidden" name="glass_face" value="0" id="glass-face" disabled="disabled"/><div id="products_ids"><?php
			echo "</div>";
           // echo tep_image_submit('button_in_cart.gif', IMAGE_BUTTON_IN_CART, "button");
        ?>
        <input type="image" onclick="return myFunction2(this.form)" button="" id="add" title=" Add to Cart " alt="Add to Cart" src="includes/languages/english/images/buttons/button_in_cart.gif" style="float: none;background: none !important;box-shadow: none;border: medium none;">
		 <input type="hidden" name="optionsid" id="optionsid" value="" disabled="disabled"/>
        </td>
    </tr>
</table>


<?php
}
else{
//mobile view
?>

<style>
table#cart-form {
    border: 1px solid #777;
    padding: 2px 0 2px 2px;
    width: 416px;
    background: #484844;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

table#cart-form tr td select#post_height {
    width: 175px;
    height: 35px;
    background: url(images/arrow.png) no-repeat right;
    margin: 0 3px;
    padding: 1px;
    font-weight: bold;
    appearance: button;
    -moz-appearance: button;
    -webkit-appearance: button;
    font-size: 20px;
}

table#cart-form tr td select {
    width: 175px;
    height: 35px;
    font-size: 20px;
    background: url(images/arrow.png) no-repeat right;
    margin: 0 3px;
    padding: 1px;
    font-weight: bold;
    appearance: button;
    -moz-appearance: button;
    -webkit-appearance: button;
    /* font-size: 16px; */
}

table#cart-form h1 {
    text-decoration: underline;
    float: left;
    /* padding-left: 1px; */
    font-size: 19px;
    padding: 17px;
}

#post_err{width: 37px;}
#glass_a_err{width: 37px;}
#glass_b_err{width: 37px;}
#glass_c_err{width: 37px;}
#glass_d_err{width: 37px;}

#endpan_err{width: 37px;}
#right_length{width: 37px;}
#left_err{width: 37px;}
#flange_err{width: 37px;}
#round_err{width: 37px;}
#light_err{width: 37px;}
#finish_err{width: 37px;}


.heading_all{font-size: 30px;}

</style>



<?php
if($_REQUEST['type']=='1BAY') {
?>
<style>
div.left {
    top: 47px;
    left: 104px;
}
div.right {
    top: 13px;
    left: 569px;
}
div.post {
    top: 186px;
    left: 665px;
}
div.msgtishu {
    display:none;
}
div.msgtishu1 {
    display:none;
}
div.msgtishu2 {
    display:none;
}

div.glass {
    top: 396px;
    left: 523px;
}

div.glass_a {
    display:none;
}
div.glass_b {
    display:none;
}
div.glass_c {
    display:none;
}
div.glass_d {
    display:none;
}
div.total {
    top: 438px;
    left: 555px;
}
</style>
<?
}
?>


<?php
if($_REQUEST['type']=='2BAY') {
?>
<style>
div.left {
    top: 124px;
    left: 46px;
}
div.right {
    top: 39px;
    left: 695px;
}
div.post {
    top: 152px;
    left: 716px;
}
div.msgtishu {
    display:none;
}
div.msgtishu1 {
    display:none;
}
div.msgtishu2 {
    display:none;
}

div.glass {
    display: none;
}

div.glass_a {
    top: 409px;
    left: 418px;
}
div.glass_b {
    top: 295px;
    left: 689px;
}
div.glass_c {
    display:none;
}
div.glass_d {
    display:none;
}
div.total {
    top: 366px;
    left: 588px;
}
</style>
<?
}
?>


<?php
if($_REQUEST['type']=='3BAY') {
?>
<style>
div.left {
    top: 179px;
    left: 41px;
}
div.right {
    top: 66px;
    left: 729px;
}
div.post {
    top: 117px;
    left: 735px;
}
div.msgtishu {
    display:none;
}
div.msgtishu1 {
    display:none;
}
div.msgtishu2 {
    display:none;
}

div.glass {
    display: none;
}

div.glass_a {
    top: 396px;
    left: 330px;
}
div.glass_b {
    top: 302px;
    left: 576px;
}
div.glass_c {
    top: 241px;
    left: 734px;
}
div.glass_d {
    display:none;
}
div.total {
    top: 320px;
    left: 590px;
}
</style>
<?
}
?>


<?php
if($_REQUEST['type']=='4BAY') {
?>
<style>
div.left {
    top: 208px;
    left: 40px;
}
div.right {
    top: 77px;
    left: 745px;
}
div.post {
    top: 104px;
    left: 753px;
}
div.msgtishu {
    display:none;
}
div.msgtishu1 {
    display:none;
}
div.msgtishu2 {
    display:none;
}

div.glass {
    display: none;
}

div.glass_a {
    top: 382px;
    left: 275px;
}
div.glass_b {
    top: 305px;
    left: 492px;
}
div.glass_c {
    top: 254px;
    left: 642px;
}
div.glass_d {
    top: 212px;
    left: 752px;
}
div.total {
    top: 291px;
    left: 588px;
}
</style>
<?
}
?>


	<style>
	#confirmBox {
    background: url(jquery.confirm/body_bg.jpg) repeat-x left bottom #e5e5e5;
    width: 793px;
	height:auto;
    position: absolute;
    left: 32%;
    top: 17%;
    margin: -130px 0 0 -230px !important;
    border: 1px solid rgba(33, 33, 33, 0.6);
    -moz-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
    -webkit-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
    box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
	}
	
	#confirmBox h1 {
    letter-spacing: 0.3px;
    color: #888;
	font-size:40px;
	}
	#confirmBox p {
    background: none;
    font-size: 28px;
    line-height: 1.4;
    padding-top: 35px;
	}

	#confirmButtons {
		padding: 15px 0 25px;
		text-align: center;
		height: 63px;
	}
	
	#confirmBox img{width: 302px;}

	
	
#TB_window {
    margin-left: -474px !important;
    width: 924px !important;
    height: 858px !important;
    margin-top: -647px !important;
    display: block;
}

#TB_window img#TB_Image {
    display: block;
    margin: 60px 0 0 15px;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    border-top: 1px solid #666;
    border-left: 1px solid #666;
    width: 883px !important;
    height: 695px !important;
}

#TB_closeWindow img{width:50px;}
#TB_closeWindowButton img{width:50px;}


#TB_window ifram{
	
	width: 922px !important;
    height: 790px !important;
}



#TB_iframeContent
{
	width: 924px !important;
    height: 793px !important;
}
#example_video_1{
	width: 906px !important;
}
#confirmBox .button{
	
	font:26px/33px 'Cuprum','Lucida Sans Unicode', 'Lucida Grande', sans-serif;
	
}

table#cart-form tr td {
    vertical-align: middle;
    padding: 0;
    margin: 0;
    font-weight: bold;
    color: #f4f4f4;
    font-size: 21px;
}
	</style>
	


<table id="cart-form" style="margin-left: 7%;">
<tr>
<td>

<table id="cart-form"> 

    <tr>
        <td>
            <table id="hidetable1" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">
                <tr>
                        <td colspan=3><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp1)</span>Measurements</center></h2></td>
                </tr>
                 <?php 
                    $fn="";
                    if($category_name=="B950 SWIVEL"){
                        $fn="B-950-SWIVEL";
                    }else{
                        $fn="B950";
                    }

                    if($_REQUEST['type']=='1BAY') {
                    echo    '<tr>
                                <td class="test-lenght1baya"><a class="thickbox" href="images/'.$fn.'/1bay_faceA.jpg"><h1>Face Length A</h1></td>
                                <td>
								<span id="face_length_span">
                                    <select name="face_length" onchange="getPriceOfProduct(this.form)">
                                        <option value="select">Select</option>
                                        <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                    </select>
									</span>
                                </td>
                                <td>
                                	<span id="errormsgfirstname">
                            			<img id="glass_a_err" src="img/iconCheckOff.gif">
                        			</span>
                                </td>
                            </tr>'; 
                }
                if($_REQUEST['type']=='2BAY'){
                    echo '<tr>
                            <td class="test-lenght2baya"><a class="thickbox" href="images/'.$fn.'/2bay_faceA.jpg"><h1>Face Length A</h1></a></td>
                            <td>
								<span id="face_length_a_span">
                                <select name="face_length_a" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
                                <span id="errormsgfirstname">
                            		<img id="glass_a_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayb"><a class="thickbox" href="images/'.$fn.'/2bay_faceB.jpg" ><h1>Face Length B</h1></a></td>
                            <td>
								<span id="face_length_b_span">
                                <select name="face_length_b" onchange="getPriceOfProduct(this.form)">
                                   <option value="select">Select</option>
                                   <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_b_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>';
                }
                if($_REQUEST['type']=='3BAY'){
                    echo '<tr>
                            <td class="test-lenght3baya"><a class="thickbox" href="images/'.$fn.'/3bay_faceA.jpg"><h1>Face Length A</h1></a></td>
                            <td>
							<span id="face_length_a_span">
                                <select name="face_length_a" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
                                <span id="errormsgfirstname">
                            		<img id="glass_a_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayb"><a class="thickbox" href="images/'.$fn.'/3bay_faceB.jpg" ><h1>Face Length B</h1></a></td>
                            <td>
								<span id="face_length_b_span">
                                <select name="face_length_b" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_b_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayc"><a class="thickbox" href="images/'.$fn.'/3bay_faceC.jpg" ><h1>Face Length C</h1></a></td>
                            <td>
							<span id="face_length_c_span">
                                <select name="face_length_c" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_c_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>';
                } if($_REQUEST['type']=='4BAY'){
                    echo '<tr>
                            <td class="test-lenght3baya"><a class="thickbox" href="images/'.$fn.'/4bay_faceA.jpg"><h1>Face Length A</h1></a></td>
                            <td>
							<span id="face_length_a_span">
                                <select name="face_length_a" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
                                <span id="errormsgfirstname">
                            		<img id="glass_a_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayb"><a class="thickbox" href="images/'.$fn.'/4bay_faceB.jpg"><h1>Face Length B</h1></a></td>
                            <td>
								<span id="face_length_b_span">
                                <select name="face_length_b" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_b_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>
                        <tr>
                            <td class="test-lenght4bayc"><a class="thickbox" href="images/'.$fn.'/4bay_faceC.jpg" ><h1>Face Length C</h1></a></td>
                            <td>
							<span id="face_length_c_span">
                                <select name="face_length_c" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_c_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr><tr>
                            <td class="test-lenght4bayd"><a class="thickbox" href="images/'.$fn.'/4bay_faceD.jpg"><h1>Face Length D</h1></a></td>
                            <td>
							<span id="face_length_d_span">
                                <select name="face_length_d" onchange="getPriceOfProduct(this.form)">
                                    <option value="select">Select</option>
                                    <option value="24">24"</option>
                                        <option value="30">30"</option>
                                        <option value="36">36"</option>
                                        <option value="42">42"</option>
                                        <option value="48">48"</option>
                                        <option value="54">54"</option>
                                        <option value="60">60"</option>
                                        <option value="66">66"</option>
										<option value="custom">Custom</option>
                                </select>
								</span>
                            </td>
                            <td>
					   			<span id="errormsgfirstname">
                            		<img id="glass_d_err" src="img/iconCheckOff.gif">
                        		</span>
                			</td>
                        </tr>';
                }?>
                </table>
            </td>
        </tr>
        <tr>
        	<td>
                <table id="hidetable2" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">
                	<tr>
                        <td colspan=3><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp2)</span>Options</center></h2></td>
                    </tr>
                    <tr>
                        <td>
                            <a class="thickbox" href="images/<?php echo $fn?>/End_panels.jpg"><h1>End Panels</h1></a>
                        </td>
                        <td>
                            <select class="option" id="end_options">
                                <option value="select">Select</option>
                                <option value="Both Closed End Panels">Both Ends</option>
                                <option value="Right Closed End Panel">Right End</option>
                                <option value="Left Closed End Panel">Left End</option>
                                <option value="No Closed End Panels">No Ends</option>
                            </select>
                        </td>
                        <td>
                            <span id="errormsgfirstname">
                                <img id="endpan_err" src="img/iconCheckOff.gif">
                            </span>
                        </td>
                    </tr>
                    <tr id="right_lenght">
                    <td class="test-lenght1bay" ><a class="thickbox" href='images/<?php echo $fn?>/Right_length.jpg' ><h1 style="margin-left:20px;">Right End</h1></a></td>
                    <td>
						<span id="right_length_span">
                        <select name="right_length" onchange="getPriceOfProduct(this.form)"> 
                            <option value="select">Select</option>
                            <option value="12">12"</option>
                            <option value="18">18"</option>
                            <option value="24">24"</option>
							<option value="custom">Custom</option>
                        </select>
						</span>
                    </td>
                    <td>
                    	<span id="errormsgfirstname">
                    		<img id="right_err" src="img/iconCheckOff.gif">
                		</span>
                    </td>
                </tr>
                <tr id="left_lenght">
                    <td class="test-lenght2baya"><a class="thickbox" href='images/<?php echo $fn?>/Left_length.jpg'><h1 style="margin-left:20px;">Left End</h1></a></td>
                    <td>
						<span id="left_length_span">
                        <select name="left_length" onchange="getPriceOfProduct(this.form)">
                            <option value="select">Select</option>
                            <option value="12">12"</option>
                            <option value="18">18"</option>
                            <option value="24">24"</option>
							<option value="custom">Custom</option>
                        </select>
						</span>
                    </td>
                    <td>
                    	<span id="errormsgfirstname">
                            <img id="left_err" src="img/iconCheckOff.gif">
                        </span>
                    </td>
                </tr>

                <tr>
                    <td class="test-light"><a class="thickbox" href="light.php?name=<?php echo $videoname;?>&type=adj&KeepThis=true&TB_iframe=true&height=780&width=840"><h1>Light Bar</h1></a></td>
                    <td>
                    	<select name="flange_covers" style="margin:4px;" onchange="getPriceOfProduct(this.form);"> 
                            <option value="select">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                        <!--input type="button" class="flange-covers-image thickbox" value="?" style="width:20px;margin: 0 4px;" onclick="javascript:window.location.href='flang.php?type=adj&KeepThis=true&TB_iframe=true&height=480&width=640'" disabled="disabled"/-->
                        <!-- <a style="width:20px;margin: 0 4px;float: right;" class="thickbox" href='light.php?type=adj&KeepThis=true&TB_iframe=true&height=480&width=640' disabled="disabled"><img src="images/flang.jpg" ></a>   -->
                        <!-- <input type="checkbox" name="flange_covers" value="0" style="margin:4px;" onclick="getPriceOfProduct(this.form);" disabled="disabled"/>     -->
                    </td>
                    <td>
                    	<span id="errormsgfirstname">
                            <img id="light_err" src="img/iconCheckOff.gif">
                        </span>
                    </td>
                </tr>
                <?php if($category_name=="B950"){
                        		$dsp="display:none";
                        	}else{
                        		$dsp="";
                        	}?>
                        <tr style="<?php echo $dsp;?>">
                        	
                            <td class="test-light"><a class="thickbox" href='light_bracket.php?type=adj&KeepThis=true&TB_iframe=true&height=780&width=840'><h1>Light Bracket</h1></a></td>
                            <td>
                                <select name="flange_covers_2" onchange="getPriceOfProduct(this.form)" style="width: 68px;">
                                	<option value="select">Select</option>
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>    
                            </td>
                            <td>
                            	<span id="errormsgfirstname">
                            		<img id="light_br_err" src="img/iconCheckOff.gif">
                        		</span>
                            </td>
                        </tr>

               <tr>
                <td colspan=2><a class="thickbox" href='images/Finishes.jpg'><h1>Post Finish</h1></a>
					<select name="choose_finish" style="margin:0" onchange="getPriceOfProduct(this.form)" >
                        <option value="SS">Brushed Stainless</option>
                        <option value="PC">Coated Black</option>
                    </select>
                </td>
                <td>
                	<span id="errormsgfirstname">
                        <img id="finish_err" src="img/iconCheckOff.gif">
                    </span>
                </td>
            </tr>
            </table>
			
							
		<?php
if($_REQUEST['type']=='2BAY'||$_REQUEST['type']=='3BAY'||$_REQUEST['type']=='4BAY') {
?>
				<div id="cart-form">
				<table id="showtable1" align="center" style="display:none" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px;">
                    <tr>
                        <td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;1)</span>Choose Option</center></h2></td>
                    </tr>
                   
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
				   <tr>
                        <td><a class="thickbox" href='light_bracket.php?type=adj&KeepThis=true&TB_iframe=true&height=780&width=840'><h1 style="text-decoration: none;">Corner Post Type</h1></a></td>
                        <td>
						<select name="posttype" style="width:117px;margin:0" onchange="getPriceOfProduct(this.form)" >
                            
                                <option value="inner">Inner</option>
                                <option value="outer">Outer</option>
                            </select>
                        </td>
						
						<td style="width:46px;">
                    <span id="errormsgfirstname">
                        <img id="post_type_err" src="img/iconCheckOff.gif">
                    </span>
                </td>
                    </tr>
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
				   <tr>
                        <td><a class="thickbox" href='light_bracket.php?type=adj&KeepThis=true&TB_iframe=true&height=780&width=840'><h1 style="text-decoration: none;">Corner Post Angle</h1></a></td>
                        <td>
                            
							<select name="degree" style="width:117px;margin:0;    font-size: 19px;" onchange="getPriceOfProduct(this.form)" >
                            
                                <option value="90degre">90 Degree</option>
                                <option value="135degre">135 Degree</option>
                            </select>
                        </td>
						
						<td style="width:46px;">
                    <span id="errormsgfirstname">
                        <img id="post_angle_err" src="img/iconCheckOff.gif">
                    </span>
                </td>
                    </tr>
					
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
					
					
					
					
                </table>
				</div>
				
				
				
				
				
				<div id="cart-form">
				<table id="showtable2" align="center" style="display:none" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px;">
                    <tr>
                        <td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;2)&nbsp; </span>Choose Corner Post</center></h2></td>
                    </tr>
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
					<?php
					if($_REQUEST['type']=='2BAY') {
					?>
                   <tr>
				  
				   <td colspan=2>
				   <label>
				   <input type="radio" name="corner_post" id="1stcenter" onchange="getPriceOfProduct(this.form)" value="1st Center Post from Left" checked>
				   <img src="images/B950S/2BAY/INNER90D1STPOST.jpg" style="width:90%;" id="postimg1" /></a> </label></td>
				  
				   </tr>
				   
				   
				   
				   
				   <?
					}
				   ?>
				   
				   
					<?php
					if($_REQUEST['type']=='3BAY') {
					?>
                   <tr>
				  
				   <td>
				   <label>
				   <input type="radio" name="corner_post" id="1stcenter" onchange="getPriceOfProduct(this.form)" value="1st Center Post from Left" checked>
				   <img src="images/B950S/4BAY/INNER90D1STPOST.jpg" style="width:90%;" id="postimg1" /></a></label></td>
				   
				   <td style="padding-left:5px;"><label>
				   <input type="radio" name="corner_post" id="2ndcenter" onchange="getPriceOfProduct(this.form)" value="2nd Center Post from Left">
				   <img src="images/B950S/4BAY/INNER90D2NDPOST.jpg" style="width:100%;" id="postimg2" /></a></label></td>
				   </tr>
				   <tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
				    
				   
				
				   
				   <?
					}
				   ?>
				   
				   
				   
					<?php
					if($_REQUEST['type']=='4BAY') {
					?>
                   <tr>
				  
				   <td>
				   <label>
				   <input type="radio" name="corner_post" id="1stcenter" onchange="getPriceOfProduct(this.form)" value="1st Center Post from Left" checked>
				   <img src="images/B950S/4BAY/INNER90D1STPOST.jpg" style="width:90%;" id="postimg1" /></a></label></td>
				   
				   
				   <td style="padding-left:15px;"><label>
				   <input type="radio" name="corner_post" id="2ndcenter" onchange="getPriceOfProduct(this.form)" value="2nd Center Post from Left">
				   <img src="images/B950S/4BAY/INNER90D2NDPOST.jpg" style="width:90%;" id="postimg2" /></a></label></td>
				   </tr>
				   
				   
				   <tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
				  <tr>
				   
				   <td colspan=2><label>
				   <input type="radio" name="corner_post" id="3rdcenter" onchange="getPriceOfProduct(this.form)" value="3rd Center Post from Left">
				   <img src="images/B950S/4BAY/INNER90D3RDPOST.jpg" style="width:50%;" id="postimg3" /></label></td>
				   
				   </tr>
				
				
				   <?
					}
				   ?>
				   
					<tr>
                        <td colspan=2 style="height:26px;"></td>
                    </tr>
					
					
                </table>
				</div>
				
	<style>				
	/* HIDE RADIO */
[type=radio] { 
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
}

/* IMAGE STYLES */
[type=radio] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
[type=radio]:checked + img {
  outline: 2px solid #d84747;
}				
	</style>			
			
				
				<style>
				table#cart-form h3 {
    text-decoration: none;
    padding-left: 0;
    font-size: 21px;
	text-align:center;
}

				table#forstarightpost h3 {
    text-decoration: none;
    padding-left: 0;
    font-size: 21px;
	text-align:center;
}
				h3 {
    color: #C7F900;
    font-size: 13px;
    padding: 4px;
    padding-left: 30px;
    text-align: left;
    /* padding-top: 10px; */
}
				table#showtable1 {
    border: 1px solid white;
    padding: 2px 0 2px 2px;
    width: 416px;
    background: #484844;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

table#showtable2 {
    border: 1px solid white;
    padding: 2px 0 2px 2px;
    width: 416px;
    background: #484844;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}
input[type="radio"] {
	width: 22px;
    height: 22px;
}
				</style>
				
<script>
$(document).ready(function() {
    $('#gotocornerpost').click(function() {
    $('#showtable1').toggle('slow');
    $('#showtable2').toggle('slow');
    $('#forstarightpost').toggle('slow');
	$('#hidetable1').toggle('hide');
	$('#hidetable2').toggle('hide');
	$('#hidetable3').toggle('hide');
	$('#forgotot').toggle('hide');
	
    });
})


$(document).ready(function() {
    $('#backtostraightpost').click(function() {
  $('#showtable1').toggle('slow');
    $('#showtable2').toggle('slow');
    $('#forstarightpost').toggle('slow');
	$('#hidetable1').toggle('hide');
	$('#hidetable2').toggle('hide');
	$('#hidetable3').toggle('hide');
	$('#forgotot').toggle('hide');
	
    });
})
</script>

<?
}
?>

			
			
			
			
			
        </td>
    </tr>
    
</table>

</td>
<td>
		<?php
if($_REQUEST['type']=='2BAY'||$_REQUEST['type']=='3BAY'||$_REQUEST['type']=='4BAY') {
?>

	
			<div id="cart-form" style="background: #484844;">
				
				<table id="forgotot" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px;">
                    <tr>
                        <td colspan=2><h3 style="text-align: left !important;"><a >
						<input type="checkbox" id="gotocornerpostcheck" onchange="getPriceOfProduct(this.form)" name="gotocornerpostcheck" value="Go To Corner Post" style="display:none;" >
						
						<span style="color:white; text-align:left;font-size: 30px;">&nbsp; 3)</span>
						<label for="gotocornerpostcheck" id="gotocornerpost">Go To Corner Post
						<span style="color:#e0946f; text-align:left !important;font-size: 33px;">&nbsp;New!</span>
						</label>
						
						</a>
						</h3></td>
                    </tr>
					</table>
					
				<table id="forstarightpost" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid white;border-radius: 5px; display:none;">
                    <tr>
                        <td colspan=2><center><h3 style="text-decoration: none;"><a id="backtostraightpost">
						<span style="color:white; text-align:left;font-size: 30px;">&nbsp;&nbsp;3)</span>
						
						<label for="gotocornerpostcheck" >Back To Straight Post</label></a></h3></center><br /></td>
                    </tr>
					</table>
					
				</div>

<?php
}

?>




<div class="test-Price" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">
    <table id="cart-form" class="price"> 
    	<tr>
        	
			<?php
			if($_REQUEST['type']=='1BAY') {
			?>
			<td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;3)</span>Quote</h2></center></td>
			
			<?php
			}
			else{
			?>
			
			<td colspan=2><center><h2 class="heading_all"><span style="float:left">&nbsp;&nbsp;4)</span>Quote</h2></center></td>
			<?php
			}
			?>
    	</tr>
        <tr>
            <td align="left">Left Post:</td><td id="left-post" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Right Post:</td><td id="right-post" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Transistions Post:</td><td id="trasition-post" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Light:</td><td id="flange-cover" align="right">0.00</td>
        </tr>
		<?php if($category_name=="B950") {?>
         <tr style="position: absolute;right: -44px;top: 0;z-index: 102;display:none;">
            <td align="left">Light Bracket:</td><td id="flange-cover2" align="right">0.00</td>
        </tr><?php }else{?><tr>
            <td align="left">Light Bracket:</td><td id="flange-cover2" align="right">0.00</td>
        </tr><?}?>
        <tr>
            <td align="left">Face Glass:</td><td id="face-glass" align="right">0.00</td>
        </tr>
        <?php if($category_name!="EP5" && $category_name!="EP15") {?>
        <tr>
            <td align="left">Left End Glass:</td><td id="left-Panel" align="right">0.00</td>
        </tr>
        <tr>
            <td align="left">Right End Glass:</td><td id="right-panel" align="right">0.00</td>
        </tr>
        <?php }?>
        <tr>
            <td colspan="2" style="padding:0px !important;background: #f4f4f4; height:1px"></td>       
        </tr>
        <tr>
            <td align="left">Total:</td><td id="total" align="right">0.00</td>
        </tr>
    </table>
</div>
<br />
<table id="cart-form" cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid white;border-radius: 5px;">

	<input type="hidden" id="c_glass_face" name="c_glass_face" value=""  />
        <input type="hidden" id="c_glass_face_val" name="c_glass_face_val" value=""  />
        
        <input type="hidden" id="c_glass_right" name="c_glass_right" value=""  />
        <input type="hidden" id="c_glass_right_val" name="c_glass_right_val" value=""  />
        
        <input type="hidden" id="c_glass_left" name="c_glass_left" value=""  />
        <input type="hidden" id="c_glass_left_val" name="c_glass_left_val" value=""  />
		
		<input type="hidden" id="c_glass_a" name="c_glass_a" value=""  />
        <input type="hidden" id="c_glass_a_val" name="c_glass_a_val" value=""  />
		
		<input type="hidden" id="c_glass_b" name="c_glass_b" value=""  />
        <input type="hidden" id="c_glass_b_val" name="c_glass_b_val" value=""  />
		
		<input type="hidden" id="c_glass_c" name="c_glass_c" value=""  />
        <input type="hidden" id="c_glass_c_val" name="c_glass_c_val" value=""  />
		
		<input type="hidden" id="c_glass_d" name="c_glass_d" value=""  />
        <input type="hidden" id="c_glass_d_val" name="c_glass_d_val" value=""  />
		
		
		 
		<input type="hidden" id="c_glass_a_light" name="c_glass_a_light" value=""  />
		<input type="hidden" id="c_glass_a_val_light" name="c_glass_a_val_light" value=""  />
		<input type="hidden" id="c_glass_b_light" name="c_glass_b_light" value=""  />
		<input type="hidden" id="c_glass_b_val_light" name="c_glass_b_val_light" value=""  />
		<input type="hidden"id="c_glass_c_light" name="c_glass_c_light" value=""  />
		<input type="hidden"id="c_glass_c_val_light" name="c_glass_c_val_light" value=""  />
		<input type="hidden"id="c_glass_d_light" name="c_glass_d_light" value=""  />
		<input type="hidden"id="c_glass_d_val_light" name="c_glass_d_val_light" value=""  />
		<input type="hidden" id="is_custom" name="is_custom" value=""  />
		
		<input type="hidden" id="post_type_val" name="post_type_val" value=""  />
		<input type="hidden" id="post_degree_val" name="post_degree_val" value=""  />
		
		
		<input type="hidden" id="product_type" name="product_type" value=""  />
    <tr>
        <!--td><h1>Add to Cart</h1></td-->
        <td colspan="2" align="center"><input type="hidden" name="type" value="<?=$_REQUEST['type']?>" />
			<input type="hidden" name="glass_face" value="0" id="glass-face" disabled="disabled"/><div id="products_ids"><?php
			echo "</div>";
           // echo tep_image_submit('button_in_cart.gif', IMAGE_BUTTON_IN_CART, "button");
        ?>
        <input type="image" onclick="return myFunction2(this.form)" button="" id="add" title=" Add to Cart " alt="Add to Cart" src="includes/languages/english/images/buttons/button_in_cart.gif" style="float: none;background: none !important;box-shadow: none;border: medium none;">
		 <input type="hidden" name="optionsid" id="optionsid" value="" disabled="disabled"/>
        </td>
    </tr>
</table>
 </td>
    </tr>
</table>
<?
}
?>
<script type="text/javascript">
    function myFunction2(form){
        if(myFunction(document.forms['cart_quantity'])){
            var bay=form.type.value;
				//alert($(".glass_a").css("top"));
		//alert($(".glass_a").css("left"));
		
        var var1=var2=var3=var4=var5=var6=var7=var8=var9=var10=var11="";
		var gotocornerpostss=$("input[name='gotocornerpostcheck']:checked").val();
		if(gotocornerpostss=="1")
		{
		var9=form.posttype.options[form.posttype.selectedIndex].text;
		var10=form.degree.options[form.degree.selectedIndex].text;
		var11=$("input[name='corner_post']:checked").val();
		//alert(var9);alert(var10);alert(var11);
		}
           // var var1=var2=var3=var4=var5=var6=var7=var8="";
            if(bay=="1BAY"){
                if(form.face_length!==undefined){
                    var1=form.face_length.options[form.face_length.selectedIndex].text;
                }else{
                    var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                }
            }else if(bay=="2BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
            }else if(bay=="3BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
                var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
            }else if(bay=="4BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
                var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
                var4=form.face_length_d.options[form.face_length_d.selectedIndex].text;
            }
            if(form.post_height!== undefined){
                var5=form.post_height.options[form.post_height.selectedIndex].text;  
            }
            if(form.right_length!== undefined){
                var6=form.right_length.options[form.right_length.selectedIndex].text;
            }
            if(form.left_length!== undefined){
                var7=form.left_length.options[form.left_length.selectedIndex].text;
            }
            end=$("input#glass-face").val();
            $.ajax({
                type: "POST",
                url: "includes/script1.php",
                data: { 
                    mod:category_name, bay:bay, face1:var1, face2:var2,face3:var3,face4:var4,post:var5,left:var7,right:var6,end:end,tot:tot1,osc:osc,im_id:im_id,sv:"save",img:img_ajx, ptype:var9, pdegree:var10, pposi:var11, corny:gotocornerpostss
                },
                cache: false,
                contentType: "application/x-www-form-urlencoded",
                success: function(data, textStatus, request){
                    //tb_show("","pop1.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
                    $("form[name='cart_quantity']").submit();
                },
                error: function (request, textStatus, errorThrown) {
                    alert("some error");
                }
            });
            return false;
        }else{
            return false;
        }
    }
function myFunction(form){
        ur=window.location.href;
        ur=ur.split("?");
        tp=ur[1].split("&")
        var check=true;
        var x='<center><img src="img/addToCartWindow.jpg" width="460px"></center>';
        x+='<ul style="margin-left:30px;">';
        if(form.type.value=="1BAY"){
            if(form.face_length.value=="select"){
                x+='<li>Face Length A <span style="color:red">?</span></li>';
                check=false;
            }
        }else if(form.type.value=="2BAY"){
            if(form.face_length_a.value=="select"){
                x+='<li>Face Length A <span style="color:red">?</span></li>';
                check=false;
            }
            if(form.face_length_b.value=="select"){
                x+='<li>Face Length B <span style="color:red">?</span></li>';
                check=false;
            }
            
        }else if(form.type.value=="3BAY"){
            if(form.face_length_a.value=="select"){
                x+='<li>Face Length A <span style="color:red">?</span></li>';
                check=false;
            }
            if(form.face_length_b.value=="select"){
                x+='<li>Face Length B <span style="color:red">?</span></li>';
                check=false;
            }
            if(form.face_length_c.value=="select"){
                x+='<li>Face Length C <span style="color:red">?</span></li>';
                check=false;
            }
        }else if(form.type.value=="4BAY"){
            if(form.face_length_a.value=="select"){
                x+='<li>Face Length A <span style="color:red">?</span></li>';
                check=false;
            }
            if(form.face_length_b.value=="select"){
                x+='<li>Face Length B <span style="color:red">?</span></li>';
                check=false;
            }
            if(form.face_length_c.value=="select"){
                x+='<li>Face Length C <span style="color:red">?</span></li>';
                check=false;
            }
            if(form.face_length_d.value=="select"){
                x+='<li>Face Length D <span style="color:red">?</span></li>';
                check=false;
            }
        }
        if($('#end_options').val()=="select"){
            x+='<li>End Panels <span style="color:red">?</span></li>';
            check=false;
        }
        if(form.glass_face.value==1){
            if(form.right_length.value=="select"){
                x+='<li>Right Length <span style="color:red">?</span></li>';
                check=false;
            }
            if(form.left_length.value=="select"){
                x+='<li>Left Length <span style="color:red">?</span></li>';
                check=false;
            }
        }else if(form.glass_face.value==2){
            if(form.right_length.value=="select"){
                x+='<li>Right Length <span style="color:red">?</span></li>';
                check=false;
            }
        }else if(form.glass_face.value==3){
            if(form.left_length.value=="select"){
                x+='<li>Left Length <span style="color:red">?</span></li>';
                check=false;
            }
        }else if(form.glass_face.value==4){

        } 
        if(form.flange_covers_2.value=="select" && tp[0]=="id=81"){
            x+='<li>Light Bracket <span style="color:red">?</span></li>';
            check=false;
        }

        if(form.flange_covers.value=="select"){
            x+='<li>Light Bar <span style="color:red">?</span></li>';
            check=false;
        }
        if(form.choose_finish.value=="select"){
            x+='<li>Post Finish <span style="color:red">?</span></li>';
            check=false;
        }
        x+='</ul>';
        if(!check){
            var elem = $(this).closest('.item');
        
            $.confirm({
            
    
                'title'     : 'More Information Is Needed.....',
                'message'   :x,
                'buttons'   : {
                    'OK'   : {
                        'class' : 'blue',
                        'action': function(){
                        }
                    }
                }
            });
            return false;
        }else{
            //javascript:document.forms['cart_quantity'].submit();
            return true;
        }

        
    }

function getBeforeChar(str){
	var f_str=str.substr(0, str.indexOf('-')); 
	if(f_str==""){
		return str.substr(0, str.indexOf('"'));;
	}else { return f_str; }
}
function getAfterChar(str){
	var f_str=str.substring(str.lastIndexOf("-")+1,str.lastIndexOf('"'));
	if(isInt(f_str)){ return ''; }else { return f_str;}

}
function isInt(value) {
   return !isNaN(value) && parseInt(value) == value;
}
function getTotal(n1,n2,f_n1,f_n2){
 if(f_n1=="" && f_n2==""){
	var t=n1+n2+2+'"';}else{var t=n1+n2+2;}
	if(f_n1=='1/4'&&f_n2=='1/4'){
		t+='-1/2"';
	}
	if(f_n1=='1/4'&&f_n2=='1/2'){
		t+='-3/4"';
	}
	if(f_n1=='1/4'&&f_n2=='3/4'){
		t+=1;
		t+='"';
	}
	if(f_n1=='1/2'&&f_n2=='1/4'){
		t+='-3/4"';
	}
	if(f_n1=='1/2'&&f_n2=='1/2'){
		t+=1;
		t+='"';
	}
	if(f_n1=='1/2'&&f_n2=='3/4'){
		t+=1;
		t+='-1/4"';
	}
	if(f_n1=='3/4'&&f_n2=='1/4'){
		t+=1;
		t+='"';
	}
	if(f_n1=='3/4'&&f_n2=='1/2'){
		t+=1;
		t+='-1/4"';
	}
	if(f_n1=='3/4'&&f_n2=='3/4'){
		t+=1;
		t+='-1/2"';
	}
	//alert(f_n1);
	if(f_n1==""&&f_n2!=""){ t+="-"+f_n2+'"'; } 
	if(f_n2==""&&f_n1!=""){ t+="-"+f_n1+'"'; } 
	return t;
	
}
function getTotal3Bay(n1,n2,n3,f_n1,f_n2,f_n3){
	if(f_n1==""&&f_n2==""&&f_n3==""){
		var t=n1+n2+n3+2+'"';
	} else{
	var t=n1+n2+n3+2;
	}
	var t=getTotal(n1,n2,f_n1,f_n2);
	
	var new1=getBeforeChar(t);
	new1-=2;
	var far=getAfterChar(t);
	var t=getTotal(new1,n3,far,f_n3);
	return t;
	
}
function getTotal3(n1,n2,n3,n4,f_n1,f_n2){
	var t=n1+n2+n3+n4+2;
	if(f_n1=='1/4'&&f_n2=='1/4'){t+='-1/2"';}
	if(f_n1=='1/4'&&f_n2=='1/2'){t+='-3/4"';}
	if(f_n1=='1/4'&&f_n2=='3/4'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='1/4'){t+='-3/4"';}
	if(f_n1=='1/2'&&f_n2=='1/2'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='3/4'){t+='-5/4"';}
	if(f_n1=='3/4'&&f_n2=='1/4'){t+=1;t+='"';}
	if(f_n1=='3/4'&&f_n2=='1/2'){t+=1;t+='-1/4"';}
	if(f_n1=='3/4'&&f_n2=='3/4'){t+=1;t+='-1/2"';}
	if(f_n1==""&&f_n2!=""){ t+="-"+f_n2+'"'; } 
	if(f_n2==""&&f_n1!=""){ t+="-"+f_n1+'"'; } 
	
	return t;}
		
	function getTotal6(n1,n2,f_n1,f_n2){
	var t=n1+n2+2;
	if(f_n1=='1/4'&&f_n2=='1/4'){t+='-1/2"';}
	if(f_n1=='1/4'&&f_n2=='1/2'){t+='-3/4"';}
	if(f_n1=='1/4'&&f_n2=='3/4'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='1/4'){t+='-3/4"';}
	if(f_n1=='1/2'&&f_n2=='1/2'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='3/4'){t+=1;t+='-1/4"';}
	if(f_n1=='3/4'&&f_n2=='1/4'){t+=1;t+='"';}
	if(f_n1=='3/4'&&f_n2=='1/2'){t+=1;t+='-1/4"';}
	if(f_n1=='3/4'&&f_n2=='3/4'){t+=1;t+='-1/2"';}
	
	if(f_n1==""&&f_n2!=""){ t+="-"+f_n2+'"'; } 
	if(f_n2==""&&f_n1!=""){ t+="-"+f_n1+'"'; } 

	
	return t;}
	
	
	function getTotal62(n1,n2,n4,f_n1,f_n2){
	var t=n1+n2+n4+2;
	if(f_n1=='1/4'&&f_n2=='1/4'){t+='-1/2"';}
	if(f_n1=='1/4'&&f_n2=='1/2'){t+='-3/4"';}
	if(f_n1=='1/4'&&f_n2=='3/4'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='1/4'){t+='-3/4"';}
	if(f_n1=='1/2'&&f_n2=='1/2'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='3/4'){t+=1;t+='-1/4"';}
	if(f_n1=='3/4'&&f_n2=='1/4'){t+=1;t+='"';}
	if(f_n1=='3/4'&&f_n2=='1/2'){t+=1;t+='-1/4"';}
	if(f_n1=='3/4'&&f_n2=='3/4'){t+=1;t+='-1/2"';}
	
	if(f_n1==""&&f_n2!=""){ t+="-"+f_n2+'"'; } 
	if(f_n2==""&&f_n1!=""){ t+="-"+f_n1+'"'; } 

	
	return t;}
	
function getTotal63(n1,n2,f_n1,f_n2){
	var t=n1+2;
	if(f_n1=='1/4'&&f_n2=='1/4'){t+='-1/2"';}
	if(f_n1=='1/4'&&f_n2=='1/2'){t+='-3/4"';}
	if(f_n1=='1/4'&&f_n2=='3/4'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='1/4'){t+='-3/4"';}
	if(f_n1=='1/2'&&f_n2=='1/2'){t+=1;t+='"';}
	if(f_n1=='1/2'&&f_n2=='3/4'){t+=1;t+='-1/4"';}
	if(f_n1=='3/2'&&f_n2=='1/4'){t+=1;t+='-3/4"';}
	if(f_n1=='3/2'&&f_n2=='3/4'){t+=2;t+='-1/4"';}
	if(f_n1=='3/2'&&f_n2=='1/2'){t+=2;}
	if(f_n1=='3/4'&&f_n2=='1/4'){t+=1;t+='"';}
	if(f_n1=='3/4'&&f_n2=='1/2'){t+=1;t+='-1/4"';}
	if(f_n1=='3/4'&&f_n2=='3/4'){t+=1;t+='-1/2"';}
	
	if(f_n1==""&&f_n2!=""){ t+="-"+f_n2+'"'; } 
	if(f_n2==""&&f_n1!=""){ t+="-"+f_n1+'"'; } 

	
	return t;}

function getTotal2(n1,n2,n3,n4,f_n1,f_n2,f_n3,f_n4){
if(f_n1==""&&f_n2==""&&f_n3==""&&f_n4==""){

		var t=n1+n2+n3+n4+2+'"';

	}else{var t=n1+n2+n3+n4+2;}
 if(f_n1==""&&f_n2==""&&f_n3==""&&f_n4!=""){t+="-"+f_n4+'"';} 
 if(f_n1==""&&f_n2==""&&f_n3!=""&&f_n4==""){ t+="-"+f_n3+'"'; }
 if(f_n1==""&&f_n2!=""&&f_n3==""&&f_n4==""){ t+="-"+f_n2+'"'; }     
 if(f_n1!=""&&f_n2==""&&f_n3==""&&f_n4==""){ t+="-"+f_n1+'"'; }      
if(f_n1!=""&&f_n2==""&&f_n3==""&&f_n4!=""){ var t='';t+=getTotal3(n1,n2,n3,n4,f_n1,f_n4); } 
if(f_n1!=""&&f_n2!=""&&f_n3==""&&f_n4==""){ var t='';t+=getTotal3(n1,n2,n3,n4,f_n1,f_n2); } 
if(f_n1!=""&&f_n2==""&&f_n3!=""&&f_n4==""){ var t=''; t+=getTotal3(n1,n2,n3,n4,f_n1,f_n3); }
if(f_n1!=""&&f_n2==""&&f_n3==""&&f_n4!=""){ var t='';t+=getTotal3(n1,n2,n3,n4,f_n1,f_n4); } 
if(f_n1==""&&f_n2!=""&&f_n3!=""&&f_n4==""){ var t='';t+=getTotal3(n1,n2,n3,n4,f_n2,f_n3); } 
if(f_n1==""&&f_n2!=""&&f_n3==""&&f_n4!=""){ var t='';t+=getTotal3(n1,n2,n3,n4,f_n2,f_n4); } 
if(f_n1==""&&f_n2==""&&f_n3!=""&&f_n4!=""){ var t='';t+=getTotal3(n1,n2,n3,n4,f_n3,f_n4); } 
 
if(f_n1==""&&f_n2!=""&&f_n3!=""&&f_n4!=""){ 
     t=getTotal6(n2,n3,f_n2,f_n3); 
     var new1=getBeforeChar(t);
	new1-=2;
	var far=getAfterChar(t);
	var t=getTotal62(new1,n4,n1,far,f_n4);
	return t;
} if(f_n1!=""&&f_n2==""&&f_n3!=""&&f_n4!=""){ 
     t=getTotal6(n1,n3,f_n1,f_n3); 
     var new1=getBeforeChar(t);
	new1-=2;
	var far=getAfterChar(t);
	var t=getTotal62(new1,n4,n2,far,f_n4);
	return t;
} if(f_n1!=""&&f_n2!=""&&f_n3==""&&f_n4!=""){ 
       t=getTotal6(n1,n2,f_n1,f_n2); 
   var new1=getBeforeChar(t);
	new1-=2;
	var far=getAfterChar(t);
	var t=getTotal62(new1,n4,n3,far,f_n4);
	return t;}
if(f_n1!=""&&f_n2!=""&&f_n3!=""&&f_n4==""){ 
     t=getTotal6(n1,n2,f_n1,f_n2); 
     var new1=getBeforeChar(t);
	new1-=2;
	var far=getAfterChar(t);
	var t=getTotal62(new1,n3,n4,far,f_n3);
	return t;
} 
if(f_n1!=""&&f_n2!=""&&f_n3!=""&&f_n4!=""){ 
     t=getTotal6(n1,n2,f_n1,f_n2); 
     var new1=getBeforeChar(t);
	new1-=2;
	var far=getAfterChar(t);
	var t=getTotal62(new1,n3,n4,far,f_n3);
	var new1=getBeforeChar(t);
	new1-=2;
	var far=getAfterChar(t);
	var t=getTotal63(new1,n4,far,f_n4);
	return t;
} 
return t;

	

}
function getTotal4Bay(n1,n2,n3,n4,f_n1,f_n2,f_n3,f_n4){

	if(f_n1==""&&f_n2==""&&f_n3==""&&f_n4==""){

		var t=n1+n2+n3+n4+2;

	}
	 var t=getTotal2(n1,n2,n3,n4,f_n1,f_n2,f_n3,f_n4);


	return t;
}






</script>

<div class="item" style="position:absolute;visibility:hidden;">
	       
           <div class="delete" style="visibility:visible"></div>
		   <div class="delete1" style="visibility:hidden"></div>
		   
        </div>