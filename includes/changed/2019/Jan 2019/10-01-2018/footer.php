<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require(DIR_WS_INCLUDES . 'counter.php');
?>
</td>


<script>
history.pushState(null, null, window.location);
window.addEventListener('popstate', function () {
    alert("Our website prevents the back button action from working to keep integrity of shopping carts, please use the tabs at the top of page to navigate.");
    history.pushState(null, null, window.location);
});
</script> 

<script type="text/javascript" src="./dist/html2canvas.js"></script>
<script>
  function abc(is){
    if(myFunction(document.forms['cart_quantity'])){
		$("body").append("<div id='TB_load'><img src='"+tb_pathToImage+"'></div>");//add loader to the page
        $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
        $('#TB_load').show();
        $("#TB_overlay").addClass("TB_overlayBG");
        html2canvas(document.getElementsByClassName("mainTable2")).then(function(canvas) {
            var pic=canvas.toDataURL("image/jpeg");
            pic = pic.replace(/^data:image\/(png|jpg);base64,/, "");
            $.ajax({
                type: "POST",
                url: "includes/script.php",
                data: { 
                    img: pic
                },
                cache: false,
                contentType: "application/x-www-form-urlencoded",
                success: function(data, textStatus, request){
                tb_show("","pop.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
                //$("#scrn").href="img/screenshot/scrn.jpg";
                // var link = document.createElement('a');
                // link.className="thickbox";
                // //alert(link.className);
                // link.href = "img/screenshot/scrn.jpg";
                // document.body.appendChild(link);
                // link.click();
            },
            error: function (request, textStatus, errorThrown) {
                alert("some error");
            }
        })
        });
    }
  }
  function layout(){
    if(myFunction(document.forms['cart_quantity'])){
		$("body").append("<div id='TB_load'><img src='"+tb_pathToImage+"'></div>");//add loader to the page
        $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
        $('#TB_load').show();
        $("#TB_overlay").addClass("TB_overlayBG");
        var form=document.forms['cart_quantity'];
        var bay=form.type.value;
		
		//alert($(".glass_a").css("top"));
		//alert($(".glass_a").css("left"));
		
        var var1=var2=var3=var4=var5=var6=var7=var8=var9=var10=var11="";
		var gotocornerpostss=$("input[name='gotocornerpostcheck']:checked").val();
		if(gotocornerpostss=="1")
		{
		var9=form.posttype.options[form.posttype.selectedIndex].text;
		var10=form.degree.options[form.degree.selectedIndex].text;
		var11=$("input[name='corner_post']:checked").val();
		
		}
		//alert(var9);alert(var10);alert(var11);
        if(bay=="1BAY"){
            if(form.face_length!==undefined){
                var1=form.face_length.options[form.face_length.selectedIndex].text;
            }else{
        
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            }
      
        }else if(bay=="2BAY"){
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
      
        }else if(bay=="3BAY"){
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
            var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
      
        }else if(bay=="4BAY"){
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
            var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
            var4=form.face_length_d.options[form.face_length_d.selectedIndex].text;
      
        }
        if(form.post_height!== undefined){
            var5=form.post_height.options[form.post_height.selectedIndex].text;  
        }
        if(form.right_length!== undefined){
            var6=form.right_length.options[form.right_length.selectedIndex].text;
        }
        if(form.left_length!== undefined){
            var7=form.left_length.options[form.left_length.selectedIndex].text;
        }
    
        end=$("input#glass-face").val();
        $.ajax({
            type: "POST",
            url: "includes/script1.php",
            data: { 
                mod:category_name, bay:bay, face1:var1, face2:var2,face3:var3,face4:var4,post:var5,left:var7,right:var6,end:end,tot:tot1,osc:"",im_id:"",sv:"",img:img_ajx, ptype:var9, pdegree:var10, pposi:var11, corny:gotocornerpostss
            },
            cache: false,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, textStatus, request){
                tb_show("","pop1.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
            },
            error: function (request, textStatus, errorThrown) {
                alert("some error");
            }
        });
    }
  }
  function layout1(){
    if(myFunction(document.forms['cart_quantity'])){
		$("body").append("<div id='TB_load'><img src='"+tb_pathToImage+"'></div>");//add loader to the page
        $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
        $('#TB_load').show();
        $("#TB_overlay").addClass("TB_overlayBG");
        var form=document.forms['cart_quantity'];
        if(category_name=="ALLIN1"){
            var1=form.optn.options[form.optn.selectedIndex].text;
            end="";
        }else{
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;;
            end=$("input#glass-face").val();
        }
    
        $.ajax({
            type: "POST",
            url: "includes/script1.php",
            data: { 
                mod:category_name, bay:"", face1:var1, face2:"",face3:"",face4:"",post:"",left:"",right:"",end:end,tot:tot1,osc:"",im_id:"",sv:"",img:img_ajx
            },
            cache: false,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, textStatus, request){
                tb_show("","pop1.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
            },
            error: function (request, textStatus, errorThrown) {
                alert("some error");
            }
        });
    }
  }
</script>




<?php

if (!$detect->isMobile())
{

 
    $folderName=$category_name;
      switch ($category_name){
           case 'B950':{
               $folderName="B950";
               break;
           }
           case 'B950 SWIVEL':{
                $folderName="B950-Swivel";
                break;
            }
      }
      
    if(isset($_REQUEST['cPath'])){
        $cpath_list=explode("_", $_REQUEST['cPath']);
        $sql="select count(*) as total from ".TABLE_CATEGORIES." where parent_id=".$cpath_list[0];
        $count=mysql_query($sql) or die("Error in count category warsi ".mysql_error());
        $count=mysql_fetch_array($count);
        $count=$count['total']; 
        
        if($count>3){
        if(count($cpath_list)>=1){
            echo '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign="top" align="left">
            
                    <div id="selected-item">'; ?>
                    
                     <ul class="head-table" id="message_wrt" style="height:0px;display:none;">
       <li id="item-text" style="margin:0;padding:0;"><div class="message_p"><div  class="message_wrt1" id="message_wrt1" ></div><div  class="message_wrt2" id="message_wrt2" ></div><div  class="message_wrt3" id="message_wrt3" ></div></div>
               </ul>
                    <ul class="head-table" style="min-height: 456px;" id="product_image">
                        <?php
                            if($category_image==''){
                            //DIE('No Images');
                                /*$sql="select c.categories_image as ci, cd.categories_name as cdn from ".TABLE_CATEGORIES." as c, ".TABLE_CATEGORIES_DESCRIPTION." as cd where c.parent_id=".$cpath_list[0]." and c.categories_id=cd.categories_id";
                                $result=mysql_query($sql) or die(" Error in Search Images Warsi ".mysql_error());
                                $rows=mysql_num_rows($result);
                                while($row=mysql_fetch_array($result)){*/
                                    echo '<li id="additional_image" style="overflow:hidden;height:450px;border-style:solid;border-width:2px;border-color:white;"><img src="img/index_page.jpg" style="width:100%"/><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/2.jpg"><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/3.jpg"><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/4.jpg"><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/5.jpg"><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/6.jpg"><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/7.jpg"><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/8.jpg"><br style="clear:both;"/></li>';
                                    // echo '<li id="additional_image"><img src="images/top_images/9.jpg"><br style="clear:both;"/></li>';
                                    
                                /*}*/
                            }
                            else{
                                echo '<li id="additional_image"><img src="images/'.$folderName.'/'.$category_image.'" style="width:100%"/><br style="clear:both;"/></li>';
                            }
                        ?>
                        <br style="clear:both;"/>
                    </ul>
                   
        <?php echo '</div>
                </td>';
        }
       }
       
    }
    else if(isset($_REQUEST['type'])){
        if($category_name=="B950"){
            $fm="B-950";
        }else if($category_name=="B950 SWIVEL"){
            $fm="B-950-SWIVEL";
        }ELSE{
            $fm=$category_name;
        }
       include(DIR_WS_MODULES."forms/".FILENAME_PRODUCT_CSS);
        echo '<td valign="top" align="left">
                <div id="selected-item">
                

                    <ul class="head-table">
                        <li id="additional_image">';
                            if(is_file('images/'.$folderName.'/'.$_REQUEST['type'].'.jpg')){
                               ///old
							   //echo '<img src="images/'.$folderName.'/'.$_REQUEST['type'].'s.jpg" style="width:568px;height:453px;">';
                                //new
								echo '';
                                echo '<div class="glass">A</div><div class="glass_a">A</div><div class="glass_b">B</div><div class="glass_c">C</div><div class="glass_d">D</div><div class="total">Total"</div><div class="post">Height</div>';
                            }else{
                                if($folderName!='B-950-SWIVEL'){
                                    //
									//echo '<img src="images/'.$folderName.$_REQUEST['type'].'/START.jpg" style="width:100%">';
                                    //
									echo '';
                                }
                                else{
                                    //old
									//echo '<img src="images/'.$folderName.'/START.jpg" style="width:100%">';   
                                    //new
									echo '<img src="images/'.$folderName.'/START.jpg" style="width:100%">';   
                                }
                            }

                        echo '</li>
                        <br style="clear:both;"/>
                    </ul>
                    <ul class="head-table" style="border-top:1px solid #888;height:170px;">
                        <li id="item-text" style="margin:0;padding:0;">
                            <table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:580px;" align="center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a onclick="abc(this);" style="height:55px;width:55px;cursor:pointer;" >
                                                <img src="img/saveQuote_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a style="height:55px;width:55px;cursor:pointer;" onclick="layout()" >
                                                <img src="img/saveLayout_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a class="thickbox" href="images/'.$fm.'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$fm.'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a href="PDF/'.$fm.'.pdf" style="height:55px;width:55px;" target="_blank" >
                                                <img src="images/pdf-icon.gif" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" onclick="wishlist()" style="height:55px;width:55px;" >
                                                <img src="images/wishlist_icon.png" title="Save for later" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td  style="display:none;">
                                            <!--<a class="thickbox" href="images/'.$category_name.'/mydrw.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$category_name.'/mydr.png" style="height:55px;width:55px;" >
                                            </a>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Save quote</b></td>
                                        <td><b>Save layout</b></td>
                                        <td><b>Post Dimensions</b></td>
                                        <td><b>PDF Spec Sheet</b></td>
                                        <td><b>Save for later</b></td>
                                        <td style="display:none;">
                                            <b>My Shop Drawing</b>
                                        </td>
                                    </tr>
                                   
                                </tbody>
                            </table>
							<table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:580px;" align="center">
                                <tbody>
								 <tr style="height:70px;">
										
                                        <td colspan=4>
                                            <a class="thickbox" href="img/'.$fm.'.jpg" style="height:55px;width:55px;">
                                                <img src="img/needHelpButton.png">
                                            </a>
                                        </td>';
										//ss
										$bay=$_REQUEST['type'];
										$cate_id=$_REQUEST['id'];
										//if($cate_id=="79")
										//{
										//}
										//else{
										echo'<td><a href="images/'.$category_name.'/'.$category_name.'_'.$bay.'_revit.rvt" style="height:55px;width:55px;">
                                                <img src="images/'.$category_name.'/Rivit.jpg" style="width:84px;">
                                            </a>
										</td>';
										//}
                                    echo'</tr>
									
							</tbody>
                            </table>
                        </li>
                   </ul>
                </div>
            </td>';
    }
    else if(isset($_REQUEST['Model']) && isset($_REQUEST['id']) ){
         include(DIR_WS_MODULES."forms/".FILENAME_PRODUCT_CSS);
            echo '<td valign="top" align="left">
                <div id="selected-item">
                
                    <ul class="head-table">
                        <li id="additional_image" style="position:relative">';
                        if($_REQUEST['Model']=="EP-950-ACRYLIC"){
                            echo '<div class="msg_red" style="position:absolute;top:222px;left:20px;"><i>Fully Adjustable<br><br>Multi Functional<br><br>Indoor and Outdoor Use<br><br>Easy Disassembly for Storage,<br><br>Ready for Shipping</i></div>';
                        }
                        if($_REQUEST['Model']=="B-950P-GLASS"){
                            echo '<div class="msg_red" style="position:absolute;top:300px;left:20px;"><i>Fully Adjustable<br>Multi Functional<br>Indoor and Outdoor Use<br>Easy Disassembly for Storage,<br>Ready for Shipping</i></div>';
                        }
                        if($_REQUEST['Model']=="ALLIN1"){
                            echo '<img src="images/'.$_REQUEST['Model'].'/STARTMAIN.jpg" style="width:100%"/>
                        </li>
                        <br style="clear:both;"/>
                    </ul>
                    <ul class="head-table" style="border-top:1px solid #888;height:170px;">
                      <li id="item-text" style="margin:0;padding:0;">
                        <table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:580px;" align="center">
                            <tbody>
                                <tr>
                                    <td>
                                        <a onclick="abc(this);" style="height:55px;width:55px;cursor:pointer;" >
                                            <img src="img/saveQuote_button.jpg" style="height:55px;width:55px;" >
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="layout1();" style="height:55px;width:55px;cursor:pointer;" >
                                            <img src="img/saveLayout_button.jpg" style="height:55px;width:55px;" >
                                        </a>
                                    </td>
                                    <td>
                                        <a class="thickbox" href="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                            <img src="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                        </a>
                                    </td>
                                    <td>
                                        <a href="PDF/'.$category_name.'.pdf" target="_blank" style="height:55px;width:55px;" >
                                            <img src="images/pdf-icon.gif" style="height:55px;width:55px;">
                                        </a>
                                    </td>
                                    <td>
                                            <a href="javascript:void(0)" onclick="wishlist()" style="height:55px;width:55px;" >
                                                <img src="images/wishlist_icon.png" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                    <td style="display:none;">
                                        <!--<a class="thickbox" href="images/'.$category_name.'/mydrw.jpg" style="height:55px;width:55px;" >
                                            <img src="images/'.$category_name.'/mydr.png" style="height:55px;width:55px;" >
                                        </a>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Save quote</b></td>
                                    <td><b>Save layout</b></td>
                                    <td><b>Post Dimensions</b></td>
                                    <td><b>PDF Spec Sheet</b></td>
                                    <td><b>Save for leter</b></td>
                                    <td style="display:none;"><b>My Shop Drawing</b></td>
                                </tr>
                                
                            </tbody>
                        </table>
						<table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:580px;" align="center">
                                <tbody>
								<tr style="height:85px;">
								
                                    <td colspan=5>
                                        <a class="thickbox" href="img/'.$category_name.'.jpg" style="height:55px;width:55px;">
                                            <img src="img/needHelpButton.png">
                                        </a>
                                    </td>';
										//ss
										$bay=$_REQUEST['type'];
										$cate_id=$_REQUEST['id'];
										//if($cate_id=="79")
										//{
										//}
										//else{
										echo'<td><a href="images/'.$category_name.'/'.$category_name.'_'.$bay.'_revit.rvt" style="height:55px;width:55px;">
                                                <img src="images/'.$category_name.'/Rivit.jpg" style="width:84px;">
                                            </a>
										</td>';
										//}
                                    echo'</tr>
									
							</tbody>
                            </table>
                        </li></tbody></table></li>
                   </ul>
                </div>
            </td>';
                        } else{

                    echo '<img src="images/'.$_REQUEST['Model'].'/START.jpg" style="width:100%"/>
                        </li>
                        <br style="clear:both;"/>
                    </ul>
                    <ul class="head-table" style="border-top:1px solid #888;height:170px;">
                        <li id="item-text" style="margin:0;padding:0;">
                            <table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:580px;" align="center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a onclick="abc(this);" style="height:55px;width:55px;cursor:pointer;" >
                                                <img src="img/saveQuote_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a onclick="layout1();" style="height:55px;width:55px;cursor:pointer;" >
                                                <img src="img/saveLayout_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a class="thickbox" href="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a href="PDF/'.$category_name.'.pdf" target="_blank" style="height:55px;width:55px;" >
                                                <img src="images/pdf-icon.gif" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" onclick="wishlist()" style="height:55px;width:55px;" >
                                                <img src="images/wishlist_icon.png" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td style="display:none;">
                                            <!--<a class="thickbox" href="images/'.$category_name.'/mydrw.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$category_name.'/mydr.png" style="height:55px;width:55px;" >
                                            </a>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Save quote</b></td>
                                        <td><b>Save layout</b></td>
                                        <td><b>Post Dimensions</b></td>
                                        <td><b>PDF Spec Sheet</b></td>
                                        <td><b>Save for leter</b></td>
                                        <td style="display:none;"><b>My Shop Drawing</b></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
							<table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:580px;" align="center">
                                <tbody>
								<tr style="height:85px;">
								
                                        <td colspan="4">
                                            <a class="thickbox" href="img/'.$category_name.'.jpg" style="height:55px;width:55px;">
                                                <img src="img/needHelpButton.png">
                                            </a>
                                        </td>';
										//ss
										$bay=$_REQUEST['type'];
										$cate_id=$_REQUEST['id'];
										//if($cate_id=="79")
										//{
										//}
										//else{
										echo'<td><a href="images/'.$category_name.'/'.$category_name.'_'.$bay.'_revit.rvt" style="height:55px;width:55px;">
                                                <img src="images/'.$category_name.'/Rivit.jpg" style="width:84px;">
                                            </a>
										</td>';
										//}
                                    echo'</tr>
									
							</tbody>
                            </table>
                        </li>
                    </tbody>
                    </table></li>
                   </ul>
                </div>
            </td>';
        } }
    if(isset($_REQUEST['Model']) && isset($_REQUEST['cPath'])&&!isset($_REQUEST['var'])){
             include(DIR_WS_MODULES."forms/".FILENAME_PRODUCT_CSS);
             //if($_REQUEST['Model']==EP11){

             //}else{
               // echo '<td valign="top" align="left">
                 //   <div id="selected-item">
                   //     <ul class="head-table" style="border-top:1px solid #888;height:80px;">
                     //       <li><h1 id="textarea56" style="padding-left: 0;"> '.$_REQUEST['Model'].'<br>126" and larger, please call so we may better assist you (1-800-690-0002)</h1></li>
                       // </ul>
                        //<ul class="head-table">
                          //  <li id="additional_image">
                            //    <img src="images/'.$_REQUEST['Model'].'/START.jpg" style="width:100%;width:568px;height:453px"/>
                            //</li>
                            //<br style="clear:both;"/>
                        //</ul>
                    //</div>
                //</td>';
             //}
            
        }
		
}
else{
$folderName=$category_name;
      switch ($category_name){
           case 'B950':{
               $folderName="B950";
               break;
           }
           case 'B950 SWIVEL':{
                $folderName="B950-Swivel";
                break;
            }
      }
      
    
   
   
    if(isset($_REQUEST['type'])){
        if($category_name=="B950"){
            $fm="B-950";
        }else if($category_name=="B950 SWIVEL"){
            $fm="B-950-SWIVEL";
        }ELSE{
            $fm=$category_name;
        }
       include(DIR_WS_MODULES."forms/".FILENAME_PRODUCT_CSS);
	  echo'						
                            <table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:848px;" align="center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a onclick="abc(this);" style="height:55px;width:55px;cursor:pointer;" >
                                                <img src="img/saveQuote_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a style="height:55px;width:55px;cursor:pointer;" onclick="layout()" >
                                                <img src="img/saveLayout_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a class="thickbox" href="images/'.$fm.'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$fm.'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a href="PDF/'.$fm.'.pdf" style="height:55px;width:55px;" target="_blank" >
                                                <img src="images/pdf-icon.gif" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" onclick="wishlist()" style="height:55px;width:55px;" >
                                                <img src="images/wishlist_icon.png" title="Save for later" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td  style="display:none;">
                                            <!--<a class="thickbox" href="images/'.$category_name.'/mydrw.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$category_name.'/mydr.png" style="height:55px;width:55px;" >
                                            </a>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Save quote</b></td>
                                        <td><b>Save layout</b></td>
                                        <td><b>Post Dimensions</b></td>
                                        <td><b>PDF Spec Sheet</b></td>
                                        <td><b>Save for later</b></td>
                                        <td style="display:none;">
                                            <b>My Shop Drawing</b>
                                        </td>
                                    </tr>
                                   
                                </tbody>
                            </table>
							<table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:849px;" align="center">
                                <tbody>
								 <tr style="height:70px;">
										
                                        <td colspan=4>
                                            <a class="thickbox" href="img/'.$fm.'.jpg" style="height:55px;width:55px;">
                                                <img src="img/needHelpButton.png">
                                            </a>
                                        </td>';
										//ss
										$bay=$_REQUEST['type'];
										$cate_id=$_REQUEST['id'];
										//if($cate_id=="79")
										//{
										//}
										//else{
										echo'<td><a href="images/'.$category_name.'/'.$category_name.'_'.$bay.'_revit.rvt" style="height:55px;width:55px;">
                                                <img src="images/'.$category_name.'/Rivit.jpg" style="width:84px;">
                                            </a>
										</td>';
										//}
                                    echo'</tr>
									
							</tbody>
                            </table>
                        ';
	   
	}
    else if(isset($_REQUEST['Model']) && isset($_REQUEST['id']) ){
         include(DIR_WS_MODULES."forms/".FILENAME_PRODUCT_CSS);
            echo '<td valign="top" align="left">
                <div id="selected-item" style="width: 88%;margin-left: 6%;">
                ';
                        if($_REQUEST['Model']=="EP-950-ACRYLIC"){
                           /* echo '<div class="msg_red" style="position:absolute;top:222px;left:20px;"><i>Fully Adjustable<br><br>Multi Functional<br><br>Indoor and Outdoor Use<br><br>Easy Disassembly for Storage,<br><br>Ready for Shipping</i></div>';*/
                        }
                        if($_REQUEST['Model']=="B-950P-GLASS"){
                           /* echo '<div class="msg_red" style="position:absolute;top:300px;left:20px;"><i>Fully Adjustable<br>Multi Functional<br>Indoor and Outdoor Use<br>Easy Disassembly for Storage,<br>Ready for Shipping</i></div>';*/
                        }
                        if($_REQUEST['Model']=="ALLIN1"){
                            echo '
                    <ul class="head-table" style="border-top:1px solid #888;height:170px;">
                      <li id="item-text" style="margin:0;padding:0;">
                        <table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:848px;" align="center">
                            <tbody>
                                <tr>
                                    <td>
                                        <a onclick="abc(this);" style="height:55px;width:55px;cursor:pointer;" >
                                            <img src="img/saveQuote_button.jpg" style="height:55px;width:55px;" >
                                        </a>
                                    </td>
                                    <td>
                                        <a onclick="layout1();" style="height:55px;width:55px;cursor:pointer;" >
                                            <img src="img/saveLayout_button.jpg" style="height:55px;width:55px;" >
                                        </a>
                                    </td>
                                    <td>
                                        <a class="thickbox" href="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                            <img src="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                        </a>
                                    </td>
                                    <td>
                                        <a href="PDF/'.$category_name.'.pdf" target="_blank" style="height:55px;width:55px;" >
                                            <img src="images/pdf-icon.gif" style="height:55px;width:55px;">
                                        </a>
                                    </td>
                                    <td>
                                            <a href="javascript:void(0)" onclick="wishlist()" style="height:55px;width:55px;" >
                                                <img src="images/wishlist_icon.png" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                    <td style="display:none;">
                                        <!--<a class="thickbox" href="images/'.$category_name.'/mydrw.jpg" style="height:55px;width:55px;" >
                                            <img src="images/'.$category_name.'/mydr.png" style="height:55px;width:55px;" >
                                        </a>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Save quote</b></td>
                                    <td><b>Save layout</b></td>
                                    <td><b>Post Dimensions</b></td>
                                    <td><b>PDF Spec Sheet</b></td>
                                    <td><b>Save for leter</b></td>
                                    <td style="display:none;"><b>My Shop Drawing</b></td>
                                </tr>
                                
                            </tbody>
                        </table>
						<table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:849px;" align="center">
                                <tbody>
								<tr style="height:85px;">
								
                                    <td colspan=5>
                                        <a class="thickbox" href="img/'.$category_name.'.jpg" style="height:55px;width:55px;">
                                            <img src="img/needHelpButton.png">
                                        </a>
                                    </td>';
										//ss
										$bay=$_REQUEST['type'];
										$cate_id=$_REQUEST['id'];
										//if($cate_id=="79")
										//{
										//}
										//else{
										echo'<td><a href="images/'.$category_name.'/'.$category_name.'_'.$bay.'_revit.rvt" style="height:55px;width:55px;">
                                                <img src="images/'.$category_name.'/Rivit.jpg" style="width:84px;">
                                            </a>
										</td>';
										//}
                                    echo'</tr>
									
							</tbody>
                            </table>
                        </li></tbody></table></li>
                   </ul>
                </div>
            </td>';
                        } else{

                    echo '
                    <ul class="head-table" style="border-top:1px solid #888;height:170px;">
                        <li id="item-text" style="margin:0;padding:0;">
                            <table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:848px;" align="center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a onclick="abc(this);" style="height:55px;width:55px;cursor:pointer;" >
                                                <img src="img/saveQuote_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a onclick="layout1();" style="height:55px;width:55px;cursor:pointer;" >
                                                <img src="img/saveLayout_button.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a class="thickbox" href="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$_REQUEST['Model'].'/POSTDIM.jpg" style="height:55px;width:55px;" >
                                            </a>
                                        </td>
                                        <td>
                                            <a href="PDF/'.$category_name.'.pdf" target="_blank" style="height:55px;width:55px;" >
                                                <img src="images/pdf-icon.gif" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" onclick="wishlist()" style="height:55px;width:55px;" >
                                                <img src="images/wishlist_icon.png" style="height:55px;width:55px;">
                                            </a>
                                        </td>
                                        <td style="display:none;">
                                            <!--<a class="thickbox" href="images/'.$category_name.'/mydrw.jpg" style="height:55px;width:55px;" >
                                                <img src="images/'.$category_name.'/mydr.png" style="height:55px;width:55px;" >
                                            </a>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Save quote</b></td>
                                        <td><b>Save layout</b></td>
                                        <td><b>Post Dimensions</b></td>
                                        <td><b>PDF Spec Sheet</b></td>
                                        <td><b>Save for leter</b></td>
                                        <td style="display:none;"><b>My Shop Drawing</b></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
							<table style="font-family: tahoma,verdana,arial;font-size:12px;text-align: center;width:849px;" align="center">
                                <tbody>
								<tr style="height:85px;">
								
                                        <td colspan="4">
                                            <a class="thickbox" href="img/'.$category_name.'.jpg" style="height:55px;width:55px;">
                                                <img src="img/needHelpButton.png">
                                            </a>
                                        </td>';
										//ss
										$bay=$_REQUEST['type'];
										$cate_id=$_REQUEST['id'];
										//if($cate_id=="79")
										//{
										//}
										//else{
										echo'<td><a href="images/'.$category_name.'/'.$category_name.'_'.$bay.'_revit.rvt" style="height:55px;width:55px;">
                                                <img src="images/'.$category_name.'/Rivit.jpg" style="width:84px;">
                                            </a>
										</td>';
										//}
                                    echo'</tr>
									
							</tbody>
                            </table>
                        </li>
                    </tbody>
                    </table></li>
                   </ul>
                </div>
            </td>';
        } }
    if(isset($_REQUEST['Model']) && isset($_REQUEST['cPath'])&&!isset($_REQUEST['var'])){
             include(DIR_WS_MODULES."forms/".FILENAME_PRODUCT_CSS);
             //if($_REQUEST['Model']==EP11){

             //}else{
               // echo '<td valign="top" align="left">
                 //   <div id="selected-item">
                   //     <ul class="head-table" style="border-top:1px solid #888;height:80px;">
                     //       <li><h1 id="textarea56" style="padding-left: 0;"> '.$_REQUEST['Model'].'<br>126" and larger, please call so we may better assist you (1-800-690-0002)</h1></li>
                       // </ul>
                        //<ul class="head-table">
                          //  <li id="additional_image">
                            //    <img src="images/'.$_REQUEST['Model'].'/START.jpg" style="width:100%;width:568px;height:453px"/>
                            //</li>
                            //<br style="clear:both;"/>
                        //</ul>
                    //</div>
                //</td>';
             //}
            
        }	
	
}		
		
		
		
?>
</tr>

</table>
<table border="0" width="856" height="10" align="center" background="images/backBottom.jpg">
    <tr>
        <td align=center></td>
    </tr>
</table>
<table border="0" width="856" height="30" align="center" style="background:#6e6e64;">
    <tr>
         <td width="185" height="55" style="padding-left: 15px"><img src="images/m24.gif" align="center" vspace="12">
         </td><td width="475" style="padding-left: 15px"><div align="center"><img src="images/comstr.jpg" >         </div>
         </td><td width="73" style="padding-left: 15px"><img src="images/nsflogo.jpg" width="49" height="49" align="right">         
         </td><td width="115" style="padding-left: 15px"><img src="images/SSL.gif" width="89" height="49" align="left">
            </td></tr>
</table>
<br>
<table border="0" width="850" align="center">
<tr>
    <td width="100%" height="45" align="center" valign="middle">
    <font size="2" face=Tahoma>
    <font size='2' color=white>

    <B>Copyright&copy;<?php echo date("Y");?>  ADM Sneezeguards A division of Advanced Design Manufacturing L.L.C.</B> | <A HREF='<?php echo tep_href_link('shipping.php');?>'>Shipping and Return</A> | <A HREF='<?php echo tep_href_link(FILENAME_PRIVACY);?>'>Privacy Notice</A> | <A HREF='<?php echo tep_href_link(FILENAME_CUSTOMLIBRARY);?>'><b>Custom Library</b></A> |<A HREF='<?php echo tep_href_link(FILENAME_CONDITIONS);?>'><b>Terms of Use</b></A> | <A HREF='<?php echo tep_href_link('projects.php');?>'><B>Projects</B></A>  | <A HREF='<?php echo tep_href_link('archive.php');?>'><B>Archive</B></A> |<A HREF='<?php echo tep_href_link('contact_us_dealer.php');?>'><B>Dealer Inquiries</B></A> |<A HREF='<?php echo tep_href_link('trade_show_program.php');?>'><B>Trade Show Display Program</B></A>

    <font size='2' color=white>
    <br>
    <br>
    <C><font size='1' color=#ffffff>Advanced Design Mfg. L.L.C. d.b.a. ADM Sneezeguards is listed and certified as a custom sneezeguard manufacturer by N.S.F., all style numbers and names of units shown on this website and subsequent literature are strictly for reference purposes only.</C>
    </font></font>
    </td>
</tr>
</table>
<?php 
    $res=tep_db_query("select * from custom_popup where id=200");
    $row=tep_db_fetch_array($res);
?>
<script src="jquery.confirm/jquery.confirm.js"></script>
<script type="text/javascript">
    function wishlist()
    {    
        var form = document.forms['cart_quantity'];
        if(myFunction(document.forms['cart_quantity'])){
            var bay=form.type.value;
            var var1=var2=var3=var4=var5=var6=var7=var8="";
            if(bay=="1BAY"){
                if(form.face_length!==undefined){
                    var1=form.face_length.options[form.face_length.selectedIndex].text;
                }else{
                    var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                }
            }else if(bay=="2BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
            }else if(bay=="3BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
                var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
            }else if(bay=="4BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
                var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
                var4=form.face_length_d.options[form.face_length_d.selectedIndex].text;
            }
            if(form.post_height!== undefined){
                var5=form.post_height.options[form.post_height.selectedIndex].text;  
            }
            if(form.right_length!== undefined){
                var6=form.right_length.options[form.right_length.selectedIndex].text;
            }
            if(form.left_length!== undefined){
                var7=form.left_length.options[form.left_length.selectedIndex].text;
            }
            end=$("input#glass-face").val();
            $.ajax({
                type: "POST",
                url: "includes/script1.php",
                data: { 
                    mod:category_name, bay:bay, face1:var1, face2:var2,face3:var3,face4:var4,post:var5,left:var7,right:var6,end:end,tot:tot1,osc:osc,im_id:im_id,sv:"save",img:img_ajx
                },
                cache: false,
                contentType: "application/x-www-form-urlencoded",
                success: function(data, textStatus, request){
                    //tb_show("","pop1.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
                    var action = $("form[name='cart_quantity']").attr("action");
                    action = action.replace("add_product", "add_wishlist");
                    $("form[name='cart_quantity']").attr("action", action);  
                    $("form[name='cart_quantity']").removeAttr("onsubmit");
                    $("form[name='cart_quantity']").submit();                    
                },
                error: function (request, textStatus, errorThrown) {
                    alert("some error");
                }
            });
            return false;
        }else{
            return false;
        }
    }
    <?php if($wishlist->isNotLogin && basename($_SERVER['PHP_SELF']) == 'login.php') { ?>
        $(document).ready(function(){
            $.confirm({
                'title'     : 'Alert!!!',
                'message'   : '<?php echo $row['message']?>',
                'buttons'   : {
                    'OK'    : {
                        'class' : 'blue',
                        'action': function(){
                        }
                    }
                }
            });
        });
    <?php } ?>
</script>