<?php
	  $servername = DB_SERVER;
		$username = DB_SERVER_USERNAME;
		$password = DB_SERVER_PASSWORD;
		$dbname = DB_DATABASE;
		$connt = mysqli_connect($servername,$username,$password,$dbname) or die(mysqli_connect_error());
?>
<?php
  if(isset($_SESSION['product_final1'])){
	  include('cart_data.php');
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sneeze Guard</title>
<meta name="description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs.">
<meta name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href='https://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="new-design/css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-compat-git.js"></script>
<script src="new-design/js/jquery-2.1.4.js"></script>
<script src="new-design/js/animatio-home.js"></script>
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>
<script src="new-design/js/main.js"></script>
<link rel="stylesheet" href="new-design/stylesheet.css">	
<link rel="stylesheet" href="new-design/instockstyle.css">
<script src="new-design/js/main-js-min.js"></script>
</head>
<body>
<div class="wrapper" >
<div class="top-header"  onmouseover="openCity(event, 'Hide');hide_cart_data();">
<a href="<?php echo tep_href_link('common.php')?>"><span>FAQS<span></a>
<?php
if (tep_session_is_registered('customer_id')) 
{ 
?>
<div class="sign-icon" onmouseover="show_myaccount_list();" onmouseout="hide_myaccount_list();"><a href="<?php echo tep_href_link('account.php', '', 'SSL') ?>" tabindex="0"> <b>MY ACCOUNT</b></a></div>
<ul class="myaccount-menu"  onmouseover="show_myaccount_list();" onmouseout="hide_myaccount_list();">
<li><a href="<?php echo tep_href_link('account.php', '', 'SSL') ?>">MY PROFILE</a></li>
<li><a href="<?php echo tep_href_link('account_history.php', '', 'SSL') ?>">MY ORDER</a></li>
<li><a href="<?php echo tep_href_link('logoff.php', '', 'SSL') ?>">LOGOUT</a></li>
</ul>
<?php
}
else
{ 
?>
<div class="sign-icon"><a href="<?php echo tep_href_link('login.php', '', 'SSL') ?>" tabindex="0"> <b>SIGN IN</b></a></div>
<?php
}
?>
</div>
<div>
  <nav>
    <a href="javascript:void(0);" class="icon" onclick="myFunctionsss()">
    <i class="fa fa-bars"></i>
  </a>
  <div class="left-icons"  onmouseover="openCity(event, 'Hide');hide_cart_data();">
    <a href="<?php  echo tep_href_link(FILENAME_DEFAULT); ?>"><h1><i>ADM Sneezeguards</i></h1></a>
  </div>
  <div class="right-icons">
  <ul>
  <li><a href="<?php echo tep_href_link('contact.php', '', 'SSL') ?>"><img src="img/contact-black.png"  id="contact-icons" onmouseover="change_contact_img();hide_cart_data()"  onmouseout="change_contactblack_img();openCity(event, 'Hide');"></a></li>
  <li><a href="<?php echo tep_href_link('wishlist.php', '', 'SSL') ?>"><img src="img/wishlist-black.png" id="wishlist-icons" onmouseover="change_wishlist_img();hide_cart_data()"  onmouseout="change_wishlistblack_img();openCity(event, 'Hide');"></a></li>
  <li><a ><img src="<?=($cart->count_contents()<=0)?"img/cart-black.png":"img/cart-red.png"?>" id="cart-icons" onmouseover="change_cart_img();show_cart_data();openCity(event, 'Hide')" onmouseout="change_cartblack_img();" class="tablinks" ></a><span class="cart-count" style="display:<?= ($cart->count_contents()>0)?"block":"none"?>"><?php echo $cart->count_contents();?></span></li>
    </ul>
  </div>
    <div class="topnav" id="myTopnav">
	<div class="option-mobile-header">
	<div class="close-nav-btn" onclick="closemobilepage()">X</div>
	<button class="tablinkss shopp" onclick="openPages('Shop', this, '#c61017')" id="defaultOpen">SHOP</button>
<button class="tablinkss insideadm" onclick="openPages('Inside-ADM', this, '#555')" >INSIDE ADM</button>
</div>
	<div id="Shop" class="tabcontenthead">
	<a href="#"><span class="homelink" >HOME</span></a>
	  <div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'instock');hide_cart_data()">PASS-OVER</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_72").''; ?>">EP5</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_122").''; ?>">EP5 Ring Adjusts</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_129").''; ?>">EP6</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_130").''; ?>">EP7</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_133").''; ?>">EP8</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_71").''; ?>">EP15</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_55").''; ?>">EP11</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_56").''; ?>">EP12</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_57").''; ?>">EP21</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_58").''; ?>">EP22</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_59").''; ?>">EP36</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_113").''; ?>">ED20</a>
    </div>
  </div> 
	  <div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'customss');hide_cart_data()">SELF-SERVE</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_114").''; ?>">ES29</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_61").''; ?>">ES31</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_62").''; ?>">ES40</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_110").''; ?>">ES53</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_63").''; ?>">ES67</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_64").''; ?>">ES73</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_111").''; ?>">ES82</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=126_124").''; ?>">ES47</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_123").''; ?>">ES90</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_125").''; ?>">ES92</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_80").''; ?>">B950</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_81").''; ?>">B950-SWIVEL</a>
	  <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=87_128").''; ?>">ORBIT360</a>
    </div>
  </div> 
	<div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'adjustable');hide_cart_data()">BARRIER</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
       <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_72").''; ?>">EP5</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_122").''; ?>">EP5 Ring Adjusts</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_129").''; ?>">EP6</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_130").''; ?>">EP7</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_133").''; ?>">EP8</a>
    </div>
  </div> 
	<div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'portable');hide_cart_data()">PORTABLE</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=85_117").''; ?>">ALLIN1</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=85_79").''; ?>">B950P-GLASS</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=85_70").''; ?>">EP950-ACRYLIC</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=85_131").''; ?>">ORBIT720</a>
    </div>
  </div> 
	<div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'addons');hide_cart_data()">ADD-ONS</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=88_121").''; ?>">Light Bar</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=88_118").''; ?>">Mid-Shelves</a>
      <a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=88_120").''; ?>">Heat Lamp</a>
    </div>
  </div> 
		<a href="#"><span class="tablinks insideddadm" onmouseover="openCity(event, 'insideadm');hide_cart_data()">INSIDE <br />ADM</span></a></li>
       <a href="#"><span class="tablinks" onmouseover="openCity(event, 'Hide');hide_cart_data();hide_cart_data();"></span></a>
	 </div>	 
<!-- 	/*vikram 15-10-2020 */ -->
<div id="Inside-ADM" class="tabcontenthead">
<div class="inside-adm-mobile-main">
<center>
<a href="<?= tep_href_link('about.php',"class=about");?>">
<div><img src="img/about-us.jpg"></div>
<div>
<h2>ABOUT</h2>
<p>Founded in Antioch, California, ADM Sneezeguards is a business-to-business (B2B) sales and a leading Manufacturing Company which has amazing Glass Models and Designs for sneeze guards and sanitary barriers. </p>
<p><a href="<?= tep_href_link('about.php',"class=about");?>">READ MORE</a></p>
</div>
</a>
</center>
</div>
<div class="inside-adm-mobile-main">
<center>
<a href="<?= tep_href_link('about.php',"class=innovation");?>">
<div><img src="img/insideadm/back1.jpg"></div>
<div>
<h2>INNOVATION</h2>
<p>ADM Sneezeguards offers a range of glass guards and protective barriers. Shop our selection of custom sizes or custom designs with various options. We work on the belief that if you are satisfied, our mission is completed.</p>
<p><a href="<?= tep_href_link('about.php',"class=innovation");?>">READ MORE</a></p>
</div>
</a>
</center>
</div>
<div class="inside-adm-mobile-main">
<center>
<a href="<?= tep_href_link('about.php',"class=stories");?>">
<div><img src="img/insideadm/back2.jpg"></div>
<div>
<h2>STORIES</h2>
<p>Within the last few years, ADM Sneezeguards has become one of the largest and most respected sneeze guard and portable screen manufacturers in the corporate and food service industry.</p>
<p><a href="<?= tep_href_link('about.php',"class=stories");?>">READ MORE</a></p>
</div>
</a>
</center>
</div>
<div class="inside-adm-mobile-main">
<center>
<a href="<?= tep_href_link('about.php',"class=sustainability");?>">
<div><img src="img/insideadm/back3.jpg"></div>
<div>
<h2>SUSTAINABILITY</h2>
<p>ADM Sneezeguards is fully dedicated to environmentally friendly manufacturing and business processes that reduce any ill effect on the surroundings. The Company makes every successful attempt to</p>
<p><a href="<?= tep_href_link('about.php',"class=sustainability");?>">READ MORE</a></p>
</div>
</a>
</center>
</div>
<div class="inside-adm-mobile-main">
<center>
<a href="<?= tep_href_link('about.php',"class=madeinusa");?>">
<div><img src="img/insideadm/back4.jpg"></div>
<div>
<h2>MADE IN THE USA</h2>
<p>Known for the perfect finishing touch, ADM Sneezeguards manufactures cheap sneeze guard and food guard in Antioch, California, USA. The Company makes every possible solution to surpass the expectations of the customers.</p>
<p><a href="<?= tep_href_link('about.php',"class=madeinusa");?>">READ MORE</a></p>
</div>
</a>
</center>
</div>
<div class="inside-adm-mobile-main">
<center>
<a href="<?= tep_href_link('about.php',"class=vision");?>">
<div><img src="img/insideadm/back5.jpg"></div>
<div>
<h2>VISION</h2>
<p>We believe that every opportunity becomes successful only if it is cherished with perfect strategy and effective solutions. To serve our customers, we deliver the best acrylic sneeze guard </p>
<p><a href="<?= tep_href_link('about.php',"class=vision");?>">READ MORE</a></p>
</div>
</a>
</center>
</div>
<div class="inside-adm-mobile-main">
<center>
<a href="<?= tep_href_link('about.php',"class=mission");?>">
<div><img src="img/insideadm/back6.jpg"></div>
<div>
<h2>MISSION</h2>
<p>To serve our customers, we do our best to deliver affordable safety solutions to our customers. We promise to be there, holding your hands in every tough situation.</p>
<p><a href="<?= tep_href_link('about.php',"class=mission");?>">READ MORE</a></p>
</div>
</a>
</center>
</div>
</div>
    </div>
  </nav>
  </div>
  <div id="cart-items-data" onmouseover="show_cart_data()" onmouseout="hide_cart_data();">
  <div class="cart-item-products">
  <?php print_r($html);?>
  <br>
  <p style="text-transform: uppercase;
    color: #ed1c24;
    font-size: 10px;">VIEW CART TO SEE ALL ITEMS</p>
  </div>
  <div class="cart-item-subtotal">
  <div class="cart-subtotal-left">
    <br>
 <h3>SUBTOTAL</h3>
 <span>Taxes are calculated at checkout</span>
  </div>
  <div class="cart-subtotal-right"><strong>$<?=number_format((float)$cart->show_total(), 2, '.', '');?></strong></div>
  </div>
  <div class="cart-item-checkout">
  <a href="shopping_cart1.php"> <button>VIEW CART & CHECKOUT</button></a>
  </div>
  </div>
<div id="instock" class="tabcontent">
  <table border="0" width="100%;">
  <tr>
  <td rowspan="2" style="width:33%;padding:20px;">
  <div style="width:100%; float:left;">
  <h3><b>PASS-OVER </b></h3>
  <ul>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_72").''; ?>" id="EP5" onmouseover="change_header_image_passover(this);"> EP5</a></li>
  <li><a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_122").''; ?>"  id="Ring-EP5" onmouseover="change_header_image_passover(this);">EP5 Ring Adjusts</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_129").''; ?>"  id="EP6" onmouseover="change_header_image_passover(this);">EP6</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_130").''; ?>"  id="EP7" onmouseover="change_header_image_passover(this);">EP7</a></li> 
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_133").''; ?>"  id="EP7" onmouseover="change_header_image_passover(this);">EP8</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_71").''; ?>"  id="EP15" onmouseover="change_header_image_passover(this);">EP15</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_55").''; ?>"  id="EP11" onmouseover="change_header_image_passover(this);"> EP11</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_56").''; ?>"  id="EP12" onmouseover="change_header_image_passover(this);">EP12</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_57").''; ?>"  id="EP21" onmouseover="change_header_image_passover(this);">EP21</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_58").''; ?>"  id="EP22" onmouseover="change_header_image_passover(this);">EP22</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_59").''; ?>"  id="EP36" onmouseover="change_header_image_passover(this);">EP36</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_113").''; ?>"  id="ED20" onmouseover="change_header_image_passover(this);">ED20</a></li>
  </ul>
   <br /><br />
  </div>
    <div class="header-dropdow-buynow" style="height:200px;text-align:center;">
<div class="model-divs">
<h2 id="passover_header-dropdown-model-heading">EP5</h2>
<span class="product_name" id="heading" >EP-5</span>
</div>
<div class="model-lernmore-by-divs">
	<span id="passover_header-dropdown-model-learnmore"><a href="../product.php?cPath=86_129" ><button>LEARN MORE </button></a></span>
		<span id="passover_header-dropdown-model-buynow"><a href="../product.php?cPath=86_129"><button>SHOP NOW </button></a></span>  
</div>  
		</div>
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
  </td>
  <td style="width:67%;">
  <div style="width:100%;" align="">
 <div style="width:33%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5.jpg" id="header_image_passover1">
  </div>
 <div style="width:33%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_2.jpg" id="header_image_passover2">
  </div>
  <div style="width:33%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_3.jpg" id="header_image_passover3">
  </div>
  </div></td>
  </tr>
  <tr>
  <td>
  <div id="demo-header1" class="carousel slide" data-ride="carousel" style="padding-top: 2%;height:325px;width: 89%;"   data-interval="2000">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slider2/EP5/slider1.png" alt="EP5" id="passover_slides1" >  
    </div>
    <div class="carousel-item">
      <img src="images/slider2/EP5/slider2.png" alt="EP5" id="passover_slides2" > 
    </div>
    <div class="carousel-item">
      <img src="images/slider2/EP5/slider3.png" alt="EP5" id="passover_slides3">
    </div>
	   <div class="carousel-item">
      <img src="images/slider2/EP5/slider4.png" alt="EP5" id="passover_slides4">   
    </div>
	   <div class="carousel-item">
      <img src="images/slider2/EP5/slider5.png" alt="EP5" id="passover_slides5">
      <div class="carousel-caption">   
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo-header1" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo-header1" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
   </div>
  </td>
  </tr>
  </table>
  <style>
  </style>
<div class="footer-address-div">
<div class="footer-address-left"><img src="img/logo-new.png"></div>
<div class="footer-address-center">
<center>
<h2>Need help find the perfect Model?
<br />Call 800-690-0002</h2>
</center>
</div>
<div class="footer-address-right">
<img src="img/close.png" onclick="openCity(event, 'Hide');hide_cart_data();">
</div>
</div>
  </div>
<div id="customss" class="tabcontent"  onmouseo="openCity(event, 'Hide');hide_cart_data();">  
  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:30%; float:left;">
  <h3><b>SELF-SERVE </b></h3>
  <ul>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_114").''; ?>" id="ES29" onmouseover="change_header_image_selfserve(this);">ES29</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_61").''; ?>"  id="ES31" onmouseover="change_header_image_selfserve(this);">ES31</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_62").''; ?>"  id="ES40" onmouseover="change_header_image_selfserve(this);">ES40</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_110").''; ?>"  id="ES53" onmouseover="change_header_image_selfserve(this);">ES53</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_63").''; ?>"  id="ES67" onmouseover="change_header_image_selfserve(this);">ES67</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_64").''; ?>"  id="ES73" onmouseover="change_header_image_selfserve(this);">ES73</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_111").''; ?>"  id="ES82" onmouseover="change_header_image_selfserve(this);">ES82</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=126_124").''; ?>"  id="ES90" onmouseover="change_header_image_selfserve(this);">ES47</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_123").''; ?>"  id="ES90" onmouseover="change_header_image_selfserve(this);">ES90</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_125").''; ?>"  id="ES92" onmouseover="change_header_image_selfserve(this);">ES92</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_80").''; ?>"  id="B-950" onmouseover="change_header_image_selfserve(this);">B950</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_81").''; ?>"  id="B-950-SWIVEL" onmouseover="change_header_image_selfserve(this);">B950-SWIVEL</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=87_128").''; ?>"  id="ORBIT360" onmouseover="change_header_image_selfserve(this);">ORBIT360</a></li>
  </ul>
  <span id="headingself">ES29</span>
  </div> 
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ES29.jpg" id="header_image_selfserve1">
  </div>
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ES29_2.jpg" id="header_image_selfserve2">
  </div>
  <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ES29_3.jpg" id="header_image_selfserve3">
  </div>
  </div>
</div>
<div id="adjustable" class="tabcontent">
  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:30%; float:left;">
  <h3><b>BARRIER</b></h3>
  <ul>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_72").''; ?>" id="EP5" onmouseover="change_header_image_barrier(this);"> EP5</a></li>
  <li><a href="<?php  echo ''.tep_href_link(FILENAME_PRODUCT,"cPath=86_122").''; ?>"  id="Ring-EP5" onmouseover="change_header_image_barrier(this);">EP5 Ring Adjusts</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_129").''; ?>"  id="EP6" onmouseover="change_header_image_barrier(this);">EP6</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=86_130").''; ?>"  id="EP7" onmouseover="change_header_image_barrier(this);">EP7</a></li>
  </ul>
  <span id="barrierheading">EP-5</span>
  </div>
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5.jpg" id="header_image_barrier1">
  </div>
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_2.jpg" id="header_image_barrier2">
  </div> 
  <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_3.jpg" id="header_image_barrier3">
  </div>
  </div>
</div>
<div id="portable" class="tabcontent">
  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:30%; float:left;">
  <h3><b>PORTABLE </b></h3>
  <ul>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=85_117").''; ?>" id="ALLIN1" onmouseover="change_header_image_portable(this);">ALLIN1</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=85_79").''; ?>" id="B-950P-GLASS" onmouseover="change_header_image_portable(this);">B950P-GLASS</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=85_70").''; ?>" id="EP-950-ACRYLIC" onmouseover="change_header_image_portable(this);">EP950-ACRYLIC</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=85_131").''; ?>"  id="ORBIT720" onmouseover="change_header_image_portable(this);">ORBIT720</a></li>	
  </ul>
  <span id="headingportable">ALLIN1</span>
  </div>
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ALLIN1.jpg" id="header_image_portable1">
  </div>
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ALLIN1_2.jpg" id="header_image_portable2">
  </div>
  <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ALLIN1_3.jpg" id="header_image_portable3">
  </div>
  </div>
</div>
<div id="faqs" class="tabcontent">    
</div>
<div id="addons" class="tabcontent">
  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:30%; float:left;">
  <h3><b>ADDONS </b></h3>
  <ul>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=88_121").''; ?>" id="Light Bar" onmouseover="change_header_image_addons(this);">Light Bar</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=88_118").''; ?>" id="Mid-Shelves" onmouseover="change_header_image_addons(this);">Mid-Shelves</a></li>
  <li><a href="<?php  echo''.tep_href_link(FILENAME_PRODUCT,"cPath=88_120").''; ?>" id="Heat Lamp" onmouseover="change_header_image_addons(this);">Heat Lamp</a></li>
  </ul>
  <span id="headingaddons">Light Bar</span>
  </div>
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/Light Bar.jpg" id="header_image_addons1">
  </div>
 <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/Light Bar_2.jpg" id="header_image_addons2">
  </div>
  <div style="width:20%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/Light Bar_3.jpg" id="header_image_addons3">
  </div>
  </div>
</div>
<div id="insideadm" class="tabcontent" >
<div style="width:100%;">
<div style="width:20%; float:left;">
 <div >
<!-- 	/*vikram 15-10-2020 */ -->
  <ul>
  <li><a href="<?= tep_href_link('about.php',"class=about");?>" id="about" class="insideadmnav">ABOUT</a></li>
  <li><a href="<?= tep_href_link('about.php',"class=innovation");?>"  id="innovation" class="insideadmnav">INNOVATION</a></li>
  <li><a href="<?= tep_href_link('about.php',"class=stories");?>"  id="stories" class="insideadmnav">STORIES</a></li>
  <li><a href="<?= tep_href_link('about.php',"class=sustainability");?>"  id="sustainability" class="insideadmnav">SUSTAINABILITY</a></li>
  <li><a href="<?= tep_href_link('about.php',"class=madeinusa");?>"  id="madeinusa" class="insideadmnav">MADE IN THE USA</a></li>
  <li><a href="<?= tep_href_link('about.php',"class=vision");?>"  id="vision" class="insideadmnav">VISION</a></li>
  <li><a href="<?= tep_href_link('about.php',"class=mission");?>"  id="mission" class="insideadmnav">MISSION</a></li>
  </ul>
  <!-- 	/*vikram 15-10-2020 */ -->
  </div></div>
  <a href="<?= tep_href_link('about.php',"class=about");?>" id="admlink">
<div id="aboutdiv" class="admdata">
<div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
<img alt="sneeze guard" title="sneeze guard for office" src="img/about-us.jpg" id="insideadm-img" />
</div>
<!-- /*vikram 15-10-2020 */ -->
<div class='insideadmtext'>
<h2 id="insideadm-heading">ABOUT</h2>
<p id="insideadm-contanet">Founded in Antioch, California, ADM Sneezeguards is a business-to-business (B2B) sales and a leading Manufacturing Company which has amazing Glass Models and Designs for sneeze guards and sanitary barriers. The Company was established in 1988 and now is dealing with thousands of customers with their different requirements of standard and custom glass products. </p>
</div>
</div>
</a>
<a href="<?= tep_href_link('about.php',"class=innovation");?>" id="admlink">
<div id="innovationdiv" class="admdata">
<div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
<img alt="sneeze guard" title="sneeze guard for office" src="img/insideadm/ranimages/1.jpg" id="insideadm-img" />
</div>
<!-- /*vikram 15-10-2020 */ -->
<div class='insideadmtext'>
<h2 id="insideadm-heading">INNOVATION</h2>
<p id="insideadm-contanet">ADM Sneezeguards offers a range of glass guards and protective barriers. Shop our selection of custom sizes or custom designs with various options. We work on the belief that if you are satisfied, our mission is completed.</p>
</div>
</div>
</a>
<a href="<?= tep_href_link('about.php',"class=stories");?>" id="admlink">
<div id="storydiv" class="admdata">
<div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
<img alt="sneeze guard" title="sneeze guard for office" src="img/insideadm/ranimages/2.jpg" id="insideadm-img" />
</div>
<!-- /*vikram 15-10-2020 */ -->
<div class='insideadmtext'>
<h2 id="insideadm-heading">OUR STORY</h2>
<p id="insideadm-contanet">ADM Sneezeguards is fully dedicated to environmentally friendly manufacturing and business processes that reduce any ill effect on the surroundings. The Company makes every successful attempt to build sustainable, long-lasting and quality products using only eco-friendly materials.</p>
</div>
</div>
</a>
<a href="<?= tep_href_link('about.php',"class=stories");?>" id="admlink">
<div id="sustainabilitydiv" class="admdata">
<div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
<img alt="sneeze guard" title="sneeze guard for office" src="img/insideadm/ranimages/3.jpg" id="insideadm-img" />
</div>
<!-- /*vikram 15-10-2020 */ -->
<div class='insideadmtext'>
<h2 id="insideadm-heading">SUSTAINABILITY</h2>
<p id="insideadm-contanet">ADM Sneezeguards is fully dedicated to environmentally friendly manufacturing and business processes that reduce any ill effect on the surroundings. The Company makes every successful attempt to build sustainable, long-lasting and quality products using only eco-friendly materials.</p>
</div>
</div>
</a>
<a href="<?= tep_href_link('about.php',"class=madeinusa");?>" id="admlink">
<div id="madeinusadiv" class="admdata">
<div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
<img alt="sneeze guard" title="sneeze guard for office" src="img/insideadm/ranimages/4.jpg" id="insideadm-img" />
</div>
<!-- /*vikram 15-10-2020 */ -->
<div class='insideadmtext'>
<h2 id="insideadm-heading">MADE IN THE USA</h2>
<p id="insideadm-contanet">Known for the perfect finishing touch, ADM Sneezeguards manufactures cheap sneeze guard and food guard in Antioch, California, USA. The Company makes every possible solution to surpass the expectations of the customers. It is ready with extra capacity in the glass production side with </p>
</div>
</div>
</a>
<a href="<?= tep_href_link('about.php',"class=vision");?>" id="admlink">
<div id="visiondiv" class="admdata">
<div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
<img alt="sneeze guard" title="sneeze guard for office" src="img/insideadm/ranimages/5.jpg" id="insideadm-img" />
</div>
<!-- /*vikram 15-10-2020 */ -->
<div class='insideadmtext'>
<h2 id="insideadm-heading">VISION</h2>
<p id="insideadm-contanet">We believe that every opportunity becomes successful only if it is cherished with perfect strategy and effective solutions. To serve our customers, we deliver the best acrylic sneeze guard and glass guard which are affordable, transparent, durable and high-quality. </p>
</div>
</div>
</a>
<a href="<?= tep_href_link('about.php',"class=mission");?>" id="admlink">
<div id="missiondiv" class="admdata">
<div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
<img alt="sneeze guard" title="sneeze guard for office" src="img/insideadm/ranimages/6.jpg" id="insideadm-img" />
</div>
<!-- /*vikram 15-10-2020 */ -->
<div class='insideadmtext'>
<h2 id="insideadm-heading">MISSION</h2>
<p id="insideadm-contanet">To serve our customers, we do our best to deliver affordable safety solutions to our customers. We promise to be there, holding your hands in every tough situation. ADM Sneezeguards is consistently working on our manufacturing process to offer the product which meets your needs of business and enterprise. </p>
</div>
</div>
</a>
  <!-- 	/*vikram 15-10-2020 */ -->
  </div>  
</div>
</div>





