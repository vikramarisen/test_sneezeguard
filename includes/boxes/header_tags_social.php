<?php
/*
  $Id: header_tags.php,v 3.3.1 2013/12/22 by Jack York of www.oscommerce-solution.com

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce
  Portions Copyright 2009 oscommerce-solution.com

  Released under the GNU General Public License
*/
?>

<!-- header_tags.php //-->
          <tr>
            <td>
<?php
 
  $info_box_contents = array();
  $info_box_contents[] = array('text' => 'Social Links');

  new infoBoxHeading($info_box_contents, false, false);
  
  $align = 'style="margin-left:auto; margin-right:auto; width:' . BOX_WIDTH . 'px;"';
  $inInfoBox = true;
  include(DIR_WS_MODULES . 'header_tags_social_bookmarks.php');
  $inInfoBox = false;
  $info_box_contents = array();
  $info_box_contents[] = array('text' => '<div ' . $align . ' >' . $dataStrBox . '</div>');

  new infoBox($info_box_contents);
?>
            </td>
          </tr>
<!-- header_tags.php_eof //-->
