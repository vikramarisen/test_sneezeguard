<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo tep_output_string_protected($oscTemplate->getTitle()); ?></title>


<meta name="description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs.">
<meta name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<base href="'. (($request_type == "SSL") ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG .'" />


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="new_template_main/stylesheet.css">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script src="new_template_main/js/main-js-min.js"></script>


 <?php

  if (!isset($lng) || (isset($lng) && !is_object($lng))) {

    include(DIR_WS_CLASSES . 'language.php');

    $lng = new language;

  }  

  

  reset($lng->catalog_languages);

  while (list($key, $value) = each($lng->catalog_languages)) {

    echo '<link rel="alternate" type="application/rss+xml" title="' . STORE_NAME . ' ' . BOX_INFORMATION_RSS . ' ' . $value['name'] . '" href="' . FILENAME_RSS . '?language=' . $key . '" />';

  }

  ?>

</head>

<body>


<div class="wrapper"  >

<div class="top-header"  onmouseover="openCity(event, 'Hide');hide_cart_data();">
<a href=""><span>FAQS<span></a>
<div class="sign-icon"><a href="../login.php" tabindex="0"> <b>SIGN IN</b></a></div>
</div>

<div>
  <nav>
  
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
  
  
  <div class="left-icons"  onmouseover="openCity(event, 'Hide');hide_cart_data();">

    <a href="#"><h1><i>ADM Sneezeguards</i></h1></a>

  </div>
  <div class="right-icons">
  <ul>
  <li><a href=""><img src="img/contact-black.png"  id="contact-icons" onmouseover="change_contact_img();hide_cart_data()"  onmouseout="change_contactblack_img();openCity(event, 'Hide');"></a></li>
  <li><a href=""><img src="img/wishlist-black.png" id="wishlist-icons" onmouseover="change_wishlist_img();hide_cart_data()"  onmouseout="change_wishlistblack_img();openCity(event, 'Hide');"></a></li>
  <li><a href=""><img src="img/cart-black.png" id="cart-icons" onmouseover="change_cart_img();show_cart_data();openCity(event, 'Hide')" onmouseout="change_cartblack_img();" class="tablinks" ></a></li>
    </ul>
  </div>
    <div class="topnav" id="myTopnav">
	<div class="option-mobile-header">
	<div class="close-nav-btn" onclick="closemobilepage()">X</div>
	<button class="tablinkss shopp" onclick="openPages('Shop', this, '#c61017')" id="defaultOpen">SHOP</button>
<button class="tablinkss insideadm" onclick="openPages('Inside-ADM', this, '#555')" >INSIDE ADM</button>
</div>
	
	
	<div id="Shop" class="tabcontenthead">
	<a href="#"><span class="homelink" >HOME</span></a>
	
	  <div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'instock');hide_cart_data()">PASS-OVER</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="#">EP5</a>
      <a href="#">EP5 Ring Adjusts</a>
      <a href="#">EP6</a>
      <a href="#">EP7</a>
      <a href="#">EP15</a>
      <a href="#">EP11</a>
      <a href="#">EP12</a>
      <a href="#">EP21</a>
      <a href="#">EP22</a>
      <a href="#">EP36</a>
      <a href="#">ED20</a>
    </div>
  </div> 
	
	
	  <div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'customss');hide_cart_data()">SELF-SERVE</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="#">ES29</a>
      <a href="#">ES31</a>
      <a href="#">ES40</a>
      <a href="#">ES53</a>
      <a href="#">ES67</a>
      <a href="#">ES73</a>
      <a href="#">ES82</a>
      <a href="#">ES90</a>
      <a href="#">ES92</a>
      <a href="#">B950</a>
      <a href="#">B950-SWIVEL</a>
	  <a href="#">ORBIT360</a>
    </div>
  </div> 
	
	<div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'adjustable');hide_cart_data()">BARRIER</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="#">EP5</a>
      <a href="#">EP5 Ring Adjusts</a>
      <a href="#">EP6</a>
    </div>
  </div> 
	
	<div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'portable');hide_cart_data()">PORTABLE</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="#">ALLIN1</a>
      <a href="#">B950P-GLASS</a>
      <a href="#">EP950-ACRYLIC</a>
      <a href="#">ORBIT720</a>
    </div>
  </div> 
	<div class="dropdown">
    <a class="dropbtn"><span class="tablinks" onmouseover="openCity(event, 'addons');hide_cart_data()">ADD-ONS</span>
      <i class="fa fa-caret-down"></i>
    </a>
    <div class="dropdown-content">
      <a href="#">Light Bar</a>
      <a href="#">Mid-Shelves</a>
      <a href="#">Heat Lamp</a>
    </div>
  </div> 
	
		<a href="#"><span class="tablinks insideddadm" onmouseover="openCity(event, 'insideadm');hide_cart_data()">INSIDE <br />ADM</span></a></li>
		
       <a href="#"><span class="tablinks" onmouseover="openCity(event, 'Hide');hide_cart_data();hide_cart_data();"></span></a>
        
     
	 
	 
	 </div>
	 
	 
	 
	 
	 
	 
	 
<div id="Inside-ADM" class="tabcontenthead">
<div class="inside-adm-mobile-main">
<center>
<div><img src="img/about-us.jpg"></div>
<div>
<h2>ABOUT US</h2>
<p>ADM Sneezeguards was founded in Antioch, California as a business-to-business (B2B) sales company. The company established the business in 1988 with amazing Glass Models and Designs.</p>
<p><a href="">READ MORE</a></p>
</div>
</center>
</div>

<div class="inside-adm-mobile-main">
<center>
<div><img src="img/about-us.jpg"></div>
<div>
<h2>INNOVATION</h2>
<p>ADM Sneezeguards was founded in Antioch, California as a business-to-business (B2B) sales company. The company established the business in 1988 with amazing Glass Models and Designs.</p>
<p><a href="">READ MORE</a></p>
</div>
</center>
</div>

<div class="inside-adm-mobile-main">
<center>
<div><img src="img/about-us.jpg"></div>
<div>
<h2>STORIES</h2>
<p>ADM Sneezeguards was founded in Antioch, California as a business-to-business (B2B) sales company. The company established the business in 1988 with amazing Glass Models and Designs.</p>
<p><a href="">READ MORE</a></p>
</div>
</center>
</div>


<div class="inside-adm-mobile-main">
<center>
<div><img src="img/about-us.jpg"></div>
<div>
<h2>SUSTAINABILITY</h2>
<p>ADM Sneezeguards was founded in Antioch, California as a business-to-business (B2B) sales company. The company established the business in 1988 with amazing Glass Models and Designs.</p>
<p><a href="">READ MORE</a></p>
</div>
</center>
</div>


<div class="inside-adm-mobile-main">
<center>
<div><img src="img/about-us.jpg"></div>
<div>
<h2>RETÜL</h2>
<p>ADM Sneezeguards was founded in Antioch, California as a business-to-business (B2B) sales company. The company established the business in 1988 with amazing Glass Models and Designs.</p>
<p><a href="">READ MORE</a></p>
</div>
</center>
</div>


<div class="inside-adm-mobile-main">
<center>
<div><img src="img/about-us.jpg"></div>
<div>
<h2>OUTRIDE</h2>
<p>ADM Sneezeguards was founded in Antioch, California as a business-to-business (B2B) sales company. The company established the business in 1988 with amazing Glass Models and Designs.</p>
<p><a href="">READ MORE</a></p>
</div>
</center>
</div>


</div>

	 
	 
	 
	 
    </div>
  </nav>
  </div>
  <script>
   
  </script>

  <div id="cart-items-data" onmouseout="show_cart_data()" onmouseover="hide_cart_data();">
  <div class="cart-item-products">
  <table width="100%" border="0">
  <tr>
  <td style="width: 30%;"><img src="https://www.sneezeguard.com/images/EP5SIDESS.jpg" style="width:85%;"></td>
  <td><h3>EP5-22 End Post Brushed Stainless Steel
</h3>
  <span>EP5-22 End Post Brushed Stainless Steel
<p>Includes: Post, Flange, and Bracketry</p>

</span>
  <strong>$64.00</strong></td>
  </tr>
<tr>
  <td colspan="2"><br /><br />
  <span class="gocart-text">VIEW CART TO SEE ALL ITEMS</span>
  </td>
  </tr>
  </table>

  </div>
  <div class="cart-item-subtotal">
  <div class="cart-subtotal-left">
 <h3>SUBTOTAL</h3>
 <span>Taxes are calculated at checkout</span>
  </div>
  <div class="cart-subtotal-right"><h3>$64.00</h3></div>
  </div>
  <div class="cart-item-checkout">
  <a href=""> <button>VIEW CART & CHECKOUT</button></a>
  </div>
  
  </div>
  
<div id="instock" class="tabcontent">
  
  <table border="0" width="100%;">
  
  <tr>
  <td rowspan="2" style="width:33%;padding:20px;">
  <div style="width:100%; float:left;">
  <h3><b>PASS-OVER </b></h3>
  <ul>
  <li><a alt="../product.php?cPath=86_72" id="EP5" onmouseover="change_header_image_passover(this);"> EP5</a></li>
  <li><a alt="../product.php?cPath=86_122"  id="Ring-EP5" onmouseover="change_header_image_passover(this);">EP5 Ring Adjusts</a></li>
  <li><a alt="../product.php?cPath=86_129"  id="EP6" onmouseover="change_header_image_passover(this);">EP6</a></li>
  <li><a alt="../product.php?cPath=86_130"  id="EP7" onmouseover="change_header_image_passover(this);">EP7</a></li>
  <li><a alt="../product.php?cPath=86_71"  id="EP15" onmouseover="change_header_image_passover(this);">EP15</a></li>
  <li><a alt="../product.php?cPath=86_55"  id="EP11" onmouseover="change_header_image_passover(this);"> EP11</a></li>
  <li><a alt="../product.php?cPath=86_56"  id="EP12" onmouseover="change_header_image_passover(this);">EP12</a></li>
  <li><a alt="../product.php?cPath=86_57"  id="EP21" onmouseover="change_header_image_passover(this);">EP21</a></li>
  <li><a alt="../product.php?cPath=86_58"  id="EP22" onmouseover="change_header_image_passover(this);">EP22</a></li>
  <li><a alt="../product.php?cPath=86_59"  id="EP36" onmouseover="change_header_image_passover(this);">EP36</a></li>
  <li><a alt="../product.php?cPath=86_113"  id="ED20" onmouseover="change_header_image_passover(this);">ED20</a></li>
	
	
  </ul>
   <br /><br />
  </div>
 
    <div class="header-dropdow-buynow" style="height:200px;text-align:center;">
  
<div class="model-divs">
<h2 id="passover_header-dropdown-model-heading">EP5</h2>

</div>

<div class="model-lernmore-by-divs">
	<span id="passover_header-dropdown-model-learnmore"><a href="../product.php?cPath=86_129" ><button>LEARN MORE </button></a></span>

		<span id="passover_header-dropdown-model-buynow"><a href="../product.php?cPath=86_129"><button>SHOP NOW </button></a></span>
		
</div>
		  
		</div>
		
		<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
  </td>
  <td style="width:67%;">
  <div style="width:100%;" align="">
  
 <div style="width:33%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5.jpg" id="header_image_passover1">
  </div>
 <div style="width:33%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_2.jpg" id="header_image_passover2">
  </div>
  
  <div style="width:33%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_3.jpg" id="header_image_passover3">
  </div>
  
  </div></td>
  </tr>
  
  <tr>
  <td>
  <div id="demo-header1" class="carousel slide" data-ride="carousel" style="padding-top: 2%;height:325px;width: 89%;"   data-interval="2000">

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slider2/EP5/slider1.png" alt="EP5" id="passover_slides1" >
      
    </div>
    <div class="carousel-item">
      <img src="images/slider2/EP5/slider2.png" alt="EP5" id="passover_slides2" >
       
    </div>
    <div class="carousel-item">
      <img src="images/slider2/EP5/slider3.png" alt="EP5" id="passover_slides3">
      
    </div>
	
	   <div class="carousel-item">
      <img src="images/slider2/EP5/slider4.png" alt="EP5" id="passover_slides4">
        
    </div>
	
	
	   <div class="carousel-item">
      <img src="images/slider2/EP5/slider5.png" alt="EP5" id="passover_slides5">
      <div class="carousel-caption">
        
    </div>
	
  </div>
  <a class="carousel-control-prev" href="#demo-header1" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo-header1" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

   </div>
   
   
    
  </td>
  </tr>
  </table>
  
  
  
  <style>
  
  </style>
  
  <!--dropdown slider -->
 
 
 
 <!--dropdown slider end-->
 
  
  


<div class="footer-address-div">
<div class="footer-address-left"><img src="img/logo-new.png"></div>
<div class="footer-address-center">
<center>
<h2>Need help find the perfect Model?
<br />Call 800-690-0002</h2>
</center>
</div>


<div class="footer-address-right">
<img src="img/close.png" onclick="openCity(event, 'Hide');hide_cart_data();">
</div>
</div>

  </div>
  
  
  

<div id="customss" class="tabcontent"  onmouseout="openCity(event, 'Hide');hide_cart_data();">
    
  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:15%; float:left;">
  <h3><b>SELF-SERVE </b></h3>
  <ul>
  <li><a href="../product.php?cPath=87_114" id="ES29" onmouseover="change_header_image_selfserve(this);">ES29</a></li>
  <li><a href="../product.php?cPath=87_61"  id="ES31" onmouseover="change_header_image_selfserve(this);">ES31</a></li>
  <li><a href="../product.php?cPath=87_62"  id="ES40" onmouseover="change_header_image_selfserve(this);">ES40</a></li>
  <li><a href="../product.php?cPath=87_110"  id="ES53" onmouseover="change_header_image_selfserve(this);">ES53</a></li>
  <li><a href="../product.php?cPath=87_63"  id="ES67" onmouseover="change_header_image_selfserve(this);">ES67</a></li>
  <li><a href="../product.php?cPath=87_64"  id="ES73" onmouseover="change_header_image_selfserve(this);">ES73</a></li>
  <li><a href="../product.php?cPath=87_111"  id="ES82" onmouseover="change_header_image_selfserve(this);">ES82</a></li>
  <li><a href="../product.php?cPath=87_123"  id="ES90" onmouseover="change_header_image_selfserve(this);">ES90</a></li>
  <li><a href="../product.php?cPath=87_125"  id="ES92" onmouseover="change_header_image_selfserve(this);">ES92</a></li>
  <li><a href="../product.php?cPath=87_80"  id="B-950" onmouseover="change_header_image_selfserve(this);">B950</a></li>
  <li><a href="../product.php?cPath=87_81"  id="B-950-SWIVEL" onmouseover="change_header_image_selfserve(this);">B950-SWIVEL</a></li>
	
  <li><a href="../product.php?cPath=87_128"  id="ORBIT360" onmouseover="change_header_image_selfserve(this);">ORBIT360</a></li>
		

  
  </ul>
  </div>
  
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ES29.jpg" id="header_image_selfserve1">
  </div>
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ES29_2.jpg" id="header_image_selfserve2">
  </div>
  
  <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ES29_3.jpg" id="header_image_selfserve3">
  </div>
  
  </div>
  
  
  <!--dropdown slider
  <div  class="header-dropdow-slider-main">
  <table class="headerslide-table">
  <tr>
  <td class="headerslide-left">

  <div class="header-dropdow-buynow">
  
		  <h2 id="selfserve_header-dropdown-model-heading">EP5 Ring Adjustable</h2>
		<span id="selfserve_header-dropdown-model-learnmore"><a href="../product.php?cPath=86_129" ><button>LEARN MORE </button></a></span>

		<span id="selfserve_header-dropdown-model-buynow"><a href="../product.php?cPath=86_129"><button>SHOP NOW </button></a></span>
		
		</div>
  
  </td>
  <td  class="headerslide-right">
   <div id="demo-header2" class="carousel slide" data-ride="carousel" style="padding-top: 2%;height:300px;"   data-interval="2000">

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slider2/slide1.png" alt="EP5" id="selfserve_slides1" >
      
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide2.png" alt="EP5" id="selfserve_slides2" >
       
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide3.png" alt="EP5" id="selfserve_slides3">
      
    </div>
	
	   <div class="carousel-item">
      <img src="images/slider2/slide4.png" alt="EP5" id="selfserve_slides4">
        
    </div>
	
	
	   <div class="carousel-item">
      <img src="images/slider2/slide5.png" alt="EP5" id="selfserve_slides5">
      <div class="carousel-caption">
        
    </div>
	
  </div>
  <a class="carousel-control-prev" href="#demo-header2" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo-header2" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

   </div>
   
   
   
  
  </td>
  </tr>
  </table>
  
 
 </div>
 <!--dropdown slider end-->
 
 
 
</div>
<div id="adjustable" class="tabcontent">
  
  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:15%; float:left;">
  <h3><b>BARRIER</b></h3>
  <ul>

<li><a href="../product.php?cPath=86_72" id="EP5" onmouseover="change_header_image_barrier(this);"> EP5</a></li>
  <li><a href="../product.php?cPath=86_122"  id="Ring-EP5" onmouseover="change_header_image_barrier(this);">EP5 Ring Adjusts</a></li>
  <li><a href="../product.php?cPath=86_129"  id="EP6" onmouseover="change_header_image_barrier(this);">EP6</a></li>
  <li><a href="../product.php?cPath=86_130"  id="EP7" onmouseover="change_header_image_barrier(this);">EP7</a></li>
		

  
  </ul>
  </div>
  
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5.jpg" id="header_image_barrier1">
  </div>
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_2.jpg" id="header_image_barrier2">
  </div>
  
  <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/EP5_3.jpg" id="header_image_barrier3">
  </div>
  
  </div>
  
  
  
  
  <!--dropdown slider
  <div  class="header-dropdow-slider-main">
  <table class="headerslide-table">
  <tr>
  <td class="headerslide-left">

  <div class="header-dropdow-buynow">
  
		  <h2 id="barrier_header-dropdown-model-heading">EP5 Ring Adjustable</h2>
		<span id="barrier_header-dropdown-model-learnmore"><a href="../product.php?cPath=86_129" ><button>LEARN MORE </button></a></span>

		<span id="barrier_header-dropdown-model-buynow"><a href="../product.php?cPath=86_129"><button>SHOP NOW </button></a></span>
		
		</div>
  
  </td>
  <td  class="headerslide-right">
   <div id="demo-header3" class="carousel slide" data-ride="carousel" style="padding-top: 2%;height:300px;"   data-interval="2000">

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slider2/slide1.png" alt="EP5" id="barrier_slides1" >
      
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide2.png" alt="EP5" id="barrier_slides2" >
       
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide3.png" alt="EP5" id="barrier_slides3">
      
    </div>
	
	   <div class="carousel-item">
      <img src="images/slider2/slide4.png" alt="EP5" id="barrier_slides4">
        
    </div>
	
	
	   <div class="carousel-item">
      <img src="images/slider2/slide5.png" alt="EP5" id="barrier_slides5">
      <div class="carousel-caption">
        
    </div>
	
  </div>
  <a class="carousel-control-prev" href="#demo-header3" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo-header3" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

   </div>
   
   
  </td>
  </tr>
  </table>
  
 
 </div>
 <!--dropdown slider end-->
 
  
</div>

<div id="portable" class="tabcontent">


  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:15%; float:left;">
  <h3><b>PORTABLE </b></h3>
  <ul>
  <li><a href="../product.php?cPath=85_117" id="ALLIN1" onmouseover="change_header_image_portable(this);">ALLIN1</a></li>
  <li><a href="../product.php?cPath=85_79" id="B-950P-GLASS" onmouseover="change_header_image_portable(this);">B950P-GLASS</a></li>
  <li><a href="../product.php?cPath=85_70" id="EP-950-ACRYLIC" onmouseover="change_header_image_portable(this);">EP950-ACRYLIC</a></li>

  <li><a href="../product.php?cPath=85_131"  id="ORBIT720" onmouseover="change_header_image_portable(this);">ORBIT720</a></li>


		

  
  </ul>
  </div>
  
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ALLIN1.jpg" id="header_image_portable1">
  </div>
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ALLIN1_2.jpg" id="header_image_portable2">
  </div>
  
  <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/ALLIN1_3.jpg" id="header_image_portable3">
  </div>
  
  </div>
  


  <!--dropdown slider 
  <div  class="header-dropdow-slider-main">
  <table class="headerslide-table">
  <tr>
  <td class="headerslide-left">

  <div class="header-dropdow-buynow">
  
		  <h2 id="portable_header-dropdown-model-heading">EP5 Ring Adjustable</h2>
		<span id="portable_header-dropdown-model-learnmore"><a href="../product.php?cPath=86_129" ><button>LEARN MORE </button></a></span>

		<span id="portable_header-dropdown-model-buynow"><a href="../product.php?cPath=86_129"><button>SHOP NOW </button></a></span>
		
		</div>
  
  </td>
  <td  class="headerslide-right">
   <div id="demo-header4" class="carousel slide" data-ride="carousel" style="padding-top: 2%;height:300px;"   data-interval="2000">

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slider2/slide1.png" alt="EP5" id="portable_slides1" >
      
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide2.png" alt="EP5" id="portable_slides2" >
       
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide3.png" alt="EP5" id="portable_slides3">
      
    </div>
	
	   <div class="carousel-item">
      <img src="images/slider2/slide4.png" alt="EP5" id="portable_slides4">
        
    </div>
	
	
	   <div class="carousel-item">
      <img src="images/slider2/slide5.png" alt="EP5" id="portable_slides5">
      <div class="carousel-caption">
        
    </div>
	
  </div>
  <a class="carousel-control-prev" href="#demo-header4" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo-header4" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

   </div>
   
   
  
  </td>
  </tr>
  </table>
  
 
 </div>
 <!--dropdown slider end-->
 
 



</div>

<div id="faqs" class="tabcontent">
    
</div>


<div id="addons" class="tabcontent">


  <div style="width:100%;" align="">
   <div style="width:5%; float:left;">&nbsp;</div>
  <div style="width:15%; float:left;">
  <h3><b>ADDONS </b></h3>
  <ul>
  <li><a href="../product.php?cPath=88_121" id="Light Bar" onmouseover="change_header_image_addons(this);">Light Bar</a></li>
  <li><a href="../product.php?cPath=88_118" id="Mid-Shelves" onmouseover="change_header_image_addons(this);">Mid-Shelves</a></li>
  <li><a href="../product.php?cPath=88_120" id="Heat Lamp" onmouseover="change_header_image_addons(this);">Heat Lamp</a></li>

		

  
  </ul>
  </div>
  
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/Light Bar.jpg" id="header_image_addons1">
  </div>
 <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/Light Bar_2.jpg" id="header_image_addons2">
  </div>
  
  <div style="width:25%; float:left;padding:3px">
  <img alt="sneeze guard" title="sneeze guard for office" src="images/hover-model/Light Bar_3.jpg" id="header_image_addons3">
  </div>
  
  </div>
  
  
  
  

  <!--dropdown slider 
  <div  class="header-dropdow-slider-main">
  <table class="headerslide-table">
  <tr>
  <td class="headerslide-left">

  <div class="header-dropdow-buynow">
  
		  <h2 id="addons_header-dropdown-model-heading">EP5 Ring Adjustable</h2>
		<span id="addons_header-dropdown-model-learnmore"><a href="../product.php?cPath=86_129" ><button>LEARN MORE </button></a></span>

		<span id="addons_header-dropdown-model-buynow"><a href="../product.php?cPath=86_129"><button>SHOP NOW </button></a></span>
		
		</div>
  
  </td>
  <td  class="headerslide-right">
   <div id="demo-header5" class="carousel slide" data-ride="carousel" style="padding-top: 2%;height:300px;"   data-interval="2000">

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="images/slider2/slide1.png" alt="EP5" id="addons_slides1" >
      
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide2.png" alt="EP5" id="addons_slides2" >
       
    </div>
    <div class="carousel-item">
      <img src="images/slider2/slide3.png" alt="EP5" id="addons_slides3">
      
    </div>
	
	   <div class="carousel-item">
      <img src="images/slider2/slide4.png" alt="EP5" id="addons_slides4">
        
    </div>
	
	
	   <div class="carousel-item">
      <img src="images/slider2/slide5.png" alt="EP5" id="addons_slides5">
      <div class="carousel-caption">
        
    </div>
	
  </div>
  <a class="carousel-control-prev" href="#demo-header5" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo-header5" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

   </div>
   
   
  
  </td>
  </tr>
  </table>
  
 
 </div>
 <!--dropdown slider end-->
 
 

  
  
  
  
</div>




<div id="insideadm" class="tabcontent" >
<div style="width:100%;">
<div style="width:20%; float:left;">
 <div >

  <ul>
  <li><a href="" id="about-uss">ABOUT</a></li>
  <li><a href=""  id="contact-uss">INNOVATION</a></li>
  <li><a href=""  id="contact-uss">STORIES</a></li>
  <li><a href=""  id="contact-uss">SUSTAINABILITY</a></li>
  <li><a href=""  id="contact-uss">RETÜL</a></li>
  <li><a href=""  id="contact-uss">OUTRIDE</a></li>
  </ul>
  </div></div>

  <div style="width: 79%;float:left;margin-top: 2px;" class="insideadm-img">
   <img alt="sneeze guard" title="sneeze guard for office" src="img/about-us.jpg" style=""  id="insideadm-img" />
  
  </div>
  
 <div style="width:35%;float:left; padding:10px;text-align:left; margin-left: -52%; margin-top: 56px;">
<h2 id="insideadm-heading">ABOUT US</h2>
<p id="insideadm-contanet">ADM Sneezeguards was founded in Antioch, California as a business-to-business (B2B) sales company. The company established the business in 1988 with amazing Glass Models and Designs.</p>


  </div>
  
  </div>  
</div>








  
  
  
</div>





