<?php
	require("configure.php");
	require_once("Mobile_Detect.php");
	$detect = new Mobile_Detect();

	$mod=str_replace("\\","", $_POST["mod"]);
	$bay=str_replace("\\","",$_POST["bay"]);
	$ends=str_replace("\\","",$_POST["end"]);
	$face1=str_replace("\\","",$_POST["face1"]);
	$face2=str_replace("\\","",$_POST["face2"]);
	$face3=str_replace("\\","",$_POST["face3"]);
	$osc=str_replace("\\","",$_POST["osc"]);
	$face4=str_replace("\\","",$_POST["face4"]);
	$post=str_replace("\\","",$_POST["post"]);
	$left=str_replace("\\","",$_POST["left"]);
	$right=str_replace("\\","",$_POST["right"]);
	$tot=str_replace("\\","",$_POST["tot"]);
	$im_id=str_replace("\\","",$_POST["im_id"]);
	$sv=str_replace("\\","",$_POST["sv"]);
	$img=str_replace("\\","",$_POST["img"]);
	
	$posttype=str_replace("\\","",$_POST["ptype"]);
	$postdegree=str_replace("\\","",$_POST["pdegree"]);
	$postposition=str_replace("\\","",$_POST["pposi"]);
	$corneryes=str_replace("\\","",$_POST["corny"]);
	//$posttype  $postdegree  $postposition  $corneryes
	
	//echo $im_id." ".$osc." ".$sv;
	$face1x=0; $face1y=0;
    $face2x=0; $face2y=0;
    $face3x=0; $face3y=0;
    $face4x=0; $face4y=0;
    $postx=0; $posty=0;
    $rightx=0; $righty=0;
    $leftx=0; $lefty=0;
    $totx=0; $toty=0;
	if($mod=="EP5"){
		if($ends==1){
			//$img="BOTHENDS.jpg";
		}else if($ends==2){
			//$img="RIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="LEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-51BAY/".$img.".jpg";
			$face1x=400;$face1y=330;
			$postx=500; $posty=150;
    		$rightx=400;$righty=20;
    		$leftx=170;$lefty=90;
    		$totx=440;$toty=370;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-52BAY/".$img.".jpg";
			$face1x=300;$face1y=340;
      		$face2x=470;$face2y=245;
      		$postx=540; $posty=120;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=430;$toty=320;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-53BAY/".$img.".jpg";
			$face1x=260;$face1y=345;
      		$face2x=410;$face2y=255;
      		$face3x=510;$face3y=190;
      		$postx=555; $posty=95;
    		$rightx=495; $righty=10;
    		$leftx=55; $lefty=155;
    		$totx=450;$toty=280;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-54BAY/".$img.".jpg";
			$face1x=220; $face1y=350;
      		$face2x=360; $face2y=265;
      		$face3x=460; $face3y=205;
      		$face4x=535; $face4y=160;
      		$postx=575; $posty=85;
    		$rightx=525; $righty=15;
    		$leftx=40; $lefty=180;
    		$totx=450;$toty=250;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		//imagejpeg($my_img,$pt."scrn1.jpg");
		
		imagedestroy( $my_img );
	}
	  else if($mod=="Ring-EP5"){
		if($ends==1){
			//$img="BOTHENDS.jpg";
		}else if($ends==2){
			//$img="RIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="LEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."Ring-EP-51BAY/".$img.".jpg";
			$face1x=400;$face1y=330;
			$postx=500; $posty=150;
    		$rightx=400;$righty=20;
    		$leftx=170;$lefty=90;
    		$totx=440;$toty=370;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."Ring-EP-52BAY/".$img.".jpg";
			$face1x=300;$face1y=340;
      		$face2x=470;$face2y=245;
      		$postx=540; $posty=120;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=430;$toty=320;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."Ring-EP-53BAY/".$img.".jpg";
			$face1x=260;$face1y=345;
      		$face2x=410;$face2y=255;
      		$face3x=510;$face3y=190;
      		$postx=555; $posty=95;
    		$rightx=495; $righty=10;
    		$leftx=55; $lefty=155;
    		$totx=450;$toty=280;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."Ring-EP-54BAY/".$img.".jpg";
			$face1x=220; $face1y=350;
      		$face2x=360; $face2y=265;
      		$face3x=460; $face3y=205;
      		$face4x=535; $face4y=160;
      		$postx=575; $posty=85;
    		$rightx=525; $righty=15;
    		$leftx=40; $lefty=180;
    		$totx=450;$toty=250;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		//imagejpeg($my_img,$pt."scrn1.jpg");
		
		imagedestroy( $my_img );
	}
	
	else if($mod=="EP15"){
		if($ends==1){
			//$img="BOTHENDS.jpg";
		}else if($ends==2){
			//$img="RIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="LEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-151BAY/".$img.".jpg";
			$face1x=310;$face1y=315;
			$postx=540; $posty=210;
    		$rightx=460;$righty=210;
    		$leftx=80;$lefty=290;
    		$totx=320;$toty=355;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			
			//$posttype  $postdegree  $postposition  $corneryes
			
			/*
			
			
			if($corneryes=="1")
			{
			if($posttype=="Outer")
			{
			if($postdegree=="90 Degree")
			{
			if($postposition=="1st Center Post from Left")
			{
			$path="images/"."EP-152BAY/".$img.".jpg";
      		$posty=215;$postx=55; 
			$face1y=322;$face1x=197;
      		$face2y=312;$face2x=450;
    		$lefty=181;$leftx=136; 
    		$righty=140;$rightx=520; 
    		$totx=1135;$toty=1465;	
			}
			}
			elseif($postdegree=="135 Degree")
			{
			if($postposition=="1st Center Post from Left")
			{
			$path="images/"."EP-152BAY/".$img.".jpg";
      		$posty=210;$postx=80; 
			$face1y=322;$face1x=197;
      		$face2y=322;$face2x=450;
    		$lefty=225;$leftx=30; 
    		$righty=190;$rightx=570; 
    		$totx=1135;$toty=1465;	
			}
			}
		
				
			}				
				
			}
			else{*/
				$path="images/"."EP-152BAY/".$img.".jpg";
			$face1x=200;$face1y=320;
      		$face2x=380;$face2y=290;
      		$postx=560; $posty=220;
    		$rightx=500; $righty=225;
    		$leftx=30; $lefty=310;
    		$totx=320;$toty=330;
			//}
			
			
			
			
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			
			
			/*if($corneryes=="1")
			{
			if($posttype=="Outer")
			{
			if($postdegree=="90 Degree")
			{
			if($postposition=="1st Center Post from Left")
			{
			$path="images/"."EP-153BAY/".$img.".jpg";
			$postx=35;  $posty=255;
			$face1x=167; $face1y=352;
      		$face2x=410; $face2y=352;
			$face3x=520; $face3y=280;
    		$leftx=100; $lefty=226;
    		$rightx=530; $righty=130; 
    		$totx=1135;$toty=1465;
			}
			elseif($postposition=="2nd Center Post from Left")
			{
			$path="images/"."EP-153BAY/".$img.".jpg";
			$postx=35;  $posty=160;
			$face1x=117; $face1y=280;
      		$face2x=260; $face2y=346;
			$face3x=520; $face3y=350;
    		$leftx=60; $lefty=180;
    		$rightx=520; $righty=190; 
    		$totx=1135;$toty=1465;
			}
			}
			elseif($postdegree=="135 Degree")
			{
			if($postposition=="1st Center Post from Left")
			{
			$path="images/"."EP-153BAY/".$img.".jpg";
			$postx=35;  $posty=210;
			$face1x=122; $face1y=310;
      		$face2x=305; $face2y=325;
			$face3x=440; $face3y=320;
    		$leftx=70; $lefty=240;
    		$rightx=540; $righty=265; 
    		$totx=1135;$toty=1465;
			}
			elseif($postposition=="2nd Center Post from Left")
			{
			$path="images/"."EP-153BAY/".$img.".jpg";	
			$postx=35;  $posty=195;
			$face1x=112; $face1y=295;
      		$face2x=240; $face2y=329;
			$face3x=440; $face3y=340;
    		$leftx=70; $lefty=225;
    		$rightx=540; $righty=280; 
    		$totx=1135;$toty=1465;
			}
			}
			}
			}
			else{*/
			$path="images/"."EP-153BAY/".$img.".jpg";	
			$face1x=190;$face1y=310;
      		$face2x=310;$face2y=285;
      		$face3x=470;$face3y=250;
      		$postx=600; $posty=205;
    		$rightx=560; $righty=190;
    		$leftx=35; $lefty=300;
    		$totx=320;$toty=330;
			//}
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			/*
			if($corneryes=="1")
			{
			if($posttype=="Outer")
			{
			if($postdegree=="90 Degree")
			{
			if($postposition=="1st Center Post from Left")
			{
			$path="images/"."EP-154BAY/".$img.".jpg";	
			$postx=25;  $posty=225;
			$face1x=130; $face1y=322;
      		$face2x=310; $face2y=322;
			$face3x=400; $face3y=260;
			$face4x=485; $face4y=200;
    		$leftx=50; $lefty=246;
    		$rightx=500; $righty=100; 
    		$totx=1135;$toty=1465;	
			}
			elseif($postposition=="2nd Center Post from Left")
			{
			$path="images/"."EP-154BAY/".$img.".jpg";
			$postx=25;  $posty=225;
			$face1x=115; $face1y=287;
      		$face2x=235; $face2y=337;
			$face3x=430; $face3y=325;
			$face4x=515; $face4y=270;
    		$leftx=50; $lefty=200;
    		$rightx=520; $righty=150; 
    		$totx=1135;$toty=1465;		
			}
			elseif($postposition=="3rd Center Post from Left")
			{
			$path="images/"."EP-154BAY/".$img.".jpg";	
			$postx=35;  $posty=158;
			$face1x=115; $face1y=257;
      		$face2x=220; $face2y=302;
			$face3x=340; $face3y=340;
			$face4x=535; $face4y=340;
    		$leftx=65; $lefty=180;
    		$rightx=550; $righty=220; 
    		$totx=1135;$toty=1465;	
			}
			
			}
			elseif($postdegree=="135 Degree")
			{
			if($postposition=="1st Center Post from Left")
			{
			$path="images/"."EP-154BAY/".$img.".jpg";	
			$postx=25;  $posty=215;
			$face1x=100; $face1y=302;
      		$face2x=260; $face2y=318;
			$face3x=380; $face3y=310;
			$face4x=490; $face4y=302;
    		$leftx=65; $lefty=240;
    		$rightx=550; $righty=250; 
    		$totx=1135;$toty=1465;	
			}
			elseif($postposition=="2nd Center Post from Left")
			{
			$path="images/"."EP-154BAY/".$img.".jpg";	
			$postx=25;  $posty=195;
			$face1x=100; $face1y=280;
      		$face2x=210; $face2y=302;
			$face3x=365; $face3y=310;
			$face4x=485; $face4y=298;
    		$leftx=65; $lefty=220;
    		$rightx=550; $righty=250; 
    		$totx=1135;$toty=1465;		
			}
			elseif($postposition=="3rd Center Post from Left")
			{
			$path="images/"."EP-154BAY/".$img.".jpg";	
			$postx=25;  $posty=180;
			$face1x=100; $face1y=265;
      		$face2x=200; $face2y=287;
			$face3x=305; $face3y=305;
			$face4x=465; $face4y=315;
    		$leftx=65; $lefty=210;
    		$rightx=550; $righty=260; 
    		$totx=1135;$toty=1465;	
			}
			
			}
			}
			}
			else{*/
			$path="images/"."EP-154BAY/".$img.".jpg";	
			$face1x=160; $face1y=290;
      		$face2x=290; $face2y=267;
      		$face3x=400; $face3y=245;
      		$face4x=500; $face4y=228;
      		$postx=595; $posty=170;
    		$rightx=560; $righty=195;
    		$leftx=30; $lefty=290;
    		$totx=350;$toty=275;
			//}
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		//imagejpeg($my_img,$pt."scrn1.jpg");
		
		imagedestroy( $my_img );
	}else if($mod=="EP11"){
		if($ends==1){
			//$img="VNORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="VNORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="VNORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="VNORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-111BAY/".$img.".jpg";
			$face1x=370;$face1y=305;
			$postx=510; $posty=110;
    		$rightx=400;$righty=20;
    		$leftx=170;$lefty=90;
    		$totx=410;$toty=335;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			
			/*if($corneryes=="1")
			{
			if($posttype=="Inner")
			{
			if($postdegree=="90 Degree")
			{
			if($postposition=="1st Center Post from Left")
			{
			$path="images/"."EP-112BAY/".$img.".jpg";
			$postx=545; $posty=80;
			$face1x=255;$face1y=315;
      		$face2x=450;$face2y=205;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=1135;$toty=1465;	
			}
			
			
			}
			}
			}
			
			else{*/
			$path="images/"."EP-112BAY/".$img.".jpg";
			$face1x=255;$face1y=315;
      		$face2x=450;$face2y=205;
      		$postx=545; $posty=80;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=400;$toty=280;
			
			//}
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-113BAY/".$img.".jpg";
			$face1x=220;$face1y=315;
      		$face2x=395;$face2y=215;
      		$face3x=515;$face3y=150;
      		$postx=580; $posty=60;
    		$rightx=495; $righty=10;
    		$leftx=55; $lefty=155;
    		$totx=425;$toty=240;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-114BAY/".$img.".jpg";
			$face1x=180; $face1y=340;
      		$face2x=340; $face2y=250;
      		$face3x=450; $face3y=185;
      		$face4x=540; $face4y=135;
      		$postx=580; $posty=65;
    		$rightx=525; $righty=15;
    		$leftx=40; $lefty=180;
    		$totx=425;$toty=230;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="EP12"){
		if($ends==1){
			//$img="VERTNORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="VERTNORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="VERTNORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="VERTNORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-121BAY/".$img.".jpg";
			$face1x=360;$face1y=290;
			$postx=495; $posty=95;
    		$rightx=400;$righty=20;
    		$leftx=170;$lefty=90;
    		$totx=415;$toty=330;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-122BAY/".$img.".jpg";
			$face1x=270;$face1y=315;
      		$face2x=460;$face2y=195;
      		$postx=550; $posty=70;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=415;$toty=280;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-123BAY/".$img.".jpg";
			$face1x=210;$face1y=335;
      		$face2x=385;$face2y=230;
      		$face3x=515;$face3y=150;
      		$postx=580; $posty=60;
    		$rightx=495; $righty=10;
    		$leftx=55; $lefty=155;
    		$totx=420;$toty=250;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-124BAY/".$img.".jpg";
			$face1x=180; $face1y=340;
      		$face2x=340; $face2y=245;
      		$face3x=450; $face3y=175;
      		$face4x=540; $face4y=125;
      		$postx=580; $posty=55;
    		$rightx=525; $righty=15;
    		$leftx=40; $lefty=180;
    		$totx=425;$toty=225;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="EP21"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-211BAY/".$img.".jpg";
			$face1x=380;$face1y=300;
			$postx=500; $posty=150;
    		$rightx=400;$righty=20;
    		$leftx=170;$lefty=90;
    		$totx=420;$toty=330;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-212BAY/".$img.".jpg";
			$face1x=300;$face1y=325;
      		$face2x=470;$face2y=200;
      		$postx=540; $posty=120;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=430;$toty=270;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-213BAY/".$img.".jpg";
			$face1x=240;$face1y=340;
      		$face2x=410;$face2y=220;
      		$face3x=515;$face3y=145;
      		$postx=555; $posty=95;
    		$rightx=495; $righty=10;
    		$leftx=55; $lefty=155;
    		$totx=430;$toty=240;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-214BAY/".$img.".jpg";
			$face1x=210; $face1y=340;
      		$face2x=360; $face2y=240;
      		$face3x=460; $face3y=170;
      		$face4x=540; $face4y=115;
      		$postx=575; $posty=85;
    		$rightx=525; $righty=15;
    		$leftx=40; $lefty=180;
    		$totx=450;$toty=220;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="EP22"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-221BAY/".$img.".jpg";
			$face1x=390;$face1y=290;
			$postx=500; $posty=150;
    		$rightx=400;$righty=20;
    		$leftx=170;$lefty=90;
    		$totx=440;$toty=330;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-222BAY/".$img.".jpg";
			$face1x=295;$face1y=315;
      		$face2x=465;$face2y=190;
      		$postx=540; $posty=120;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=430;$toty=270;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-223BAY/".$img.".jpg";
			$face1x=240;$face1y=335;
      		$face2x=405;$face2y=220;
      		$face3x=515;$face3y=140;
      		$postx=555; $posty=95;
    		$rightx=495; $righty=10;
    		$leftx=55; $lefty=155;
    		$totx=435;$toty=240;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-224BAY/".$img.".jpg";
			$face1x=210; $face1y=340;
      		$face2x=360; $face2y=240;
      		$face3x=460; $face3y=170;
      		$face4x=540; $face4y=115;
      		$postx=575; $posty=85;
    		$rightx=525; $righty=15;
    		$leftx=40; $lefty=180;
    		$totx=440;$toty=220;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="EP36"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-361BAY/".$img.".jpg";
			$face1x=380;$face1y=325;
			$postx=500; $posty=150;
    		$rightx=400;$righty=20;
    		$leftx=170;$lefty=90;
    		$totx=415;$toty=350;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-362BAY/".$img.".jpg";
			$face1x=280;$face1y=330;
      		$face2x=450;$face2y=225;
      		$postx=540; $posty=120;
    		$rightx=465; $righty=15;
    		$leftx=85; $lefty=105;
    		$totx=415;$toty=300;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-363BAY/".$img.".jpg";
			$face1x=230;$face1y=340;
      		$face2x=395;$face2y=240;
      		$face3x=515;$face3y=170;
      		$postx=555; $posty=95;
    		$rightx=495; $righty=10;
    		$leftx=55; $lefty=155;
    		$totx=435;$toty=265;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$left="";
				$right="";
				$tot="";
			}
			$path="images/"."EP-364BAY/".$img.".jpg";
			$face1x=190; $face1y=340;
      		$face2x=340; $face2y=250;
      		$face3x=455; $face3y=190;
      		$face4x=540; $face4y=140;
      		$postx=575; $posty=85;
    		$rightx=525; $righty=15;
    		$leftx=40; $lefty=180;
    		$totx=430;$toty=235;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		//imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		//imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		//imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ES29"){
		if($ends==1){
			//$img="BOTHENDS.jpg";
		}else if($ends==2){
			//$img="RIGHTEND.jpg";
			//$left="";
		}else if($ends==3){
			//$img="LEFTEND.jpg";
			//$right="";
		}else if($ends==4){
			//$img="NOENDS.jpg";
			//$right="";
			//$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES291BAY/".$img.".jpg";
			$face1x=390;$face1y=300;
			$postx=500; $posty=150;
    		$rightx=140;$righty=330;
    		$leftx=420;$lefty=190;
    		$totx=460;$toty=335;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES292BAY/".$img.".jpg";
			$face1x=300;$face1y=310;
      		$face2x=495;$face2y=205;
      		$postx=540; $posty=120;
    		$rightx=65; $righty=340;
    		$leftx=340; $lefty=215;
    		$totx=465;$toty=280;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES293BAY/".$img.".jpg";
			$face1x=235;$face1y=315;
      		$face2x=415;$face2y=225;
      		$face3x=530;$face3y=165;
      		$postx=555; $posty=95;
    		$rightx=45; $righty=340;
    		$leftx=285; $lefty=230;
    		$totx=450;$toty=245;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES294BAY/".$img.".jpg";
			$face1x=200; $face1y=310;
      		$face2x=360; $face2y=230;
      		$face3x=470; $face3y=178;
      		$face4x=555; $face4y=135;
      		$postx=575; $posty=85;
    		$rightx=45; $righty=325;
    		$leftx=250; $lefty=240;
    		$totx=450;$toty=215;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, (str_replace('"', "", $right)-4).'"', $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ES31"){
		if($ends==1){
			//$img="BOTHENDS.jpg";
		}else if($ends==2){
			//$img="RIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="LEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-311BAY/".$img.".jpg";
			$face1x=380;$face1y=325;
			$postx=500; $posty=150;
    		$rightx=140;$righty=330;
    		$leftx=170;$lefty=90;
    		$totx=410;$toty=350;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-312BAY/".$img.".jpg";
			$face1x=280;$face1y=330;
      		$face2x=450;$face2y=220;
      		$postx=540; $posty=120;
    		$rightx=65; $righty=340;
    		$leftx=85; $lefty=105;
    		$totx=410;$toty=290;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-313BAY/".$img.".jpg";
			$face1x=225;$face1y=340;
      		$face2x=400;$face2y=240;
      		$face3x=510;$face3y=170;
      		$postx=555; $posty=95;
    		$rightx=45; $righty=340;
    		$leftx=55; $lefty=155;
    		$totx=410;$toty=260;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-314BAY/".$img.".jpg";
			$face1x=190; $face1y=340;
      		$face2x=345; $face2y=250;
      		$face3x=450; $face3y=190;
      		$face4x=540; $face4y=140;
      		$postx=575; $posty=85;
    		$rightx=45; $righty=325;
    		$leftx=40; $lefty=180;
    		$totx=430;$toty=235;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ES40"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-401BAY/".$img.".jpg";
			$face1x=360;$face1y=310;
			$postx=500; $posty=150;
    		$rightx=140;$righty=330;
    		$leftx=170;$lefty=90;
    		$totx=420;$toty=360;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-402BAY/".$img.".jpg";
			$face1x=270;$face1y=325;
      		$face2x=470;$face2y=235;
      		$postx=540; $posty=120;
    		$rightx=65; $righty=340;
    		$leftx=85; $lefty=105;
    		$totx=440;$toty=310;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-403BAY/".$img.".jpg";
			$face1x=220;$face1y=335;
      		$face2x=395;$face2y=255;
      		$face3x=515;$face3y=195;
      		$postx=555; $posty=95;
    		$rightx=45; $righty=340;
    		$leftx=55; $lefty=155;
    		$totx=430;$toty=280;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-404BAY/".$img.".jpg";
			$face1x=170; $face1y=315;
      		$face2x=330; $face2y=245;
      		$face3x=450; $face3y=195;
      		$face4x=535; $face4y=160;
      		$postx=575; $posty=85;
    		$rightx=45; $righty=325;
    		$leftx=40; $lefty=180;
    		$totx=430;$toty=235;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ES53"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES531BAY/".$img.".jpg";
			$face1x=415;$face1y=275;
			$postx=500; $posty=150;
    		$rightx=140;$righty=330;
    		$leftx=170;$lefty=90;
    		$totx=460;$toty=290;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES532BAY/".$img.".jpg";
			$face1x=335;$face1y=300;
      		$face2x=525;$face2y=175;
      		$postx=540; $posty=120;
    		$rightx=65; $righty=340;
    		$leftx=85; $lefty=105;
    		$totx=480;$toty=240;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES533BAY/".$img.".jpg";
			$face1x=270;$face1y=295;
      		$face2x=450;$face2y=185;
      		$face3x=550;$face3y=123;
      		$postx=555; $posty=95;
    		$rightx=45; $righty=340;
    		$leftx=55; $lefty=155;
    		$totx=470;$toty=197;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES534BAY/".$img.".jpg";
			$face1x=230; $face1y=290;
      		$face2x=390; $face2y=200;
      		$face3x=495; $face3y=140;
      		$face4x=570; $face4y=100;
      		$postx=575; $posty=85;
    		$rightx=45; $righty=325;
    		$leftx=40; $lefty=180;
    		$totx=470;$toty=175;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, "", $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, "", $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ES67"){
		if($ends==1){
			//$img="BOTHENDS.jpg";
		}else if($ends==2){
			//$img="RIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="LEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-671BAY/".$img.".jpg";
			$face1x=340;$face1y=310;
			$postx=500; $posty=150;
    		$rightx=140;$righty=330;
    		$leftx=170;$lefty=90;
    		$totx=390;$toty=345;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-672BAY/".$img.".jpg";
			$face1x=270;$face1y=325;
      		$face2x=450;$face2y=220;
      		$postx=540; $posty=120;
    		$rightx=65; $righty=340;
    		$leftx=85; $lefty=105;
    		$totx=410;$toty=290;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-673BAY/".$img.".jpg";
			$face1x=220;$face1y=330;
      		$face2x=395;$face2y=235;
      		$face3x=515;$face3y=165;
      		$postx=555; $posty=95;
    		$rightx=45; $righty=340;
    		$leftx=55; $lefty=155;
    		$totx=430;$toty=253;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-674BAY/".$img.".jpg";
			$face1x=190; $face1y=330;
      		$face2x=345; $face2y=245;
      		$face3x=450; $face3y=185;
      		$face4x=540; $face4y=135;
      		$postx=575; $posty=85;
    		$rightx=45; $righty=325;
    		$leftx=40; $lefty=180;
    		$totx=430;$toty=226;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ES73"){
		if($ends==1){
			//$img="BOTHENDS.jpg";
		}else if($ends==2){
			//$img="RIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="LEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-731BAY/".$img.".jpg";
			$face1x=350;$face1y=320;
			$postx=500; $posty=150;
    		$rightx=140;$righty=330;
    		$leftx=170;$lefty=90;
    		$totx=400;$toty=350;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-732BAY/".$img.".jpg";
			$face1x=260;$face1y=345;
      		$face2x=440;$face2y=230;
      		$postx=540; $posty=120;
    		$rightx=65; $righty=340;
    		$leftx=85; $lefty=105;
    		$totx=410;$toty=300;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-733BAY/".$img.".jpg";
			$face1x=230;$face1y=345;
      		$face2x=400;$face2y=245;
      		$face3x=510;$face3y=180;
      		$postx=555; $posty=95;
    		$rightx=45; $righty=340;
    		$leftx=55; $lefty=155;
    		$totx=425;$toty=265;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES-734BAY/".$img.".jpg";
			$face1x=190; $face1y=350;
      		$face2x=340; $face2y=265;
      		$face3x=445; $face3y=205;
      		$face4x=533; $face4y=155;
      		$postx=575; $posty=85;
    		$rightx=45; $righty=325;
    		$leftx=40; $lefty=180;
    		$totx=420;$toty=250;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ES82"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			//$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			//$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES821BAY/".$img.".jpg";
			$face1x=420;$face1y=310;
			$postx=500; $posty=150;
    		$rightx=80;$righty=320;
    		$leftx=125;$lefty=305;
    		$totx=465;$toty=340;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES822BAY/".$img.".jpg";
			$face1x=300;$face1y=320;
      		$face2x=495;$face2y=240;
      		$postx=540; $posty=120;
    		$rightx=35; $righty=330;
    		$leftx=75; $lefty=320;
    		$totx=440;$toty=295;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES823BAY/".$img.".jpg";
			$face1x=235;$face1y=325;
      		$face2x=415;$face2y=245;
      		$face3x=540;$face3y=185;
      		$postx=555; $posty=95;
    		$rightx=30; $righty=340;
    		$leftx=55; $lefty=325;
    		$totx=440;$toty=260;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES824BAY/".$img.".jpg";
			$face1x=190; $face1y=335;
      		$face2x=350; $face2y=257;
      		$face3x=465; $face3y=200;
      		$face4x=560; $face4y=155;
      		$postx=575; $posty=85;
    		$rightx=25; $righty=345;
    		$leftx=50; $lefty=335;
    		$totx=435;$toty=240;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, (str_replace('"',"", $right)-4).'"', $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}
	else if($mod=="ES92"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			//$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			//$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES921BAY/".$img.".jpg";
			$face1x=355;$face1y=335;
			$postx=500; $posty=150;
			
    		$rightx=490;$righty=90;
    		//$rightx=155;$righty=340;
    		$leftx=155;$lefty=340;
    		//$leftx=190;$lefty=315;
			
    		$totx=375;$toty=355;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES922BAY/".$img.".jpg";
			$face1x=265;$face1y=326;
      		$face2x=430;$face2y=250;
      		$postx=540; $posty=120;
    		$rightx=550; $righty=75;
    		$leftx=85; $lefty=318;
    		$totx=370;$toty=310;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES923BAY/".$img.".jpg";
			$face1x=245;$face1y=335;
      		$face2x=375;$face2y=268;
      		$face3x=480;$face3y=213;
      		$postx=515; $posty=95;
    		$rightx=570; $righty=75;
    		$leftx=75; $lefty=340;
    		$totx=390;$toty=315;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES924BAY/".$img.".jpg";
			$face1x=200; $face1y=318;
      		$face2x=320; $face2y=267;
      		$face3x=427; $face3y=218;
      		$face4x=513; $face4y=178;
      		$postx=575; $posty=85;
    		$rightx=580; $righty=58;
    		$leftx=54; $lefty=318;
    		$totx=390;$toty=280;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		$inner_t=str_replace('"',"", $right)-15.75;
		if($inner_t=="8.25"){ $inner="8-1/4"; }
		if($inner_t=="9.25"){ $inner="9-1/4"; }
		if($inner_t=="10.25"){ $inner="10-1/4"; }
		if($inner_t=="11.25"){ $inner="11-1/4"; }
		if($inner_t=="12.25"){ $inner="12-1/4"; }
		if($inner_t=="13.25"){ $inner="13-1/4"; }
		if($inner_t=="14.25"){ $inner="14-1/4"; }
		if($inner_t=="15.25"){ $inner="15-1/4"; }
		if($inner_t=="16.25"){ $inner="16-1/4"; }
		if($inner_t=="17.25"){ $inner="17-1/4"; }
		if($inner_t=="18.25"){ $inner="18-1/4"; }
		if($inner_t=="19.25"){ $inner="19-1/4"; }
		if($inner_t=="20.25"){ $inner="20-1/4"; }
		if($inner_t=="21.25"){ $inner="21-1/4"; }
		if($inner_t=="22.25"){ $inner="22-1/4"; }
		if($inner_t=="23.25"){ $inner="23-1/4"; }
		if($inner_t=="24.25"){ $inner="24-1/4"; }
		if($inner_t=="25.25"){ $inner="25-1/4"; }
		if($inner_t=="26.25"){ $inner="26-1/4"; }
		if($inner_t=="27.25"){ $inner="27-1/4"; }
		if($inner_t=="28.25"){ $inner="28-1/4"; }
		if($inner_t=="29.25"){ $inner="29-1/4"; }
		if($inner_t=="30.25"){ $inner="30-1/4"; }
		if($inner_t=="31.25"){ $inner="31-1/4"; }
		if($inner_t=="32.25"){ $inner="32-1/4"; }
		if($inner_t=="33.25"){ $inner="33-1/4"; }
		if($inner_t=="34.25"){ $inner="34-1/4"; }
		if($inner_t=="35.25"){ $inner="35-1/4"; }
		if($inner_t=="36.25"){ $inner="36-1/4"; }
		if($inner_t=="37.25"){ $inner="37-1/4"; }
		if($inner_t=="38.25"){ $inner="38-1/4"; }
		$counter_t=str_replace('"',"", $right)-2.25;
		if($counter_t=="21.75"){ $counter="21-3/4"; }
		if($counter_t=="22.75"){ $counter="22-3/4"; }
		if($counter_t=="23.75"){ $counter="23-3/4"; }
		if($counter_t=="24.75"){ $counter="24-3/4"; }
		if($counter_t=="25.75"){ $counter="25-3/4"; }
		if($counter_t=="26.75"){ $counter="26-3/4"; }
		if($counter_t=="27.75"){ $counter="27-3/4"; }
		if($counter_t=="28.75"){ $counter="28-3/4"; }
		if($counter_t=="29.75"){ $counter="29-3/4"; }
		if($counter_t=="30.75"){ $counter="30-3/4"; }
		if($counter_t=="31.75"){ $counter="31-3/4"; }
		if($counter_t=="32.75"){ $counter="32-3/4"; }
		if($counter_t=="33.75"){ $counter="33-3/4"; }
		if($counter_t=="34.75"){ $counter="34-3/4"; }
		if($counter_t=="35.75"){ $counter="35-3/4"; }
		if($counter_t=="36.75"){ $counter="36-3/4"; }
		if($counter_t=="37.75"){ $counter="37-3/4"; }
		if($counter_t=="38.75"){ $counter="38-3/4"; }
		if($counter_t=="39.75"){ $counter="39-3/4"; }
		if($counter_t=="40.75"){ $counter="40-3/4"; }
		if($counter_t=="41.75"){ $counter="41-3/4"; }
		if($counter_t=="42.75"){ $counter="42-3/4"; }
		if($counter_t=="43.75"){ $counter="43-3/4"; }
		if($counter_t=="44.75"){ $counter="44-3/4"; }
		if($counter_t=="45.75"){ $counter="45-3/4"; }
		if($counter_t=="46.75"){ $counter="46-3/4"; }
		if($counter_t=="47.75"){ $counter="47-3/4"; }
		if($counter_t=="48.75"){ $counter="48-3/4"; }
		if($counter_t=="49.75"){ $counter="49-3/4"; }
		if($counter_t=="50.75"){ $counter="50-3/4"; }
		if($counter_t=="51.75"){ $counter="51-3/4"; }
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, ($inner).'"', $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, ($counter).'"', $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}
	else if($mod=="ES90"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			//$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			//$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES901BAY/".$img.".jpg";
			$face1x=420;$face1y=320;
			$postx=500; $posty=150;
    		$rightx=80;$righty=320;
    		$leftx=125;$lefty=305;
    		$totx=485;$toty=390;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES902BAY/".$img.".jpg";
			$face1x=294;$face1y=370;
      		$face2x=455;$face2y=295;
      		$postx=540; $posty=120;
    		$rightx=35; $righty=330;
    		$leftx=75; $lefty=320;
    		$totx=400;$toty=370;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES903BAY/".$img.".jpg";
			$face1x=245;$face1y=385;
      		$face2x=399;$face2y=305;
      		$face3x=510;$face3y=250;
      		$postx=555; $posty=95;
    		$rightx=30; $righty=340;
    		$leftx=55; $lefty=325;
    		$totx=390;$toty=350;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES904BAY/".$img.".jpg";
			$face1x=210; $face1y=355;
      		$face2x=360; $face2y=277;
      		$face3x=465; $face3y=220;
      		$face4x=565; $face4y=170;
      		$postx=575; $posty=85;
    		$rightx=25; $righty=345;
    		$leftx=50; $lefty=335;
    		$totx=420;$toty=295;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		//imagestring( $my_img, 5, $leftx, $lefty, (str_replace('"',"", $right)-0).'"', $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}
	else if($mod=="ES47"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			//$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			//$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES471BAY/".$img.".jpg";
			$face1x=370;$face1y=320;
			$postx=500; $posty=150;
    		$rightx=80;$righty=320;
    		$leftx=125;$lefty=305;
    		$totx=390;$toty=360;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES472BAY/".$img.".jpg";
			$face1x=294;$face1y=323;
      		$face2x=475;$face2y=242;
      		$postx=540; $posty=120;
    		$rightx=35; $righty=330;
    		$leftx=75; $lefty=320;
    		$totx=410;$toty=315;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES473BAY/".$img.".jpg";
			$face1x=245;$face1y=315;
      		$face2x=399;$face2y=255;
      		$face3x=510;$face3y=210;
      		$postx=555; $posty=95;
    		$rightx=30; $righty=340;
    		$leftx=55; $lefty=325;
    		$totx=375;$toty=295;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				//$left="";
				//$right="";
				$tot="";
			}
			$path="images/"."ES474BAY/".$img.".jpg";
			$face1x=170; $face1y=320;
      		$face2x=300; $face2y=270;
      		$face3x=425; $face3y=225;
      		$face4x=525; $face4y=185;
      		$postx=575; $posty=85;
    		$rightx=25; $righty=345;
    		$leftx=50; $lefty=335;
    		$totx=410;$toty=265;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		//imagestring( $my_img, 5, $leftx, $lefty, (str_replace('"',"", $right)-4).'"', $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}
	
	else if($mod=="ED20"){
		if($ends==1){
			//$img="VERTSSNOLYTBOTHENDS.jpg";
		}else if($ends==2){
			//$img="VERTSSNOLYTRIGHTEND.jpg";
			//$left="";
		}else if($ends==3){
			//$img="VERTSSNOLYTLEFTEND.jpg";
			//$right="";
		}else if($ends==4){
			//$img="VERTSSNOLYTNOENDS.jpg";
			//$right="";
			//$left="";
		}
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$tot="";
			}
			$path="images/"."ED201BAY/".$img.".jpg";
			$face1x=445;$face1y=305;
			$postx=580; $posty=125;
    		$rightx=100;$righty=335;
    		$leftx=135;$lefty=320;
    		$totx=490;$toty=330;
		}else if($bay=="2BAY"){
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$tot="";
			}
			$path="images/"."ED202BAY/".$img.".jpg";
			$face1x=325;$face1y=310;
      		$face2x=515;$face2y=210;
      		$postx=580; $posty=110;
    		$rightx=50; $righty=340;
    		$leftx=80; $lefty=325;
    		$totx=470;$toty=265;
		}else if($bay=="3BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$tot="";
			}
			$path="images/"."ED203BAY/".$img.".jpg";
			$face1x=260;$face1y=303;
      		$face2x=430;$face2y=218;
      		$face3x=545;$face3y=162;
      		$postx=580; $posty=90;
    		$rightx=39; $righty=325;
    		$leftx=65; $lefty=313;
    		$totx=460;$toty=233;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass"||$face3=="No Glass"||$face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$tot="";
			}
			$path="images/"."ED204BAY/".$img.".jpg";
			$face1x=220; $face1y=315;
      		$face2x=365; $face2y=245;
      		$face3x=480; $face3y=190;
      		$face4x=555; $face4y=155;
      		$postx=580; $posty=100;
    		$rightx=45; $righty=330;
    		$leftx=72; $lefty=320;
    		$totx=450;$toty=225;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, (str_replace('"', "", $right)-4).'"', $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="B950"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			$path="images/"."B9501BAY/".$img.".jpg";
			$face1x=390;$face1y=330;
			$postx=580; $posty=125;
    		$rightx=440;$righty=6;
    		$leftx=75;$lefty=34;
    		$totx=420;$toty=350;
		}else if($bay=="2BAY"){
			$path="images/"."B9502BAY/".$img.".jpg";
			$face1x=300;$face1y=335;
      		$face2x=510;$face2y=243;
      		$postx=580; $posty=110;
    		$rightx=530; $righty=30;
    		$leftx=33; $lefty=100;
    		$totx=445;$toty=295;
		}else if($bay=="3BAY"){
			$path="images/"."B9503BAY/".$img.".jpg";
			$face1x=240;$face1y=325;
      		$face2x=424;$face2y=247;
      		$face3x=545;$face3y=198;
      		$postx=580; $posty=90;
    		$rightx=560; $righty=48;
    		$leftx=28; $lefty=140;
    		$totx=445;$toty=260;
		}else if($bay=="4BAY"){
			$path="images/"."B9504BAY/".$img.".jpg";
			$face1x=200; $face1y=310;
      		$face2x=360; $face2y=250;
      		$face3x=475; $face3y=206;
      		$face4x=560; $face4y=175;
      		$postx=580; $posty=100;
    		$rightx=575; $righty=60;
    		$leftx=25; $lefty=170;
    		$totx=435;$toty=235;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="B950 SWIVEL"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			$path="images/"."B950-Swivel1BAY/".$img.".jpg";
			$face1x=390;$face1y=330;
			$postx=580; $posty=125;
    		$rightx=440;$righty=8;
    		$leftx=75;$lefty=35;
    		$totx=420;$toty=350;
		}else if($bay=="2BAY"){
			$path="images/"."B950-Swivel2BAY/".$img.".jpg";
			$face1x=300;$face1y=335;
      		$face2x=510;$face2y=243;
      		$postx=580; $posty=110;
    		$rightx=530; $righty=30;
    		$leftx=30; $lefty=100;
    		$totx=445;$toty=295;
		}else if($bay=="3BAY"){
			$path="images/"."B950-Swivel3BAY/".$img.".jpg";
			$face1x=240;$face1y=325;
      		$face2x=424;$face2y=247;
      		$face3x=545;$face3y=198;
      		$postx=580; $posty=90;
    		$rightx=560; $righty=50;
    		$leftx=20; $lefty=140;
    		$totx=445;$toty=260;
		}else if($bay=="4BAY"){
			$path="images/"."B950-Swivel4BAY/".$img.".jpg";
			$face1x=200; $face1y=310;
      		$face2x=360; $face2y=250;
      		$face3x=475; $face3y=206;
      		$face4x=560; $face4y=175;
      		$postx=580; $posty=100;
    		$rightx=575; $righty=60;
    		$leftx=25; $lefty=165;
    		$totx=435;$toty=235;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="A-PUSH"){
		if($ends==1){
			//$img="NORADBOTHENDS.jpg";
		}else if($ends==2){
			//$img="NORADRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="NORADLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="NORADNOENDS.jpg";
			$right="";
			$left="";
		}
		
		if($bay=="1BAY"){
			$path="images/"."A-PUSH1BAY/".$img.".jpg";
			$face1x=190;$face1y=320;
			$postx=580; $posty=125;
    		$rightx=100;$righty=335;
    		$leftx=170;$lefty=90;
    		$totx=0;$toty=0;
		}else if($bay=="2BAY"){
			$path="images/"."A-PUSH2BAY/".$img.".jpg";
			$face1x=300;$face1y=335;
      		$face2x=510;$face2y=243;
      		$postx=580; $posty=110;
    		$rightx=50; $righty=340;
    		$leftx=85; $lefty=105;
    		$totx=0;$toty=0;
		}else if($bay=="3BAY"){
			$path="images/"."A-PUSH3BAY/".$img.".jpg";
			$face1x=240;$face1y=325;
      		$face2x=424;$face2y=247;
      		$face3x=545;$face3y=198;
      		$postx=580; $posty=90;
    		$rightx=39; $righty=325;
    		$leftx=55; $lefty=155;
    		$totx=0;$toty=0;
		}else if($bay=="4BAY"){
			$path="images/"."A-PUSH4BAY/".$img.".jpg";
			$face1x=200; $face1y=310;
      		$face2x=360; $face2y=250;
      		$face3x=475; $face3y=206;
      		$face4x=560; $face4y=175;
      		$postx=580; $posty=100;
    		$rightx=45; $righty=330;
    		$leftx=40; $lefty=180;
    		$totx=0;$toty=0;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://esneezeguards.com/testserver/catalog/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		//imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		//imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		//imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, 440, 370,"",$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="Mid-Shelves"){
		//$img="BOTHEND.jpg";
		
		
		
		if($bay=="1BAY"){
			if($face1=="No Glass"){
				$face1="";
				$tot="";
			}
			$path="images/"."Mid-Shelves1BAY/".$img.".jpg";
			$face1x=470;$face1y=190;
			$postx=85; $posty=295;
    		$rightx=100;$righty=335;
    		$leftx=170;$lefty=90;
    		$totx=0;$toty=0;
		}else if($bay=="2BAY"){
			//$img="NORADBOTHENDS.jpg";
			if($face1=="No Glass"||$face2=="No Glass"){
				$face1="";
				$face2="";
				$tot="";
			}
			$path="images/"."Mid-Shelves2BAY/".$img.".jpg";
			$face1x=360;$face1y=250;
      		$face2x=535;$face2y=95;
      		$postx=70; $posty=345;
    		$rightx=50; $righty=340;
    		$leftx=85; $lefty=105;
    		$totx=0;$toty=0;
		}else if($bay=="3BAY"){
			//$img="NORADBOTHENDS.jpg";
			if($face1=="No Glass"||$face2=="No Glass" || $face3=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$tot="";
			}
			$path="images/"."Mid-Shelves3BAY/".$img.".jpg";
			$face1x=290;$face1y=300;
      		$face2x=450;$face2y=155;
      		$face3x=550;$face3y=60;
      		$postx=50; $posty=390;
    		$rightx=39; $righty=325;
    		$leftx=55; $lefty=155;
    		$totx=0;$toty=0;
		}else if($bay=="4BAY"){
			if($face1=="No Glass"||$face2=="No Glass" || $face3=="No Glass" || $face4=="No Glass"){
				$face1="";
				$face2="";
				$face3="";
				$face4="";
				$tot="";
			}
			//$img="NORADBOTHENDS.jpg";
			$path="images/"."Mid-Shelves4BAY/".$img.".jpg";
			$face1x=250; $face1y=310;
      		$face2x=400; $face2y=190;
      		$face3x=500; $face3y=110;
      		$face4x=570; $face4y=50;
      		$postx=50; $posty=380;
    		$rightx=45; $righty=330;
    		$leftx=40; $lefty=180;
    		$totx=0;$toty=0;
		}
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		echo getcwd();

		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $face2, $text_colour );
		imagestring( $my_img, 5, $face3x, $face3y, $face3, $text_colour);
		imagestring( $my_img, 5, $face4x, $face4y, $face4, $text_colour);
		imagestring( $my_img, 5, $postx, $posty, $post, $text_colour);
		//imagestring( $my_img, 5, $leftx, $lefty, $left, $text_colour);
		//imagestring( $my_img, 5, $rightx, $righty, $right, $text_colour);
		imagestring( $my_img, 5, 440, 370,"",$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="ALLIN1"){
		$img="start2.jpg";
		$ar=explode("-", $face1);
		$path="images/"."ALLIN1/".$img;
		$face1x=360;$face1y=30;
		$totx=230; $toty=345;
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		imagestring( $my_img, 5, $face1x, $face1y, $ar[2]==""?$ar[1]:$ar[1]."-".$ar[2], $text_colour);
		if($ar[2]!=""){
			if($ar[2]=="1/4"){
				$ar[1]=$ar[1]+4;
				$ar[2]="3/4";
			}else if($ar[2]=="1/2"){
				$ar[1]=$ar[1]+5;
				$ar[2]="";
			}else if($ar[2]=="3/4"){
				$ar[1]=$ar[1]+5;
				$ar[2]="1/4";
			}
		}
		imagestring( $my_img, 5, $totx, $toty, $ar[2]==""?($ar[1]+4)."-1/2":($ar[1])."-".$ar[2], $text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			echo $osc.$im_id;
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="B950P"){
		$ar=explode("-",$face1);
		echo $ar[1];
		if($ar[1]!=""){
			$face1=(str_replace('"',"", $ar[0])-2)."-".$ar[1];
			$tot=($ar[0]+6)."-".$ar[1];
		}else{
			$face1=(str_replace('"',"", $face1)-2).'"';
			$tot=($face1+8).'"';
		}
		if($ends==1){
			//$img="BLACKBOTHENDS.jpg";
		}else if($ends==2){
			//$img="BLACKRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="BLACKLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="BLACKNOENDS.jpg";
			$right="";
			$left="";
		}
		$path="images/"."B950P/".$img.".jpg";
		$face1x=265;$face1y=290;
		$totx=220;$toty=320;
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $totx, $toty,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="EP950"){
		$ar=explode("-",$face1);
		if($ar[1]!=""){
			$tot=($ar[0]-8)."-".$ar[1];
		}else{
			$tot=($face1-8).'"';
		}
		if($ends==1){
			//$img="BLACKBOTHENDS.jpg";
		}else if($ends==2){
			//$img="BLACKRIGHTEND.jpg";
			$left="";
		}else if($ends==3){
			//$img="BLACKLEFTEND.jpg";
			$right="";
		}else if($ends==4){
			//$img="BLACKNOENDS.jpg";
			$right="";
			$left="";
		}
		$path="images/"."EP950/".$img.".jpg";
		$face1x=225;$face1y=320;
		$totx=0;$toty=0;
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, 275, 285,$tot,$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="Light Bar"){
		//$img="start2.jpg";
		$path="images/"."LB/".$img.".jpg";
		$face1x=365;$face1y=290;
		
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, 400, 320,(str_replace('"', "", $face1)+2).'-1/2"',$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}else if($mod=="Heat Lamp"){
		//$img="start2.jpg";
		$path="images/"."HL/".$img.".jpg";
		$face1x=340;$face1y=310;
		$totx=350; $toty=350;
		$face2x=320;$face2y=150;
		// echo $_SERVER['DOCUMENT_ROOT']."/".$_SERVER["SERVER_NAME"]."/".__FILE__;
		$my_img =imagecreatefromjpeg("http://sneezeguard.com/".$path);
		$text_colour = imagecolorallocate( $my_img, 255, 0, 0 );
		//echo $face1;
		imagestring( $my_img, 5, $face1x, $face1y, $face1, $text_colour);
		imagestring( $my_img, 5, $face2x, $face2y, $tot.'"', $text_colour);
		imagestring( $my_img, 5, $totx, $toty,(str_replace('"', "", $face1)+2).'-1/2"',$text_colour);
		if($sv!="save"){
			imagejpeg($my_img,"scrn1.jpg");
		}else{
			imagejpeg($my_img,"img/".$osc."_".$im_id.".jpg");
			datasave($osc,$im_id,$mod);
		}
		imagedestroy( $my_img );
	}
	function datasave($osc,$img,$mod){
		$servername = DB_SERVER;
		$username = DB_SERVER_USERNAME;
		$password = DB_SERVER_PASSWORD;
		$dbname = DB_DATABASE;

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);

		// Check connection
		if ($conn->connect_error) {
    		die("Connection failed: " . $conn->connect_error);
		}else{
//		    echo "Connect";
		}
		$stmt = $conn->prepare("INSERT INTO screen_table (osc_id,img_id,category,date_time) VALUES (?,?,?,?)");
    	$stmt->bind_param("ssss", $o_id,$im,$mo,$dt);
    	$o_id=$osc;
    	$im=$img;
    	$mo=$mod;
    	$dt=date("Y/m/d");
        $t=$stmt->execute();
        if($t==0){
            $done=false;
        }
	}
?>