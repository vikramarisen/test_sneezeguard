



<div class="main"  onmouseover="openCity(event, 'Hide');hide_cart_data()">
<footer class="ct-footer">
  <div class="container" >
    
    <ul class="ct-footer-list text-center-sm">
	<li class="footer-main-list1">
       <div class="footer-logo-img"  >
		  <img src="img/logo-new.png" ></div>
	</li>
	
	
	<li class="footer-main-list1">
        <h2 class="ct-footer-list-header" onclick="show_list_footer1(this)">SUPPORT <i class="fa fa-caret-down" id="down-button"></i></h2>
			<ul class="footerlist1">
			<?php $sel = mysqli_query($connt,"select * from footer_links where menu='1'");
				while($result = mysqli_fetch_assoc($sel))
				{
					?>
					<li >
						<a href="<?php echo $result['url'] ?>" ><?php echo $result['sub_menu'] ?></a>
					  </li>
					<?php
				}
			?>
		  </ul>
	</li>
	<li class="footer-main-list2">
        <h2 class="ct-footer-list-header" onclick="show_list_footer2(this)">RESOURCES <i class="fa fa-caret-down" id="down-button"></i></h2>
			<ul class="footerlist2">
			<?php
				$sel = mysqli_query($connt,"select * from footer_links where menu='2'");
				while($result = mysqli_fetch_assoc($sel))
				{
					?>
						<li >
							<a href="<?php echo $result['url'] ?>" ><?php echo $result['sub_menu'] ?></a>
						</li>
					<?php
				}
			?>
		  </ul>
	</li>
	
	
	<li class="footer-main-list3">
        <h2 class="ct-footer-list-header" onclick="show_list_footer3(this)">ABOUT <i class="fa fa-caret-down" id="down-button"></i></h2>
		
			<ul  class="footerlist3">
			<?php 
				$sel = mysqli_query($connt,"select * from footer_links where menu='3'");
				while($result = mysqli_fetch_assoc($sel))
				{
					?>
						<li >
							<a href="<?php echo $result['url'] ?>" ><?php echo $result['sub_menu'] ?></a>
						</li>
					<?php
				}
			?>
		  </ul>
	</li>
	
	  
	  
	  
      <li class="footer-main-list4">
        
		
		<ul>
	
		
			<li>&nbsp;</li>	
			<li>&nbsp;</li>
			<li>
			<div style="width:100%;" align="left" class="footer-social-icon">
			<a href="https://www.facebook.com/admsneezeguards"  target="_blank" class="fa fa-facebook"></a>
			<a href="https://twitter.com/ASneezeguards"  target="_blank" class="fa fa-twitter"></a>
			<a href="https://www.linkedin.com/company/adm-sneezeguards"  target="_blank" class="fa fa-linkedin"></a>
			<a href="https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg"  target="_blank" class="fa fa-youtube"></a>
			<a href="https://www.pinterest.com/admsneezeguards1"  target="_blank" class="fa fa-pinterest"></a>
			</div>
			</li>	
				
        </ul>
      </li>
  
      <li class="footer-main-list5">
        <h2 class="ct-footer-list-header">NEWSLETTER</h2>
        <ul>
       	<li>
		<div align="left" class="newsletter">
			<input type="text" placeholder="Email Address" name="name" >
			<button type="submit">JOIN</button>
			<br /><br />
			<span class="newsletter-terms">* By submitting your email address you agree to the <strong style="text-decoration:underline;font-faminly:DIN;color:white;">Terms & Conditions</strong></span>
			
			</div>
		</li>
		<li>&nbsp;</li>
			<li>
			<div class="dealer-button"><button >Dealer Inquiries</button> </div>
			
		</li>
		
        </ul>
      </li>
    </ul>
    
  </div>
  <div class="ct-footer-post">
    <div class="container">
	
	<div class="ct-footer-left">
	<img src="img/nsf-logo.png">
	</div>
	
	
	<div class="ct-footer-right">
	<img src="images/SSL.gif">
	</div>
	
      <div class="inner-left">
        <ul>
          <li>
            <a href="<?php echo tep_href_link('common.php')?>">FAQ</a>
          </li>
          <li>
            <a href="">Blog</a>
          </li> 
          <li>
            <a href="<?php echo tep_href_link('term.php')?>"> Helpful Terminology</a>
          </li>
          <li>
            <a href="<?php echo tep_href_link('issue-report.php');?>">Report an Issue</a>
          </li>
           
		  <li>
            <a href="<?php echo tep_href_link(FILENAME_PRIVACY);?>">Privacy Policy</a>
          </li>
           
        </ul>
      </div>
	  
      <div class="inner-right">
        <p>
Copyright©2020 ADM Sneezeguards A division of Advanced Design Manufacturing L.L.C. </p>
        
      </div>
    </div>
  </div>
</footer>
</div>
</body>
</html> 
