<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

// start the timer for the page parse time log
  define('PAGE_PARSE_START_TIME', microtime());

// set the level of error reporting
  error_reporting(E_ALL & ~E_NOTICE);

// check support for register_globals
  if (function_exists('ini_get') && (ini_get('register_globals') == false) && (PHP_VERSION < 4.3) ) {
    exit('Server Requirement Error: register_globals is disabled in your PHP configuration. This can be enabled in your php.ini configuration file or in the .htaccess file in your catalog directory. Please use PHP 4.3+ if register_globals cannot be enabled on the server.');
  }

// load server configuration parameters
  if (file_exists('includes/local/configure.php')) { // for developers
    include('includes/local/configure.php');
  } else {
    include('includes/configure.php');
  }

  if (strlen(DB_SERVER) < 1) {
    if (is_dir('install')) {
      header('Location: install/index.php');
    }
  }

// define the project version --- obsolete, now retrieved with tep_get_version()
  define('PROJECT_VERSION', 'osCommerce Online Merchant v2.3');

// some code to solve compatibility issues
  require(DIR_WS_FUNCTIONS . 'compatibility.php');

// set the type of request (secure or not)
  $request_type = (getenv('HTTPS') == 'on') ? 'SSL' : 'NONSSL';

// set php_self in the local scope
  $PHP_SELF = (((strlen(ini_get('cgi.fix_pathinfo')) > 0) && ((bool)ini_get('cgi.fix_pathinfo') == false)) || !isset($HTTP_SERVER_VARS['SCRIPT_NAME'])) ? basename($HTTP_SERVER_VARS['PHP_SELF']) : basename($HTTP_SERVER_VARS['SCRIPT_NAME']);

  if ($request_type == 'NONSSL') {
    define('DIR_WS_CATALOG', DIR_WS_HTTP_CATALOG);
  } else {
    define('DIR_WS_CATALOG', DIR_WS_HTTPS_CATALOG);
  }

// include the list of project filenames
  require(DIR_WS_INCLUDES . 'filenames.php');

// include the list of project database tables
  require(DIR_WS_INCLUDES . 'database_tables.php');

// include the database functions
  require(DIR_WS_FUNCTIONS . 'database.php');

// make a connection to the database... now
  tep_db_connect() or die('Unable to connect to database server!');

// set the application parameters
  $configuration_query = tep_db_query('select configuration_key as cfgKey, configuration_value as cfgValue from ' . TABLE_CONFIGURATION);
  while ($configuration = tep_db_fetch_array($configuration_query)) {
    define($configuration['cfgKey'], $configuration['cfgValue']);
  }

// if gzip_compression is enabled, start to buffer the output
  if ( (GZIP_COMPRESSION == 'true') && ($ext_zlib_loaded = extension_loaded('zlib')) && (PHP_VERSION >= '4') ) {
    if (($ini_zlib_output_compression = (int)ini_get('zlib.output_compression')) < 1) {
      if (PHP_VERSION >= '4.0.4') {
        ob_start('ob_gzhandler');
      } else {
        include(DIR_WS_FUNCTIONS . 'gzip_compression.php');
        ob_start();
        ob_implicit_flush();
      }
    } else {
      ini_set('zlib.output_compression_level', GZIP_LEVEL);
    }
  }

// set the HTTP GET parameters manually if search_engine_friendly_urls is enabled
  if (SEARCH_ENGINE_FRIENDLY_URLS == 'true') {
    if (strlen(getenv('PATH_INFO')) > 1) {
      $GET_array = array();
      $PHP_SELF = str_replace(getenv('PATH_INFO'), '', $PHP_SELF);
      $vars = explode('/', substr(getenv('PATH_INFO'), 1));
      do_magic_quotes_gpc($vars);
      for ($i=0, $n=sizeof($vars); $i<$n; $i++) {
        if (strpos($vars[$i], '[]')) {
          $GET_array[substr($vars[$i], 0, -2)][] = $vars[$i+1];
        } else {
          $HTTP_GET_VARS[$vars[$i]] = $vars[$i+1];
        }
        $i++;
      }

      if (sizeof($GET_array) > 0) {
        while (list($key, $value) = each($GET_array)) {
          $HTTP_GET_VARS[$key] = $value;
        }
      }
    }
  }

// define general functions used application-wide
  require(DIR_WS_FUNCTIONS . 'general.php');
  require(DIR_WS_FUNCTIONS . 'html_output.php');

// set the cookie domain
  $cookie_domain = (($request_type == 'NONSSL') ? HTTP_COOKIE_DOMAIN : HTTPS_COOKIE_DOMAIN);
  $cookie_path = (($request_type == 'NONSSL') ? HTTP_COOKIE_PATH : HTTPS_COOKIE_PATH);

// include cache functions if enabled
  if (USE_CACHE == 'true') include(DIR_WS_FUNCTIONS . 'cache.php');

// include shopping cart class
  require(DIR_WS_CLASSES . 'shopping_cart.php');

// include navigation history class
  require(DIR_WS_CLASSES . 'navigation_history.php');

// define how the session functions will be used
  require(DIR_WS_FUNCTIONS . 'sessions.php');

// set the session name and save path
  tep_session_name('osCsid');
  tep_session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
   if (function_exists('session_set_cookie_params')) {
    session_set_cookie_params(0, $cookie_path, $cookie_domain);
  } elseif (function_exists('ini_set')) {
    ini_set('session.cookie_lifetime', '0');
    ini_set('session.cookie_path', $cookie_path);
    ini_set('session.cookie_domain', $cookie_domain);
  }

  @ini_set('session.use_only_cookies', (SESSION_FORCE_COOKIE_USE == 'True') ? 1 : 0);

// set the session ID if it exists
   if (isset($HTTP_POST_VARS[tep_session_name()])) {
     tep_session_id($HTTP_POST_VARS[tep_session_name()]);
   } elseif ( ($request_type == 'SSL') && isset($HTTP_GET_VARS[tep_session_name()]) ) {
     tep_session_id($HTTP_GET_VARS[tep_session_name()]);
   }

// start the session
  $session_started = false;
  if (SESSION_FORCE_COOKIE_USE == 'True') {
    tep_setcookie('cookie_test', 'please_accept_for_session', time()+60*60*24*30, $cookie_path, $cookie_domain);

    if (isset($HTTP_COOKIE_VARS['cookie_test'])) {
      tep_session_start();
      $session_started = true;
    }
  } elseif (SESSION_BLOCK_SPIDERS == 'True') {
    $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
    $spider_flag = false;

    if (tep_not_null($user_agent)) {
      $spiders = file(DIR_WS_INCLUDES . 'spiders.txt');

      for ($i=0, $n=sizeof($spiders); $i<$n; $i++) {
        if (tep_not_null($spiders[$i])) {
          if (is_integer(strpos($user_agent, trim($spiders[$i])))) {
            $spider_flag = true;
            break;
          }
        }
      }
    }

    if ($spider_flag == false) {
      tep_session_start();
      $session_started = true;
    }
  } else {
    tep_session_start();
    $session_started = true;
  }

  if ( ($session_started == true) && (PHP_VERSION >= 4.3) && function_exists('ini_get') && (ini_get('register_globals') == false) ) {
    extract($_SESSION, EXTR_OVERWRITE+EXTR_REFS);
  }

// initialize a session token
  if (!tep_session_is_registered('sessiontoken')) {
    $sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());
    tep_session_register('sessiontoken');
  }

// set SID once, even if empty
  $SID = (defined('SID') ? SID : '');

// verify the ssl_session_id if the feature is enabled
  if ( ($request_type == 'SSL') && (SESSION_CHECK_SSL_SESSION_ID == 'True') && (ENABLE_SSL == true) && ($session_started == true) ) {
    $ssl_session_id = getenv('SSL_SESSION_ID');
    if (!tep_session_is_registered('SSL_SESSION_ID')) {
      $SESSION_SSL_ID = $ssl_session_id;
      tep_session_register('SESSION_SSL_ID');
    }

    if ($SESSION_SSL_ID != $ssl_session_id) {
      tep_session_destroy();
      tep_redirect(tep_href_link(FILENAME_SSL_CHECK));
    }
  }

// verify the browser user agent if the feature is enabled
  if (SESSION_CHECK_USER_AGENT == 'True') {
    $http_user_agent = getenv('HTTP_USER_AGENT');
    if (!tep_session_is_registered('SESSION_USER_AGENT')) {
      $SESSION_USER_AGENT = $http_user_agent;
      tep_session_register('SESSION_USER_AGENT');
    }

    if ($SESSION_USER_AGENT != $http_user_agent) {
      tep_session_destroy();
      tep_redirect(tep_href_link(FILENAME_LOGIN));
    }
  }

// verify the IP address if the feature is enabled
  if (SESSION_CHECK_IP_ADDRESS == 'True') {
    $ip_address = tep_get_ip_address();
    if (!tep_session_is_registered('SESSION_IP_ADDRESS')) {
      $SESSION_IP_ADDRESS = $ip_address;
      tep_session_register('SESSION_IP_ADDRESS');
    }

    if ($SESSION_IP_ADDRESS != $ip_address) {
      tep_session_destroy();
      tep_redirect(tep_href_link(FILENAME_LOGIN));
    }
  }

// create the shopping cart
  if (!tep_session_is_registered('cart') || !is_object($cart)) {
    tep_session_register('cart');
    $cart = new shoppingCart;
  }

// include currencies class and create an instance
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

// include the mail classes
  require(DIR_WS_CLASSES . 'mime.php');
  require(DIR_WS_CLASSES . 'email.php');

// set the language
  if (!tep_session_is_registered('language') || isset($HTTP_GET_VARS['language'])) {
    if (!tep_session_is_registered('language')) {
      tep_session_register('language');
      tep_session_register('languages_id');
    }

    include(DIR_WS_CLASSES . 'language.php');
    $lng = new language();

    if (isset($HTTP_GET_VARS['language']) && tep_not_null($HTTP_GET_VARS['language'])) {
      $lng->set_language($HTTP_GET_VARS['language']);
    } else {
      $lng->get_browser_language();
    }

    $language = $lng->language['directory'];
    $languages_id = $lng->language['id'];
  }

// include the language translations
  require(DIR_WS_LANGUAGES . $language . '.php');

// currency
  if (!tep_session_is_registered('currency') || isset($HTTP_GET_VARS['currency']) || ( (USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && (LANGUAGE_CURRENCY != $currency) ) ) {
    if (!tep_session_is_registered('currency')) tep_session_register('currency');

    if (isset($HTTP_GET_VARS['currency']) && $currencies->is_set($HTTP_GET_VARS['currency'])) {
      $currency = $HTTP_GET_VARS['currency'];
    } else {
      $currency = ((USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && $currencies->is_set(LANGUAGE_CURRENCY)) ? LANGUAGE_CURRENCY : DEFAULT_CURRENCY;
    }
  }

// navigation history
  if (!tep_session_is_registered('navigation') || !is_object($navigation)) {
    tep_session_register('navigation');
    $navigation = new navigationHistory;
  }
  $navigation->add_current_page();

// action recorder
  include('includes/classes/action_recorder.php');

// Shopping cart actions
  if (isset($HTTP_GET_VARS['action'])) {
// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled
    if ($session_started == false) {
      tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE));
    }

    if (DISPLAY_CART == 'true') {
      $goto =  FILENAME_SHOPPING_CART;
      $parameters = array('action', 'cPath', 'products_id', 'pid');
    } else {
      $goto = basename($PHP_SELF);
      if ($HTTP_GET_VARS['action'] == 'buy_now') {
        $parameters = array('action', 'pid', 'products_id');
      } else {
        $parameters = array('action', 'pid');
      }
    }
    switch ($HTTP_GET_VARS['action']) {
	
	
      // customer wants to update the product quantity in their shopping cart
      case 'update_product' : for ($i=0, $n=sizeof($HTTP_POST_VARS['products_id']); $i<$n; $i++) {
                                if (in_array($HTTP_POST_VARS['products_id'][$i], (is_array($HTTP_POST_VARS['cart_delete']) ? $HTTP_POST_VARS['cart_delete'] : array()))) {
                                  $cart->remove($HTTP_POST_VARS['products_id'][$i]);
                                } else {
                                  $attributes = ($HTTP_POST_VARS['id'][$HTTP_POST_VARS['products_id'][$i]]) ? $HTTP_POST_VARS['id'][$HTTP_POST_VARS['products_id'][$i]] : '';
                                  $cart->add_cart($HTTP_POST_VARS['products_id'][$i], $HTTP_POST_VARS['cart_quantity'][$i], $attributes, false);
                                }
								
								
								
								
                              }
							  /*ani code for custom update in cart*/
								$i=0;
								$k=0;
								$ch=false;
								$_SESSION['product_custom']=array();
								if(isset($HTTP_POST_VARS['products_id1'])){
								foreach($HTTP_POST_VARS['products_id1'] as $p_ids){
								
								
									for($j=1;$j<=$HTTP_POST_VARS['cart_quantity1'][$i];$j++){
										if($HTTP_POST_VARS['custom_type'][$i]=='Yes'){
											$ch=true;
										}
									 	//echo $p_ids."   ".stripslashes($HTTP_POST_VARS['custom_val'][$i])." ".$HTTP_POST_VARS['custom_type'][$i];
									 	
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>stripslashes($HTTP_POST_VARS['custom_val'][$i]),'qty'=>1,'custom'=>$HTTP_POST_VARS['custom_type'][$i]);
										$k++;	
									}
									$i++;
								}
								}else{
								
									foreach($HTTP_POST_VARS['products_id'] as $p_ids){
								
								
									for($j=1;$j<=$HTTP_POST_VARS['cart_quantity'][$i];$j++){
										if($HTTP_POST_VARS['custom_type'][$i]=='Yes'){
											$ch=true;
										}
									 	//echo $p_ids."   ".stripslashes($HTTP_POST_VARS['custom_val'][$i])." ".$HTTP_POST_VARS['custom_type'][$i];
									 	
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>stripslashes($HTTP_POST_VARS['custom_val'][$i]),'qty'=>1,'custom'=>$HTTP_POST_VARS['custom_type'][$i]);
										$k++;	
									}
									$i++;
								}
								
								}
								calCulateQuantity($_SESSION['product_custom']);
						
								//print_r($_SESSION['product_final1']); 
//								exit;
								
								/*ani code*/
							 
							 	 $goto='shopping_cart1.php';
							
                              tep_redirect(tep_href_link($goto, tep_get_all_get_params($parameters)));
                              break;
      // customer adds a product from the products page
      case 'add_product' : 
	 
	
	 
	 
	 
	 
	 
	 
	
	  if(is_array($HTTP_POST_VARS['products_id'])){
	  
          					// ani code
							if((($HTTP_POST_VARS['is_custom']=='Yes'&&$HTTP_POST_VARS['is_custom']!=''&& $HTTP_POST_VARS['product_type']!=''))||isset($_SESSION['product_custom'])){
							$goto='shopping_cart1.php';
							}
							$goto='shopping_cart1.php';	
								if($cart->count_contents()<=0){
									unset($_SESSION['product_custom']);
								}
								if(empty($_SESSION['product_custom'])){
									$_SESSION['product_custom']=array();
									$k=0;
								}else{
									   $k=sizeof( $_SESSION['product_custom']);
								}
								
								/* for ep 82 there are 2 galasses for same face so declearing variables for that */
								$glassa=false;
								$glassb=false;
								$glassc=false;
								/* fro ed 20 shelevs */
								$nosheleves=$HTTP_POST_VARS['shelves'];
								$start_top_glassa=0;
								$start_top_glassb=0;
								$start_top_glassc=0;
								
								foreach($HTTP_POST_VARS['products_id'] as $p_ids){
								$isglass=false;
								
								
								// for ep5 custom product
								if($HTTP_POST_VARS['product_type']=='EP5'){
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									     if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post_val']." ".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}	
									if(($HTTP_POST_VARS['c_glass_right']!='')&&($isglass==false)){
									      if($HTTP_POST_VARS['c_glass_right_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_right'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post_val']." ".$HTTP_POST_VARS['c_glass_right_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; 
											 unset($HTTP_POST_VARS['c_glass_right']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_left']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_left_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_left'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post_val']." ".$HTTP_POST_VARS['c_glass_left_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_left']);$isglass=true;
									}	
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post_val']." ".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post_val']." ".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post_val']." ".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}
									
								}
								//for ep15 custom product
								if($HTTP_POST_VARS['product_type']=='EP15'){
									
									if($_POST['c_glass_post_val']!=""){
									$height=$_POST['c_glass_post_val'];
									}else{
									$height=$_POST['glass_face'];
									}
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$height." ".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$height." ".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$height." ".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$height." ".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type']."-".$height,'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								//for ep21 custom product
								if($HTTP_POST_VARS['product_type']=='EP21'){
									
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								//for ep22 custom product
								if($HTTP_POST_VARS['product_type']=='EP22'){
									
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									 if($HTTP_POST_VARS['c_glass_face_val']==''){
										      
									
										tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
							          if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										
										                      }
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									         if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								//for ep36 custom product
								if($HTTP_POST_VARS['product_type']=='EP36'){
									
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								
								//for es31 custom product
								if($HTTP_POST_VARS['product_type']=='ES31'){
									
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								
								//for es40 custom product
								if($HTTP_POST_VARS['product_type']=='ES40'){
									
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								//for es67 custom product
								if($HTTP_POST_VARS['product_type']=='ES67'){
									
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								//for es73 custom product
								if($HTTP_POST_VARS['product_type']=='ES73'){
									
									
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}	
									
								}
								
								//for es11 and ep12 custom product
								if($HTTP_POST_VARS['product_type']=='EP11'||$HTTP_POST_VARS['product_type']=='EP12'){
									$postheight="";
									if($HTTP_POST_VARS['c_glass_post_val']!=""){
									
												$postheight1=$HTTP_POST_VARS['c_glass_post_val'];
												$postheight=$HTTP_POST_VARS['product_type']." ".$postheight1." ".$HTTP_POST_VARS['c_glass_face_val'];
									}else{
									
										$postheight1=" ";
										$postheight=$HTTP_POST_VARS['product_type']." ".$HTTP_POST_VARS['c_glass_face_val'];
									}
									if(($HTTP_POST_VARS['c_glass_face']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_face_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_face'],'val'=>$HTTP_POST_VARS['product_type']." ".$postheight1." ".$HTTP_POST_VARS['c_glass_face_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_face']);$isglass=true;
									}	
									if(($HTTP_POST_VARS['c_glass_right']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_right_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_right'],'val'=>$HTTP_POST_VARS['product_type']." ".$postheight1." ".$HTTP_POST_VARS['c_glass_right_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; 
											 unset($HTTP_POST_VARS['c_glass_right']);$isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_left']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_left_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_left'],'val'=>$HTTP_POST_VARS['product_type']." ".$postheight1." ".$HTTP_POST_VARS['c_glass_left_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_left']);$isglass=true;
									}	
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_a_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']." ".$postheight1." ".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_b_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']." ".$postheight1." ".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)){
									if($HTTP_POST_VARS['c_glass_c_val']==''){
										      tep_session_destroy();
										}
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']." ".$postheight1." ".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type']." ".$postheight1,'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}																	
								}
								//for es29
								if($HTTP_POST_VARS['product_type']=='ES29'||$HTTP_POST_VARS['product_type']=='ES82'){
									
									
									
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_a'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; if($HTTP_POST_VARS['product_type']=='ES29'){unset($HTTP_POST_VARS['c_glass_a']);}else{ if($glassa){unset($HTTP_POST_VARS['c_glass_a']);}}
											$glassa=true;
											 $isglass=true; 
									}
									if(($HTTP_POST_VARS['c_glass_a_top']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_a_top'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a_top'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val']."/".$HTTP_POST_VARS['right_length']."TG",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_a_top']);unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; 
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_b'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; if($HTTP_POST_VARS['product_type']=='ES29'){unset($HTTP_POST_VARS['c_glass_a']);}else{ if($glassa){unset($HTTP_POST_VARS['c_glass_a']);}}
											$glassa=true;
											 $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_b_top']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_b_top'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b_top'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val']."/".$HTTP_POST_VARS['right_length']."TG",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_b_top']);unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_c'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; if($HTTP_POST_VARS['product_type']=='ES29'){unset($HTTP_POST_VARS['c_glass_a']);}else{ if($glassa){unset($HTTP_POST_VARS['c_glass_a']);}}
											$glassa=true;
											 $isglass=true; 
									}
									if(($HTTP_POST_VARS['c_glass_c_top']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_c_top'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c_top'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val']."/".$HTTP_POST_VARS['right_length']."TG",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;unset($HTTP_POST_VARS['c_glass_c_top']);unset($HTTP_POST_VARS['c_glass_c']);$isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}																	
								}
								// for ed20
								
								//for es29
								if($HTTP_POST_VARS['product_type']=='ED20'){
									$face='';
									if($HTTP_POST_VARS['c_glass_post']!=''){
										$face=$HTTP_POST_VARS['c_glass_post']." ";
									}
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_a'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$face.$HTTP_POST_VARS['c_glass_a_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_a']);
											 $isglass=true; 
									}
									if(($HTTP_POST_VARS['c_glass_a_top']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_a_top'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a_top'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val']."/".$HTTP_POST_VARS['right_length']."TG",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;if($start_top_glassa==$nosheleves){ unset($HTTP_POST_VARS['c_glass_a_top']);  }unset($HTTP_POST_VARS['c_glass_a']);$isglass=true; $start_top_glassa++;
									}	
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_b'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$face.$HTTP_POST_VARS['c_glass_b_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_a']);
											 $isglass=true;
									}
									if(($HTTP_POST_VARS['c_glass_b_top']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_b_top'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b_top'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val']."/".$HTTP_POST_VARS['right_length']."TG",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;if($start_top_glassb==$nosheleves){unset($HTTP_POST_VARS['c_glass_b_top']);  }unset($HTTP_POST_VARS['c_glass_b']); $isglass=true;$start_top_glassb++;
									}
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_c'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$face.$HTTP_POST_VARS['c_glass_c_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_a']);
											 $isglass=true; 
									}
									if(($HTTP_POST_VARS['c_glass_c_top']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_c_top'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c_top'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val']."/".$HTTP_POST_VARS['right_length']."TG",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;if($start_top_glassc==$nosheleves){unset($HTTP_POST_VARS['c_glass_c_top']);}unset($HTTP_POST_VARS['c_glass_c']);$start_top_glassc++; $isglass=true; 
									}	
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_post']."-".$HTTP_POST_VARS['counter'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}																	
								}
								
								
								//for es 53
								if($HTTP_POST_VARS['product_type']=='ES53'){
									
									
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_a'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_a_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_a']);
											 $isglass=true; 
									}
									
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_b'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_b_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_b']); 
											 $isglass=true;
									}
									
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_c'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>$HTTP_POST_VARS['product_type']."-".$HTTP_POST_VARS['c_glass_c_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_c']);
											 $isglass=true; 
									}
										
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>$HTTP_POST_VARS['product_type'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}																	
								}
								
								
								
								//for B950 SWIVEL and B950 both
								if(($HTTP_POST_VARS['product_type']=='B950 SWIVEL')||($HTTP_POST_VARS['product_type']=='B950')){
									
									
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_a'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>"B950"."-".$HTTP_POST_VARS['c_glass_a_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_a']);
											 $isglass=true; 
									}
									
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_b'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>"B950"."-".$HTTP_POST_VARS['c_glass_b_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_b']); 
											 $isglass=true;
									}
									
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_c'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>"B950"."-".$HTTP_POST_VARS['c_glass_c_val']."GL",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_c']);
											 $isglass=true; 
									}
									if(($HTTP_POST_VARS['c_glass_right']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_right'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_right'],'val'=>"B950"."-g".$HTTP_POST_VARS['c_glass_right_val']."REP",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_right']);
											 $isglass=true; 
									}
									if(($HTTP_POST_VARS['c_glass_left']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_left'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_left'],'val'=>"B950"."-g".$HTTP_POST_VARS['c_glass_left_val']."LEP",'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; unset($HTTP_POST_VARS['c_glass_left']);
											 $isglass=true; 
									}
									
										
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>'B950','qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}																	
								}
								
								
								
								//for Adjustable-Shelving
								if(($HTTP_POST_VARS['product_type']=='Adjustable-Shelving')){
									
									
									if(($HTTP_POST_VARS['c_glass_a']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_a'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_a'],'val'=>"SLV GL"." ".$HTTP_POST_VARS['c_glass_a_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; //unset($HTTP_POST_VARS['c_glass_a']);
											 $isglass=true; 
									}
									
									if(($HTTP_POST_VARS['c_glass_b']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_b'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_b'],'val'=>"SLV GL"." ".$HTTP_POST_VARS['c_glass_b_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++; //unset($HTTP_POST_VARS['c_glass_b']); 
											 $isglass=true;
									}
									
									if(($HTTP_POST_VARS['c_glass_c']!='')&&($isglass==false)&&($p_ids==$HTTP_POST_VARS['c_glass_c'])){
											$_SESSION['product_custom'][$k]=array('id'=>$HTTP_POST_VARS['c_glass_c'],'val'=>"SLV GL"." ".$HTTP_POST_VARS['c_glass_c_val'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);									
											 $k++;//unset($HTTP_POST_VARS['c_glass_c']);
											 $isglass=true; 
									}
									
									
										
									if($isglass==false){
										
										$_SESSION['product_custom'][$k]=array('id'=>$p_ids,'val'=>'SLV','qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
									}																	
								}
								
								
								
								
								
								
								
								
								
								}
								
								
								//for B-950P custom choosen
								if(($HTTP_POST_VARS['product_type']=='B950P')&&isset($HTTP_POST_VARS['product_type'])){
								 
								   $_SESSION['product_custom'][$k]=array('id'=>$_POST['products_id'][0],'val'=>$HTTP_POST_VARS['custom_glass'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
								}
								if(($HTTP_POST_VARS['product_type']=='EP950')&&isset($HTTP_POST_VARS['product_type'])){
								 
								   $_SESSION['product_custom'][$k]=array('id'=>$_POST['products_id'][0],'val'=>$HTTP_POST_VARS['custom_glass'],'qty'=>1,'custom'=>$HTTP_POST_VARS['is_custom']);
										$k++;
								}
								
								
								
								
								//print_r($_SESSION['product_custom']);exit;
								
								
//								tep_session_register('custom');
//								
//								$_SESSION['post_height']=$HTTP_POST_VARS['c_glass_post_val'];
//								$_SESSION['product_type']=$HTTP_POST_VARS['product_type'];
//								$_SESSION['glass_face']=$HTTP_POST_VARS['c_glass_face'];
//								$_SESSION['glass_face_val']=$HTTP_POST_VARS['c_glass_face_val'];
//								$_SESSION['glass_right']=$HTTP_POST_VARS['c_glass_right'];
//								$_SESSION['glass_right_val']=$HTTP_POST_VARS['c_glass_right_val'];
//								$_SESSION['glass_left']=$HTTP_POST_VARS['c_glass_left'];
//								$_SESSION['glass_left_val']=$HTTP_POST_VARS['c_glass_left_val'];
//								$_SESSION['glass_a']=$HTTP_POST_VARS['c_glass_a'];
//								$_SESSION['glass_a_val']=$HTTP_POST_VARS['c_glass_a_val'];
//								$_SESSION['glass_b']=$HTTP_POST_VARS['c_glass_b'];
//								$_SESSION['glass_b_val']=$HTTP_POST_VARS['c_glass_b_val'];
//								$_SESSION['glass_c']=$HTTP_POST_VARS['c_glass_c'];
//								$_SESSION['glass_c_val']=$HTTP_POST_VARS['c_glass_c_val'];
								
								
							
							
							$_SESSION['totalnocustom']=$k;
							        					
							//	print_r($_SESSION['product_custom']);
//						echo "<br>";
							calCulateQuantity($_SESSION['product_custom']);
						//print_r($_SESSION['product_custom']);
//						echo "<br>";
					//print_r($_SESSION['product_final1']);
						 //exit;
                            // ani code 
                                $produts_ids_list=$HTTP_POST_VARS['products_id'];
                                $i=0;
                                foreach($produts_ids_list as $p_ids){
                                    $attributes = '';
									//echo $cart->get_quantity(tep_get_uprid($p_ids, $attributes));echo "<br>";
									//echo tep_get_uprid($p_ids, $attributes);echo "<br>";
								 $cart->add_cart($p_ids, $cart->get_quantity(tep_get_uprid($p_ids, $attributes))+1, $attributes);
                                    $i++;
                                }
								
                            }  
                            else{
							
							
							
							
							
                                 $attributes = isset($HTTP_POST_VARS['id']) ? $HTTP_POST_VARS['id'] : '';
								 
								 if(empty($_SESSION['product_custom'])){
									$_SESSION['product_custom']=array();
									$k=0;
								}else{
									   $k=sizeof( $_SESSION['product_custom']);
								}
								$id=$HTTP_GET_VARS['products_id'];
								foreach($_POST['id'] as $key=>$val){
								 $id.="{".$key."}".$val;
								
								}
								
								
								  $_SESSION['product_custom'][$k]=array('id'=>$id,'val'=>"type",'qty'=>1,'custom'=>"beyond");
								
								calCulateQuantity($_SESSION['product_custom']);
								
														 
								 
								 
                                 $cart->add_cart($HTTP_POST_VARS['products_id'], $cart->get_quantity(tep_get_uprid($HTTP_POST_VARS['products_id'], $attributes))+1, $attributes);
                            }
							$goto="shopping_cart1.php" ;
                            if(is_array($HTTP_POST_VARS['products_id'])){
                                tep_redirect(tep_href_link($goto));
                            }
                           else if(isset($HTTP_POST_VARS['products_id'])||isset($HTTP_GET_VARS['products_id'])){
                               tep_redirect(tep_href_link($goto, tep_get_all_get_params($parameters)));
                            }
                              break;
      // customer removes a product from their shopping cart
      case 'remove_product' : if (isset($HTTP_GET_VARS['products_id'])) {
								  /* ani code */
							    $i=0;
								if(!empty($_SESSION['product_custom'])){
								//print_r($_SESSION['product_custom']);
								$totalquantity=0;
	  							foreach($_SESSION['product_custom'] as $val){
								//echo stripslashes($_SESSION['product_custom'][$i]['val']);
								    $a=$HTTP_GET_VARS['val'];
								 	if($_SESSION['product_custom'][$i]['id']==$HTTP_GET_VARS['products_id']){
										$totalquantity++;
									}
									if(stripslashes($_SESSION['product_custom'][$i]['val'])==str_replace("\\","", $a)&&$_SESSION['product_custom'][$i]['id']==$HTTP_GET_VARS['products_id']&&$_SESSION['product_custom'][$i]['custom']==$HTTP_GET_VARS['custom']){
										
										
										unset($_SESSION['product_custom'][$i]['id']);
										unset($_SESSION['product_custom'][$i]['val']);
										unset($_SESSION['product_custom'][$i]['qty']);
										unset($_SESSION['product_custom'][$i]['custom']);

										
									}
									$i++;
									
									
								}
								$goto='shopping_cart1.php';
								$finalq=$totalquantity-$HTTP_GET_VARS['qty'];
								calCulateQuantity($_SESSION['product_custom']);
								
							   //print_r($_SESSION['product_final1']);
							   if ($HTTP_GET_VARS['val']=='type'){
							   	$cart->remove($HTTP_GET_VARS['products_id']);
							   }
								
								$cart->add_cart($HTTP_GET_VARS['products_id'], $finalq, '', false);
								
								}else{
								 /* ani code */
                                $cart->remove($HTTP_GET_VARS['products_id']);
								}
								if($cart->count_contents()<=0){
								 unset($_SESSION['product_custom']);
								}
                              }
                              tep_redirect(tep_href_link($goto, tep_get_all_get_params($parameters)));
                              break;
      // performed by the 'buy now' button in product listings and review page
      case 'buy_now' :        if (isset($HTTP_GET_VARS['products_id'])) {
                                if (tep_has_product_attributes($HTTP_GET_VARS['products_id'])) {
                                  tep_redirect(tep_href_link(FILENAME_PRODUCT_INFO1, 'products_id=' . $HTTP_GET_VARS['products_id']));
                                } else {
								if(empty($_SESSION['product_custom'])){
									$_SESSION['product_custom']=array();
									$k=0;
								}else{
									   $k=sizeof( $_SESSION['product_custom']);
								}
								  $_SESSION['product_custom'][$k]=array('id'=>$HTTP_GET_VARS['products_id'],'val'=>$HTTP_GET_VARS['keywords'],'qty'=>1,'custom'=>"beyond");
								
								calCulateQuantity($_SESSION['product_custom']);
								
								  
                                  $cart->add_cart($HTTP_GET_VARS['products_id'], $cart->get_quantity($HTTP_GET_VARS['products_id'])+1);
                                }
									
								
                              }
							  $goto="shopping_cart1.php";
                              tep_redirect(tep_href_link($goto, tep_get_all_get_params($parameters)));
                              break;
      case 'notify' :         if (tep_session_is_registered('customer_id')) {
                                if (isset($HTTP_GET_VARS['products_id'])) {
                                  $notify = $HTTP_GET_VARS['products_id'];
                                } elseif (isset($HTTP_GET_VARS['notify'])) {
                                  $notify = $HTTP_GET_VARS['notify'];
                                } elseif (isset($HTTP_POST_VARS['notify'])) {
                                  $notify = $HTTP_POST_VARS['notify'];
                                } else {
                                  tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'notify'))));
                                }
                                if (!is_array($notify)) $notify = array($notify);
                                for ($i=0, $n=sizeof($notify); $i<$n; $i++) {
                                  $check_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . $notify[$i] . "' and customers_id = '" . $customer_id . "'");
                                  $check = tep_db_fetch_array($check_query);
                                  if ($check['count'] < 1) {
                                    tep_db_query("insert into " . TABLE_PRODUCTS_NOTIFICATIONS . " (products_id, customers_id, date_added) values ('" . $notify[$i] . "', '" . $customer_id . "', now())");
                                  }
                                }
                                tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'notify'))));
                              } else {
                                $navigation->set_snapshot();
                                tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
                              }
                              break;
      case 'notify_remove' :  if (tep_session_is_registered('customer_id') && isset($HTTP_GET_VARS['products_id'])) {
                                $check_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . $HTTP_GET_VARS['products_id'] . "' and customers_id = '" . $customer_id . "'");
                                $check = tep_db_fetch_array($check_query);
                                if ($check['count'] > 0) {
                                  tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . $HTTP_GET_VARS['products_id'] . "' and customers_id = '" . $customer_id . "'");
                                }
                                tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action'))));
                              } else {
                                $navigation->set_snapshot();
                                tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
                              }
                              break;
      case 'cust_order' :     if (tep_session_is_registered('customer_id') && isset($HTTP_GET_VARS['pid'])) {
                                if (tep_has_product_attributes($HTTP_GET_VARS['pid'])) {
                                  tep_redirect(tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $HTTP_GET_VARS['pid']));
                                } else {
                                  $cart->add_cart($HTTP_GET_VARS['pid'], $cart->get_quantity($HTTP_GET_VARS['pid'])+1);
                                }
                              }
                              tep_redirect(tep_href_link($goto, tep_get_all_get_params($parameters)));
                              break;
    }
  }

// include the who's online functions
  require(DIR_WS_FUNCTIONS . 'whos_online.php');
  tep_update_whos_online();

// include the password crypto functions
  require(DIR_WS_FUNCTIONS . 'password_funcs.php');

// include validation functions (right now only email address)
  require(DIR_WS_FUNCTIONS . 'validations.php');

// split-page-results
  require(DIR_WS_CLASSES . 'split_page_results.php');

// infobox
  require(DIR_WS_CLASSES . 'boxes.php');

// auto activate and expire banners
  require(DIR_WS_FUNCTIONS . 'banner.php');
  tep_activate_banners();
  tep_expire_banners();

// auto expire special products
  require(DIR_WS_FUNCTIONS . 'specials.php');
  tep_expire_specials();

  require(DIR_WS_CLASSES . 'osc_template.php');
  $oscTemplate = new oscTemplate();

// calculate category path
  if (isset($HTTP_GET_VARS['cPath'])) {
    $cPath = $HTTP_GET_VARS['cPath'];
  } elseif (isset($HTTP_GET_VARS['products_id']) && !isset($HTTP_GET_VARS['manufacturers_id'])) {
    $cPath = tep_get_product_path($HTTP_GET_VARS['products_id']);
  } else {
    $cPath = '';
  }

  if (tep_not_null($cPath)) {
    $cPath_array = tep_parse_category_path($cPath);
    $cPath = implode('_', $cPath_array);
    $current_category_id = $cPath_array[(sizeof($cPath_array)-1)];
  } else {
    $current_category_id = 0;
  }

// include the breadcrumb class and start the breadcrumb trail
  require(DIR_WS_CLASSES . 'breadcrumb.php');
  $breadcrumb = new breadcrumb;

  $breadcrumb->add(HEADER_TITLE_TOP, HTTP_SERVER);
  $breadcrumb->add(HEADER_TITLE_CATALOG, tep_href_link(FILENAME_DEFAULT));

// add category names or the manufacturer name to the breadcrumb trail
  if (isset($cPath_array)) {
    for ($i=0, $n=sizeof($cPath_array); $i<$n; $i++) {
      $categories_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$cPath_array[$i] . "' and language_id = '" . (int)$languages_id . "'");
      if (tep_db_num_rows($categories_query) > 0) {
        $categories = tep_db_fetch_array($categories_query);
        $breadcrumb->add($categories['categories_name'], tep_href_link(FILENAME_DEFAULT, 'cPath=' . implode('_', array_slice($cPath_array, 0, ($i+1)))));
      } else {
        break;
      }
    }
  } elseif (isset($HTTP_GET_VARS['manufacturers_id'])) {
    $manufacturers_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$HTTP_GET_VARS['manufacturers_id'] . "'");
    if (tep_db_num_rows($manufacturers_query)) {
      $manufacturers = tep_db_fetch_array($manufacturers_query);
      $breadcrumb->add($manufacturers['manufacturers_name'], tep_href_link(FILENAME_DEFAULT, 'manufacturers_id=' . $HTTP_GET_VARS['manufacturers_id']));
    }
  }

// add the products model to the breadcrumb trail
  if (isset($HTTP_GET_VARS['products_id'])) {
    $model_query = tep_db_query("select products_model from " . TABLE_PRODUCTS . " where products_id = '" . (int)$HTTP_GET_VARS['products_id'] . "'");
    if (tep_db_num_rows($model_query)) {
      $model = tep_db_fetch_array($model_query);
      $breadcrumb->add($model['products_model'], tep_href_link(FILENAME_PRODUCT_INFO, 'cPath=' . $cPath . '&products_id=' . $HTTP_GET_VARS['products_id']));
    }
  }

// initialize the message stack for output messages
  require(DIR_WS_CLASSES . 'message_stack.php');
  $messageStack = new messageStack;
  
  // Discount Code 2.6 - start
if (MODULE_ORDER_TOTAL_DISCOUNT_STATUS == 'true') {
if (!tep_session_is_registered('sess_discount_code'))
tep_session_register('sess_discount_code');
if (!empty($HTTP_GET_VARS['discount_code'])) $sess_discount_code =
tep_db_prepare_input($HTTP_GET_VARS['discount_code']);
if (!empty($HTTP_POST_VARS['discount_code'])) $sess_discount_code =
tep_db_prepare_input($HTTP_POST_VARS['discount_code']);
}
// Discount Code 2.6 - end
 
 
/*ani code */ 
 function calCulateQuantity($array){
		//print_r($_SESSION['product_custom']);
			$_SESSION['product_final1']=array();
				$l=0;
				$m=0;
				//print_r($_SESSION['product_custom']);echo "<br>";
				
				for($l=0;$l<sizeof($_SESSION['product_custom']);$l++){
					$currnetid=$_SESSION['product_custom'][$l]['id'];
					$currnetval=$_SESSION['product_custom'][$l]['val'];
					$currnetCustom=$_SESSION['product_custom'][$l]['custom'];
					//if(sizeof($_SESSION['product_final'])<=0){
//										$_SESSION['product_final'][0]=array('id'=>$_SESSION['product_custom'][0]['id'],'val'=>$_SESSION['product_custom'][0]['val'],'qty'=>1);
//									}
	//echo searchArrayVal1($_SESSION['product_final1'], $currnetCustom,$currnetid);
					if(isset($_SESSION['product_custom'][$l]['id']))	{
					if(searchArray($_SESSION['product_final1'], $currnetid)&& searchArrayVal($_SESSION['product_final1'], $currnetval,$currnetid)&&searchArrayVal1($_SESSION['product_final1'], $currnetCustom,$currnetid)||($currnetid=='')){
						
						//do nothing
					//echo $currnetval;
//						echo $l."hi  ".searchArray($_SESSION['product_final1'], $currnetid).searchArrayVal($_SESSION['product_final1'], $currnetval,$currnetid)."<br>";
					  }
					 else{
									 	$totalq=0;
										for($k=$l;$k<sizeof($_SESSION['product_custom']);$k++){
											if(($currnetid==$_SESSION['product_custom'][$k]['id'])&&($currnetval==$_SESSION['product_custom'][$k]['val'])&&($currnetCustom==$_SESSION['product_custom'][$k]['custom'])){
												
												$totalq++;
											}
										}
										$_SESSION['product_final1'][$m]=array('id'=>$_SESSION['product_custom'][$l]['id'],'val'=>$_SESSION['product_custom'][$l]['val'],'qty'=>$totalq,'custom'=>$currnetCustom);
									//print_r($_SESSION['product_final1']);
										
										$m++;
									}
					
					//$l++;
				}
				}
				
	//	print_r( $_SESSION['product_final1']);
//exit;
//echo $m;
		
			}	

function searchArray($myarray, $id) {
    foreach ($myarray as $item) {
        if ($item['id'] == $id)
            return true;
    }
    return false;
}
function searchArrayVal($myarray,$id,$currnetid) {
    foreach ($myarray as $item) {
        if ($item['val'] == $id&&$item['id']==$currnetid)
            return true;
    }
    return false;
}
function searchArrayVal1($myarray,$id,$currnetid) {
    foreach ($myarray as $item) {
        if ($item['custom'] == $id&&$item['id']==$currnetid)
            return true;
    }
    return false;
}
								
/*ani code */


  
?>
