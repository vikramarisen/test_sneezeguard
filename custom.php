<?php

ob_start();
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_DEFAULT));

//  require(DIR_WS_INCLUDES . 'template_top.php');
  
  
   
 
 /*Replace Titles start ob_start(); is on top*/
 //ob_start();
 /*
    $buffer=ob_get_contents();
    ob_end_clean();
 
 
 
 $titlessss = 'ADM Sneezeguards - Online Custom Restaurant Supply Sneeze Guards';

    $buffer = preg_replace('/(<title>)(.*?)(<\/title>)/i', '$1' . $titlessss . '$3', $buffer);

    $keyword = 'Sneeze Guard, custom sneeze guards, Food service products, Sneeze Guard For Restaurants';

	//add anew desc and author
$buffer = str_replace('name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment"', 'name="keywords" content="'.$keyword.'"', $buffer);

    echo $buffer;	
*/


  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>


<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>ADM Sneezeguards - Online Custom Restaurant Supply Sneeze Guards</title>
<!-- End Google Add Conversion -->





<link rel="preconnect" href="https://www.sneezeguard.com">
<link rel="dns-prefetch" href="https://www.sneezeguard.com">


<link rel="preload" as="script" href="jquery-latest.js">
<link rel="preload" as="script" href="thickbox.js">
<link rel="preload" as="script" href="ext/jquery/jquery-1.4.2.min.js">
<link rel="preload" as="script" href="ext/jquery/ui/jquery-ui-1.8.6.min.js">
<link rel="preload" as="script" href="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js">
<link rel="preload" as="script" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js">
<link rel="preload" as="script" href="jquery.colorbox3.js">
<link rel="preload" as="script" href="./dist/html2canvas.js">
<link rel="preload" as="script" href="./dist/canvas2image.js">
<link rel="preload" as="script" href="https://platform.twitter.com/widgets.js">
<link rel="preload" as="script" href="jquery.confirm/jquery.confirm.js">





<link rel="preload" as="style" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css">
<link rel="preload" as="style" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css">
<link rel="preload" as="style" href="ext/960gs/960_24_col.css">
<link rel="preload" as="style" href="stylesheet.css">
<link rel="preload" as="style" href="colorbox3.css">
<link rel="preload" as="style" href="jquery/jquery.simplyscroll.css">



<link rel="preload" as="image" href="https://www.sneezeguard.com/images/new_logo_main.png">
<link rel="preload" as="image" href="images/wishlist_icon.png">
<link rel="preload" as="image" href="image_new/office.png">
<link rel="preload" as="image" href="image_new/banking.png">
<link rel="preload" as="image" href="image_new/grossery.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/1.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/3.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/5.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/16.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/21.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/22.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/23.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/24.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/25.png">


<link rel="preload" as="image" href="img/close.png">
<link rel="preload" as="image" href="img/mail/mail_img.png">
<link rel="preload" as="image" href="social_icons/rss-icon.png">
<link rel="preload" as="image" href="img/social/facebook.png">
<link rel="preload" as="image" href="img/social/twittee.png">
<link rel="preload" as="image" href="img/social/linklind.png">
<link rel="preload" as="image" href="img/social/pintrest.png">
<link rel="preload" as="image" href="img/social/instagram.png">
<link rel="preload" as="image" href="img/social/youtube.png">




<link rel="preload" as="image" href="images/bulletpt.jpg">
<link rel="preload" as="image" href="images/logo.jpg">
<link rel="preload" as="image" href="images/navbar/homeup.jpg">
<link rel="preload" as="image" href="images/navbar/instockup.jpg">
<link rel="preload" as="image" href="images/navbar/customup.jpg">
<link rel="preload" as="image" href="images/navbar/adjustableup.jpg">
<link rel="preload" as="image" href="images/navbar/portableup.jpg">
<link rel="preload" as="image" href="images/navbar/orbitup.jpg">
<link rel="preload" as="image" href="images/navbar/shelvingup.jpg">
<link rel="preload" as="image" href="background.jpg">
<link rel="preload" as="image" href="images/middleback.jpg">



<link rel="preload" as="image" href="images/shortVertSepIS.jpg">


<link rel="preload" as="image" href="sneezegaurd/custompagepopup/images2/12.gif">
<link rel="preload" as="image" href="sneezegaurd/custompagepopup/images/12.gif">
<link rel="preload" as="image" href="images/popup/learnMore2.png">
<link rel="preload" as="image" href="images/Models/P-100.jpg">
<link rel="preload" as="image" href="images/Models/P-150.jpg">
<link rel="preload" as="image" href="images/Models/P-400.jpg">
<link rel="preload" as="image" href="images/Models/P-263.jpg">
<link rel="preload" as="image" href="images/Models/P-445.jpg">
<link rel="preload" as="image" href="images/Models/S-295.jpg">
<link rel="preload" as="image" href="images/Models/S-221.jpg">
<link rel="preload" as="image" href="images/Models/S-280.jpg">
<link rel="preload" as="image" href="images/Models/S-280IS.jpg">
<link rel="preload" as="image" href="images/Models/S-277.jpg">
<link rel="preload" as="image" href="images/Models/S-553.jpg">
<link rel="preload" as="image" href="images/Models/S-445.jpg">
<link rel="preload" as="image" href="images/Models/S-211.jpg">
<link rel="preload" as="image" href="images/Models/S-828.jpg">
<link rel="preload" as="image" href="images/Models/S-439.jpg">





<link rel="preload" as="image" href="img/social/new_img_bestprice.jpg">
<link rel="preload" as="image" href="images/backBottom.jpg">
<link rel="preload" as="image" href="images/comstr.jpg">
<link rel="preload" as="image" href="images/nsflogo.jpg">









<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="Custom Sizing sneeze guard is now available on the in-stock line at ADM Sneezeguards. Get online quote for Glass Barrier custom face lenght and Post Height.">
<meta name="keywords" content="Sneeze Guard, custom sneeze guards, Food service products, Sneeze Guard For Restaurants">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>

<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />
<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />




<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>


<script>

//$("meta[name='keywords']").attr("content","Sneeze Guard, custom sneeze guards, Food service products, Sneeze Guard For Restaurants");
</script>
<?php
  /*Replace Titles End */
 
 
  
  
  
  $ImageData2=mysql_query("SELECT * FROM  custompopup WHERE publish='1' ") or die(mysql_error());
	 $ROWDATA=mysql_fetch_array($ImageData2)
?>
<link rel="stylesheet" href="colorbox4.css" />
<script type="text/javascript" src="jquery.colorbox4.js"></script>
<?php if ($ROWDATA['publish']==1): ?>
 <script type="text/javascript">
    $(document).ready(function(){
			
				//Examples of how to assign the Colorbox event to elements
				$("#cboxContent").addClass("blue_border_example");
				$("#cboxTopLeft").hide();
$("#cboxTopRight").hide();
$("#cboxBottomLeft").hide();
$("#cboxBottomRight").hide();
$("#cboxMiddleLeft").hide();
$("#cboxMiddleRight").hide();
$("#cboxTopCenter").hide();
$("#cboxBottomCenter").hide();
var tis='<table width="800" bordercolor="#000000" border="2" height="400" cellpadding="0" cellspacing="0"><tr>  <td  rowspan="2"><img src="sneezegaurd/custompagepopup/images/<?php echo $ROWDATA['imagename1'];?>" alt="sneeze guard" width="400" height="405" /></td> <td ><img src="sneezegaurd/custompagepopup/images2/<?php echo $ROWDATA['imagename1'];?>" alt="sneeze guard" width="400" height="328" /></td></tr><tr>  <td height="75" align="center"><a style="color:#CCC; border-bottom:0px solid #CCC;" href="<?php echo $ROWDATA['linkname'];?>"><img src="images/popup/learnMore2.png"  /></a></td></tr></table>';
              $.colorbox({html:tis, open:true, width:"677", height:"410"});
			   
				
			});
 </script>
 <?php endif; ?>
 
	<?php
	if (!$detect->isMobile())
	{
	
	?>
	<style>
	.modText {
    font-size: 12px;
    color: #CCCCCC;
    line-height: 16px;
    text-align: left;
	}
	
	</style>
	
	
	
	<?php
	}
	else{
	?>
 
	<style>
	.modText {
    font-size: 16px !important;
    
	}
	
	.linkClass A:visited {
    
    font-size: 16px !important;
   
}

#modelnameid{font-size: 16px !important;line-height: 30px;}
	</style>
	
	
	<?php
	}
	?>
 
 
 
 
 
 
 <?php
  if (!$detect->isMobile())
{
	//echo'<td id="ex1" align=center width="190" valign="top">';
}
else{
	echo'<td id="ex1" align=center width="190" valign="top">';

}

?>
<?php
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
    
  
    // Display all models
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == "Orbit-360" || $row["Name"] == "P-100" || $row["Name"] == "P-400" || $row["Name"] == "P-150" || $row["Name"] == "P-263" || $row["Name"] == "P-445" || $row["Name"] == "S-295" || $row["Name"] == "S-221" || $row["Name"] == "S-280" || $row["Name"] == "S-280IS" || $row["Name"] == "S-277"  || $row["Name"] == "S-553" || $row["Name"] == "S-445" || $row["Name"] == "S-211" || $row["Name"] == "S-828" || $row["Name"] == "S-439"){//|| $row["Name"] == "D-467" This condition has removed as client said!!
	
      $Height = $row["Height"];
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      $FrontGlass = $row["FrontGlass"];
      $Tubing = $row["Tubing"];
     echo('<P>');
      
      echo('<TABLE WIDTH="751" HEIGHT="110" BORDER="0" class="custom" CELLPADDING=5>');
      echo('<tr>');
      echo('<td align=right width=20%>');
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"]).'">' . '<IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/Models/' . $row["Name"] . '.jpg" BORDER=0>' . '</A>');
      echo('</td>');
      echo('<td width=5 align=right>');
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      echo('</td>');
      echo('<td valign=top>');
      echo('<div class="linkClass">');
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"]).'"><span id="modelnameid">Model Name: ' . $row["Name"] . '</span></A><BR>');
      echo('</div>');
      echo('<div class="modText">');
      echo('Height: ' . $Height . '<br>');
      if($Depth != "0"){
      echo('Depth: ' . $Depth . '<br>');
      }
      echo('Glass: ' . $FrontGlass . '<br>');
      echo('Tubing: ' . $Tubing . '<br>');
      echo('</div>');
      echo('</td>');
      echo('<td valign=center align=left width="5">');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      //}
      echo('</td>');
      echo('<td valign=top width=11%>');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<div class="modName">');
      echo("<B><span id='modelnameid'>Options:</span></B><BR>");
      echo('</div>');
      echo('<div class="modText">');
      if($EndPan != "0")
      echo("End Panels " . "<br>");
      if($HeatLamps != "0")
      echo("Heat Lamps " . "<br>");
      if($Lighting != "0")
      echo("Lighting " . "<br>");
      //echo('</div>');
      
      //}
      //echo('<div class="modName">');
      echo("Finishes");
      echo('</div>');
      echo('</td>');
      echo('</tr>');
      echo('</TABLE>');
      echo('<P>');
      
      }
      }
      
?>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>

