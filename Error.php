<?php
// Error Redirects from many different states of Error, and handles them in one file.
// Records the output in the database for error trapping and also hacking attempts.
// You will recieve many 404 errors during a hacking attempt and i expect you would be very surprised if you knew how many attempts your site gets in a day!
// Also recorded Errors will show up any scripting errors, missing images, malformed code etc... giving you a much better web site
// Written by FImble for osCommerce
// osCommerce Forums URL -  http://forums.oscommerce.com/index.php?showuser=15542
// Add on URL http://addons.oscommerce.com/info/
// to hire me ... linuxux.group@gmail.com
// If you have concerns about security please get in touch with me for fast expert service,
// hacked? I am more than capable of clearing malicious code from your site, and securing it to prevent further attempts.

include('includes/application_top.php');
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LINUXUK_ERROR_HTTP_LOG);

  if (isset($_GET['Error']))
  {
    $Error = $_GET['Error'];
  }
  else
  {
    header('Location: index.php');
  }
    // set some server variables
    // Get the intended destination of the user.
    $request = $_SERVER['REQUEST_URI'];
    // Get the referrer if there is one.
    if(isset($_SERVER['HTTP_REFERER'])){
    $referrer = $_SERVER['HTTP_REFERER'];
    }
    else
    {
    $referrer = '';
    }
    // Get the IP number of the user.
    $address = $_SERVER['REMOTE_ADDR'];
    // Get the User Agent of the user.
    $agent   = $_SERVER['HTTP_USER_AGENT'];


    // Take the above and sanitize it.
    $request_esc = tep_sanitize_string($request);
    $referrer_esc = tep_sanitize_string($referrer);
    $address_esc = tep_sanitize_string($address);
    $agent_esc   = tep_sanitize_string($agent);
    // Start the Switch to output different scenarios, resulting from the error codes
  switch($Error)
  {
    case 400:
      $title = LINUXUK_ERROR_400_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_400_HTTP_ERROR_MESSAGE. '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_LINUXUK_HTTP_EXPLAINATION_400;
      if(LINUXUK_HTTP_ERROR_400 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'400'."', now() )");
      }
   break;

    case 401:
      $title = LINUXUK_ERROR_401_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_401_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_401;
      if(LINUXUK_HTTP_ERROR_401 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'401'."', now() )");
      }
    break;

    case 403:
      $title = LINUXUK_ERROR_403_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_403_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_403;
      if(LINUXUK_HTTP_ERROR_403 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'403'."', now() )");
      }
    break;

    case 404:
      $title = LINUXUK_ERROR_404_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_404_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_404;
      if(LINUXUK_HTTP_ERROR_404 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'404'."', now() )");
      }
    break;

    case 405:
      $title = LINUXUK_ERROR_405_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_405_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_405;
      if(LINUXUK_HTTP_ERROR_405 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'405'."', now() )");
      }
    break;

    case 408:
      $title = LINUXUK_ERROR_408_HTTP_ERROR;
      $explaination =  LINUXUK_ERROR_408_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_408;
      if(LINUXUK_HTTP_ERROR_408 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'408'."', now() )");
      }
    break;

    case 415:
      $title = LINUXUK_ERROR_415_HTTP_ERROR;
      $explaination =  LINUXUK_ERROR_415_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_415;
      if(LINUXUK_HTTP_ERROR_415 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'415'."', now() )");
      }
    break;

    case 416:
      $title = LINUXUK_ERROR_416_HTTP_ERROR;
      $explaination =  LINUXUK_ERROR_416_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_416;
      if(LINUXUK_HTTP_ERROR_416 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'416'."', now() )");
      }
    break;

    case 417:
      $title = LINUXUK_ERROR_417_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_417_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_417;
      if(LINUXUK_HTTP_ERROR_417 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'417'."', now() )");
      }
    break;

    case 500:
      $title = LINUXUK_ERROR_500_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_500_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_500;
      if(LINUXUK_HTTP_ERROR_500 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'500'."', now() )");
      }
    break;

    case 501:
      $title = LINUXUK_ERROR_501_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_501_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_501;
      if(LINUXUK_HTTP_ERROR_501 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'501'."', now() )");
      }
    break;

    case 502:
      $title = LINUXUK_ERROR_502_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_502_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_502;
      if(LINUXUK_HTTP_ERROR_502 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'502'."', now() )");
      }
    break;

    case 503:
      $title = LINUXUK_ERROR_503_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_503_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_503;
      if(LINUXUK_HTTP_ERROR_503 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'503'."', now() )");
      }
    break;

    case 504:
      $title = LINUXUK_ERROR_504_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_504_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_504;
      if(LINUXUK_HTTP_ERROR_504 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'504'."', now() )");
      }
    break;

    case 505:
      $title = LINUXUK_ERROR_505_HTTP_ERROR;
      $explaination = LINUXUK_ERROR_505_HTTP_ERROR_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_HTTP_EXPLAINATION_505;
      if(LINUXUK_HTTP_ERROR_505 == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'505'."', now() )");
      }
    break;

    default:
      $title = UNKNOWN_HTTP_ERROR;
      $explaination = LINUXUK_UNKNOWN_HTTP_MESSAGE . '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . LINUXUK_HTTP_HREF_LINK_TEXT . '</a>';
      $error_message=  LINUXUK_UNKNOWN_HTTP_ERROR_TEXT;
      if(LINUXUK_HTTP_ERROR_UNK == 'True'){
      $query  = tep_db_query("INSERT INTO " .   TABLE_LINUXUK_HTTP_ERROR . " (linuxuk_error_log_request,linuxuk_error_log_referrer,linuxuk_error_log_address,linuxuk_error_log_agent,linuxuk_error_log_error, linuxuk_error_log_date) VALUES ('" .$request_esc ."','" .$referrer_esc."','".$address_esc."','".$agent_esc."','".'UNKNOWN'."', now() )");
      }
    break;
  }
if (preg_match("/google|MSN|Yahoo|bing/i", $agent_esc))
  {
    tep_redirect(FILENAME_DEFAULT);
  }
  else
  {
  if (LINUXUK_IP_TRAP_EXCESSIVE_ERRORS == 'True') {
  $linuxuk_http_exclude = array();
  $linuxuk_http_exclude = explode(',', LINUXUK_HTTP_INCLUDE_BAN);
  if (!in_array($_GET['Error'], $linuxuk_http_exclude) )
  {
  $result = tep_db_query("SELECT * FROM  " .TABLE_LINUXUK_HTTP_ERROR ." WHERE linuxuk_error_log_address =  '" . $address_esc . "'") ;
  $number_of_rows = tep_db_num_rows($result);
  if($number_of_rows == LINUXUK_HTTP_ERROR_STOP) {
           if(LINUXUK_IP_TRAP_EXCESSIVE_ERRORS == 'True'){
            $result = tep_db_query("SELECT * FROM  " .TABLE_IPTRAP ." WHERE IP_Number =  '" . $address_esc . "'") ;
            $linuxuk_url = 'http://www.linuxuk.co.uk';
            $number_of_rows = tep_db_num_rows($result);
            if ($number_of_rows == 0){
            tep_db_query("INSERT INTO " .TABLE_IPTRAP ." (date,IP_Number,UserAgent,BAN,BY_ADMIN)values (now(),'" .$address_esc. "','" . LINUXUK_NOTIFY_EMAIL_TEXT_4 . "','BLACK',1)");
            $new_notify_email = LINUXUK_NOTIFY_EMAIL_TEXT_2 ."\n\n"
            . "===================================================\n"
            . LINUXUK_NOTIFY_EMAIL_TEXT_1 . "\n\n"
            . LINUXUK_NOTIFY_EMAIL_IP . $address_esc . "\n"
            . LINUXUK_NOTIFY_EMAIL_USER_NAME . $agent_esc . "\n"
            . LINUXUK_NOTIFY_EMAIL_ERROR_NUMBER . $_GET['Error'] . "\n\n"
            . LINUXUK_NOTIFY_EMAIL_ERROR_CREDIT . $linuxuk_url;
             // Email Shop owener to notify of Alert
              tep_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, LINUXUK_NOTIFY_EMAIL_SUBJECT, $new_notify_email,STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
              tep_redirect(FILENAME_BLOCKED);
            }
           }
         }
         else
         {

         }
       }
     }
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
      <head>
          <title><?php echo $title;?></title>
          <meta name="Author" content="http://www.linuxuk.co.uk">
          <meta name="ROBOTS" content="INDEX, NOFOLLOW">
          <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
          <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
          </head>
          <body background="images/error_page_bg.gif"  text="black" link="#333333" vlink="black" alink="#CC00CC" style="font-family:'Arial'; font-style:normal; font-weight:normal; font-size:11pt;">
          <table cellpadding="0" cellspacing="0" width="900" bgcolor="white" align="center" height="600px">
             <tr>
                 <td bgcolor="#333" height="140px" align="center">
                 <?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . tep_image(HTTP_SERVER . DIR_WS_HTTP_CATALOG . DIR_WS_IMAGES . 'store_logo.png', STORE_NAME) . '</a>'; ?>
                 <p align="center"><b><font face="Arial Narrow" color="white"><span style="font-size:18pt;"><?php echo LINUXUK_HTTP_ERROR_TITLE;?></span></font></b><font face="Arial Narrow"></font></p>
                 </td>
             </tr>
             <tr>
                 <td>
          <table align="center" cellpadding="0" cellspacing="0" width="100%" bgcolor="white">
             <tr>
                 <td align="center">
                 <p><font face="Arial Narrow"><h1><?php echo $_GET['Error'] . ' Error.';?></h1></font></p>
                 </td>
             </tr>
             <tr>
                 <td align="center">
			     <form id="search" action="http://www.google.com/search" method="get">
	             <input type="hidden" name="as_sitesearch" value="<?php echo HTTP_SERVER . DIR_WS_HTTP_CATALOG;?>">
	             <label for="search-query">Enter a search query</label>
	             <input type="search" name="as_q" id="search-query" placeholder="Enter a search query" value="">
                 <input type="submit" value="Search"></form></td></tr>
          </table>
                 </td>
             </tr>
             <tr>
                 <td class="main"><center><h4><?php echo $error_message ;?></h4></center>
                 <h1 align="center"><font face="Arial Narrow"><?php echo $explaination;?></font></h1>
                 </td>
             </tr>
             <tr>
                 <td class="main"><center><?php echo '<a href="' . tep_href_link(FILENAME_ADVANCED_SEARCH) . '">' . BOX_SEARCH_ADVANCED_SEARCH . '</a>' ;?>  |  <?php echo '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '">' . BOX_INFORMATION_CONTACT . '</a>';?>  |  <?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . HEADER_TITLE_CATALOG . '</a>' ;?>  |  <?php echo '<a href ="http://www.linuxuk.co.uk/secure_your_site.php">' . 'Linuxuk' . '</a>' ;?></center></td>
             </tr>
             <tr>
                 <td height="30px"></td>
             </tr>
          </table>
   </body>
</html>