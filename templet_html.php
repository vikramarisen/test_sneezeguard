<table>
<tbody>
<tr>
<td colspan="4" style="font-size:12px;padding:2px"><a href="http://www.sneezeguards.com" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.sneezeguards.com&amp;source=gmail&amp;ust=1528792139097000&amp;usg=AFQjCNHc6ZBHw4O7M-9DVt3L5xfFvyUdZg"><img src="https://ci3.googleusercontent.com/proxy/Rzewkl4Ws8zIAMPbpKhiOAk6Lo8j_gBh3AFv6xbTbFVM_XL5nInaiTGu2x4FoRS_fk6fpjuUnDnppxyuf61lh6Pby49CseJSFTijO9DlTA=s0-d-e1-ft#http://www.sneezeguards.com/catalog/images/oscommerce.gif" border="0" class="CToWUd"></a></td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px"><strong>Dear <? echo$person ?></strong></td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px">Thank you for shopping with us today.</td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px">Note: Your tracking information will arrive in a seperate email from FedEx Shipment Notification</td>
</tr>
<tr>
<td><br></td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px">Please find below the details of your order:</td>
</tr>
<tr>
<td><br></td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px">Order Number: <u></u>909708</td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px">Date Ordered: <strong><span class="aBn" data-term="goog_1538995006" tabindex="0"><span class="aQJ">Monday 11 June, 2018</span></span></strong></td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px"><a href="https://www.sneezeguard.com/account_history_info.php?order_id=909708" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.sneezeguard.com/account_history_info.php?order_id%3D909708&amp;source=gmail&amp;ust=1528792139097000&amp;usg=AFQjCNFF50Yr5jP4nvUk660fJtK13MO0Ig">Detailed Invoice:</a></td>
</tr>
<tr>
<td><br></td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px">Products</td>
</tr>
<tr style="border:1px solid black">
<td style="background-clor:#879385;" align="left" style="font-size:12px;padding:2px;color:#fff;font-weight:bold">Item</td>
<td style="background-clor:#879385;" align="left" style="font-size:12px;padding:2px;color:#fff;font-weight:bold">Model</td>
<td style="background-clor:#879385;" align="center" style="font-size:12px;padding:2px;color:#fff;font-weight:bold">Quantity</td>
<td style="background-clor:#879385;" align="right" style="font-size:12px;padding:2px;color:#fff;font-weight:bold">Total:</td>
</tr>
<tr style="border:1px solid black">
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px">EP5-18" 24"Glass (Squared Corners)</td>
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px">EP5-18-24GLN </td>
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px" align="center">1</td>
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px" align="right">$88.00</td>
</tr style="border:1px solid black;">
<tr></tr>
<tr style="border:1px solid black">
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px">EP5-18"  End Post Brushed Stainless Steel</td>
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px">EP5-18EPSS </td>
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px" align="center">2</td>
<td style="background-clor:#f0f0f0;" style="font-size:12px;padding:2px" align="right">$128.00</td>
</tr>
<tr></tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px;"><div style="text-align:right">Sub-Total: $216.00<br><div style="background:white">Fedex Shipping (1 x 20lbs) (Ground Delivery):<br>$33.87</div><br>CA Sales Tax: $18.90<br>Total: $268.77<br></div></td>
</tr>
<tr>
<td><br></td>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px;background:yellow">Customer Comment : This is test order by developer.</td>
<td></td>
</tr>
<tr>
<td style="background-clor:#879385;" colspan="2" style="font-size:12px;padding:2px;color:#fff;font-weight:bold">Delivery Address</td>
<td style="background-clor:#879385;" colspan="2" style="font-size:12px;padding:2px;color:#fff;font-weight:bold">Billing Address</td>
</tr>
<tr>
<td style="background-clor:#f0f0f0;" colspan="2" style="font-size:12px;padding:2px">Company<br>Satendra Kumar<br>B-19 Sector-64,<br>Noida, CA    20130<br>United States</td>
<td style="background-clor:#f0f0f0;" colspan="2" style="font-size:12px;padding:2px">Company<br>Satendra Kumar<br>B-19 Sector-64,<br>Noida, CA    20130<br>United States</td>
</tr>
<tr>
<td style="background-clor:#879385;" colspan="4" style="font-size:12px;padding:2px;color:#fff;font-weight:bold">Payment Method</td>
</tr>
<tr>
<td style="background-clor:#f0f0f0;" colspan="4" style="font-size:12px;padding:2px">Credit Card</td>
</tr>
<tr>
<td colspan="4"><img src="https://ci3.googleusercontent.com/proxy/HJJ0voTuXe9JO56GI1wT4ESKFdYhdl7fvHGRQbwaneufpIcluvAG2E6nkUmH_55U_g8kn2SETOsuhykSBurfJ86i3IZFuK2CV3WPGnABOngLa6eyZlxNiGerNqbYhES4Mw8gaT-M2T8U=s0-d-e1-ft#http://sneezeguard.com/includes/img/184a48c60977262d8a628134358816ee_1004153617.jpg" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 594.6px; top: 1050.8px;"><div id=":20f" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V"><div class="aSK J-J5-Ji aYr"></div></div></div></td>
</tr>
<tr>
</tr>
<tr>
<td colspan="4" style="font-size:12px;padding:2px"><p>This email address was given to us by you or by one of our customers. If you feel that you <br>have received this email in error, please send an email to <a href="mailto:info@sneezeguard.com" target="_blank">info@sneezeguard.com</a><br>Copyright © 2018 <a href="http://www.sneezeguard.com" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.sneezeguard.com&amp;source=gmail&amp;ust=1528792139097000&amp;usg=AFQjCNGXXyh6dbdSiYcF_CqURfcnitNWiw">Sneezeguard.com</a></p></td>
</tr>
</tbody></table>