<?php
/*
  $Id: index.php,v 1.1 2003/06/11 17:38:00 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  
  Prepared by Lunaxod http://www.charger.od.ua
*/
    header("Content-type: application/rss+xml");

    require('includes/application_top.php');

   function mysqlTimestamp2unix($input){
            $y = substr($input,0,4);$m = substr($input,6,2);$d = substr($input,9,2);
            $h = substr($input,12,2);$min = substr($input,15,2);$s = substr($input,18,2);
                return mktime($h,$min,$s,$m,$d,$y);
            }
//1234567890123456789 
//2009-03-01 12:00:00

    require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_RSS);

    $language_query = tep_db_query("select code from " . TABLE_LANGUAGES . " where languages_id = '" . (int)$languages_id . "'");
        $language_code = tep_db_fetch_array($language_query);
        $code_lang = $language_code['code'];

     echo "<?xml version='1.0' ?><rss version='2.0'><channel>\n

     <title>" . ARTICLE_TITLE . "</title>\n";

     echo "<link>" . HTTP_SERVER . "</link>\n";

     echo "<description>" . ARTICLE_DESCRIPTION . "</description>\n";

     echo "<language>" . $code_lang . "</language>\n";

     echo "<docs>" . HTTP_SERVER . DIR_WS_CATALOG . FILENAME_RSS . "</docs>\n";

   $article_query_raw = tep_db_query("select nd.name, n.date_created, nd.content, n.id from " . TABLE_ARTICLE . " n, " . TABLE_ARTICLE_DESC . " nd where nd.language_id = '" . (int)$languages_id . "' and n.id = nd.article_id order by n.id desc limit " . ARTICLE_RSS_ARTICLE);

       while ($content_rec = tep_db_fetch_array($article_query_raw)) {
        echo "<item>";

        $headline = $content_rec['name'];
        $mydate = mysqlTimestamp2unix($content_rec['date_created']);
        $date = date('r',$mydate);
        $content_1 = substr($content_rec['content'], 0, ARTICLE_RSS_CHARACTERS);
        $content = strip_tags($content_1);
        if (strlen($content_rec['content']) > ARTICLE_RSS_CHARACTERS) {
            $content = $content . "....";
        }
        echo "<title>$headline</title>\n";
        echo "<pubDate>$date</pubDate>\n";
        echo "<description>$content</description>\n";
        $item_link = HTTP_SERVER . DIR_WS_CATALOG . FILENAME_ARTICLE . "?article=" . $content_rec['id'];
        echo "<link>$item_link</link>\n";
        echo "<guid>$item_link</guid>\n";
        echo "</item>\n";
    }

       echo "</channel></rss>";


 

?>