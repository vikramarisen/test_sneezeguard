<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

  <meta charset="utf-8" />
  
  
  <?php  
 // $modelname='EP5';
  if(isset($_GET['modelname']))
  {
 $modelname=$_GET['modelname'];	  
  }
  else{
  $modelname='EP5';
  }
  if(strpos($modelname,'EP-950') !== false){
  
  ?>
  
   <title>Sneeze Guards | Choose In-stock Glass designs | <?php echo$modelname; ?></title>
  
  
  <?php
  }
  else{
  ?>
  
  <title>Sneeze Guards | Choose In-stock Glass designs | <?php echo$modelname; ?></title>
  
  <?php
  }
  ?>
  
  
  
  <meta name="description" content="Choose our Sneeze Guard Model design <?php echo$modelname; ?> for reduce germs and virus. Select our latest <?php echo$modelname; ?> Glass Barriers custom designs and commercial supply equipment for protection.">
  
  
<meta name="keywords" content="Sneeze Guard <?php echo$modelname; ?>, Sneeze Guard Protection, Sneeze Guard <?php echo$modelname; ?> Design, Sneeze Guard <?php echo$modelname; ?> For Bakeries">
  
  
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">


<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />

<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />



  
  <!-- Include the VideoJS Library -->
  <script src="video.js" type="text/javascript" charset="utf-8"></script>

  <script type="text/javascript">
    // Must come after the video.js library

    // Add VideoJS to all video tags on the page when the DOM is ready
    VideoJS.setupAllWhenReady();


  </script>

  


  
  
  <!-- Include the VideoJS Stylesheet -->
  <link rel="stylesheet" href="video-js.css" type="text/css" media="screen" title="Video JS">
  
  
  <style>
    
  @media screen and (max-width: 992px) {
  
 #TB_window{
    height: 50% !important;
 }  
  }
  @media screen and (max-width: 768px) {
  
 #TB_window{
    height: 50% !important;
 }
  }
  @media screen and (max-width: 480px) {
  
 #TB_window{
    height: 50% !important;
 }	 
  #example_video_1{   
 width: 320px !important;
    height: 240px !important;
	}
	
  }
  </style>
  
</head>
<body  bgcolor="#000000">

  <!-- Begin sdcscVideoJS -->
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay="true" >
      <source src="sneezegaurd/productvideo/videos/<?php echo $_GET["name"]; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="sneezegaurd/productvideo/videos/<?php echo $_GET["name"]; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="sneezegaurd/productvideo/videos/<?php echo $_GET["name"]; ?>.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "sneezeguard/upload/videos/<?php echo $_GET["name"]; ?>.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
    <!-- Download links provided for devices that can't play video in the browser. -->
    <p class="vjs-no-video"><strong>Download Video:</strong>
      <a href="sneezegaurd/productvideo/videos/<?php echo $_GET["name"]; ?>.mp4">MP4</a>,
      <a href="sneezegaurd/productvideo/videos/<?php echo $_GET["name"]; ?>.webm">WebM</a>,
      <a href="sneezegaurd/productvideo/videos/<?php echo $_GET["name"]; ?>.ogv">Ogg</a><br>
      <!-- Support VideoJS by keeping this link. -->
      </p>
  </div>
  <!-- End VideoJS -->

  
  
  
  
  
  
  
  
  
  <h1 style="font-size: 1px;">Sneeze Guard for Bakeries, Pizza Store  | <?php echo$modelname; ?></h1>
  
  
  
  
</body>
</html>