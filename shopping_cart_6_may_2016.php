<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require("includes/application_top.php");
  if ($cart->count_contents() > 0) {
    include(DIR_WS_CLASSES . 'payment.php');
    $payment_modules = new payment;
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_SHOPPING_CART);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_SHOPPING_CART));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<div class="form_white " style="height:auto !important;  border: 1px #666666 solid; margin-bottom:40px; padding-bottom:70px;" >
<h1><?php // echo HEADING_TITLE; ?></h1>
<?php
  if ($cart->count_contents() > 0) {
?>

<?php echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_SHOPPING_CART, 'action=update_product')); ?>

<div class="contentContainer">
  <h2 align="left"  ><font color="black">&nbsp;&nbsp; Shopping Cart <?php //echo TABLE_HEADING_PRODUCTS; ?></font></h2>

  <div class="contentText">
<?php
    $any_out_of_stock = 0;
    $products = $cart->get_products();
	echo array_search("705", array_keys($products));
	//print_r($products);
    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
// Push all attributes information in an array
      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
        while (list($option, $value) = each($products[$i]['attributes'])) {
          echo tep_draw_hidden_field('id[' . $products[$i]['id'] . '][' . $option . ']', $value);
          $attributes = tep_db_query("select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix
                                      from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa
                                      where pa.products_id = '" . (int)$products[$i]['id'] . "'
                                       and pa.options_id = '" . (int)$option . "'
                                       and pa.options_id = popt.products_options_id
                                       and pa.options_values_id = '" . (int)$value . "'
                                       and pa.options_values_id = poval.products_options_values_id
                                       and popt.language_id = '" . (int)$languages_id . "'
                                       and poval.language_id = '" . (int)$languages_id . "'");
          $attributes_values = tep_db_fetch_array($attributes);
              
          $products[$i][$option]['products_options_name'] = $attributes_values['products_options_name'];
          $products[$i][$option]['options_values_id'] = $value;
          $products[$i][$option]['products_options_values_name'] = $attributes_values['products_options_values_name'];
          $products[$i][$option]['options_values_price'] = $attributes_values['options_values_price'];
          $products[$i][$option]['price_prefix'] = $attributes_values['price_prefix'];
        }
      }
    }
?>  <?php if(false){ //if($cart->count_contents()>0){?>
    <script type="text/javascript">
         $(function(){
                $(".price_table").css("opacity","1.3");
                $(".test-hide").css("opacity","1.3");
                var cssObj={
                    "background-color":"#111",
                    "border-style":"solid",
                    "border-width":"2px",
                    "border-color":"#C7F900"};
                $(".price_table").css(cssObj);
                $("#message_w").html('Verify your shopping cart contents and press "Check Out Now" to continue, or enter your zip code for a "Shipping Quotation" first*');
                setInterval(action_event, 4000);
            });
            action_event = function(){
                    $(".price_table").css("opacity","1.0");
                     var cssObj={
                        "background":"none",
                        "border":"none",
                        "box-shadow":"none"};
                    $(".price_table").css(cssObj);
                    $(".test-hide").css("opacity","1.0");
                    $(".message_p").remove();
                };
   </script>
   <style type="text/css">
       
		
		
		.message_p{
            position:relative;
            z-index: 1000000;
        }
        .message_w{
            position:absolute;
            color:#C7F900;
            text-shadow:2px 2px 3px #111;
            font-size: 18px;
            left:200px;
            top:50px;
            background: #000;
            border-radius:10px;
            padding:10px;
            border:1px solid #C7F900;
            font-weight: bold;
            text-align: center;
            width:400px;
        }
   </style>
   <? } ?>
    <div class="price_table">
    <table border="0" width="98%" cellspacing="0" cellpadding="8"    >
    

<?php
	//print_r($_SESSION['product_final1']);
	
	$_SESSION['product_final']=array();
	
	//print_r($_SESSION['product_final1']);echo "<br>  <br><br><br>  <br><br>";
	//print_r($products);
	
	$l=0;

	foreach($_SESSION['product_final1'] as $key=>$val){
        //echo $_SESSION['product_final1'][$n]['id'];
			
			
			
		for ($i=0, $n=sizeof($products); $i<$n; $i++)
		{
			
			if($_SESSION['product_final1'][$l]['id']==$products[$i]['id']){
				
				
					
				if (strpos($_SESSION['product_final1'][$l]['val'],'SLV') !== false){
					
					if($_SESSION['product_final1'][$l]['val']=='SLV'){
						$newname=$products[$i]['name'];
						$newdesc=$products[$i]['description'];
					}else{
						
						$newname=stripslashes($_SESSION['product_final1'][$l]['val']);
						$newdesc=$products[$i]['description'];
						
						$glassArray=array(24,30,36,42,48,54,60,66);
						$array = explode('"', stripslashes(str_replace("SLV GL ","",$newname)));
						
						if(!in_array($array[0],$glassArray)){
							$newname.=" Custom Products***";
						}
					}
				}
				else	
				
				if (strpos($products[$i]['name'],'B950') !== false){
					if($_SESSION['product_final1'][$l]['val']=='B950'){
						$newname=$products[$i]['name'];
						$newdesc=$products[$i]['description'];
						
						
						
						
					}else{
						
						$newname=stripslashes($_SESSION['product_final1'][$l]['val']);
						
						if(strpos($_SESSION['product_final1'][$l]['val'],'GL') !== false){
						
						    $newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("B950-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
							
						}else{
							$newdesc=$products[$i]['description'];
						}
						if(strpos($_SESSION['product_final1'][$l]['val'],'LEP') !== false){
							 $newdesc=stripslashes(str_replace("\"LEP","",str_replace("B950-g","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"X')));
							 
						}
						if(strpos($_SESSION['product_final1'][$l]['val'],'REP') !== false){
							 $newdesc=stripslashes(str_replace("\"REP","",str_replace("B950-g","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"X')));
							 
						}
					}
					
					
					
					if (strpos($newdesc,'Radiused Corners') !== false){
					
						if (strpos($newdesc,'B950-') !== false){
							$remove='B950-';
						}else{
							$remove='B950';
						}
						$glassArray=array(24,30,36,42,48,54,60,66);
						$array = explode('"', stripslashes(str_replace($remove,"",$newdesc)));
						$a= str_replace("<p>","",$array[0]);
						
						if(!in_array($a,$glassArray)){
						
							$newname.=" Custom Products"."***";
						}
					}
									
					if ((strpos($products[$i]['name'],'LEP') !== false)||(strpos($products[$i]['name'],'REP') !== false)){
						
						
						$postArray=array(12,18,24);
						
						$array = explode('"', stripslashes(str_replace("B950-g","",$_SESSION['product_final1'][$l]['val'])));
					
						if(in_array($array[0],$postArray)){
						
							//echo " Not custom";
						}else{
							$newname.=" Custom Products ***";
						}
						
					
						
					}
					
					
				}else
				if (strpos($products[$i]['name'],'ED20') !== false){

					if($_SESSION['product_final1'][$l]['val']=='ED20'){

						$newname=$products[$i]['name'];

						$newdesc=$products[$i]['description'];

					}else{

						$newname=stripslashes($_SESSION['product_final1'][$l]['val']." Custom Products");
							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                             $newdesc=$products[$i]['description'];
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							} 

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							
							if (strpos($products[$i]['name'],'ED20EPBSS') !== false){
							$newname="ED20 END PANEL GLASS CLIP";
							} else {
 							 $newname="ED20-CW".$counter." Custom Products ";
							}
                      
					  
					 // /*for light bar*/
//					  
					  if(strpos($products[$i]['name'],'LYT') !== false){
//								
//							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
//                                
//							$arr=explode("-", $newname);
//
//							if(sizeof($arr)>=3){
//
//								$counter=$arr[2];
//
//								$post=$arr[0]."-".$arr[1];
//
//							}else{
//
//								$counter=$arr[1];
//
//								$post=$arr[0];
//
//							} 
//							
							//$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
//							$array = explode('<p>', $newdesc);
//							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
//
//							$arr=explode("-", $newname);
//							
//							if(sizeof($arr)>=2){
//
//								$counter=$arr[0];
//                              
//								$post=$arr[1];
//								$arr2 = str_split($post, 1);
//                                $newname="ED20-".$counter."-".$arr2[0].$arr2[1].$arr2[2].$arr2[3].$arr2[4]." LYT Custom Products";
//							}else{
//
//								$counter=$arr[0];
//                                 
//								 $arr2 = str_split($counter, 1);
//								 if(sizeof($arr2)>=8){
//                               $newname="ED20-".$arr2[0].$arr2[1].$arr2[2].$arr2[3]." LYT Custom Products"; 
//								 }
//								 else{
//									 $newname="ED20-".$arr2[0].$arr2[1].$arr2[2]." LYT Custom Products"; 
//									 
//									 }
//							} 
								
							$newname="ED20-" .$post."-".$counter." Custom Products";	
							//$newname="ED20-".$arr2[0].$arr2[1].$arr2[2]." LYT Custom Products";
							}
							
							
							
					  /* for glass*/

							 if (strpos($products[$i]['name'],'GL') !== false&&strpos($products[$i]['name'],'SLP') === false){
							 $newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
			                $array2 = explode('<p>', $array);
				            $qtyNew345=edT2Desc($array[1]);
						    $qtyNew34=ed20TDesc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1, ".$qtyNew34." x ".$qtyNew345."</p><p>".$array[3]."</p>";
				           $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                                
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							} 

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ED20-PH".$post."-".$counter." Custom Products**";
							 }
							 
							  if (strpos($products[$i]['name'],'SLP') !== false ){
							 $newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
				           $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                                
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							} 

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ED20-PH".$post."-"."CW".$counter." Left Shelf Arm"." Custom Products";
							 }
							 if (strpos($products[$i]['name'],'SRP') !== false ){
							 $newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
				           $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                                
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							} 

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ED20-PH".$post."-"."CW".$counter." Right Shelf Arm"." Custom Products";
							 }
							  if (strpos($products[$i]['name'],'SCP') !== false ){
							 $newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
				           $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                                
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							} 

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ED20-"."CW".$counter." Center Shelf Arm"." Custom Products";
							 }
							 
							 if (strpos($products[$i]['name'],'FLANGE') !== false ){
							 $newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
				           $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                                
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							} 

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ED20"." FLANGE COVER"." Custom Products";
							 }
							 /* for top glass*/
							 
							 
            if (strpos($products[$i]['name'],'TG') !== false&&strpos($products[$i]['name'],'SLP') === false){
			             $newdesc=stripslashes(str_replace("TG","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
							$result = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($array[1]));
							$array9 = explode('<p>', $result);
							$array9 = $array9[0];
							$arr9 = str_split($array9, 2);
						    //$qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1, ".$qtyNew345." x ".($arr9[0]-1).' 1/8'."</p><p>".$array[3]."</p>";
				            $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));

							$arr=explode("-", $newname);
							
							if(sizeof($arr)>=2){

								$counter=$arr[0];
                              
								$post=$arr[1];
								$arr2 = str_split($post, 1);
                                $newname="ED20-".$counter."-".$arr2[0].$arr2[1].$arr2[2].$arr2[3].$arr2[4]."CW".$arr2[5].$arr2[6].$arr2[7].$arr2[8]."Custom Products";
							}else{

								$counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 1);
								 if(sizeof($arr2)>=8){
                               $newname="ED20-".$arr2[0].$arr2[1].$arr2[2].$arr2[3]."CW".$arr2[4].$arr2[5].$arr2[6].$arr2[7]; 
								 }
								 else{
									 $newname="ED20-".$arr2[0].$arr2[1].$arr2[2]."CW".$arr2[3].$arr2[4].$arr2[5].$arr2[6].$arr2[7]; 
									 
									 }
							}
						
				   
					
							
			}
						
								 //$newdesc=$products[$i]['description'];
						if (strpos($products[$i]['name'],'LP') !== false&&strpos($products[$i]['name'],'SLP') === false){

							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                              $newdesc=$products[$i]['description'];
							

							

							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							}

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ED20LP-CW".$counter."\"/PH".$post."SS";

						}

						if (strpos($products[$i]['name'],'RP') !== false&&strpos($products[$i]['name'],'SRP') === false){

							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                            $newdesc=$products[$i]['description'];
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							}

							 $newname="ED20RP-CW".$counter."\"/PH".$post."SS";

						}
						if (strpos($products[$i]['name'],'LEP') !== false){
							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
							//$newdesc=$products[$i]['description'];
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
							$result = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($array[1]));
							$array9 = explode('<p>', $result);
							$array9 = $array9[0];
							$arr9 = str_split($array9, 2);
							 $qtyNew346=edLEPDesc($arr[0],$arr[1]);
						    //$qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  
							  $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                                
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];
								
								$newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1, ".$qtyNew346." x ".($arr9[0]-2).' 3/8'."</p><p>".$array[3]."</p>";

							}else{

								$counter=$arr[1];

								$post=$arr[0];
                               
								$newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1, ".($arr[0]-2).$qtyNew346." x ".($arr9[0]-2).' 3/8'."</p><p>".$array[3]."</p>";
							} 
							  $newname="ED20-"."PH ".$post." CW ".$arr9[0]." LEP";
							  $newname= $newname." Custom Products**";
						}
						if (strpos($products[$i]['name'],'REP') !== false){
							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
							//$newdesc=$products[$i]['description'];
							$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
							$result = preg_replace("/[^a-zA-Z0-9]+/", "", html_entity_decode($array[1]));
							$array9 = explode('<p>', $result);
							$array9 = $array9[0];
							$arr9 = str_split($array9, 2);
							 $qtyNew346=edLEPDesc($arr[0],$arr[1]);
						    //$qtyNew34=B950Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  //$newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1,".$qtyNew346." x ".($arr9[0]-1).' 1/8'."</p><p>".$array[3]."</p>";
							  $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                                
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];
								$newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1, ".$qtyNew346." x ".($arr9[0]-2).' 3/8'."</p><p>".$array[3]."</p>";

							}else{

								$counter=$arr[1];

								$post=$arr[0];
								$newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1, ".($arr[0]-2).$qtyNew346." x ".($arr9[0]-2).' 3/8'."</p><p>".$array[3]."</p>";

							} 
							 $newname="ED20-"."PH ".$post." CW ".$arr9[0]." REP";
							  $newname= $newname." Custom Products**";
						}

						if (strpos($products[$i]['name'],'CP') !== false&&strpos($products[$i]['name'],'SCP') === false){

							$newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
                             $newdesc=$products[$i]['description'];
							$arr=explode("-", $newname);

							if(sizeof($arr)>=3){

								$counter=$arr[2];

								$post=$arr[0]."-".$arr[1];

							}else{

								$counter=$arr[1];

								$post=$arr[0];

							}

							 $newname="ED20CP-CW".$counter."\"/PH".$post."SS";

						}

					}

				}else if (strpos($products[$i]['name'],'ES82') !== false){
					if($_SESSION['product_final1'][$l]['val']=='ES82'){
						$newname=$products[$i]['name'];
						$newdesc=$products[$i]['description'];
							if (strpos($products[$i]['name'],'LP') !== false&&strpos($products[$i]['name'],'SLP') === false){

							$newname=$products[$i]['name'];

							
							

							$arr=explode(" ", $newname);
                                $counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 2);

						

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ES82LP-CW".$arr2[3];

							}
							if (strpos($products[$i]['name'],'RP') !== false&&strpos($products[$i]['name'],'SLP') === false){

							$newname=$products[$i]['name'];

							
							

							$arr=explode(" ", $newname);
                                $counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 2);

						

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ES82RP-CW".$arr2[3];

							}
							if (strpos($products[$i]['name'],'GL') !== false&&strpos($products[$i]['name'],'SLP') === false){

							$newname=$products[$i]['name'];

							
							

							$arr=explode(" ", $newname);
                                $counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 1);

						

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ES82-".$arr2[5].$arr2[6]."GL";

							}
						
					
					}else{
						$newname=stripslashes($_SESSION['product_final1'][$l]['val']);
						$newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ES82-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=es29Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
						//$newdesc=$products[$i]['description'];
						     if (strpos($products[$i]['name'],'TG') !== false&&strpos($products[$i]['name'],'SLP') === false){
							 $newdesc=stripslashes(str_replace("TG","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ES82-","",$_SESSION['product_final1'][$l]['val'])));
			                $array2 = explode('<p>', $array);
				            $qtyNew345=edT2Desc($array[0]);
						   $newname=$products[$i]['name'];
							  $arr=explode(" ", $newname);
                                $counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 2);

						

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>"."Qty-1, ".($arr2[4]-19).'-'.'9/16"'.' x '.$qtyNew345.' Top Glass'."</p><p>".$array[3]."</p>";
				            $newname= stripslashes(str_replace("ES82-","",$_SESSION['product_final1'][$l]['val']));

							$arr=explode("-", $newname);
                           if(sizeof($arr)>=2){

								$counter=$arr[0];
                              
								$post=$arr[1];
								$arr2 = str_split($post, 1);
                                $newname="ES82-".$counter."-".$arr2[0].$arr2[1].$arr2[2].$arr2[3].$arr2[4]."CW".$arr2[5].$arr2[6].$arr2[7].$arr2[8]."Custom Products";
							}else{

								$counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 1);
								 if(sizeof($arr2)>=8){
                               $newname="ES82-".$arr2[0].$arr2[1].$arr2[2].$arr2[3]."CW".$arr2[4].$arr2[5].$arr2[6].$arr2[7]; 
								 }
								 else{
									 $newname="ES82-".$arr2[0].$arr2[1].$arr2[2]."CW".$arr2[3].$arr2[4].$arr2[5].$arr2[6].$arr2[7]; 
									 
									 }
							}							
			}
						
					}
					if(strpos($products[$i]['name'],'RND') !== false){
					
						$newname=$newname."-"."RND";
					}
					//for check custom  or not
					$custom="";
					if (strpos($newdesc,'Square Corners') !== false){
						$custom='**';
					}
					if((strpos($newdesc,'Radiused Corners') !== false)||(strpos($newdesc,'Rounded Corners') !== false)){
						$custom='***';
					}
					$glassArray=array(12,18,24,30,36,42,48,54);
					if (strpos($newdesc,'Face Glass') !== false){
						$array = explode('"', stripslashes(str_replace("ES82-","",$_SESSION['product_final1'][$l]['val'])));
						if(!in_array($array[0],$glassArray)){
							$newname.=" Custom Products".$custom;
						}
					}
					if (strpos($newdesc,'Top Glass') !== false){
						$array = explode('"', stripslashes(str_replace("ES82-","",$_SESSION['product_final1'][$l]['val'])));
						if(!in_array($array[0],$glassArray)){
							$newname.=" Custom Products".$custom;
						}
					}
					
				}else
				if (strpos($products[$i]['name'],'ES53') !== false){
				
					if($_SESSION['product_final1'][$l]['val']=='ES53'){
					
						$newname=$products[$i]['name'];
						$newdesc=$products[$i]['description'];
					
					
					}else{
						$newname=stripslashes($_SESSION['product_final1'][$l]['val']);
						$newdesc=$products[$i]['description'];
						
							$array = explode('"', stripslashes(str_replace("ES53-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=es53Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
					}
					//for check custom or not
					$custom="";
					if (strpos($newdesc,'Rounded Glass') !== false){
						$custom='***';
					}else{
						$custom='**';
					}
					$glassArray=array(12,18,24,30,36,42,48,54);
					if (strpos($newname,'GL') !== false){
						$array = explode('"', stripslashes(str_replace("ES53-","",$_SESSION['product_final1'][$l]['val'])));
						if(!in_array($array[0],$glassArray)){
							$newname.=" Custom Products".$custom;
						}
					}
					
					
					
				}else
				if (strpos($products[$i]['name'],'ES29') !== false){
					
					if($_SESSION['product_final1'][$l]['val']=='ES29'){
						$newname=$products[$i]['name'];
						$newdesc=$products[$i]['description'];
						if (strpos($products[$i]['name'],'LP') !== false&&strpos($products[$i]['name'],'SLP') === false){

							$newname=$products[$i]['name'];

							
							

							$arr=explode(" ", $newname);
                                $counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 2);

						

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ES29LP-CW".$arr2[3];

							}
							if (strpos($products[$i]['name'],'RP') !== false&&strpos($products[$i]['name'],'SLP') === false){

							$newname=$products[$i]['name'];

							
							

							$arr=explode(" ", $newname);
                                $counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 2);

						

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							 $newname="ES29RP-CW".$arr2[3];

							}
					}else{
						$newname=stripslashes($_SESSION['product_final1'][$l]['val']);
						$newdesc=stripslashes(str_replace("GL","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ES29-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=es29Desc($array[0]);
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34."</p><p>".$array[3]."</p>";
						 if (strpos($products[$i]['name'],'TG') !== false&&strpos($products[$i]['name'],'SLP') === false){
						 $newdesc=stripslashes(str_replace("TG","",$_SESSION['product_final1'][$l]['val']).strstr($products[$i]['description'],'"'));
							$array = explode('"', stripslashes(str_replace("ES29-","",$_SESSION['product_final1'][$l]['val'])));
							
						    $qtyNew34=es29TDesc($array[0]);
							$newname=$products[$i]['name'];
							$arr=explode(" ", $newname);
                                $counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 2);

						

							//echo $counter;

							//strstr(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']),'-');

							//$post =stripslashes(current(explode("-", str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']))));

							

							
							$array = explode('<p>', $newdesc);
							 
							  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew34.($arr2[4]-13).'-'.'1/2"'.'Top Glass'."</p><p>".$array[3]."</p>";
				          // $newname= stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val']));
				            $newname= stripslashes(str_replace("ES29-","",$_SESSION['product_final1'][$l]['val']));
                               
							$arr=explode("-", $newname);
							
						if(sizeof($arr)>=2){

								$counter=$arr[0];
                              
								$post=$arr[1];
								$arr2 = str_split($post, 1);
                                $newname="ES29-".$counter."-".$arr2[0].$arr2[1].$arr2[2].$arr2[3].$arr2[4]."CW".$arr2[5].$arr2[6].$arr2[7].$arr2[8]."Custom Products";
							}else{

								$counter=$arr[0];
                                 
								 $arr2 = str_split($counter, 1);
								 if(sizeof($arr2)>=8){
                               $newname="ES29-".$arr2[0].$arr2[1].$arr2[2].$arr2[3]."CW".$arr2[4].$arr2[5].$arr2[6].$arr2[7]; 
								 }
								 else{
									 $newname="ES29-".$arr2[0].$arr2[1].$arr2[2]."CW".$arr2[3].$arr2[4].$arr2[5].$arr2[6].$arr2[7]; 
									 
									 }
							}}
							
						 if(strpos($products[$i]['name'],'BKLYT') !== false){
					
						$newname=$newname." BACK LIGHT";
					}
					}
					//for check custom  or not
					$custom="";
					if (strpos($newdesc,'Square Corners') !== false){
						$custom='**';
					}
					if((strpos($newdesc,'Radiused Corners') !== false)||(strpos($newdesc,'Rounded Corners') !== false)){
						$custom='***';
					}
					$glassArray=array(12,18,24,30,36,42,48,54);
					if (strpos($newdesc,'Face Glass') !== false){
						$array = explode('"', stripslashes(str_replace("ES29-","",$_SESSION['product_final1'][$l]['val'])));
						if(!in_array($array[0],$glassArray)){
							$newname.=" Custom Products".$custom;
						}
					}
					if (strpos($newdesc,'Top Glass') !== false){
						$array = explode('"', stripslashes(str_replace("ES29-","",$_SESSION['product_final1'][$l]['val'])));
						if(!in_array($array[0],$glassArray)){
							$newname.=" Custom Products".$custom;
						}
					}
					
					
					
				}else{
				/*creating custom product name */
				$newname="";
				$newdesc="";
				$qtyNew="";
				$qtyNew1="";
				if (strpos($products[$i]['name'],'Glass') !== false){
					$newname=strstr($products[$i]['name'],'Glass');
					$newname=stripslashes(($_SESSION['product_final1'][$l]['val'].$newname));
					
					
					//making description custom
					if(strpos($products[$i]['name'],'EP5') !== false){
						$array = explode('"', stripslashes(str_replace("EP5-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=ep5Desc($array[0],$array[1]);
					}if(strpos($products[$i]['name'],'EP22') !== false){
						$array = explode('"', stripslashes(str_replace("EP22-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=ep11Desc($array[0],$array[1]);
						
					}if(strpos($products[$i]['name'],'EP21') !== false){
						$array = explode('"', stripslashes(str_replace("EP21-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=ep11Desc($array[0],$array[1]);
						
					}if(strpos($products[$i]['name'],'EP36') !== false){
						$array = explode('"', stripslashes(str_replace("EP36-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=es31Desc($array[0],$array[1]);
						
					}
					if(strpos($products[$i]['name'],'ES31') !== false){
						$array = explode('"', stripslashes(str_replace("ES31-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=es31Desc($array[0],$array[1]);
					}if(strpos($products[$i]['name'],'ES40') !== false){
						$array = explode('"', stripslashes(str_replace("ES40-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=es31Desc($array[0],$array[1]);
					}if(strpos($products[$i]['name'],'ES67') !== false){
						$array = explode('"', stripslashes(str_replace("ES67-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=es67Desc($array[0],$array[1]);
					}if(strpos($products[$i]['name'],'ES73') !== false){
						$array = explode('"', stripslashes(str_replace("ES73-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=es67Desc($array[0],$array[1]);
					}
					if(strpos($products[$i]['name'],'EP15') !== false){
						$array = explode('"', stripslashes(str_replace("EP15-","",$_SESSION['product_final1'][$l]['val'])));
						$qtyNew=ep5Desc($array[0],$array[1]);
					}
					if(strpos($products[$i]['name'],'EP11') !== false){
						$array = explode('"', stripslashes(str_replace("EP11","",$_SESSION['product_final1'][$l]['val'])));
						//print_r($array);
						$qtyNew1=ep11Desc($array[0],$array[1]);
						
					}
					if(strpos($products[$i]['name'],'EP12') !== false){
						$array = explode('"', stripslashes(str_replace("EP12","",$_SESSION['product_final1'][$l]['val'])));
						//print_r($array);
						$qtyNew1=ep11Desc($array[0],$array[1]);
						
					}
					
					$newdesc=strstr($products[$i]['description'],'Glass')." ";
					$newdesc=stripslashes(($_SESSION['product_final1'][$l]['val'].$newdesc));
					if($qtyNew!=""){
					  $array = explode('<p>', $newdesc);
					  //print_r($array);
					  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew."</p><p>".$array[8]."</p>";
					}
					if($qtyNew1!=""){
					  $array = explode('<p>', $newdesc);
					  //print_r($array);
					  $newdesc="<p>".$array[0]."</p><p>".$array[1]."</p><p>".$qtyNew1."</p><p>".$array[4]."</p>";
					}
					
				}else if(strpos($products[$i]['name'],'FLANGE')== true){
					$newname=$products[$i]['name'];
					$newdesc=$products[$i]['description'];
				}else{
					$newname=strstr($products[$i]['name'],' ');
					$newname=stripslashes($_SESSION['product_final1'][$l]['val']." ".$newname);
					$newdesc=strstr($products[$i]['description'],' ');
					$newdesc=stripslashes($_SESSION['product_final1'][$l]['val']." ".$newdesc);
				}
				}
				/*end*/
				if ((strpos($products[$i]['name'],'Left End Panel') !== false)||(strpos($products[$i]['name'],' Right End Panel') !== false)||(strpos($products[$i]['name'],' Right End') !== false)||(strpos($products[$i]['name'],' Left End') !== false)){
				
					if(strpos($products[$i]['name'],'EP11') !== false){
						$postArray=array('17-1/4');
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP11","",$_SESSION['product_final1'][$l]['val'])));
						echo $array[0];
						if(trim($array[0])!=''){
						if((trim($array[0])=='17-1/4')){
						
							//echo " Not custom";
						}else{
							$newname.=" Custom Products ***";
						}
						}
						
					}
					if(strpos($products[$i]['name'],'EP12') !== false){
						$postArray=array('17-1/4');
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP12","",$_SESSION['product_final1'][$l]['val'])));
						
						if(trim($array[0])!=''){
						
						if((trim($array[0])=='17-1/4')){
						
							//echo " Not custom";
						}else{
							$newname.=" Custom Products ***";
						}
						}
						
					}
				
				
				}
				
				if ((strpos($products[$i]['name'],'Squared Corners') !== false)||(strpos($products[$i]['name'],'Radiused Corners') !== false)){
					$custom="";
					if(strpos($products[$i]['name'],'Squared Corners') !== false){
						$custom='**';
					}else if(strpos($products[$i]['name'],'Radiused Corners') !== false){
						$custom='***';
					}
					
					if(strpos($products[$i]['name'],'EP5') !== false){
						$postArray=array(12,18,22);
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP5-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$postArray)&&in_array($array[1],$glassArray)){
							//echo "Not Custom";
						}else{
							$newname.=" Custom Products".$custom;
						}
						
					}
					if(strpos($products[$i]['name'],'EP15') !== false){
						$postArray=array(18,22,26);
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP15-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$postArray)&&in_array($array[1],$glassArray)){
							//echo "Not Custom";
						}else{
							$newname.=" Custom Products".$custom;
						}
						
					}
					if(strpos($products[$i]['name'],'EP11') !== false){
						$postArray=array('17-1/4');
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP11","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						if((trim($array[0])=='17-1/4')&&in_array($array[1],$glassArray)&&($array[1]!='')){
						
							//echo " Not custom";
						}else{
							$newname.=" Custom Products".$custom;
						}
						
					}
					if(strpos($products[$i]['name'],'EP12') !== false){
						$postArray=array('17-1/4');
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP12","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						if((trim($array[0])=='17-1/4')&&in_array($array[1],$glassArray)&&($array[1]!='')){
						
							//echo " Not custom";
						}else{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'EP21') !== false){
						
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP21-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'EP22') !== false){
						
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP22-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'ED20') !== false){
						
						$glassArray=array(8,9,10,11,12,13);
						$array = explode('"', stripslashes(str_replace("ED20-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'EP36') !== false){
						
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("EP36-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'ES31') !== false){
						
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("ES31-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'ES40') !== false){
						
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("ES40-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'ES67') !== false){
						
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("ES67-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
					if(strpos($products[$i]['name'],'ES73') !== false){
						
						$glassArray=array(12,18,24,30,36,42);
						$array = explode('"', stripslashes(str_replace("ES73-","",$_SESSION['product_final1'][$l]['val'])));
						
						if(in_array($array[0],$glassArray)&&($array[1]=='')){
							//echo "Not Custom";
						}else
						{
							$newname.=" Custom Products".$custom;
						}
				
					}
				}
				
				
				if($_SESSION['product_final1'][$l]['custom']=='beyond'){
					$newname=$products[$i]['name'];
					$newdesc=$products[$i]['description'];
				}
			$_SESSION['product_final'][$l]=array('id'=>$products[$i]['id'],'name'=>$newname,'amt'=>$products[$i]['final_price'],'price'=>$currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), $_SESSION['product_final1'][$l]['qty']),'qty'=>$_SESSION['product_final1'][$l]['qty'],'model'=>$products[$i]['model'])	;
				
				 echo '      <tr>';

      $products_name = '<table border="0" cellspacing="0" cellpadding="5" >' .
                       '  <tr >' .
					   '<table border="0" cellspacing="0" cellpadding="0" style="border:1px solid #ccc; padding:10px; " ><tr><td>'.
                       '    <td align="left"  width="120px" valign="top" ><a href="' . tep_href_link(/*FILENAME_PRODUCT_INFO ,*/ 'product_info1.php?products_id=' . $products[$i]['id']) . '">' . tep_image(DIR_WS_IMAGES . $products[$i]['image'], $products[$i]['name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a></td>' .
                       '    <td ><a href="javascript:void(0)"><strong>' . $newname . '</strong></a><br>'.$newdesc;

      if (STOCK_CHECK == 'true') {
        $stock_check = tep_check_stock($products[$i]['id'], 1);
        if (tep_not_null($stock_check)) {
          $any_out_of_stock = 1;

          $products_name .= $stock_check;
        }
      }

      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
        reset($products[$i]['attributes']);
        while (list($option, $value) = each($products[$i]['attributes'])) {
          $products_name .= '<br /><small><i> - ' . $products[$i][$option]['products_options_name'] . ' ' . $products[$i][$option]['products_options_values_name'] . '</i></small>';
        }
      }

      $products_name .=	'   '  .
						'  ' .
                        '';
      echo '        <td valign="top">' . $products_name . '</td>' .
           '        <td valign="top" align="left" width="5%">'.tep_draw_input_field('cart_quantity1[]', $_SESSION['product_final1'][$l]['qty'], 'class="cart" size="4"') . tep_draw_hidden_field('products_id1[]', $products[$i]['id'],'id="pro"') .tep_draw_hidden_field('custom_val[]', $_SESSION['product_final1'][$l]['val']) .tep_draw_hidden_field('custom_type[]', $_SESSION['product_final1'][$l]['custom']) .'<br><br><table><tr><td>' .tep_image_submit("button_update_new.gif", IMAGE_BUTTON_CHECKOUT, 'button').'</td><td><a href="' . tep_href_link(FILENAME_SHOPPING_CART, 'products_id=' . $products[$i]['id']."&qty=".$_SESSION['product_final1'][$l]['qty'] ."&val=".$_SESSION['product_final1'][$l]['val']."&custom=".$_SESSION['product_final1'][$l]['custom']. '&action=remove_product') . '">'.tep_image_button("remove.gif").'</a></td><td><a href="'.tep_href_link(basename($PHP_SELF), 'action=add_wishlist&products_qty='.$_SESSION['product_final1'][$l]['qty'] .'&products_id=' . $products[$i]['id'].'&prodetatil='.tep_db_input(json_encode($_SESSION['product_final1'][$l]))).'"><img src=images/wishlist_icon.png height=20 border=0 title="Save for leter"></a></td></tr></table></td>'.
           '        <td align="right" valign="top" width="5%"style="color:black ; font-size:12px; font-weight:bold;" ><strong>' . $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), $_SESSION['product_final1'][$l]['qty']) . '</strong></td>' .
                                         '      </tr>'. '</table>';

	
										
	 echo '   </td> ' .
                        '  </tr>' ;
				
				
				
				
				break;
			}
			

		}
				
			
		$l++;

	}	
	
    
			
	?>		
	<span class="custompr"></span>
		<?php	
						
	
	
	

    for ($i=0, $n=sizeof($products); $i<$n; $i++) {}                    
?>

    </table>
    </div>
    
    </form><div  class="form_white4 "  >
	
	<?php $tis=0;
	
	
	if(($_SESSION['product_final1'][$tis]['custom'])=='Yes') { ?>
	
	<div style="color:#000000;padding:8px 50px;border:1px solid #000000;background:YELLOW;float:left;width:718px;margin-left: 16px;" align="center" >
	       ** for square corner glass 3-4 business days.
	<br />*** for radius corner glass 5-6 business days. </div><? } else { } ?>
          
        <?php if (CARTSHIP_ONOFF == 'Enabled') { require(DIR_WS_MODULES . 'shipping_estimator.php'); } else {}; ?>
      </div>
<?php
    if ($any_out_of_stock == 1) {
      if (STOCK_ALLOW_CHECKOUT == 'true') {
?>

    <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CAN_CHECKOUT; ?></p>

<?php
      } else {
?>

    <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CANT_CHECKOUT; ?></p>

<?php
      }
    }
?>

  <div>

  <div class="buttonSet" style="float: left; margin: 5px 0;width:100%;text-align:left;padding:5px;">
    <?php echo tep_image_submit("button_update.gif", IMAGE_BUTTON_CHECKOUT, 'button').'<a href="'.tep_href_link(FILENAME_DEFAULT, "cPath=86").'" style="margin:5px;">'.tep_image_button("button_continue_shopping.gif", IMAGE_BUTTON_CONTINUE).'</a>'.'<a href="'.tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL').'" style="margin: -65px 20px 0 0;float:right; background:#ccc;">'.tep_image_button("button_checkout.gif", IMAGE_BUTTON_CHECKOUT, 'button').'</a>';?>
  </div>
</div>

<?php
  } else {
?>

<div class="contentContainer">
  <div class="contentText">
    <?php 	$message = "ERROR OCCURRED:In your shopping please try again ";
                                          echo "<script type='text/javascript'>alert('$message');
										  window.location.href='index.php';</script>"; 
										  echo TEXT_CART_EMPTY; ?>

    <p align="right" style="width: 140px;"><?php echo '<a href="'.tep_href_link(FILENAME_DEFAULT, "cPath=86").'">'.tep_image_button("continue.gif", IMAGE_BUTTON_CONTINUE).'</a>'; ?></p>
  </div>
</div>
</div>
<?php
  }

  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
  //print_r($_SESSION['product_final']);
  echo $face;
  function ep5Desc($n1,$n2){
  		$desc="Qty 1- ";
  		$n1=explode('-',$n1);
		if($n1[1]==''){
			$desc=$desc.($n1[0]-2).' 1/2';
		} else {
			if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/4'; }
			if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-1).' '; }
			if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-1).' 1/4'; }
		}
		$desc=$desc." x ";
		$n2=explode('-',$n2);
		if($n2[1]==''){
			$desc=$desc.($n2[0]-2).' 1/8';
		} else {
			if($n2[1]=='1/4'){ $desc=$desc.($n2[0]-2).' 3/8'; }
			if($n2[1]=='1/2'){ $desc=$desc.($n2[0]-2).' 5/8'; }
			if($n2[1]=='3/4'){ $desc=$desc.($n2[0]-2).' 7/8'; }
		}
		
		return $desc;
  }
    function es53Desc($n1,$n2){
  		$desc='Qty 1- 20" x ';
	$n1=explode('-',$n1);
			if($n1[1]==''){
				$desc=$desc.($n1[0]-2).' 1/8';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/8'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 5/8'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 7/8'; }
			}
			return $desc.'"';
		
		return $desc;
  }  
  function B950Desc($n1){
    $desc='Qty 1- 14" x ';
	$n1=explode('-',$n1);
			if($n1[1]==''){
				$desc=$desc.' '.($n1[0]-2);
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 1/4'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 1/2'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 3/4'; }
			}
			return $desc.'"';
  }
    function es31Desc($n1,$n2){
  		$desc='Qty 2- 11 1/2" x ';
	$n1=explode('-',$n1);
			if($n1[1]==''){
				$desc=$desc.($n1[0]-2).' 1/8';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/8'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 5/8'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 7/8'; }
			}
			return $desc.'"';
		
		return $desc;
  }  function es67Desc($n1,$n2){
  		$desc='Qty 1- 11 1/2" x ';
	$n1=explode('-',$n1);
			if($n1[1]==''){
				$desc=$desc.($n1[0]-2).' 1/8';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/8'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 5/8'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 7/8'; }
			}
			return $desc.'"';
		
		return $desc;
  }  
   function ep11Desc($n1,$n2){
  		$desc="Qty 1- ";
		$desc1="Qty 1- ";
		$n3=$n1;
		
		if($n2==''){
			
			$n1=explode('-',$n1);
			
			
			if($n1[1]==''){
				$desc=$desc.'11 1/2 x '.($n1[0]-2).' 1/8';
				$desc1=$desc1.'14 3/8 x '.($n1[0]-2).' 1/8';
			}else{
				$desc=$desc.'11 1/2 x ';
				$desc1=$desc1.'14 3/8 x ';
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/8';$desc1=$desc1.($n1[0]-2).' 3/8'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 5/8';$desc1=$desc1.($n1[0]-2).' 5/8'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 7/8';$desc1=$desc1.($n1[0]-2).' 7/8'; }
			}
			
		}
		else{
			$n3=explode('-',$n3);
			$desc=$desc.'11 1/2 x';
			if($n3[1]==''){
				$desc1=$desc1.($n3[0]-3).' 1/8';
			} else {
				if($n3[1]=='1/4'){ $desc1=$desc1.($n3[0]-3).' 3/8'; }
				if($n3[1]=='1/2'){ $desc1=$desc1.($n3[0]-3).' 5/8'; }
				if($n3[1]=='3/4'){ $desc1=$desc1.($n3[0]-3).' 7/8'; }
			}
			$desc1=$desc1." x ";
			$n2=explode('-',$n2);
			if($n2[1]==''){
				$desc1=$desc1.($n2[0]-2).' 1/8';
				$desc=$desc.($n2[0]-2).' 1/8';
			} else {
				if($n2[1]=='1/4'){ $desc=$desc.($n2[0]-2).' 3/8';$desc1=$desc1.($n2[0]-2).' 3/8'; }
				if($n2[1]=='1/2'){ $desc=$desc.($n2[0]-2).' 5/8';$desc1=$desc1.($n2[0]-2).' 5/8'; }
				if($n2[1]=='3/4'){ $desc=$desc.($n2[0]-2).' 7/8';$desc1=$desc1.($n2[0]-2).' 7/8'; }
			}
		}
		return $desc."</p><p>".$desc1;
  }   function ed20TDesc($n1){
    $desc='';
	$n1=explode('-',$n1);
		if($n1[1]==''){
				$desc=$desc.($n1[0]-2).' 1/8';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/8'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 5/8'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 7/8'; }
			}
			return $desc.'"';
  }  
  function edT2Desc($n1){
    $desc='';
	$n1=explode('-',$n1);
		if($n1[1]==''){
				$desc=$desc.($n1[0]-2).' 1/8';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/8'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 5/8'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 7/8'; }
			}
			return $desc.'"';
  }   function edLEPDesc($n1,$n2){
  	//$desc="Qty 1- ";
		//$desc1="Qty 1- ";
		//$n3=$n1;
		$n3=explode('-',$n2);
		$n1=$n1;
		if($n3[2]==$n1){
			
			//$desc1="dcdfvdv";
			
		}
		else{
			//$n3=explode('-',$n3);
			$desc=$n1;
		$n2=$n2;
		$n1=$n1;
		  //$desc=$n1;
		  
				if($n2=='1/4"'){ $desc=$desc.($n1-2).' 3/8';$desc1=$desc1.($n1-2).' 1/4'; }
				if($n2=='1/2"'){ $desc=$desc.($n1-2).' 5/8';$desc1=$desc1.($n1-2).' 1/2'; }
				if($n2=='3/4"'){ $desc=$desc.($n1-2).' 7/8';$desc1=$desc1.($n1-2).' 3/4'; }
			
			//$desc1=$desc1." x ";
			
		}
		return $desc1;
  }
   function es29Desc($n1){
    $desc='Qty 1- 11 1/2" x ';
	$n1=explode('-',$n1);
		if($n1[1]==''){
				$desc=$desc.($n1[0]-2).' 1/8';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc.($n1[0]-2).' 3/8'; }
				if($n1[1]=='1/2'){ $desc=$desc.($n1[0]-2).' 5/8'; }
				if($n1[1]=='3/4'){ $desc=$desc.($n1[0]-2).' 7/8'; }
			}
			return $desc.'"';
  }  function es29TDesc($n1){
    $desc='Qty 1- 11 1/2" x ';
	$n1=explode('-',$n1);
			if($n1[1]==''){
				$desc=$desc.' ';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc; }
				if($n1[1]=='1/2'){ $desc=$desc; }
				if($n1[1]=='3/4'){ $desc=$desc; }
			}
			return $desc;
  } function es82TDesc($n1){
    $desc='Qty 1- 11 1/2" x ';
	$n1=explode('-',$n1);
			if($n1[1]==''){
				$desc=$desc.' ';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc; }
				if($n1[1]=='1/2'){ $desc=$desc; }
				if($n1[1]=='3/4'){ $desc=$desc; }
			}
			return $desc;
  } function es82T1Desc($n1){
    $desc='';
	$n1=explode('-',$n1);
			if($n1[1]==''){
				$desc=$desc.' ';
				
			}else{
				if($n1[1]=='1/4'){ $desc=$desc; }
				if($n1[1]=='1/2'){ $desc=$desc; }
				if($n1[1]=='3/4'){ $desc=$desc; }
			}
			return $desc;
  } 
// print_r($_SESSION['product_final']);
?>
<script type="text/javascript">
$(document).ready(function(){
 
 
 $('.cart').change(function(){
 	var i=0;
	var str="";
	var idArray=new Array(); 
 	$("input[type=hidden][id^=pro][value]").each(function(){
    	
		idArray[i]=$(this).val();
		
		i++;
	})
	//return uniqe array start
	b = {};
	for (var i = 0; i < idArray.length; i++) {
		b[idArray[i]] = idArray[i];
	}
	c = [];
	for (var key in b) {
		c.push(key);
	}
	//end unique array
	var str="";
	for(j=0;j<c.length;j++){
		i=0;
		var qty=0;
		$("input[type=hidden][id^=pro][value]").each(function(){
    	
			if($(this).val()==c[j]){
				
				qty+=$('.cart').eq(i).val()-0;
			}
		
		i++;
		})
		str+='<input type="hidden" value="'+qty+'" name="cart_quantity[]" >';
		str+='<input type="hidden" value="'+c[j]+'" name="products_id[]" >';
		
		
	}
	$('.custompr').html(str);
 })


})
</script>