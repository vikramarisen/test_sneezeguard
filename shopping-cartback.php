<?php 
  	require('includes/application_top.php');
    include('includes/header_new_design.php'); 
?>
<style>
  body{
    background-color: white;
  }
   .cartheader{
    margin-top: 12%;
    margin-bottom: 5% !important;
    text-align: center;
  }
  .cartheader h1{
    text-align: center;
    padding-bottom: 10px;
    font-weight: 600;
    font-size: 28px;
    font-family: Lato,sans-serif;
    border-bottom: 1px solid #e5e5e6;
  }
  .cart-item-product{
    font-size: 12px;
  }
  .cart-item-price{
    font-size: 12px;
    text-align: center;
  }
  .cart-item-quantity{
    font-size: 12px;
    text-align: center;
  }
  .cart-item-total{
    font-size: 12px;
    text-align: right;
  }
  .cart-item-total span{
    font-size: 1.125em;
    font-weight: 600;
  }
  .title{
    display: block;
    color: #7b7b7c;
    text-transform: uppercase;
    font-weight: 600;
    text-decoration: none;
    font-size: 1.25em;
  }
  a.remove{
    display: block;
    font-size: 1.25em;
    margin-top: 10px;
    text-align: center;
    text-decoration: none;
    color: #929393;
  }
  .cart-item{
    padding-top: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #dbdbdb;
  }
  .cart .cart-items{
    padding-top: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #dbdbdb;
  }
  
  .cart-image{
    float: left;
    width: 160px;
    margin-right: 16px;
  }
  .cart-image img{
    width: 100%;
    height: auto;
  }
  .variant{
    font-size: 1.12em;
    color: #7b7b7c;
    text-transform: uppercase;
    font-weight: 600;
  }

  .product-description a{
    display: block;
    color: #030405;
    font-weight: 600;
    text-decoration: none;
    font-size: 1.50em;
  }
  .fa{
    font-size: 13px !important;
    color:red;
  }
  .money{
    font-size: 14px;
    font-weight: 600;
  }
  .quantity-select{
    position: relative;
    display: flex;
    width: 100%;
    justify-content: left;
    align-items: flex-end;
    flex-direction: row;
    max-width: none;
  }
  .quantity-select .button-wrapper{
    flex: 1 0 30px;
  }
  .quantity-select .button-wrapper button{
    width: 100%;
  }
  .quantity-select button, .quantity-select input{
    border: 1px solid #e5e5e5;
  }
  .quantity-select .input-wrapper input{
    width: calc(100% - 16px);
  }
  .input-wrapper{
    flex: 1 1 auto;
    display: flex;
    width: auto;
    justify-content: center;
  }
  .quantity-select button, .quantity-select input{
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    height: 30px;
    border: 0px;
    border: 1px solid #d8d8e8 !important;
    font-size: 15px;
    text-transform: uppercase;
    background: transparent;
    text-align: center;
  } 

  .row{
    border: none;
    border-bottom: 1px solid #dbdbdb;
    margin-top: 2px;
  }
  .subtotal{
    font-size: 2.5em;
    text-align: right;
    font-weight: 600;
  }
  .subtotal > .money{
    font-weight: 600;
    font-size: 0.70em;
  }

  .label{
    color: #030405;
    text-align: right;
  }
  .shipping-at-checkout{
    text-align: right;
    font-size: 17px;
  }
  .check-out{
    text-align: right;
    margin-top: 2em !important;
  }
  .check-out input{
    font-size: 1.25em;
    font-weight: bold;
    padding: 14px 40px;
    text-transform: uppercase;
    width: 143px;
    border: 2px solid #ed7006;
    background: transparent;
    color: #ed7006;
    transition: all .1s ease-in-out;  
  }
  .check-out input:hover{
    border: 2px solid rgba(237,112,6,0.75);
    color: rgba(237,112,6,0.75);
    background: transparent;
    transition: all .1s ease-in-out;
  }
  .subtotaldiv{
    margin-top: 4em;
  }
  .mobile-total{
      display: none;
    }
  @media screen and (max-width: 576px){
    .cart-image img{
      width: 100px;
      height: 83px;
    }
    .cartheader{
      margin-top: 20%;
    }
    .cartheader h1{
      font-size: 12px;
    }
    .th{
      display: none;
    }
    .qtycont{
      position: relative !important;
      width: 60% !important;
      left: 33% !important;
      top: -29px;
    }
    .pricecont{
      position: relative;
      left: -37%;
      top: 11px;

    }
    .mobile-total{
      display: block;
      position: relative;
      left: -87%;
      top: 20px;
      text-align: right;
      font-size: 1.12em;
      color: #7b7b7c;
      text-transform: uppercase;
      font-weight: 600;
    }
    .cart-item-total{
      position: relative;
      top: -49px;
      margin-bottom: -54px !important;
    }
    .check-out{
      text-align: center;
    }
    .shipping-at-checkout{
      text-align: center;
    }
    .subtotaldiv p{
      text-align: center;
    }
    }
  @media  screen and (max-width: 1920px) {}

</style>

<div class="cartheader mb-5">
        <h1>SHOPPING CART</h1>
</div>
<div class="container-lg"  >
  <div class="row pb-4 th">
    <div class="col-md-5 col-sm-5  cart-item-product title">
    PRODUCT
    </div>
    <div class="col-md-2 col-sm-2 cart-item-price title">
    PRICE
      </div>
      <div class="col-md-3 col-sm-3 cart-item-quantity title">
      QUANTITY
      </div>
      <div class="col-md-2 col-sm-2 cart-item-total title">
      TOTAL
      </div>
  </div>
  <div class="row mt-4 pb-5">
    <div class="col-md-5 col-sm-5">
    <div class="cart-image .img-fluid"><img src="https://esneezeguard.com/test_server_sneezeguard/images/EP5/START.jpg" alt=""></div>
    <div class="product-description">
        <a href="#">Ring-EP5</a>
        <span class="variant">Regular / Soundshield Black Gloss with Smoke Black Lenses</span>
      </div>  
  </div>
    <div class="col-md-2 col-sm-2 pricecont">
    <div class="td cart-item-price"><p class="money"><span class="fa fa-dollar"></span><span>134.99</span></p></div>
      </div>
      <div class="col-md-3 col-sm-3 qtycont">
      <span>
      <div class="quantity-select" data-quantity-select-enabled="true">
      <div class="button-wrapper">
      <button type="button" class="adjust adjust-minus" data-cart-item-quantity-minus="">-</button>
      </div>
      <div class="input-wrapper">
      <input type="text" class="quantity" data-cart-item-quantity="" min="0" pattern="[0-9]*" name="updates[]" id="updates_32199375618113" value="3" step="1">
      </div>
      <div class="button-wrapper">
      <button type="button" class="adjust adjust-plus" data-cart-item-quantity-plus="">+</button>
      </div>
      </div>
      </span>
      <a href="/cart/change?line=1&amp;quantity=0" class="remove" data-cart-item-remove="">Remove</a>
      </div>
      <div class="col-md-2 col-sm-2">
      <div class="td cart-item-total">
      <p class="mobile-total">Total</p>  
      <p class="money"><span class="fa fa-dollar"></span><span>134.99</span></p></div>
      </div>
  </div>

  <div class="row mt-4 pb-5">
    <div class="col-md-5 col-sm-5">
    <div class="cart-image .img-fluid"><img src="https://esneezeguard.com/test_server_sneezeguard/images/EP5/START.jpg" alt=""></div>
    <div class="product-description">
        <a href="#">Ring-EP5</a>
        <span class="variant">Regular / Soundshield Black Gloss with Smoke Black Lenses</span>
      </div>  
  </div>
    <div class="col-md-2 col-sm-2 pricecont">
    <div class="td cart-item-price"><p class="money"><span class="fa fa-dollar"></span><span>134.99</span></p></div>
      </div>
      <div class="col-md-3 col-sm-3 qtycont">
      <span>
      <div class="quantity-select" data-quantity-select-enabled="true">
      <div class="button-wrapper">
      <button type="button" class="adjust adjust-minus" data-cart-item-quantity-minus="">-</button>
      </div>
      <div class="input-wrapper">
      <input type="text" class="quantity" data-cart-item-quantity="" min="0" pattern="[0-9]*" name="updates[]" id="updates_32199375618113" value="3" step="1">
      </div>
      <div class="button-wrapper">
      <button type="button" class="adjust adjust-plus" data-cart-item-quantity-plus="">+</button>
      </div>
      </div>
      </span>
      <a href="/cart/change?line=1&amp;quantity=0" class="remove" data-cart-item-remove="">Remove</a>
      </div>
      <div class="col-md-2 col-sm-2">
      <div class="td cart-item-total">
      <p class="mobile-total">Total</p>  
      <p class="money"><span class="fa fa-dollar"></span><span>134.99</span></p></div>
      </div>
  </div>

  <div class="row mt-4 pb-5">
    <div class="col-md-5 col-sm-5">
      <div class="row">
        <div class="col-md-5 col-sm-5">
        <div class="cart-image .img-fluid"><img src="https://esneezeguard.com/test_server_sneezeguard/images/EP5/START.jpg" alt=""></div>
        </div>
        <div class="col-md-7 col-sm-7">
        <a href="#">Ring-EP5</a>
        <span class="variant">Regular / Soundshield Black Gloss with Smoke Black Lenses</span>
        </div>
      </div>
  </div>
    <div class="col-md-3 col-sm-3">
    <div class="td cart-item-price"><p class="money"><i class="fa fa-dollar"></i><span>134.99</span></p></div>
      </div>
      <div class="col-md-2 col-sm-2">
      <span>
      <div class="quantity-select" data-quantity-select-enabled="true">
      <div class="button-wrapper">
      <button type="button" class="adjust adjust-minus" data-cart-item-quantity-minus="">-</button>
      </div>
      <div class="input-wrapper">
      <input type="text" class="quantity" data-cart-item-quantity="" min="0" pattern="[0-9]*" name="updates[]" id="updates_32199375618113" value="3" step="1">
      </div>
      <div class="button-wrapper">
      <button type="button" class="adjust adjust-plus" data-cart-item-quantity-plus="">+</button>
      </div>
      </div>
      </span>
      <a href="/cart/change?line=1&amp;quantity=0" class="remove" data-cart-item-remove="">Remove</a>
      </div>
      <div class="col-md-2 col-sm-2">
      <div class="td cart-item-total"><p class="money"><i class="fa fa-dollar"></i><span>134.99</span></p></div>
      </div>
  </div>
  <div class="mt-5 subtotaldiv">
        <p class="subtotal price money">Subtotal $404.97 </p>
  </div>
  <div class="shipping-at-checkout mt-5">
  <em>Shipping &amp; taxes calculated at checkout</em>
  </div>
  <div class="check-out">
    <input type="submit" name="checkout" class="button solid" value="Check Out">
  </div>
</div>
<?php include('includes/footer_new_design.php');?>