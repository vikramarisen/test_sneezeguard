<?php
require('includes/application_top.php');
  //require(DIR_WS_INCLUDES . 'template_top.php');
  
  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard for Grocery | Archive | Adm Sneezeguards</title>
<!-- End Google Add Conversion -->

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="ADM Sneezeguards manufactures Glass Barriers for office, we provide industry standard sneeze guard with latest innovative models with various size and color.">
<meta name="keywords" content="Sneeze Guard portable for Office, archive sneeze guard, Sneeze Guard for Counter, Sneeze Guard Divider, Glass Divider portable">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>




<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />

<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />



<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->
</style>



<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<table border="0"  align=center BACKGROUND="middleback.jpg">
<tr>
<td align=center>
<font size="4" color="white">
<?php
echo '<P>';
echo '<B>' . $Model . ' Archived Models</B>';
echo '<P>';
?>
</font>
</td>
</tr>
</table>

<table border="0" align=center BACKGROUND="middleback.jpg">
<tr>
<td align=center>


<?php
$ExNum = 1;
$dir = "/home/esneezeg/public_html/sneezeguard/images/" . $Model . "/addlImages/";

// Open a known directory, and proceed to read its contents
if (is_dir($dir)) {
if ($dh = opendir($dir)) {
while (($file = readdir($dh)) !== false) {
	if($file != "." && $file != ".."){
echo '<font color="white">' . $Model . ' / Example ' . $ExNum . '<br>';
echo '<IMG BORDER="5" SRC="/images/' . $Model . "/addlImages/" . $file . '">';
echo '<P>';
$ExNum = $ExNum + 1;
}
}
closedir($dh);
}
}
?>
	<!-- Start VisualLightBox.com BODY section id=1 -->
<DIV ALIGN=CENTER> 
	<div id="vlightbox1">
<?php
  if (!$detect->isMobile())
{
?>	

<a class="vlightbox1" href="data/images1/1bay-sideglass2000.jpg" title="1BAY-SIDEGLASS2000"><img src="data/thumbnails1/1bay-sideglass2000.jpg" alt="1BAY-SIDEGLASS2000"/></a>
<a class="vlightbox1" href="data/images1/b950-1350000.jpg" title="B950-1350000"><img src="data/thumbnails1/b950-1350000.jpg" alt="B950-1350000"/></a>
<a class="vlightbox1" href="data/images1/newcount10000.jpg" title="NEWCOUNT10000"><img src="data/thumbnails1/newcount10000.jpg" alt="NEWCOUNT10000"/></a>
<a class="vlightbox1" href="data/images1/updbrac30000.jpg" title="UPDBRAC30000"><img src="data/thumbnails1/updbrac30000.jpg" alt="UPDBRAC30000"/></a>
<a class="vlb" style="display:none" href="http://visuallightbox.com">Photo Gallery Script by VisualLightBox.com v4.8</a>

 <?php
}
  else
{
?>
<style>
p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 37px;
    /* padding-left: 35px; */
}
#style img{width: 459px;}
</style>
<table id="style">
<tr><td>
<a class="vlightbox1" href="data/images1/1bay-sideglass2000.jpg" title="1BAY-SIDEGLASS2000"><img src="data/thumbnails1/1bay-sideglass2000.jpg" alt="1BAY-SIDEGLASS2000"/></a>
</td></tr>
<tr><td>
<a class="vlightbox1" href="data/images1/b950-1350000.jpg" title="B950-1350000"><img src="data/thumbnails1/b950-1350000.jpg" alt="B950-1350000"/></a>
</td></tr>
<tr><td></td></tr>
<tr><td>

<a class="vlightbox1" href="data/images1/newcount10000.jpg" title="NEWCOUNT10000"><img src="data/thumbnails1/newcount10000.jpg" alt="NEWCOUNT10000"/></a>
</td></tr>
<tr><td>
<a class="vlightbox1" href="data/images1/updbrac30000.jpg" title="UPDBRAC30000"><img src="data/thumbnails1/updbrac30000.jpg" alt="UPDBRAC30000"/></a>
<a class="vlb" style="display:none" href="http://visuallightbox.com">Photo Gallery Script by VisualLightBox.com v4.8</a>
</td></tr>
</table>

 <?php
}
?>	
	

	</div>
<DIV ALIGN=CENTER>
	<!-- End VisualLightBox.com BODY section -->

</td>
</tr>
</table>


<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>