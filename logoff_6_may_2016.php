<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGOFF);

  $breadcrumb->add(NAVBAR_TITLE);

  tep_session_unregister('customer_id');
  tep_session_unregister('customer_default_address_id');
  tep_session_unregister('customer_first_name');
  tep_session_unregister('customer_country_id');
  tep_session_unregister('customer_zone_id');
  tep_session_unregister('comments');
  $wishlist->removeSession();
  session_destroy();
  unset($_SESSION['product_custom']);
  unset($_SESSION['product_final1']);
// Discount Code 2.7 - start
if (MODULE_ORDER_TOTAL_DISCOUNT_STATUS == 'true' &&
tep_session_is_registered('sess_discount_code')) {
tep_session_unregister('sess_discount_code');
}
// Discount Code 2.7 - end
  $cart->reset();

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script>
	$(document).ready(function(){
		if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
   			$("#ex1").css('width',"100%");
   			$(".price_table").css("text-align","left");
		}
   	});

</script>
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 
<tr>
<td colspan="3" style="background:url(img/TopBak.jpg) !important ; height:26px; border:1px solid #ccc;">&nbsp;</td>
</tr>
<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td width="35%"  style="border-right:1px solid #ccc;" valign="top" >
<h2 style=" height:26px;padding:5px 0 0 10px"><?php echo HEADING_TITLE; ?></h2>

<div class="contentContainer" style="text-align: left;">
  <div class="contentText">
    <?php echo TEXT_MAIN; ?>
  </div>

  <div class="buttonSet">
    <span class="buttonAction"><?php echo '<a href="'.tep_href_link(FILENAME_DEFAULT).'">'.tep_image_button("continue.gif", IMAGE_BUTTON_CONTINUE, 'button').'</a>'; ?></span>
  </div>
</div>
</td></tr></table></td></tr></table>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
