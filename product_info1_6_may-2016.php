<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_INFO);

  $product_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int)$HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "'");
  $product_check = tep_db_fetch_array($product_check_query);

  require(DIR_WS_INCLUDES . 'template_top.php');

  if ($product_check['total'] < 1) {
?>

<div class="contentContainer">
  <div class="contentText"> <?php echo TEXT_PRODUCT_NOT_FOUND; ?> </div>
  <div style="float: right;"> <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?> </div>
</div>
<?php
  } else {
    $product_info_query = tep_db_query("select p.products_id, pd.products_name, pd.products_description, p.products_model, p.products_quantity, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.manufacturers_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int)$HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "'");
    $product_info = tep_db_fetch_array($product_info_query);

    tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int)$HTTP_GET_VARS['products_id'] . "' and language_id = '" . (int)$languages_id . "'");

    if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
      $products_price = '<del>' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
    } else {
      $products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
    }

    if (tep_not_null($product_info['products_model'])) {
      $products_name = $product_info['products_name'] . '<br /><span class="smallText">[' . $product_info['products_model'] . ']</span>';
    } else {
      $products_name = $product_info['products_name'];
    }
      $image = tep_db_query("select c.categories_image, cd.categories_name from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd, ".TABLE_PRODUCTS_TO_CATEGORIES. " pc where c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' and c.categories_id=pc.categories_id and products_id='".$product_info['products_id']."'");
      $image = tep_db_fetch_array($image);


        if (tep_not_null($product_info['products_image'])) {
          $pi_query = tep_db_query("select image, htmlcontent from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int)$product_info['products_id'] . "' order by sort_order");
          if (tep_db_num_rows($pi_query) > 0) {
    ?>
    <div id="piGal" style="float: right;">
      <ul>
        <?php
            $pi_counter = 0;
            while ($pi = tep_db_fetch_array($pi_query)) {
              $pi_counter++;
    
              $pi_entry = '        <li><a href="';
    
              if (tep_not_null($pi['htmlcontent'])) {
                $pi_entry .= '#piGalimg_' . $pi_counter;
              } else {
                $pi_entry .= tep_href_link(DIR_WS_IMAGES . $pi['image']);
              }
    
              $pi_entry .= '" target="_blank" rel="fancybox">' . tep_image(DIR_WS_IMAGES . $pi['image']) . '</a>';
    
              if (tep_not_null($pi['htmlcontent'])) {
                $pi_entry .= '<div style="display: none;"><div id="piGalimg_' . $pi_counter . '">' . $pi['htmlcontent'] . '</div></div>';
              }
    
              $pi_entry .= '</li>';
    
              echo $pi_entry;
            }
        ?>
      </ul>
    </div>
    <script type="text/javascript">
$('#piGal ul').bxGallery({
  maxwidth: 300,
  maxheight: 200,
  thumbwidth: <?php echo (($pi_counter > 1) ? '75' : '0'); ?>,
  thumbcontainer: 300,
  load_image: 'ext/jquery/bxGallery/spinner.gif'
});
</script>
    <?php
      } else {
?>
    <style type="text/css">
    div.detail ul{
        margin: 5px 0;
        padding:0 10px;
    }
    div.detail ul li{
        list-style: none;
    }
</style>
    <?php echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_PRODUCT_INFO1, tep_get_all_get_params(array('action')) . 'action=add_product')); ?>
    <div id="piGal" style="width:470px;border:1px dotted #444;">
      <div style="background: url('images/m99.gif') -20px 0px ;height:13px;padding:10px"><strong>Product Name</strong></div>
      <div style="border-bottom: 1px dotted #999;padding-bottom:10px;"> <?php echo '<a href="' . tep_href_link(DIR_WS_IMAGES . $product_info['products_image']) . '" target="_blank" rel="fancybox" style="float:left;background:#353535;padding:20px 50px;margin:10px 20px;border:1px solid #666;">' . tep_image(DIR_WS_IMAGES . $product_info['products_image'], addslashes($product_info['products_name']), '100', '80', 'hspace="5" vspace="5"') . '</a>'; ?>
        <div class="detail" style="float: left; border-left:1px dotted #777;text-align:left;">
          <ul>
            <li>
              <h1 style="padding: 0;">
                <?=$product_info['products_name']?>
              </h1>
            </li>
            <br />
            <li>[
              <?=$product_info['products_model']?>
              ]</li>
            <br />
            <br />
            <li style="border-top: 1px dotted #444;padding:5px 0;margin:50px 0 0;">
              <div style="color: #DAA823; font-size: 14px; font-weight: bold;border-right:1px dotted #ccc; float:left; width:100px; text-align: center;margin:0 10px">
                <?=$currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']))?>
              </div>
              
              <?=tep_image_submit('m19.gif', IMAGE_BUTTON_IN_CART, "button")?>
              </li>
          </ul>
        </div>
        <br style="clear:both;"/>
      </div>
      <div class="detail" style="float: left;text-align:left;width:100%">
        <img src="images/wishlist_icon.png" height=32 border=0 class="add-product-wishlist" align="right" style="float:right;margin-right:10px;margin-top:10px"/>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".add-product-wishlist").click(function(){
                    var action = $("form[name='cart_quantity']").attr("action");
                    action = action.replace("add_product", "add_wishlist");
                    $("form[name='cart_quantity']").attr("action", action);                    
                    $("form[name='cart_quantity']").removeAttr("onsubmit");
                    $("form[name='cart_quantity']").submit();
                });    
            });
        </script>
        <ul style="float:left">
          <li>
            <h1 style="padding: 0;">Item description</h1>
          </li>
          <li>
            <?=trim($product_info['products_description'])?>
          </li>
        </ul>
      </div>
      <br style="clear:both;"/>
    </div>
    <?php
      }
?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="23%" height="13" valign="top"><table width="87%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="sideborders1">
                <table border="0" align="center" cellpadding="4" cellspacing="2" class="linkarea" style="width:500px;">
                  <?php
                //BOF - Zappo - Option Types v2 - ONE LINE - Initialize $number_of_uploads
                    $number_of_uploads = 0;
                    $products_attributes_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int)$HTTP_GET_VARS['products_id'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int)$languages_id . "'");
                    $products_attributes = tep_db_fetch_array($products_attributes_query);
                    if ($products_attributes['total'] > 0) {
                ?>
                  <tr>
                    <td colspan="2" align="left" class="heading"><?php echo TEXT_PRODUCT_OPTIONS; ?></td>
                  </tr>
                  <?php		
//BOF - Zappo - Option Types v2 - Add extra Option Values to Query && Placed Options in new file: option_types.php
      $products_options_name_query = tep_db_query("select distinct popt.products_options_id, popt.products_options_name, popt.products_options_type, popt.products_options_length, popt.products_options_comment from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int)$product_info['products_id'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int)$languages_id . "' order by popt.products_options_order, popt.products_options_name");
	  		$numberofopt = tep_db_num_rows($products_options_name_query);	  
		$opt_count = 0;	  
				$products_attributes = array();

      while ($products_options_name = tep_db_fetch_array($products_options_name_query)) {
		$opt_count++;		
		array_push($products_attributes,$products_options_name['products_options_id']);

		$comma = "";
			  $all_option_js = "[";
			  for($t = 1;$t<=$numberofopt; $t++)
			  {
			  	$all_option_js .= $comma.'document.getElementById(\'cmbooption_'.$t.'\').value';
			  	$comma = ",";				
			  }
			  $all_option_js .= "]";			  
        // - Zappo - Option Types v2 - Include option_types.php - Contains all Option Types, other than the original Drowpdown...
        include(DIR_WS_MODULES . 'option_types.php');

        if ($Default == true) {  // - Zappo - Option Types v2 - Default action is (standard) dropdown list. If something is not correctly set, we should always fall back to the standard.
        $products_options_array = array();
        $products_options_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov where pa.products_id = '" . (int)$HTTP_GET_VARS['products_id'] . "' and pa.options_id = '" . (int)$ProdOpt_ID . "' and pa.options_values_id = pov.products_options_values_id and pov.language_id = '" . (int)$languages_id . "' GROUP BY pov.products_options_values_id order by pov.products_options_values_id");
		
	   

        while ($products_options = tep_db_fetch_array($products_options_query)) {
          $products_options_array[] = array('id' => $products_options['products_options_values_id'], 'text' => $products_options['products_options_values_name']);
          if ($products_options['options_values_price'] != '0') {
            $products_options_array[sizeof($products_options_array)-1]['text'] .= ' (' . $products_options['price_prefix'] . $currencies->display_price($products_options['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) .') ';
          }
        }

        if (isset($cart->contents[$HTTP_GET_VARS['products_id']]['attributes'][$products_options_name['products_options_id']])) {
          $selected_attribute = $cart->contents[$HTTP_GET_VARS['products_id']]['attributes'][$products_options_name['products_options_id']];
        } else {
          $selected_attribute = false;
        }


?>
                  <tr>
                    <td  colspan="2" align="left" class="heading">
                    <table><tr><td width="50">
					<?php echo $ProdOpt_Name . ':'; ?></td><td><?php echo tep_draw_pull_down_menu('id[' . $ProdOpt_ID . ']', $products_options_array, $selected_attribute) . ' &nbsp; ' . $ProdOpt_Comment;  ?></td></tr></table></td>
                  </tr>
                  <?php
	  } // End if Default=true
  }
//EOF - Zappo - Option Types v2 - Add extra Option Values to Query && Placed Options in new file: option_types.php
?>
                  <?php
    }
?>
                  <tr>
                    <td align="left" id="display_price"></td>
                    <td  align="left" class="heading">
                      <input type="hidden" name="optionsid" id="optionsid" value="<?php echo implode(",",$products_attributes); ?>" />
                      <input type="hidden" name="products_id" value="<?=$HTTP_GET_VARS['products_id']?>"
                    </td>
                  </tr>
                </table></td>
              <!--  Low cost ERP -->
            </tr>
          </table>
          <br /></td>
        <td width="77%" align="left" valign="top" class="welcome_area"><?php
    if (tep_not_null($product_info['products_image'])) {
      $pi_query = tep_db_query("select image, htmlcontent from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int)$product_info['products_id'] . "' order by sort_order");

      if (tep_db_num_rows($pi_query) > 0) {
?>
          <div id="piGal" style="float: right;">
            <ul>
              <?php
        $pi_counter = 0;
        while ($pi = tep_db_fetch_array($pi_query)) {
          $pi_counter++;

          $pi_entry = '        <li><a href="';

          if (tep_not_null($pi['htmlcontent'])) {
            $pi_entry .= '#piGalimg_' . $pi_counter;
          } else {
            $pi_entry .= tep_href_link(DIR_WS_IMAGES . $pi['image']);
          }

          $pi_entry .= '" target="_blank" rel="fancybox">' . tep_image(DIR_WS_IMAGES . $pi['image']) . '</a>';

          if (tep_not_null($pi['htmlcontent'])) {
            $pi_entry .= '<div style="display: none;"><div id="piGalimg_' . $pi_counter . '">' . $pi['htmlcontent'] . '</div></div>';
          }

          $pi_entry .= '</li>';

          echo $pi_entry;
        }
?>
            </ul>
          </div>
          <script type="text/javascript">
$('#piGal ul').bxGallery({
  maxwidth: 300,
  maxheight: 200,
  thumbwidth: <?php echo (($pi_counter > 1) ? '75' : '0'); ?>,
  thumbcontainer: 300,
  load_image: 'ext/jquery/bxGallery/spinner.gif'
});
</script>
          <?php
      } else {
?>
          <?php
        }}
    ?>
          <script type="text/javascript">
$("#piGal a[rel^='fancybox']").fancybox({
  cyclic: true
});
</script>
          <?php
    }
?>
         
        </td>
      </tr>
    </table>
     </form>
    <?php
  }

  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>