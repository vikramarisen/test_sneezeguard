<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_EDIT);

  if (isset($HTTP_POST_VARS['action']) && ($HTTP_POST_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
   // if (ACCOUNT_GENDER == 'true') $gender = tep_db_prepare_input($HTTP_POST_VARS['gender']);
    $firstname = tep_db_prepare_input($HTTP_POST_VARS['firstname']);
    $lastname = tep_db_prepare_input($HTTP_POST_VARS['lastname']);
   // if (ACCOUNT_DOB == 'true') $dob = tep_db_prepare_input($HTTP_POST_VARS['dob']);
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);
    $telephone = tep_db_prepare_input($HTTP_POST_VARS['telephone']);
    $fax = tep_db_prepare_input($HTTP_POST_VARS['fax']);




$firstname2 = tep_db_prepare_input($HTTP_POST_VARS['firstname2']);
    $lastname2 = tep_db_prepare_input($HTTP_POST_VARS['lastname2']);
    //if (ACCOUNT_DOB == 'true') $dob = tep_db_prepare_input($HTTP_POST_VARS['dob']);
    if (ACCOUNT_COMPANY == 'true') $company = tep_db_prepare_input($HTTP_POST_VARS['company']);
    $street_address = tep_db_prepare_input($HTTP_POST_VARS['street_address']);
    if (ACCOUNT_SUBURB == 'true') $suburb = tep_db_prepare_input($HTTP_POST_VARS['suburb']);
    $postcode = tep_db_prepare_input($HTTP_POST_VARS['postcode']);
    $city = tep_db_prepare_input($HTTP_POST_VARS['city']);
    if (ACCOUNT_STATE == 'true') {
      $state = tep_db_prepare_input($HTTP_POST_VARS['state']);
      if (isset($HTTP_POST_VARS['zone_id'])) {
        $zone_id = tep_db_prepare_input($HTTP_POST_VARS['zone_id']);
      } else {
        $zone_id = false;
      }
    }
    $country = tep_db_prepare_input($HTTP_POST_VARS['country']);
    $addressiddd = tep_db_prepare_input($HTTP_POST_VARS['addressiddd']);







    $error = false;

    /*if (ACCOUNT_GENDER == 'true') {
      if ( ($gender != 'm') && ($gender != 'f') ) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_GENDER_ERROR);
      }
    }*/

    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_FIRST_NAME_ERROR);
    }

    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_LAST_NAME_ERROR);
    }

    /*if (ACCOUNT_DOB == 'true') {
      if ((is_numeric(tep_date_raw($dob)) == false) || (@checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4)) == false)) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_DATE_OF_BIRTH_ERROR);
      }
    }*/

    if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR);
    }

    if (!tep_validate_email($email_address)) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    }




    if (strlen($firstname2) < ENTRY_FIRST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_FIRST_NAME_ERROR);
    }

    if (strlen($lastname2) < ENTRY_LAST_NAME_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_LAST_NAME_ERROR);
    }

  


    if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_STREET_ADDRESS_ERROR);
    }

    if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_POST_CODE_ERROR);
    }

    if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_CITY_ERROR);
    }

    if (!is_numeric($country) != false) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_COUNTRY_ERROR);
    }

    if (ACCOUNT_STATE == 'true') {
      $zone_id = 0;
      $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
      $check = tep_db_fetch_array($check_query);
      $entry_state_has_zones = ($check['total'] > 0);
      if ($entry_state_has_zones == true) {
        $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and (zone_name = '" . tep_db_input($state) . "' or zone_code = '" . tep_db_input($state) . "')");
        if (tep_db_num_rows($zone_query) == 1) {
          $zone = tep_db_fetch_array($zone_query);
          $zone_id = $zone['zone_id'];
        } else {
          $error = true;

          $messageStack->add('account_edit', ENTRY_STATE_ERROR_SELECT);
        }
      } else {
        if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
          $error = true;

          $messageStack->add('account_edit', ENTRY_STATE_ERROR);
        }
      }
    }













    $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . encrypt_email(tep_db_input($email_address), ENCRYPTION_KEY_EMAIL) . "' and customers_id != '" . (int)$customer_id . "'");
    $check_email = tep_db_fetch_array($check_email_query);
    if ($check_email['total'] > 0) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
    }

    if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
      $error = true;

      $messageStack->add('account_edit', ENTRY_TELEPHONE_NUMBER_ERROR);
    }

    if ($error == false) {
      $sql_data_array = array('customers_firstname' => $firstname,
                              'customers_lastname' => $lastname,
                              'customers_email_address' => encrypt_email($email_address, ENCRYPTION_KEY_EMAIL),
                              'customers_telephone' => $telephone,
                              'customers_fax' => $fax);

      if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
      if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);

      tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

      tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

      $sql_data_array = array('entry_firstname' => $firstname,
                              'entry_lastname' => $lastname);

      tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$customer_default_address_id . "'");



/* update prmary billing address start */
	$servername = DB_SERVER;
	$username = DB_SERVER_USERNAME;
	$password = DB_SERVER_PASSWORD;
	$dbname = DB_DATABASE;
$conn=mysqli_connect($servername,$username,$password,$dbname) or die(mysqli_connect_error());
$update_primaryaddress=tep_db_query("UPDATE `address_book` SET `entry_company`='$company',`entry_firstname`='$firstname2',`entry_lastname`='$lastname2',`entry_street_address`='$street_address',`entry_suburb`='$suburb',`entry_postcode`='$postcode',`entry_city`='$city',`entry_state`='$state',`entry_country_id`='$country',`entry_zone_id`='$zone_id' WHERE `address_book_id`='$addressiddd' AND `customers_id`='$customer_id'");



/* update prmary billing address start end*/



// reset the session variables
      $customer_first_name = $firstname;

      $messageStack->add_session('account', SUCCESS_ACCOUNT_UPDATED, 'success');

      tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
    }
  }

  $account_query = tep_db_query("select customers_gender, customers_firstname, customers_lastname, customers_dob, customers_email_address, customers_telephone, customers_fax from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
  $account = tep_db_fetch_array($account_query);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL'));

  require(DIR_WS_INCLUDES . 'template_top.php');
  require('includes/form_check.js.php');
?>













<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>

/*
$(document).ready(function(){ 
	
		 $('#firstname').on('blur',function(){     
		 var myfirstname= $(this).val();
		 if(myfirstname.length==0){
		 $('#errormsgfirstname').html("<img src='img/iconCheckX.gif' /> Please Enter Your First Name");
		  }
		  else
		  {
		   $('#errormsgfirstname').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		 $('#lastname').on('blur',function(){     
		 var lastname= $(this).val();
		 if(lastname.length==0){
	     $('#errormsglastname').html("<img src='img/iconCheckX.gif' /> Please Enter Your Last Name");
		  }
		  else
		  {
		   $('#errormsglastname').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
	
	 
	 $('#email').on('blur',function(){     
		 var email= $(this).val();
		 if(email.length==0){
		 $('#errormsgemail').html("<img src='img/iconCheckX.gif' /> Please Enter Your Email Address");
		  }
		  else
		  {
		  validateEmail(email);
		   
		
		   }		  
	      });
		  
		  
		  
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        $('#errormsgemail').html("<img src='img/iconCheckOn.gif' />");
    }
    else {
         $('#errormsgemail').html("<img src='img/iconCheckX.gif' /> Please Enter Correct Email Address");
    }
}
		  
		  $('#email2').on('blur',function(){     
		 var email2= $(this).val();
		 if(email2.length==""){
		 $('#errormsgemail2').html("<img src='img/iconCheckX.gif' /> Please Enter Your Email Address");
		  }
		  else if( $("#email").val() != $("#email2").val())
		  {
		  $('#errormsgemail2').html("<img src='img/iconCheckX.gif' /> Please Match Your Confirm Email Address");
		  }
		  else
		  {
		   $('#errormsgemail2').html("<img src='img/iconCheckOn.gif' />");
		   
		
		   }		  
	      });
		  
		  
		  
		  
		  
		   $('#company').on('blur',function(){     
		 var company= $(this).val();
		 if(company.length==0){
		 $('#errormsgcompany').html("<img src='img/iconCheckX.gif' />  Company Name Optional");
		  }
		  else
		  {
		   $('#errormsgcompany').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		   
		   
		    $('#street_address').on('blur',function(){     
		 var street_address= $(this).val();
		 if(street_address.length==0){
		 $('#errormsgstreet').html("<img src='img/iconCheckX.gif' /> Please Enter Your Street Address");
		  }
		  else
		  {
		   $('#errormsgstreet').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		  
		  
		      $('#street_address222').on('blur',function(){     
		 var street_address= $(this).val();
		 if(street_address.length==0){
		 $('#errormsgstreet_address222').html("<img src='img/iconCheckX.gif' /> Please Enter Your Street Address");
		  }
		  else
		  {
		   $('#errormsgstreet_address222').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		  $('#postcode').on('blur',function(){     
		 var postcode= $(this).val();
		 if(postcode.length==0){
		 $('#errormsgpostcode').html("<img src='img/iconCheckX.gif' /> Please Enter Post Code");
		  }
		  else
		  {
		   $('#errormsgpostcode').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
	
	 $('#city').on('blur',function(){     
		 var city= $(this).val();
		 if(city.length==0){
		 $('#errormsgcity').html("<img src='img/iconCheckX.gif' /> Please Enter Your City Name");
		  }
		  else
		  {
		   $('#errormsgcity').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		   $('#state').on('blur',function(){     
		 var state= $(this).val();
		 if(state.length==0){
		 $('#errormsgstate').html("<img src='img/iconCheckX.gif' /> Please Enter Your State");
		  }
		  else
		  {
		   $('#errormsgstate').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		  $('#country').on('blur',function(){     
		 var country= $(this).val();
		 if(country.length==0){
		 $('#errormsgcountry').html("<img src='img/iconCheckX.gif' /> Please Enter Country Name");
		  }
		  else
		  {
		   $('#errormsgcountry').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });  
		  
		  
		   $('#firstname2').on('blur',function(){     
		 var country= $(this).val();
		 if(country.length==0){
		 $('#errormsgfirstname2').html("<img src='img/iconCheckX.gif' /> Please Enter First Name");
		  }
		  else
		  {
		   $('#errormsgfirstname2').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		   $('#lastname2').on('blur',function(){     
		 var country= $(this).val();
		 if(country.length==0){
		 $('#errormsglastname2').html("<img src='img/iconCheckX.gif' /> Please Enter Last Name");
		  }
		  else
		  {
		   $('#errormsglastname2').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		   $('#telephone2').on('blur',function(){     
		 var country= $(this).val();
		 if(isNaN(country)|| country.indexOf("-")!=-1){
              alert("Phone number error-  Please no symbols or spaces.");return false;
			  }
			  if (country.charAt(0)=="1"){
                alert("Phone number error- Please remove '1' from area code");
                return false
           }if (country.charAt(0)==" "){
                alert("Phone number error-  Please no symbols or spaces.");
                return false
           }if (country.charAt(0)==""){
                alert("Please enter a contact phone number");
                return false
           }
		 if(country.length<10  ){
		 $('#errormsgtelephone2').html("<img src='img/iconCheckX.gif' /> 10 digits max required");
		  }
		  else
		  {
		   $('#errormsgtelephone2').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		   $('#telephone').on('blur',function(){     
		 var country= $(this).val();
		if(isNaN(country)|| country.indexOf("-")!=-1){
              alert("Phone number error-  Please no symbols or spaces.");return false;
			  }
			  if (country.charAt(0)=="1"){
                alert("Phone number error- Please remove '1' from area code");
                return false
           }if (country.charAt(0)==" "){
                alert("Phone number error-  Please no symbols or spaces.");
                return false
           }if (country.charAt(0)==""){
                alert("Please enter a contact phone number");
                return false
           }
		 if(country.length<10 ){
		 $('#errormsgtelephone').html("<img src='img/iconCheckX.gif' /> 10 digits max required");
		  }
		  else
		  {
		   $('#errormsgtelephone').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		  $('#fax').on('blur',function(){     
		 var country= $(this).val();
		 if(country.length==0){
		 $('#errormsgfax').html("<img src='img/iconCheckX.gif' /> Please Enter Telephone No.");
		  }
		  else
		  {
		   $('#errormsgfax').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		  
	  $('#password').on('blur',function(){     
		 var password= $(this).val();
		 if(password.length==0){
		 $('#errormsgpassword').html("<img src='img/iconCheckX.gif' /> Please Enter Password");
		  }
		  else
		  {
		   $('#errormsgpassword').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		   $('#confirmation').on('blur',function(){     
		 var confirmation= $(this).val();
		 if(confirmation.length==0){
		 $('#errormsgconfirmation').html("<img src='img/iconCheckX.gif' /> Enter Confirmation Password");
		  }
		  else if( $("#password").val() != $("#confirmation").val())
		  {
		  $('#errormsgconfirmation').html("<img src='img/iconCheckX.gif' /> Confirm Password Not Matched");
		 // alert('Password & Confirm Password Not Matched');
		  
		  }
		  else
		  {
		   $('#errormsgconfirmation').html("<img src='img/iconCheckOn.gif' />");
		
		   }		  
	      });
		  
		  
		 
		  
		  
});
https://esneezeguard.com/test_server_sneezeguard/info.php?Model=EP11&mid=55&osCsid=gjfpen2up56js5o0rrnkeuiv50
https://esneezeguard.com/test_server_sneezeguard/info.php?Model=EP12&mid=56&osCsid=gjfpen2up56js5o0rrnkeuiv50
*/
</script>
<script>
window.onload = function() {
 var myInput = document.getElementById('email2');
 myInput.onpaste = function(e) {
   e.preventDefault();
 }
}
</script>
<script>
	$(document).ready(function(){
		if (document.all && !document.querySelector) {
   			$("#ex1").css('width',"100%");
   			$(".price_table").css("text-align","left");
		}
   	});

</script>



<?
  if (!$detect->isMobile())
{
?>
<style>
  
  .contactprimary tr{height:63px; text-align:center;}
  .contactprimary input[type=text] {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
    .contactprimary input[type=password] {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
    .contactprimary select {
	  width:300px;
	  height:31px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
  .button2 {
    border-radius: 10px;
    padding-top: 0px;
}


.button224 {
        background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 23px;
    cursor: pointer;
    width: 141px;
    height: 49px;
}
.inputRequirement {
    color: #FF0000;
    font-family: Verdana,Arial,sans-serif;
    font-size: 14px;
}

.contactprimary select {
    width: 300px;
    height: 31px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
}
  </style>
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;" class="contactprimary"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td   valign="top" >
<h2 style=" height:26px;"><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('account_edit') > 0) {
    echo $messageStack->output('account_edit');
  }
?>

<?php echo tep_draw_form('account_edit', tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL'), 'post', 'onsubmit="return check_form(account_edit);"', true) . tep_draw_hidden_field('action', 'process'); ?>

<div class="contentContainer">
  <div>
  <h2><?php echo MY_ACCOUNT_TITLE; ?></h2>
    <div class="inputRequirement" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></div>

    
  </div>

  <div class="contentText">
    <table border="0" cellspacing="2" cellpadding="2" width="100%">

<?php
  if (ACCOUNT_GENDER == 'true') {
    if (isset($gender)) {
      $male = ($gender == 'm') ? true : false;
    } else {
      $male = ($account['customers_gender'] == 'm') ? true : false;
    }
    $female = !$male;
?>

<?php
  }
?>

      <tr >
        <!--<td width="18%" class="fieldKey"><?php echo ENTRY_FIRST_NAME; ?></td>-->
        <td width="70%" align="right" class="fieldValue">
<?php echo tep_draw_input_field('firstname', $account['customers_firstname']) . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></td>
<td align="left" width="30%" ></td>
      </tr>
      <tr >
        <!--<td class="fieldKey"><?php echo ENTRY_LAST_NAME; ?></td>-->
       <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('lastname', $account['customers_lastname']) . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></td>
	   <td align="left" width="30%" ></td>
      </tr>

<?php
  if (ACCOUNT_DOB == 'true') {
?>

<?php
  }
?>

      <tr  > 
        <!--<td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('email_address', decrypt_email($account['customers_email_address'],ENCRYPTION_KEY_EMAIL)) . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>': ''); ?></td>
     <td align="left" width="30%" ></td>
	 </tr>
      <tr >
        <!--<td class="fieldKey"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('telephone', $account['customers_telephone']) . '&nbsp;' . (tep_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></td>
		<td align="left" width="30%" ></td>
      </tr>
      <tr >
        <!--<td class="fieldKey"><?php echo ENTRY_FAX_NUMBER; ?></td>-->
        <td width="70%" align="right" class="fieldValue">
		
			<?php echo tep_draw_input_field('fax','',  'placeholder="Fax" value="'.$account['customers_fax'].'" style="width:300px;height:31px;border: 2px solid #d1cbcb;border-radius: 8px;"', 'text') . '' . (tep_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?>
		&nbsp;&nbsp;
		<?php// echo tep_draw_input_field('fax','', 'id="fax" placeholder="Fax No" style="width:300px;height:31px;border: 2px solid #d1cbcb;border-radius: 8px;" ', $account['customers_fax']) . '&nbsp;' . (tep_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?></td>
		<td align="left" width="30%" ></td>
      </tr>
	  
	  
	  
	   
	  
	  
	  <?php
	  
	
	  $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' order by firstname, lastname");
  while ($addresses = tep_db_fetch_array($addresses_query)) {
    $format_id = tep_get_address_format_id($addresses['country_id']);
	
	if ($addresses['address_book_id'] == $customer_default_address_id)
		
		{
			
			
		 $street = tep_output_string_protected($addresses['street_address']);
    $suburb = tep_output_string_protected($addresses['suburb']);
    $city = tep_output_string_protected($addresses['city']);
    $state = tep_output_string_protected($addresses['state']);
    if (isset($addresses['country_id']) && tep_not_null($addresses['country_id'])) {
      $country = tep_get_country_name($addresses['country_id']);

      if (isset($addresses['zone_id']) && tep_not_null($addresses['zone_id'])) {
        $state = tep_get_zone_code($addresses['country_id'], $addresses['zone_id'], $state);
      }
    } elseif (isset($addresses['country']) && tep_not_null($addresses['country'])) {
      $country = tep_output_string_protected($addresses['country']['title']);
    } else {
      $country = '';
    }
    $postcode = tep_output_string_protected($addresses['postcode']);
    $zip = $postcode;	
			
			
	
		}
	
	
	//echo'<b style="color:red;"><pre>'; print_r($addresses); echo'</b><br>';
	
	$firstname2=$addresses['firstname'];
	$lastname2=$addresses['lastname'];
	$company=$addresses['company'];
	$street_address=$addresses['street_address'];
	$suburb=$addresses['suburb'];
	$city=$addresses['city'];
	$postcodesss=$addresses['postcode'];
	
	$country_idsss=$addresses['country_id'];
	$country=$country;
	$state=$state;
	$addressbookid=$addresses['address_book_id'];
	
	
  }
	?>
	  
	  <input type="hidden" name="addressiddd" value="<?php echo$addressbookid; ?>">
	  <tr>
	  <td width="70%" align="right" class="fieldValue">
	  <h3 style="color:black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  Primary Billing Address</h3>
	  </td>
		<td align="left" width="30%" ></td>
	  </tr>
	  
	  
	  
	   <tr>
       <!-- <td width="19%" class="fieldKey" align="right"><?php echo ENTRY_FIRST_NAME; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('firstname2','','id="firstname2" placeholder="First Name" value="'.$firstname2.'" size="20"'); ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" width="30%" ><span id="errormsgfirstname2"></span></td>
			
      </tr>
      <tr> 
        <!--<td width="19%"class="fieldKey" align="right"><?php echo ENTRY_LAST_NAME; ?></td>-->
        <td  width="70%" class="fieldValue" align="right"><?php echo tep_draw_input_field('lastname2','','id="lastname2"  placeholder="Last Name"  value="'.$lastname2.'"') 
		//. '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" width="30%"  ><span id="errormsglastname2"></span></td>
      </tr>
      <tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo "Business Name:"; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('company','','id="company" size="20" placeholder="Business Name" value="'.$company.'"' )
	//	. '&nbsp;' . (tep_not_null(ENTRY_COMPANY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COMPANY_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left"  width="30%" ><span id="errormsgcompany"></span></td>
	
      </tr> 
    
	  
	
	  <?php
$_SESSION['street']=$_POST['street_address'];
?>



<tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo ENTRY_STREET_ADDRESS; ?></td>-->
        <td width="70%" align="right"  width="30%" class="fieldValue" ><?php echo tep_draw_input_field('street_address','','id="street_address"  size="20" placeholder="Street Address" value="'.$street_address.'"') 
		//. '&nbsp;' . (tep_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" ><span id="errormsgstreet"></span></td>
      </tr>
	  

 	  <?php
  if (ACCOUNT_SUBURB == 'true') {
?>

      <tr>
        <!--<td width="19%" cellpadding="2" class="fieldKey" align="right" ><?php echo "Suite/Apt/Unit Number:"; ?></td>-->
        <td width="70%" align="right" class="fieldValue" ><?php echo tep_draw_input_field('suburb','',' placeholder="" id="street_address222" value="'.$suburb.'"') 
//	. '&nbsp;' . (tep_not_null(ENTRY_SUBURB_TEXT) ? '<span class="inputRequirement">' . ENTRY_SUBURB_TEXT . '</span>': '')
; ?> &nbsp;&nbsp;</td>
<td align="left" width="30%" ><span id="errormsgstreet_address222"></span></td>
      </tr>

<?php
  }
?>    
	  
	  <?php //echo'postcode== '.$postcode;
$_SESSION['postcode']=$_POST['postcode'];
?>
      <tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo ENTRY_CITY; ?></td>-->
        <td  width="70%" align="right" class="fieldValue" ><?php echo tep_draw_input_field('city','','id="city"  size="20" placeholder="City" value="'.$city.'"') 
		//. '&nbsp;' . (tep_not_null(ENTRY_CITY_TEXT) ? '<span class="inputRequirement">' . ENTRY_CITY_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" width="30%" ><span id="errormsgcity"></span></td>
      </tr>
 
	  <?php
$_SESSION['city']=$_POST['city'];
?>
<?php
  if (ACCOUNT_STATE == 'true') {
?>


      <tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo ENTRY_STATE; ?></td>-->
        <td width="70%" align="right" class="fieldValue" >
<?if ($process == true) {
      if ($entry_state_has_zones == true) {
        $zones_array = array();
        $zones_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' order by zone_name");
        while ($zones_values = tep_db_fetch_array($zones_query)) {
          $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
        }
        echo tep_draw_pull_down_menu('state', $zones_array);
      } else {
        echo tep_draw_input_field('state');
      }
    } else {
      echo '<select name="state" id="state" size="0">
<option value >State/Province</option>
<optgroup label="Canada"> 
	<option value="AB">Alberta</option>
	<option value="BC">British Columbia</option>
	<option value="MB">Manitoba</option>
	<option value="NB">New Brunswick</option>
	<option value="NL">Newfoundland and Labrador</option>
	<option value="NS">Nova Scotia</option>
	<option value="NT">Northwest Territories</option>
	<option value="NU">Nunavut</option>	
	<option value="ON">Ontario</option>
	<option value="PE">Prince Edward Island</option>
	<option value="QC">Quebec</option>
	<option value="SK">Saskatchewan</option>
	<option value="YT">Yukon</option>
</optgroup>
<optgroup label="Puerto Rico"> 
	<option value="SK">Puerto Rico</option>
</optgroup>
<optgroup label="United States"> 
		<option value="AL">Alabama</option>
		<option value="AK">Alaska</option>
		<option value="AZ">Arizona</option>
		<option value="AR">Arkansas</option>
		<option value="CA">California</option>
		<option value="CO">Colorado</option>
		<option value="CT">Connecticut</option>
		<option value="DE">Delaware</option>
		<option value="DC">District of Columbia</option>
		<option value="FL">Florida</option>
		<option value="GA">Georgia</option>
		<option value="HI">Hawaii</option>
		<option value="ID">Idaho</option>
		<option value="IL">Illinois</option>
		<option value="IN">Indiana</option>
		<option value="IA">Iowa</option>
		<option value="KS">Kansas</option>
		<option value="KY">Kentucky</option>
		<option value="LA">Louisiana</option>
		<option value="ME">Maine</option>
		<option value="MD">Maryland</option>
		<option value="MA">Massachusetts</option>
		<option value="MI">Michigan</option>
		<option value="MN">Minnesota</option>
		<option value="MS">Mississippi</option>
		<option value="MO">Missouri</option>
		<option value="MT">Montana</option>
		<option value="NE">Nebraska</option>
		<option value="NV">Nevada</option>
		<option value="NH">New Hampshire</option>
		<option value="NJ">New Jersey</option>
		<option value="NM">New Mexico</option>
		<option value="NY">New York</option>
		<option value="NC">North Carolina</option>
		<option value="ND">North Dakota</option>
		<option value="OH">Ohio</option>
		<option value="OK">Oklahoma</option>
		<option value="OR">Oregon</option>
		<option value="PA">Pennsylvania</option>
		<option value="RI">Rhode Island</option>
		<option value="SC">South Carolina</option>
		<option value="SD">South Dakota</option>
		<option value="TN">Tennessee</option>
		<option value="TX">Texas</option>
		<option value="UT">Utah</option>
		<option value="VT">Vermont</option>
		<option value="VA">Virginia</option>
		<option value="WA">Washington</option>
		<option value="WV">West Virginia</option>
		<option value="WI">Wisconsin</option>
		<option value="WY">Wyoming</option>
        </optgroup>
</select>';
    }
	?>
		
     &nbsp;&nbsp; </td>
		<td align="left" width="30%"><span align="left" id="errormsgstate"></span>
        </td>
      </tr>
	  

	  <?php
$_SESSION['state']=$_POST['state'];
?>
<?php
  }
  
  //echo$postcode;
?>

 <tr><?php //echo'postcode== '.$postcodesss;?>
       <!-- <td width="19%"class="fieldKey" align="right"><?php echo "Zip/ ".ENTRY_POST_CODE; ?></td>-->
        <td width="70%" align="right"class="fieldValue"><?php echo tep_draw_input_field('postcode','','id="postcode"  size="20" placeholder="Zip/Postal Code" value="'.$postcodesss.'"')
//		. '&nbsp;' . (tep_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="inputRequirement">' . ENTRY_POST_CODE_TEXT . '</span>': ''); 
?>&nbsp;&nbsp;&nbsp;</td>
<td align="left" width="30%"><span id="errormsgpostcode"></span></td>
      </tr>

      <tr>
        <!--<td width="19%"class="fieldKey" align="right"><?php echo ENTRY_COUNTRY; ?></td>-->
        <td  width="70%" align="right" class="fieldValue" >
		<select name="country" id="country" size="0" onchange="getPrice(this.form)">
		
		<option value="" >Please Select</option>
		
		
		<?php
		if($country_idsss=='38')
		{
		echo'<option value="38" selected>Canada</option>';	
		}
		else{
		echo'<option value="38">Canada</option>';	
		}
		
		
		if($country_idsss=='172')
		{
		echo'<option value="172" selected>Puerto Rico</option>';	
		}
		else{
		echo'<option value="172">Puerto Rico</option>';	
		}
		if($country_idsss=='223')
		{
		echo'<option value="223" selected>United States</option>';	
		}
		else{
		echo'<option value="223" >United States</option>';	
		}
		
		?>
		
		
		
		
			
		</select>
		
		
		
		<?php 
		//echo$country_idsss;
		
		//echo tep_get_country_list('country','','id="country"   STYLE="width: 150px" size="0" placeholder="Last Name" ') 
		//. '&nbsp;' . (tep_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COUNTRY_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;</td>
		
		<td align="left" width="30%"><span id="errormsgcountry"></span></td>
		
      </tr>
	    
      

 
	  

	  
	  
	  
	  
	  
	  
      <tr>
        <td><?php //echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">'.tep_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>';?>
		<?php echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">';?><button class="button224 button2">Back</button></a>
		</td>
		
		
	  
	  
    
		<td align="right">
		<?php //echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONTINUE, 'primary'); ?>
		<input type="image" class="updatebutton" src="img/new_icons/continue.png" style="width: 229px;height: 60px;" alt="Continue" title=" Continue " primary="" onclick="javascript:document.forms['checkout_payment'].submit();">
		</td>
      </tr>
    </table>

    <br />
  </div>
</div>





</form>
</td></tr></table></td></tr></table>


<?
}

else{
?>
<td id="ex1" align=center width="190" valign="top">
<style>
  .form_white h2 {
    color: black;
    font-size: 27px;
}
  .contactprimary tr{height:105px; text-align:center;}
  
  .contactprimary tr span{font-size:26px;}
  .contactprimary tr img{width: 65px;}
  
  .contactprimary input[type=text] {
	      width: 489px;
    height: 69px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
    font-size: 27px;
  }
  
    .contactprimary input[type=password] {
	      width: 489px;
    height: 69px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
    font-size: 27px;
  }
  
    .contactprimary select {
	  width: 481px;
    height: 75px;
    font-size: 28px;
	  border: 2px solid #d1cbcb;
    border-radius: 8px;
  }
  
    .button2 {
    border-radius: 10px;
    padding-top: 0px;
}

.button224 {
    background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 35px;
    cursor: pointer;
    width: 158px;
    height: 66px;
}
.inputRequirement {
    color: #FF0000;
    font-family: Verdana,Arial,sans-serif;
    font-size: 20px;
}
  </style>
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;" class="contactprimary"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td   valign="top" >
<h2 style=" height:26px;"><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('account_edit') > 0) {
    echo $messageStack->output('account_edit');
  }
?>

<?php echo tep_draw_form('account_edit', tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL'), 'post', 'onsubmit="return check_form(account_edit);"', true) . tep_draw_hidden_field('action', 'process'); ?>

<div class="contentContainer">
  <div>
  <h2><?php echo MY_ACCOUNT_TITLE; ?></h2>
    <div class="inputRequirement" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></div>

    
  </div>

  <div class="contentText">
    <table border="0" cellspacing="2" cellpadding="2" width="100%" align="center">

<?php
  if (ACCOUNT_GENDER == 'true') {
    if (isset($gender)) {
      $male = ($gender == 'm') ? true : false;
    } else {
      $male = ($account['customers_gender'] == 'm') ? true : false;
    }
    $female = !$male;
?>

<?php
  }
?>

      
      <tr >
        <!--<td width="18%" class="fieldKey"><?php echo ENTRY_FIRST_NAME; ?></td>-->
        <td width="70%" align="right" class="fieldValue">
<?php echo tep_draw_input_field('firstname', $account['customers_firstname']) . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></td>
<td align="left" width="30%" ></td>
      </tr>
      <tr >
        <!--<td class="fieldKey"><?php echo ENTRY_LAST_NAME; ?></td>-->
       <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('lastname', $account['customers_lastname']) . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></td>
	   <td align="left" width="30%" ></td>
      </tr>

<?php
  if (ACCOUNT_DOB == 'true') {
?>

<?php
  }
?>

      <tr  > 
        <!--<td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('email_address', decrypt_email($account['customers_email_address'],ENCRYPTION_KEY_EMAIL)) . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>': ''); ?></td>
     <td align="left" width="30%" ></td>
	 </tr>
      <tr >
        <!--<td class="fieldKey"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('telephone', $account['customers_telephone']) . '&nbsp;' . (tep_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></td>
		<td align="left" width="30%" ></td>
      </tr>
      <tr >
        <!--<td class="fieldKey"><?php echo ENTRY_FAX_NUMBER; ?></td>-->
        <td width="70%" align="right" class="fieldValue">
		
			<?php echo tep_draw_input_field('fax','',  'placeholder="Fax" value="'.$account['customers_fax'].'" ', 'text') . '' . (tep_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?>
		&nbsp;&nbsp;
		<?php// echo tep_draw_input_field('fax','', 'id="fax" placeholder="Fax No" style="width:300px;height:31px;border: 2px solid #d1cbcb;border-radius: 8px;" ', $account['customers_fax']) . '&nbsp;' . (tep_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?></td>
		<td align="left" width="30%" ></td>
      </tr>
	  
	  
	  
	  
	   
	  
	  
	  <?php
	  
	
	  $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' order by firstname, lastname");
  while ($addresses = tep_db_fetch_array($addresses_query)) {
    $format_id = tep_get_address_format_id($addresses['country_id']);
	
	if ($addresses['address_book_id'] == $customer_default_address_id)
		
		{
			
			
		 $street = tep_output_string_protected($addresses['street_address']);
    $suburb = tep_output_string_protected($addresses['suburb']);
    $city = tep_output_string_protected($addresses['city']);
    $state = tep_output_string_protected($addresses['state']);
    if (isset($addresses['country_id']) && tep_not_null($addresses['country_id'])) {
      $country = tep_get_country_name($addresses['country_id']);

      if (isset($addresses['zone_id']) && tep_not_null($addresses['zone_id'])) {
        $state = tep_get_zone_code($addresses['country_id'], $addresses['zone_id'], $state);
      }
    } elseif (isset($addresses['country']) && tep_not_null($addresses['country'])) {
      $country = tep_output_string_protected($addresses['country']['title']);
    } else {
      $country = '';
    }
    $postcode = tep_output_string_protected($addresses['postcode']);
    $zip = $postcode;	
			
			
	
		}
	
	
	//echo'<b style="color:red;"><pre>'; print_r($addresses); echo'</b><br>';
	
	$firstname2=$addresses['firstname'];
	$lastname2=$addresses['lastname'];
	$company=$addresses['company'];
	$street_address=$addresses['street_address'];
	$suburb=$addresses['suburb'];
	$city=$addresses['city'];
	$postcodesss=$addresses['postcode'];
	
	$country_idsss=$addresses['country_id'];
	$country=$country;
	$state=$state;
	$addressbookid=$addresses['address_book_id'];
	
	
  }
	?>
	  
	  <input type="hidden" name="addressiddd" value="<?php echo$addressbookid; ?>">
	  <tr>
	  <td width="70%" align="right" class="fieldValue">
	  <h3 style="color:black;font-size:27px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  Primary Billing Address</h3>
	  </td>
		<td align="left" width="30%" ></td>
	  </tr>
	  
	  
	  
	   <tr>
       <!-- <td width="19%" class="fieldKey" align="right"><?php echo ENTRY_FIRST_NAME; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('firstname2','','id="firstname2" placeholder="First Name" value="'.$firstname2.'" size="20"'); ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" width="30%" ><span id="errormsgfirstname2"></span></td>
			
      </tr>
      <tr> 
        <!--<td width="19%"class="fieldKey" align="right"><?php echo ENTRY_LAST_NAME; ?></td>-->
        <td  width="70%" class="fieldValue" align="right"><?php echo tep_draw_input_field('lastname2','','id="lastname2"  placeholder="Last Name"  value="'.$lastname2.'"') 
		//. '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" width="30%"  ><span id="errormsglastname2"></span></td>
      </tr>
      <tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo "Business Name:"; ?></td>-->
        <td width="70%" align="right" class="fieldValue"><?php echo tep_draw_input_field('company','','id="company" size="20" placeholder="Business Name" value="'.$company.'"' )
	//	. '&nbsp;' . (tep_not_null(ENTRY_COMPANY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COMPANY_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left"  width="30%" ><span id="errormsgcompany"></span></td>
	
      </tr> 
    
	  
	
	  <?php
$_SESSION['street']=$_POST['street_address'];
?>



<tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo ENTRY_STREET_ADDRESS; ?></td>-->
        <td width="70%" align="right"  width="30%" class="fieldValue" ><?php echo tep_draw_input_field('street_address','','id="street_address"  size="20" placeholder="Street Address" value="'.$street_address.'"') 
		//. '&nbsp;' . (tep_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" ><span id="errormsgstreet"></span></td>
      </tr>
	  

 	  <?php
  if (ACCOUNT_SUBURB == 'true') {
?>

      <tr>
        <!--<td width="19%" cellpadding="2" class="fieldKey" align="right" ><?php echo "Suite/Apt/Unit Number:"; ?></td>-->
        <td width="70%" align="right" class="fieldValue" ><?php echo tep_draw_input_field('suburb','',' placeholder="" id="street_address222" value="'.$suburb.'"') 
//	. '&nbsp;' . (tep_not_null(ENTRY_SUBURB_TEXT) ? '<span class="inputRequirement">' . ENTRY_SUBURB_TEXT . '</span>': '')
; ?> &nbsp;&nbsp;</td>
<td align="left" width="30%" ><span id="errormsgstreet_address222"></span></td>
      </tr>

<?php
  }
?>    
	  
	  <?php //echo'postcode== '.$postcode;
$_SESSION['postcode']=$_POST['postcode'];
?>
      <tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo ENTRY_CITY; ?></td>-->
        <td  width="70%" align="right" class="fieldValue" ><?php echo tep_draw_input_field('city','','id="city"  size="20" placeholder="City" value="'.$city.'"') 
		//. '&nbsp;' . (tep_not_null(ENTRY_CITY_TEXT) ? '<span class="inputRequirement">' . ENTRY_CITY_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;&nbsp;</td>
		<td align="left" width="30%" ><span id="errormsgcity"></span></td>
      </tr>
 
	  <?php
$_SESSION['city']=$_POST['city'];
?>
<?php
  if (ACCOUNT_STATE == 'true') {
?>


      <tr>
        <!--<td width="19%" class="fieldKey" align="right"><?php echo ENTRY_STATE; ?></td>-->
        <td width="70%" align="right" class="fieldValue" >
<?if ($process == true) {
      if ($entry_state_has_zones == true) {
        $zones_array = array();
        $zones_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' order by zone_name");
        while ($zones_values = tep_db_fetch_array($zones_query)) {
          $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
        }
        echo tep_draw_pull_down_menu('state', $zones_array);
      } else {
        echo tep_draw_input_field('state');
      }
    } else {
      echo '<select name="state" id="state" size="0">
<option value >State/Province</option>
<optgroup label="Canada"> 
	<option value="AB">Alberta</option>
	<option value="BC">British Columbia</option>
	<option value="MB">Manitoba</option>
	<option value="NB">New Brunswick</option>
	<option value="NL">Newfoundland and Labrador</option>
	<option value="NS">Nova Scotia</option>
	<option value="NT">Northwest Territories</option>
	<option value="NU">Nunavut</option>	
	<option value="ON">Ontario</option>
	<option value="PE">Prince Edward Island</option>
	<option value="QC">Quebec</option>
	<option value="SK">Saskatchewan</option>
	<option value="YT">Yukon</option>
</optgroup>
<optgroup label="Puerto Rico"> 
	<option value="SK">Puerto Rico</option>
</optgroup>
<optgroup label="United States"> 
		<option value="AL">Alabama</option>
		<option value="AK">Alaska</option>
		<option value="AZ">Arizona</option>
		<option value="AR">Arkansas</option>
		<option value="CA">California</option>
		<option value="CO">Colorado</option>
		<option value="CT">Connecticut</option>
		<option value="DE">Delaware</option>
		<option value="DC">District of Columbia</option>
		<option value="FL">Florida</option>
		<option value="GA">Georgia</option>
		<option value="HI">Hawaii</option>
		<option value="ID">Idaho</option>
		<option value="IL">Illinois</option>
		<option value="IN">Indiana</option>
		<option value="IA">Iowa</option>
		<option value="KS">Kansas</option>
		<option value="KY">Kentucky</option>
		<option value="LA">Louisiana</option>
		<option value="ME">Maine</option>
		<option value="MD">Maryland</option>
		<option value="MA">Massachusetts</option>
		<option value="MI">Michigan</option>
		<option value="MN">Minnesota</option>
		<option value="MS">Mississippi</option>
		<option value="MO">Missouri</option>
		<option value="MT">Montana</option>
		<option value="NE">Nebraska</option>
		<option value="NV">Nevada</option>
		<option value="NH">New Hampshire</option>
		<option value="NJ">New Jersey</option>
		<option value="NM">New Mexico</option>
		<option value="NY">New York</option>
		<option value="NC">North Carolina</option>
		<option value="ND">North Dakota</option>
		<option value="OH">Ohio</option>
		<option value="OK">Oklahoma</option>
		<option value="OR">Oregon</option>
		<option value="PA">Pennsylvania</option>
		<option value="RI">Rhode Island</option>
		<option value="SC">South Carolina</option>
		<option value="SD">South Dakota</option>
		<option value="TN">Tennessee</option>
		<option value="TX">Texas</option>
		<option value="UT">Utah</option>
		<option value="VT">Vermont</option>
		<option value="VA">Virginia</option>
		<option value="WA">Washington</option>
		<option value="WV">West Virginia</option>
		<option value="WI">Wisconsin</option>
		<option value="WY">Wyoming</option>
        </optgroup>
</select>';
    }
	?>
		
     &nbsp;&nbsp; </td>
		<td align="left" width="30%"><span align="left" id="errormsgstate"></span>
        </td>
      </tr>
	  

	  <?php
$_SESSION['state']=$_POST['state'];
?>
<?php
  }
  
  //echo$postcode;
?>

 <tr><?php //echo'postcode== '.$postcodesss;?>
       <!-- <td width="19%"class="fieldKey" align="right"><?php echo "Zip/ ".ENTRY_POST_CODE; ?></td>-->
        <td width="70%" align="right"class="fieldValue"><?php echo tep_draw_input_field('postcode','','id="postcode"  size="20" placeholder="Zip/Postal Code" value="'.$postcodesss.'"')
//		. '&nbsp;' . (tep_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="inputRequirement">' . ENTRY_POST_CODE_TEXT . '</span>': ''); 
?>&nbsp;&nbsp;&nbsp;</td>
<td align="left" width="30%"><span id="errormsgpostcode"></span></td>
      </tr>

      <tr>
        <!--<td width="19%"class="fieldKey" align="right"><?php echo ENTRY_COUNTRY; ?></td>-->
        <td  width="70%" align="right" class="fieldValue" >
		<select name="country" id="country" size="0" onchange="getPrice(this.form)">
		
		<option value="" >Please Select</option>
		
		
		<?php
		if($country_idsss=='38')
		{
		echo'<option value="38" selected>Canada</option>';	
		}
		else{
		echo'<option value="38">Canada</option>';	
		}
		
		
		if($country_idsss=='172')
		{
		echo'<option value="172" selected>Puerto Rico</option>';	
		}
		else{
		echo'<option value="172">Puerto Rico</option>';	
		}
		if($country_idsss=='223')
		{
		echo'<option value="223" selected>United States</option>';	
		}
		else{
		echo'<option value="223" >United States</option>';	
		}
		
		?>
		
		
		
		
			
		</select>
		
		
		
		<?php 
		//echo$country_idsss;
		
		//echo tep_get_country_list('country','','id="country"   STYLE="width: 150px" size="0" placeholder="Last Name" ') 
		//. '&nbsp;' . (tep_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COUNTRY_TEXT . '</span>': '')
		; ?>&nbsp;&nbsp;</td>
		
		<td align="left" width="30%"><span id="errormsgcountry"></span></td>
		
      </tr>
	    
      

 
	  
	  
	  
	  
	  
	  
      <tr>
        <td><?php //echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">'.tep_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>';?>
		<?php echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">';?><button class="button224 button2">Back</button></a>
		</td>
		
		
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  

    
		<td align="right">
		<?php //echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONTINUE, 'primary'); ?>
		<input type="image" class="updatebutton" src="img/new_icons/continue.png" style="width: 301px;height: 84px;" alt="Continue" title=" Continue " primary="" onclick="javascript:document.forms['checkout_payment'].submit();">
		</td>
      </tr>
    </table>

    <br />
  </div>
</div>

</form>
</td></tr></table></td></tr></table>

<?
}
?>



<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
