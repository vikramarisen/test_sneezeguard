-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 21, 2020 at 08:00 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `esneezeg_new_sneezeguard`
--

-- --------------------------------------------------------

--
-- Table structure for table `homepage_blog`
--

CREATE TABLE IF NOT EXISTS `homepage_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `homepage_blog`
--

INSERT INTO `homepage_blog` (`id`, `model_name`, `content`, `image`, `url`, `time`) VALUES
(2, 'ES47', 'Take on the safety measures by installing ES47 sneeze guard and reducing the risk of inadvertently infecting others. ', 'es47.jpg', 'ES47', '2020-10-16 11:26:52'),
(3, 'ES90', 'Take advantage of ES90 using custom dimensions and specifications with the best material quality of our sneeze guard.', 'es90.jpg', 'ES90', '2020-10-16 11:27:01'),
(4, 'ES92', 'Search is over now. We have ES92, a model sneeze guard that naturally offers a perfect dimension as you could ever need. ', 'es92.jpg', 'ES92', '2020-10-16 11:27:09'),
(6, 'ES53', 'Create visual appeal with our sneeze guard as ES53 becomes an effective barrier against transmission while remaining open', 'es53.jpg', 'ES53', '2020-10-16 11:27:17'),
(7, 'ES47123', 'Take on the safety measures by installing ES47 sneeze guard and reducing the risk of inadvertently infecting others.	', 'es53.jpg', '', '2020-10-15 12:59:12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
