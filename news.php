<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_NEWS);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_NEWS));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<?php

function getNewsList(){
   $fileList = array();
	
	if ($handle = opendir('news/'.$_GET["year"])) {
		
		while ($file = readdir($handle))  {
		    if (!is_dir($file)) {
		       $fileList[] = $file;
      	}
		}
	}	
	
	rsort($fileList);
	
	return $fileList;
}

?>
<link href="style/style.css" rel="stylesheet" type="text/css" />
<h1><?php echo HEADING_TITLE; ?></h1>

<div class="contentContainer">
  <div class="contentText">
    <?php echo TEXT_INFORMATION; ?>
    <a href="news.php?year=2016">2016</a> 
 <a href="news.php?year=2015">2015</a>
  <div id="main">
    <div id="caption"></div>
    <table width="100%">
    <?php
      $list = getNewsList();
      foreach ($list as $value) {
      	$newsData = file('news/'.$_GET["year"].'/'.$value);
      	$newsTitle  = $newsData[0];
         $submitDate = $newsData[1];	
         unset ($newsData['0']);
         unset ($newsData['1']);
      	
         $newsContent = "";
         foreach ($newsData as $value) {
    	       $newsContent .= $value;
         }
      	
      	echo "<tr><th align='left'>$newsTitle</th><th align='right'>$submitDate</th></tr>";
      	echo "<tr><td colspan='2'>".$newsContent."<br/><hr size='1'/></td></tr>";
      }
    ?>
    </table>
  </div>
</div>
  <div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?></span>
  </div>
</div>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
