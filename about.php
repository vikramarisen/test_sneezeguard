<?php
    require('includes/application_top.php');
    include('includes/header_new_design.php');
    $toshow=$_GET['class'];
?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        let toShow = "<?=$toshow;?>";
        $('.'+toShow).css('display','block');
    })
</script>
<style>
    .about-us{
        margin-top: 11% !important;
        /*42% !important*/
    }
    .tohide{
        display: none;
    }
    .about-content{
        width: 53%;
        font-size: 15px;
        position: absolute;
        top: 46%;
        left: 5%;
        color: white;
    }
    .main{
        margin-top: 7.7%;
    }
    .list{
        list-style-type: square;
    }
    @media only screen and (max-width: 600px) {
        .about-content{
            display: none !important;
        }
        .main{
            margin-top: 20.7%;
        }
    }
</style>
<div class="main">
    <div class="about tohide">
        <img src="img/about-us.jpg" alt="sneezguard" style="width: 100%;">
        <div class="about-content">
            <p>
                Founded in Antioch, California, ADM Sneezeguards is a business-to-business (B2B) sales and a leading Manufacturing Company which has amazing Glass Models and Designs for sneeze guards and sanitary barriers. The Company was established in 1988 and now is dealing with thousands of customers with their different requirements of standard and custom glass products. 
            </p>
        </div>
        <div class="container-fluid mt-3">
            <h4>ABOUT</h4>
            <p>Want a transparent Glass Sneeze Guard and Customized Portable Barrier?</p>
            <p>Is your business ready to re-open? 
            Just go on and take advantage of our customized Restaurant Sneeze Guard and Portable Screen.
            </p>
            <p>
                Founded in Antioch, California, ADM Sneezeguards is a business-to-business (B2B) sales and a leading Manufacturing Company which has amazing Glass Models and Designs for sneeze guards and sanitary barriers. The Company was established in 1988 and now is dealing with thousands of customers with their different requirements of standard and custom glass products. 
            </p>
            <p>We recommend picking from a range of glass products and take advantage of various styles, shapes and designs. In order to face this pandemic, you have the option to buy cost-effective online barriers and shields for industrial, architectural, glazing, automotive and construction industries. ADM Sneezeguards has recently added a new range of barriers, dividers and protectors which can be easily customized and fit your space perfectly. The Company is ready with extra capacity in the glass production side with the addition of 8 new CNC machines during the outbreak. Also, we have added 10000' sq.ft. of floor space just to introduce more offerings such as arched glass. Working as per customer’s expectations, ADM Sneezeguards has now become the only manufacturer in the industry with its own glass tempering mill and production house under one roof. We invite you to review our latest sneeze guard catalog of our projects as well as the innovative designs and patented models of various glass shields and barriers.</p>
            <h4>Connect with ADM Sneezeguards</h4>
            <p>To keep updated with newly launched glass products, contact us now and start your journey with us. We are eagerly waiting to assist you to fight against germs and viruses. Give us a call to discuss your requirements and take your space to the next level. </p>
        </div>
    </div>
    <div class="stories tohide">
        <img src="img/insideadm/back2.jpg" alt="sneezguard" style="width: 100%;">
        <div class="about-content">
            <p>
            Within the last few years, ADM Sneezeguards has become one of the largest and most respected sneeze guard and portable screen manufacturers in the corporate and food service industry. The Company was founded in Antioch, California in 1988. With years of experience and agile hard work, it is now handling thousands of customers having different needs for their workplaces ..
            </p>
        </div>
        <div class="container-fluid mt-3">
        <h4 id="STORY">OUR STORY</h4>
        <p>Within the last few years, ADM Sneezeguards has become one of the largest and most respected sneeze guard and portable screen manufacturers in the corporate and food service industry. The Company was founded in Antioch, California in 1988. With years of experience and agile hard work, it is now handling thousands of customers having different needs for their workplaces and residential areas as well. Our main motive is to help maintain social distance and physical separation with our effective portable sneezeguards. ADM Sneezeguards have a range of glass barriers and shields that work best and serve as visual reminders to use proper hygiene to prevent the spread of germs and viruses. Determined to bring a higher level of professionalism to the industry, our Company has developed a simple philosophy of delighting the customers by learning their needs and offering a solution that exceeds expectations.</p>
        <p>We manufacture in the United States with quick turn-around time for small and large quantities. Options include food guard, nsf sneeze guard, breath guard, portable sneeze guard and many more. Let us manufacture your custom glass sneeze guard in nearly any size or shape with any options you may need. We suggest to always check our website <a href="https://www.sneezeguard.com/">sneezeguard.com</a> so that we can better serve you.</p>
        </div>
    </div>
    <div class="innovation tohide">
    <img src="img/insideadm/back1.jpg" alt="sneezguard" style="width: 100%;">
        <div class="about-content">
            <p>
            ADM Sneezeguards offers a range of glass guards and protective barriers. Shop our selection of custom sizes or custom designs with various options. We work on the belief that if you are satisfied, our mission is completed. In order to innovate or launch new products .
            </p>
        </div>
        <div class="container-fluid mt-3">
        <h4 id="INNOVATION">INNOVATION</h4>
        <p><i class="fas fa-chevron-circle-right"></i>ADM Sneezeguards offers a range of glass guards and protective barriers. Shop our selection of custom sizes or custom designs with various options. We work on the belief that if you are satisfied, our mission is completed. In order to innovate or launch new products, you always inspire us to:</p>
        <ul class="list">
            <li>Put your interest ahead of our own.</li>
            <li>Delivery of more value than expected. </li>
            <li>Ensuring the highest standards, every single time.</li>
            <li>Maintaining long lasting relationships with our clients.</li>
        </ul>
        <p>The Company provides every suitable option for safety, renovation and replacement for your commercial and residential places. Your motivation inspires us for innovating new safety products on a regular basis. Give us a chance to serve you and know about our  responsiveness and overall first class service. We assure that our tireless working strategy  brings the best experience for your place. </p>
        <p>Ready to get started?</p>
        <p>Have a free quote today!!</p>
        </div>
    </div>
    <div class="madeinusa tohide">
        <img src="img/insideadm/back3.jpg" alt="sneezguard" style="width: 100%;">
        <div class="about-content">
            <p>
            Known for the perfect finishing touch, ADM Sneezeguards manufactures cheap sneeze guard and food guard in Antioch, California, USA. The Company makes every possible solution to surpass the expectations of the customers. It is ready with extra capacity in the glass production side with the addition of 8 new CNC machines during the outbreak.
            </p>
        </div>
        <div class="container-fluid mt-3">
        <h4>MADE IN THE USA</h4>
        <p>Known for the perfect finishing touch, ADM Sneezeguards manufactures cheap sneeze guard and food guard in Antioch, California, USA. The Company makes every possible solution to surpass the expectations of the customers. It is ready with extra capacity in the glass production side with the addition of 8 new CNC machines during the outbreak. For a large production of glass shields, the Company has added 10000' sq.ft. of floor space just to introduce more offerings such as arched glass. In order to be ahead in the industry , ADM Sneezeguards have opted for an extra production that delivers the products in a timely manner and on a wide scale of manufacture. </p>
        </div>
    </div>
    <div class="sustainability tohide">
        <img src="img/insideadm/back4.jpg" alt="sneezguard" style="width: 100%;">
        <div class="about-content">
            <p>
            ADM Sneezeguards is fully dedicated to environmentally friendly manufacturing and business processes that reduce any ill effect on the surroundings. The Company makes every successful attempt to build sustainable, long-lasting and quality products using only eco-friendly materials. Our engineers opt for the techniques which maximize material 
            </p>
        </div>
        <div class="container-fluid mt-3">
        <h4>SUSTAINABILITY</h4>
        <p>ADM Sneezeguards is fully dedicated to environmentally friendly manufacturing and business processes that reduce any ill effect on the surroundings. The Company makes every successful attempt to build sustainable, long-lasting and quality products using only eco-friendly materials. Our engineers opt for the techniques which maximize material usage and reduce scrap and waste.</p>
        </div>
    </div>
    <div class="vision tohide">
        <img src="img/insideadm/back5.jpg" alt="sneezguard" style="width: 100%;">
        <div class="about-content">
            <p>
            We believe that every opportunity becomes successful only if it is cherished with perfect strategy and effective solutions. To serve our customers, we deliver the best acrylic sneeze guard and glass guard which are affordable, transparent, durable and high-quality. Using highly innovative glass shields and protective barriers, ADM Sneezeguards
            </p>
        </div>
        <div class="container-fluid mt-3">
        <h4 id="VISION">VISION </h4>
        <p>We believe that every opportunity becomes successful only if it is cherished with perfect strategy and effective solutions. To serve our customers, we deliver the best acrylic sneeze guard and glass guard which are affordable, transparent, durable and high-quality. Using highly innovative glass shields and protective barriers, ADM Sneezeguards has become a prime Manufacturing Company in the global marketplace. With years of experience in this field, the Company is fully dedicated for maximum coverage and offering low-maintenance solutions. Our vision is to see you satisfied with our services.</p>
        </div>
    </div>
    <div class="mission tohide">
    <img src="img/insideadm/back6.jpg" alt="sneezguard" style="width: 100%;">
        <div class="about-content">
            <p>
            Our Mission is to develop the solutions that will assist our customers in the future. Also, we take the responsibility to lead the industry with our spirit of inspiration and innovation.
            </p>
        </div>
        <div class="container-fluid mt-3">
        <h4 id="MISSION">MISSION</h4>
        <p>To serve our customers, we do our best to deliver affordable safety solutions to our customers. We promise to be there, holding your hands in every tough situation. ADM Sneezeguards is consistently working on our manufacturing process to offer the product which meets your needs of business and enterprise. Also, our solutions depend on the innovative strategies, business-centered techniques and transparency. We are fully focused on long-standing relationships in providing quality glass sneeze guard kits for your business.</p>
        <p>Our Mission is to develop the solutions that will assist our customers in the future. Also, we take the responsibility to lead the industry with our spirit of inspiration and innovation.  To discuss how to get custom sneeze guard and barrier products for your workplace, reach out to our knowledgeable team experts today. We would be delighted to speak about how we could help your firm. </p>
        <p>Pleased to see you at our website and looking forward to our continued relationship!!</p>
        </div>
    </div>
</div>
<?php
include('includes/footer_new_design.php');
?>