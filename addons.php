<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ob_start();

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_DEFAULT));


/*
  require(DIR_WS_INCLUDES . 'template_top.php');


    $buffer=ob_get_contents();
    ob_end_clean();


$titlessss = 'ADM Sneezeguards - Sneeze Guard Manufacturer | Kitchen Equipment';
$buffer = preg_replace('/(<title>)(.*?)(<\/title>)/i', '$1' . $titlessss . '$3', $buffer);
$keyword = 'Buffet Sneeze Guards, Commercial Kitchen Equipment online, online restaurant supply store, Shop sneeze guard, Manufacturer Sneeze Guard';
  //add anew desc and author
$buffer = str_replace('name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment"', 'name="keywords" content="'.$keyword.'"', $buffer);

echo $buffer;
*/







  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">


<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard | Food Protection | ADDONS - ADM Sneezeguards</title>
<!-- End Google Add Conversion -->





<link rel="preconnect" href="https://www.sneezeguard.com">
<link rel="dns-prefetch" href="https://www.sneezeguard.com">


<link rel="preload" as="script" href="jquery-latest.js">
<link rel="preload" as="script" href="thickbox.js">
<link rel="preload" as="script" href="ext/jquery/jquery-1.4.2.min.js">
<link rel="preload" as="script" href="ext/jquery/ui/jquery-ui-1.8.6.min.js">
<link rel="preload" as="script" href="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js">
<link rel="preload" as="script" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js">
<link rel="preload" as="script" href="jquery.colorbox3.js">
<link rel="preload" as="script" href="./dist/html2canvas.js">
<link rel="preload" as="script" href="./dist/canvas2image.js">
<link rel="preload" as="script" href="https://platform.twitter.com/widgets.js">
<link rel="preload" as="script" href="jquery.confirm/jquery.confirm.js">





<link rel="preload" as="style" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css">
<link rel="preload" as="style" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css">
<link rel="preload" as="style" href="ext/960gs/960_24_col.css">
<link rel="preload" as="style" href="stylesheet.css">
<link rel="preload" as="style" href="colorbox3.css">
<link rel="preload" as="style" href="jquery/jquery.simplyscroll.css">



<link rel="preload" as="image" href="https://www.sneezeguard.com/images/new_logo_main.png">
<link rel="preload" as="image" href="images/wishlist_icon.png">
<link rel="preload" as="image" href="image_new/office.png">
<link rel="preload" as="image" href="image_new/banking.png">
<link rel="preload" as="image" href="image_new/grossery.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/1.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/3.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/5.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/16.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/21.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/22.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/23.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/24.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/25.png">


<link rel="preload" as="image" href="img/close.png">
<link rel="preload" as="image" href="img/mail/mail_img.png">
<link rel="preload" as="image" href="social_icons/rss-icon.png">
<link rel="preload" as="image" href="img/social/facebook.png">
<link rel="preload" as="image" href="img/social/twittee.png">
<link rel="preload" as="image" href="img/social/linklind.png">
<link rel="preload" as="image" href="img/social/pintrest.png">
<link rel="preload" as="image" href="img/social/instagram.png">
<link rel="preload" as="image" href="img/social/youtube.png">




<link rel="preload" as="image" href="images/bulletpt.jpg">
<link rel="preload" as="image" href="images/logo.jpg">
<link rel="preload" as="image" href="images/navbar/homeup.jpg">
<link rel="preload" as="image" href="images/navbar/instockup.jpg">
<link rel="preload" as="image" href="images/navbar/customup.jpg">
<link rel="preload" as="image" href="images/navbar/adjustableup.jpg">
<link rel="preload" as="image" href="images/navbar/portableup.jpg">
<link rel="preload" as="image" href="images/navbar/orbitup.jpg">
<link rel="preload" as="image" href="images/navbar/shelvingup.jpg">
<link rel="preload" as="image" href="background.jpg">
<link rel="preload" as="image" href="images/middleback.jpg">


<link rel="preload" as="image" href="images/Models/Heat-Lamp.jpg">
<link rel="preload" as="image" href="images/shortVertSepIS.jpg">
<link rel="preload" as="image" href="images/Models/Mid-Shelves.jpg">
<link rel="preload" as="image" href="images/Models/Light-Bar.jpg">
<link rel="preload" as="image" href="images/Models/adm_glass.jpg">
<link rel="preload" as="image" href="img/social/new_img_bestprice.jpg">


<link rel="preload" as="image" href="images/backBottom.jpg">
<link rel="preload" as="image" href="images/comstr.jpg">
<link rel="preload" as="image" href="images/nsflogo.jpg">











<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="Maintain social distance and physical separation in Hospital, Grocery, Office, Counter while protecting from virus and germs with our clear glasss barriers.">
<meta name="keywords" content="Buffet Sneeze Guards, addon sneeze guard, Commercial Kitchen Equipment online, online restaurant supply store, Shop sneeze guard, Manufacturer Sneeze Guard">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">

<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>


<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />

<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />





<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

	
 
 <?php
  if (!$detect->isMobile())
{
	//echo'<td id="ex1" align=center width="190" valign="top">';
}
else{
	echo'<td id="ex1" align=center width="190" valign="top">';

}

?>
<?php
    $portable_id="";
     $category_list=tep_db_query("select distinct parent_id as pid from ".TABLE_CATEGORIES." where parent_id!=0");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
      	$category_list_array[$i]=$category_list_row['pid'];
    	$i++;
      }
       $ids=implode(', ', $category_list_array);
      
      $category_list=tep_db_query("select c.categories_id as pid, cd.categories_name as cname from ".TABLE_CATEGORIES." as c, ".TABLE_CATEGORIES_DESCRIPTION." as cd where c.categories_id in (".$ids.") and c.categories_id=cd.categories_id order by sort_order");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
        if($category_list_row['cname']=="Portable"){
      	     $portable_id=$category_list_row['pid'];
        }
      }
      
      $category_list=tep_db_query("select c.categories_id as pid from ".TABLE_CATEGORIES." as c where c.parent_id=".$portable_id." order by sort_order");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
	     $category_list_array[$i]=$category_list_row['pid'];
         $i++;
      }
     
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
   
  $i=0;
    // Display all models
	
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == "Orbit-720" || $row["Name"] == "Heat-Lamp" || $row["Name"] == "Mid-Shelves" || $row["Name"] == "Light-Bar"){
	if($row["Name"] == "EP-950-ACRYLIC"){
	   $category_list_array[$i]="70";
	} if($row["Name"] == "Heat-Lamp"){
	   $category_list_array[$i]="120";
	} if($row["Name"] == "Light-Bar"){
	   $category_list_array[$i]="121";
	}  if($row["Name"] == "Mid-Shelves"){
	   $category_list_array[$i]="118";
	}
if($row["Name"] == "B-950P-GLASS"){
	   $category_list_array[$i]="118";
	}
	$Height = $row["Height"];
	$lenght=$row["FlangeSize"];
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      $FrontGlass = $row["FrontGlass"];
      $Tubing = $row["Tubing"];
      echo('<P>');
      echo('<TABLE WIDTH="751" HEIGHT="110" BORDER="0" class="custom" CELLPADDING=5>');
      echo('<tr>');
      echo('<td align=right width=20%>');
	   if($row["Name"] == "ALLIN1"){
	        echo('<A HREF="javascript:submit()">' . '<IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/Models/' . $row["Name"] . '.jpg" BORDER=0>' . '</A>');
	   }else{
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].'&mid='.$category_list_array[$i]).'">' . '<IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/Models/' . $row["Name"] . '.jpg" BORDER=0>' . '</A>');
	  }
      echo('</td>');
      echo('<td width=5 align=right>');
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      echo('</td>');
      echo('<td valign=top>');
      echo('<div class="linkClass">');
	  if($row["Name"] == "ALLIN1"){
	   echo('<A HREF="javascript:submit()">Model Name: ' . $row["Name"] . '</A><BR>');
	  }
	  else{
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].'&mid='.$category_list_array[$i]).'">Model Name: ' . $row["Name"] . '</A><BR>');}
      echo('</div>');
      echo('<div class="modText">');
	  if($row["Name"] == "Mid-Shelves"){
      echo('T-Clamp: ' . $Height . '<br>');
	  echo('Length: ' . $lenght . '<br>');
	  } else{
	  echo('Height: ' . $Height . '<br>');
	  }
	  if($row["Name"] == "Heat-Lamp" || $row["Name"] == "Light-Bar"){
	  if($Depth != "0"){
      echo('Electrical: ' . $Depth . '<br>');
      }
	  echo('FlangeSize: ' . $FrontGlass . '<br>');
	  }else{
      if($Depth != "0"){
      echo('Depth: ' . $Depth . '<br>');
      }
	  echo('Glass: ' . $FrontGlass . '<br>');
	  }
      
      echo('Tubing: ' . $Tubing . '<br>');
      echo('</div>');
      echo('</td>');
      echo('<td valign=center align=left width="5">');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      //}
      echo('</td>');
      echo('<td valign=top width=11%>');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<div class="modName">');
      echo("<B>Options:</B><BR>");
      echo('</div>');
      echo('<div class="modText">');
      if($EndPan != "0")
      echo("End Panels " . "<br>");
      if($HeatLamps != "0")
      echo("Heat Lamps " . "<br>");
      if($Lighting != "0")
      echo("Lighting " . "<br>");
      //}
      //echo("Finishes");
      echo('</div>');
      echo('</td>');
      echo('</tr>');
      echo('</TABLE>');
      echo('<P>');
      $i++;
      }
	   //print_r($category_list_array[$i]);
      }
      
?>
<TABLE WIDTH="751" HEIGHT="110" BORDER="0" class="custom" CELLPADDING=5>
<tr>
      <td align=right width=20%>
	 
     <A HREF="https://www.admglass.com/" target="blank"><IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/Models/adm_glass.jpg" BORDER=0 style="width:110px;"></A>
	 
    </td>
     <td width=5 align=right>
     <IMG SRC="">
     </td>
      <td valign=top>
     <div class="linkClass">
	 
	  
	        <A  onclick="myFunction33()" target="blank" style="color:#c38731;font-size: 13px;line-height: 22pxtext-decoration: none;font-weight: bold;">ADM GLASS</A><BR>

      </div>
      <div class="modText">
	<!-- ADM GLASS provides you with CUSTOM CUT, CUSTOM GLASS of desired shape and size according to customer needs. We have a variety of glass products in our glass shop such as Glass Table Top, Tempered Glass, Glass Replacement, Glass Shelf, Beveled Glass which are best in quality .You can easily place orders online through our website.-->
	 
	 ADM GLASS provides you with CUSTOM CUT and size according to your needs. We have a variety of glass products in our glass shop such as Glass Table Top, Tempered Glass, Glass Replacement, Glass Shelves and including shower doors  .You can easily place orders online through our website.
      </div>
      </td>
      <td valign=center align=left width="5">
    <IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/shortVertSep.jpg">
     
      </td>
     <td valign=top width=11%>
      
      </div>
      </td>
     </tr>
</TABLE>
<script>
function myFunction33() {
    var myWindow = window.open("https://www.admglass.com/", "", "width=1500,height=1100");
}
</script>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>