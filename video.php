<!DOCTYPE html>
<html>
<head>


<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

  <meta charset="utf-8" />
<?php  
  
  $modelllname=$_GET['name'];
  if(strpos($modelllname,'EP-950') !== false){
  
  ?>
   <title>Sneeze Guard Feature Model | <?php echo$modelllname; ?> | ADM Sneezeguards</title>
  
  
  <?php
  }
  else{
  ?>
  
  <title>Sneeze Guard Feature Model | <?php echo$modelllname; ?> Guards | ADM Sneezeguards</title>
  
  <?php
  }
  ?>
  
  
  <meta name="description" content="Sneeze Guard Model design <?php echo$modelllname; ?> is perfect for your Restaurants, Bakeries etc store business. Choose online all Glass Shield <?php echo$modelllname; ?> designs for reduce germs and virus.">
  
  <meta name="keywords" content="Sneeze Guard <?php echo$modelllname; ?>, Sneeze Guard <?php echo$modelllname; ?> Model, Sneeze Guard Designs, Sneeze Guard Custom for Business">
 

<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">

<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />




 <!-- Include the VideoJS Library -->
  <script src="video.js" type="text/javascript" charset="utf-8"></script>

  <script type="text/javascript">
    // Must come after the video.js library

    // Add VideoJS to all video tags on the page when the DOM is ready
    VideoJS.setupAllWhenReady();

    /* ============= OR ============ */

    // Setup and store a reference to the player(s).
    // Must happen after the DOM is loaded
    // You can use any library's DOM Ready method instead of VideoJS.DOMReady

    /*
    VideoJS.DOMReady(function(){
      
      // Using the video's ID or element
      var myPlayer = VideoJS.setup("example_video_1");
      
      // OR using an array of video elements/IDs
      // Note: It returns an array of players
      var myManyPlayers = VideoJS.setup(["example_video_1", "example_video_2", video3Element]);

      // OR all videos on the page
      var myManyPlayers = VideoJS.setup("All");

      // After you have references to your players you can...(example)
      myPlayer.play(); // Starts playing the video for this player.
    });
    */

    /* ========= SETTING OPTIONS ========= */

    // Set options when setting up the videos. The defaults are shown here.

    /*
    VideoJS.setupAllWhenReady({
      controlsBelow: false, // Display control bar below video instead of in front of
      controlsHiding: true, // Hide controls when mouse is not over the video
      defaultVolume: 0.85, // Will be overridden by user's last volume if available
      flashVersion: 9, // Required flash version for fallback
      linksHiding: true // Hide download links when video is supported
    });
    */

    // Or as the second option of VideoJS.setup
    
    /*
    VideoJS.DOMReady(function(){
      var myPlayer = VideoJS.setup("example_video_1", {
        // Same options
      });
    });
    */

  </script>

  <!-- Include the VideoJS Stylesheet -->
  <link rel="stylesheet" href="video-js.css" type="text/css" media="screen" title="Video JS">
</head>
<body  bgcolor="#000000">




  
  
<?php
require_once("Mobile_Detect.php");
$detect = new Mobile_Detect();
if (!$detect->isMobile())
{
?>


  <!-- Begin VideoJS -->
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
	<?php 
	if($_GET["name"]=='ES90' && $_GET["type"]=='adj'){ ?>
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" controls loop autoplay >
      <source src="images/ES90/adj.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /*<source src="images/ES90/adj.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES90/adj.ogv" type='video/ogg; codecs="theora, vorbis"' />
	  
	  <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
	  
    </video>
	<?php }
	
	else if($_GET["name"]=='ES90' && $_GET["type"]=='anim'){
	?>
	<video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay>
      <source src="images/ES90/anim.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /* <source src="images/ES90/anim.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES90/anim.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
     <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
    </video>
		<?php
	}
	else if($_GET["name"]=='ES47' && $_GET["type"]=='anim'){ ?>
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" controls loop autoplay >
      <source src="images/ES47/anim.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /*  <source src="images/ES47/anim.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES47/anim.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
     <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>*/
	  ?>
    </video>
	<?php }
	else if($_GET["name"]=='ES47' && $_GET["type"]=='adj'){ ?>
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" controls loop autoplay >
      <source src="images/ES47/adj.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /* <source src="images/ES47/adj.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES47/adj.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>*/
	  ?>
    </video>
	<?php }
	else{?>
		<video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay>
      <source src="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
       <?php
      /*<source src="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
    </video>
		<?php }?>
    <!-- Download links provided for devices that can't play video in the browser. -->
    <p class="vjs-no-video"><strong>Download Video:</strong>
      <a href="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4">MP4</a>,
      <a href="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.webm">WebM</a>,
      <a href="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.ogv">Ogg</a><br>
      <!-- Support VideoJS by keeping this link. -->
      </p>
  </div>
  <!-- End VideoJS -->


  <?php

}
else
{
?>




  <style>
  #example_video_1{
	width: 906px !important;
}


div.vjs-big-play-button {
    display: none;
    z-index: 2;
    position: absolute;
    top: 187%;
    left: 50%;
    width: 80px;
    height: 80px;
    margin: -43px 0 0 -43px;
    text-align: center;
    vertical-align: center;
    cursor: pointer !important;
    border: 3px solid #fff;
    opacity: 0.9;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    background-color: #0B151A;
    background: #1F3744 -webkit-gradient(linear, left top, left bottom, from(#0B151A), to(#1F3744)) left 40px;
    background: #1F3744 -moz-linear-gradient(top, #0B151A, #1F3744) left 40px;
    box-shadow: 4px 4px 8px #000;
    -webkit-box-shadow: 4px 4px 8px #000;
    -moz-box-shadow: 4px 4px 8px #000;
}
  </style>

  <!-- Begin VideoJS -->
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
	<?php 
	if($_GET["name"]=='ES90' && $_GET["type"]=='adj'){ ?>
    <video id="example_video_1" class="video-js" width="840" height="680" controls="controls" preload="auto" poster="pic.jpg" controls loop autoplay >
      <source src="images/ES90/adj.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /*<source src="images/ES90/adj.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES90/adj.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="680" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="840" height="680" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
    </video>
	<?php }
	
	else if($_GET["name"]=='ES90' && $_GET["type"]=='anim'){
	?>
	<video id="example_video_1" class="video-js" width="840" height="680" controls="controls" preload="auto" poster="pic.jpg" autoplay>
      <source src="images/ES90/anim.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /*<source src="images/ES90/anim.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES90/anim.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="840" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="840" height="680" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
    </video>
		<?php
	}
	else if($_GET["name"]=='ES47' && $_GET["type"]=='anim'){ ?>
    <video id="example_video_1" class="video-js" width="840" height="680" controls="controls" preload="auto" poster="pic.jpg" controls loop autoplay >
      <source src="images/ES47/anim.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /*<source src="images/ES47/anim.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES47/anim.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="840" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="840" height="680" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
    </video>
	<?php }
	else if($_GET["name"]=='ES47' && $_GET["type"]=='adj'){ ?>
    <video id="example_video_1" class="video-js" width="840" height="680" controls="controls" preload="auto" poster="pic.jpg" controls loop autoplay >
      <source src="images/ES47/adj.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /*<source src="images/ES47/adj.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/ES47/adj.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="840" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="840" height="680" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
    </video>
	<?php }
	else{?>
		<video controls autoplay id="example_video_1" class="video-js" width="840" height="680" controls="controls" preload="auto" poster="pic.jpg" autoplay>
      <source src="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <?php
      /*<source src="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="840" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4","autoPlay":false,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="840" height="680" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  */
	  ?>
    </video>
		<?php }?>
    <!-- Download links provided for devices that can't play video in the browser. -->
    <p class="vjs-no-video"><strong>Download Video:</strong>
      <a href="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.mp4">MP4</a>,
      <a href="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.webm">WebM</a>,
      <a href="images/<?php echo $_GET["name"]; ?>/<?php echo $_GET["type"]; ?>.ogv">Ogg</a><br>
      <!-- Support VideoJS by keeping this link. -->
      </p>
  </div>
  <!-- End VideoJS -->


<?
}
?>



  
  
  
  
  <h1 style="font-size: 1px;">Sneeze Guard commercial custom Features | <?php echo$modelllname; ?></h1>
  
  
  
  
  
  
  
</body>
</html>