<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Esneezeguard Vedio Player</title>
  <script src="video.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    VideoJS.setupAllWhenReady();
  </script>
<link rel="stylesheet" href="video-js.css" type="text/css" media="screen" title="Video JS" />
</head>
<body  bgcolor="#000000">
  
  
  
<?php
require_once("Mobile_Detect.php");
$detect = new Mobile_Detect();
if (!$detect->isMobile())
{
?>

  <div class="video-js-box" style="height:200px;">
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay >
      <source src="images/flange_under_counter.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="images/flange_under_counter.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/flange_under_counter.ogv" type='video/ogg; codecs="theora, vorbis"' />
	  <source src="images/flange_under_counter.swf"/>
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="480" type="application/x-shockwave-flash"
        data="images/flange_under_counter.swf">
        <param name="movie" value="images/flange_under_counter.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/flange_under_counter.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
  </div>
      
  <?php

}
else
{
?>
<style>
  #example_video_1{
	width: 906px !important;
}


div.vjs-big-play-button {
    display: none;
    z-index: 2;
    position: absolute;
    top: 187%;
    left: 50%;
    width: 80px;
    height: 80px;
    margin: -43px 0 0 -43px;
    text-align: center;
    vertical-align: center;
    cursor: pointer !important;
    border: 3px solid #fff;
    opacity: 0.9;
    border-radius: 20px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    background-color: #0B151A;
    background: #1F3744 -webkit-gradient(linear, left top, left bottom, from(#0B151A), to(#1F3744)) left 40px;
    background: #1F3744 -moz-linear-gradient(top, #0B151A, #1F3744) left 40px;
    box-shadow: 4px 4px 8px #000;
    -webkit-box-shadow: 4px 4px 8px #000;
    -moz-box-shadow: 4px 4px 8px #000;
}
  </style>
 <div class="video-js-box" style="height:200px;">
    <video id="example_video_1" class="video-js" width="840" height="780" controls="controls" preload="auto" poster="pic.jpg" autoplay >
      <source src="images/flange_under_counter.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="images/flange_under_counter.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/flange_under_counter.ogv" type='video/ogg; codecs="theora, vorbis"' />
	  <source src="images/flange_under_counter.swf"/>
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="840" height="780" type="application/x-shockwave-flash"
        data="images/flange_under_counter.swf">
        <param name="movie" value="images/flange_under_counter.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/flange_under_counter.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <img src="pic.jpg" width="840" height="780" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
  </div>
  
    
<?php
}
?>
  
  
  
  
  
  
</body>
</html>