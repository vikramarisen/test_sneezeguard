<?php
require('includes/application_top.php');
  //require(DIR_WS_INCLUDES . 'template_top.php');




  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>


<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard Latest Models design | Privacy - ADM Sneezeguards</title>
<!-- End Google Add Conversion -->

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="ADM Sneezeguards offers made to order glass barriers and sneeze guards for effective virus broadcast prevention. Online place an order.">
<meta name="keywords" content="social distance Glass Barrier, Physical sepration Glass Models, Sneeze Guard models for office, ADM Sneezeguards Models Privacy">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>


<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />

<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />




<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>


<?
if (!$detect->isMobile())
{
?>

<?
}
else{
?>
<td id="ex1" align=center width="190" valign="top">

<style>
.form_white h2 {
    color: black;
    font-size: 33px;
}
p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 25px;
    /* padding-left: 35px; */
}
</style>
<?
}
?>
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td >

<table border="0" height="40" valign=center align=center BACKGROUND="middleback.jpg">
<tr>
<td align=center>
<font  face="Arial, Helvetica, sans-serif">
<h2>Privacy Notice</h2><br><br>
<table align=center background="squarePriv.jpg" height=99 cellpadding=15>
<tr>
<td>

<p><strong>Information We Collect</strong></p>

<p>The information we gather from customers helps us personalize and continually improve your shopping experience at sneezeguard.com. Here are the types of information we gather.</p>

<p>We receive and store any information you enter on our Web site or give us in any other way. You can choose not to provide certain information, but this can limit your access to many of our features. We use the information that you provide for such purposes as responding to your requests, customizing future shopping for you, improving our stores, and communicating with you.</p>

<p>By providing your email address you agree to receive marketing emails from sneezeguard.com and may unsubscribe at any time. We receive and store certain types of information whenever you interact with us. For example, like many Web sites, we use cookies, and we obtain certain types of information when your Web browser accesses sneezeguard.com.</p>

<p><strong>Cookies</strong></p>

<p>Sneezeguard.com uses "cookies" to improve our customer's shopping experience. The cookie itself does not contain personal information although it will enable us to relate your use of this site to information that you have specifically and knowingly provided.</p>

<p><strong>Information Sharing</strong></p>

<p>Information about our customers is an important part of our business, and we are NOT in the business of selling it to others.</p>

<p>We work in conjunction with other companies and individuals to perform functions on our behalf. Examples include fulfilling orders, delivering orders, and processing credit card payments. They have access to address information needed to perform their functions, but may not use it for other purposes.</p>

<p>We release account and other personal information when we believe release is appropriate to comply with the law; enforce or apply our Conditions of Use and other agreements; or protect the rights, property, or safety of sneezeguard.com, our users, or others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction. Obviously, however, this does not include selling, renting, sharing, or otherwise disclosing personally identifiable information from customers for commercial purposes in violation of the commitments set forth in this Privacy Notice.</p>

<p><strong>Information Security</strong></p>

<p>We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you input.</p>

<p>We reveal only the last four digits of your credit card numbers when confirming an order. Of course, we transmit the entire credit card number to the appropriate credit card company during order processing.</p>

<p>It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off and close your browser window when finished using a shared computer.</p>

</div>
</font>
</td>
</tr>
</table></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>


<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
