<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Esneezeguard Vedio Player</title>
  <script src="video.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    VideoJS.setupAllWhenReady();
  </script>
<link rel="stylesheet" href="video-js.css" type="text/css" media="screen" title="Video JS" />



  <style>
    
  @media screen and (max-width: 992px) {
  
 #TB_window{
    height: 50% !important;
 }  
  }
  @media screen and (max-width: 768px) {
  
 #TB_window{
    height: 50% !important;
 }
  }
  @media screen and (max-width: 480px) {
  
 #TB_window{
    height: 50% !important;
 }	 
  #example_video_1{   
 width: 320px !important;
    height: 240px !important;
	}
	
  }
  </style>
</head>
<body  bgcolor="#000000">
  
  
  

  <div class="video-js-box" style="height:200px;">
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay >
      <source src="images/flang.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="images/flang.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="images/flang.ogv" type='video/ogg; codecs="theora, vorbis"' />
	  <source src="images/flang.swf"/>
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="480" type="application/x-shockwave-flash"
        data="images/flang.swf">
        <param name="movie" value="images/flang.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "images/flang.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
  </div>
    
  
</body>
</html>