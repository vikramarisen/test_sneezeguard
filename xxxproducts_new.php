<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
  
  and this porject modifed and developed by Arisen Technologies 
  http://www.arisen.in
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCTS_NEW);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_PRODUCTS_NEW));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>

<h1><?php //echo HEADING_TITLE; ?></h1>

<div class="contentContainer">
  <div class="contentText">

<?php
  $products_new_array = array();

  $products_new_query_raw = "select p.products_id, pd.products_name, p.products_image, p.products_price, p.products_tax_class_id, p.products_date_added, m.manufacturers_name from " . TABLE_PRODUCTS . " p left join " . TABLE_MANUFACTURERS . " m on (p.manufacturers_id = m.manufacturers_id), " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' order by p.products_date_added DESC, pd.products_name";
  $products_new_split = new splitPageResults($products_new_query_raw, MAX_DISPLAY_PRODUCTS_NEW);

  if (($products_new_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3'))) {
?>

    <div>
      <span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $products_new_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

      <span><?php echo $products_new_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW); ?></span>
    </div>

    <br />

<?php
  }
?>

<?php
  if ($products_new_split->number_of_rows > 0) {
?>

   

	<table>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><a href="demo.php?name=b950_swivel_commercial&KeepThis=true&TB_iframe=true&height=480&width=640" class="thickbox"  target="_blank"  title="b950_swivel_commercial"><img src="images/data/thumbnails/b950_swivel_commercial.png" /></a></td>
		</tr>
		
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		
		<tr>
			<td align="center"><h1 style="font-size:16px;">REVOLUTION&trade; Series - (COMING SOON)</h1></td>
		</tr>
		
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
		<tr>
			<td align="center"><br /></td>
		</tr>
	</table>

<?php
  } else {
?>

    <div>
      <?php echo TEXT_NO_NEW_PRODUCTS; ?>
    </div>

<?php
  }

  if (($products_new_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
?>

    <!--br />

    <div>
      <span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $products_new_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

      <span><?php echo $products_new_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW); ?></span>
    </div-->

<?php
  }
?>

  </div>
</div>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
