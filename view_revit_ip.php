<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide="visibility: hidden;display: none;";
    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
	require(DIR_WS_INCLUDES . 'function_country_name.php');
?>
    <h2 class="pageHeading">Report Of IP's of Download Revit</h2>

	<a id="dlink"  style="display:none;"></a>
	 <input type="button" onclick="tableToExcel('testTable', 'W3C Example Table', 'Report_IP_of_download_revit.xls')" value="Export to Excel">
	 <br />
	 <br />
	<table border="0" width="100%" cellspacing="0" cellpadding="2" id="testTable" summary="Code page support in different versions of MS Windows." rules="groups" frame="hsides">
	<tr class="dataTableHeadingRow" width="5%">
	<th class="dataTableHeadingContent">Sr. No</th>
	<th class="dataTableHeadingContent">IP Address</th>
	<th class="dataTableHeadingContent">Customer Name</th>
	<th class="dataTableHeadingContent">Customer Email</th>
	<th class="dataTableHeadingContent">City</th>
	<th class="dataTableHeadingContent">State</th>
	<th class="dataTableHeadingContent">Country</th>
	<th class="dataTableHeadingContent">Category</th>
	<th class="dataTableHeadingContent">Bay</th>
	<th class="dataTableHeadingContent">Date</th>
	</tr>
	
	<?php
		$res=tep_db_query("select * from ip_address_revit_file_downloaded order by revit_id desc");//Fetching the popups from database!
		$sr=0;
    	while($row=tep_db_fetch_array($res)){
        	$category_name=$row['category_name'];
        	$customer_id=$row['customer_id'];
			$customer_city=$row['customer_city'];
			$customer_state=$row['customer_state'];
			$customer_country=$row['customer_country'];
			if($customer_id=='0')
			{
			$customer_name=$row['customer_name'];
			$customer_email=$row['customer_email'];
			}
			else{
				$res_cust=tep_db_query("select * from customers where customers_id=".$customer_id."");
				$row_cust=tep_db_fetch_array($res_cust);
				$customer_name=''.$row_cust['customers_firstname'].' '.$row_cust['customers_lastname'].'';
				$customer_email=decrypt_email($row_cust['customers_email_address'], ENCRYPTION_KEY_EMAIL);
			}
			
			
			//$cat_id=$row['category_name'];
			
			
			$sr=$sr+1;
			echo'<tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
			<td width="5%" class="dataTableContent" align="center">'.$sr.'</td>
			<td class="dataTableContent" align="center">'.$row['ip_address'].'</td>
			<td class="dataTableContent" align="center">'.$customer_name.'</td>
			<td class="dataTableContent" align="center">'.$customer_email.'</td>
			<td class="dataTableContent" align="center">'.$customer_city.'</td>
			<td class="dataTableContent" align="center">'.$customer_state.'</td>
			<td class="dataTableContent" align="center">'.code_to_country($customer_country).'</td>
			<td class="dataTableContent" align="center">'.$row['category_name'].'</td>
			<td class="dataTableContent" align="center">'.$row['bay'].'</td>
			<td class="dataTableContent" align="center">'.$row['date'].'</td>
			</tr>';
    	}
    
	?>
	</table>
	

	
	
	<script>
	var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        }, format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    return function (table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {
            worksheet: name || 'Worksheet',
            table: table.innerHTML
        }
   document.getElementById("dlink").href = uri + base64(format(template, ctx));
            document.getElementById("dlink").download = filename;
            document.getElementById("dlink").click();
    }
})()
	</script>
	
	
	
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>