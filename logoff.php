<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGOFF);

  $breadcrumb->add(NAVBAR_TITLE);

  tep_session_unregister('customer_id');
  tep_session_unregister('customer_default_address_id');
  tep_session_unregister('customer_first_name');
  tep_session_unregister('customer_country_id');
  tep_session_unregister('customer_zone_id');
  tep_session_unregister('comments');
  //kgt - discount coupons
  tep_session_unregister('coupon');
  //end kgt - discount coupons 
  $wishlist->removeSession();
  session_destroy();
  unset($_SESSION['product_custom']);
  unset($_SESSION['product_final1']);
// Discount Code 2.7 - start
if (MODULE_ORDER_TOTAL_DISCOUNT_STATUS == 'true' &&
tep_session_is_registered('sess_discount_code')) {
tep_session_unregister('sess_discount_code');
}
// Discount Code 2.7 - end
  $cart->reset();

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script>
	$(document).ready(function(){
		if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
   			$("#ex1").css('width',"100%");
   			$(".price_table").css("text-align","left");
		}
   	});

</script>
 
 <?php
  if (!$detect->isMobile())
{
?>

	<!--<td id="ex1" align=center width="190" valign="top">-->
	
	
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td width="35%"  style="border-right:1px solid #ccc;" valign="top" >
<h2 style=" height:26px;padding:5px 0 0 10px"><?php echo HEADING_TITLE; ?></h2>

<div class="contentContainer" style="text-align: left;">
  <div class="contentText">
    <?php echo TEXT_MAIN; ?>
  </div>

  <div class="buttonSet">
    <span class="buttonAction">
	<a href="index.php"><img style="width: 230px;" src="img/new_icons/continue.png" alt="Continue" title=" Continue " ></a>
	<?php //echo '<a href="'.tep_href_link(FILENAME_DEFAULT).'">'.tep_image_button("continue.gif", IMAGE_BUTTON_CONTINUE, 'button').'</a>'; ?>
	</span>
  </div>
</div>
</td></tr></table></td></tr></table>

<?}



else{
	
?>
<td id="ex1" align=center width="190" valign="top">


<style>
.form_white h2 {
    color: black;
    font-size: 30px;
}
.contentText, .contentText table {
    /* padding: 5px 0 5px 0; */
    font-size: 26px;
    line-height: 1.5;
}
</style>


<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td width="35%"  style="border-right:1px solid #ccc;" valign="top" >
<h2 style=" height:26px;padding:5px 0 0 10px"><?php echo HEADING_TITLE; ?></h2>

<div class="contentContainer" style="text-align: left;">
  <div class="contentText">
    <?php echo TEXT_MAIN; ?>
  </div>

  <div class="" style="text-align:center;    margin-top: 39px;">
    <span>
	<a href="index.php"><img style="width: 322px;" src="img/new_icons/continue.png" alt="Continue" title=" Continue " ></a>
	<?php //echo '<a href="'.tep_href_link(FILENAME_DEFAULT).'">'.tep_image_button("continue.gif", IMAGE_BUTTON_CONTINUE, 'button').'</a>'; ?>
	</span>
  </div>
</div>
</td></tr></table></td></tr></table>

<?
}

?>




<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
