<?php
require('includes/application_top.php');
//  require(DIR_WS_INCLUDES . 'template_top.php');


  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>


<link rel="apple-touch-icon" href="https://www.sneezeguard.com/images/new_logo_180x180.png">


<meta name="theme-color" content="#171717"/>
<link rel="manifest" href="manifest.webmanifest">

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard Desktop & Counter | Shipping - ADM Sneezeguards</title>
<!-- End Google Add Conversion -->





<link rel="preconnect" href="https://www.sneezeguard.com">
<link rel="dns-prefetch" href="https://www.sneezeguard.com">


<link rel="preload" as="script" href="jquery-latest.js">
<link rel="preload" as="script" href="thickbox.js">
<link rel="preload" as="script" href="ext/jquery/jquery-1.4.2.min.js">
<link rel="preload" as="script" href="ext/jquery/ui/jquery-ui-1.8.6.min.js">
<link rel="preload" as="script" href="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js">
<link rel="preload" as="script" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js">
<link rel="preload" as="script" href="jquery.colorbox3.js">
<link rel="preload" as="script" href="./dist/html2canvas.js">
<link rel="preload" as="script" href="./dist/canvas2image.js">
<link rel="preload" as="script" href="https://platform.twitter.com/widgets.js">
<link rel="preload" as="script" href="jquery.confirm/jquery.confirm.js">





<link rel="preload" as="style" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css">
<link rel="preload" as="style" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css">
<link rel="preload" as="style" href="ext/960gs/960_24_col.css">
<link rel="preload" as="style" href="stylesheet.css">
<link rel="preload" as="style" href="colorbox3.css">
<link rel="preload" as="style" href="jquery/jquery.simplyscroll.css">



<link rel="preload" as="image" href="https://www.sneezeguard.com/images/new_logo_main.png">
<link rel="preload" as="image" href="images/wishlist_icon.png">
<link rel="preload" as="image" href="image_new/office.png">
<link rel="preload" as="image" href="image_new/banking.png">
<link rel="preload" as="image" href="image_new/grossery.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/1.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/3.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/5.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/16.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/21.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/22.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/23.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/24.png">
<link rel="preload" as="image" href="sneezegaurd/upload/images/25.png">


<link rel="preload" as="image" href="img/close.png">
<link rel="preload" as="image" href="img/mail/mail_img.png">
<link rel="preload" as="image" href="social_icons/rss-icon.png">
<link rel="preload" as="image" href="img/social/facebook.png">
<link rel="preload" as="image" href="img/social/twittee.png">
<link rel="preload" as="image" href="img/social/linklind.png">
<link rel="preload" as="image" href="img/social/pintrest.png">
<link rel="preload" as="image" href="img/social/instagram.png">
<link rel="preload" as="image" href="img/social/youtube.png">




<link rel="preload" as="image" href="images/bulletpt.jpg">
<link rel="preload" as="image" href="images/logo.jpg">
<link rel="preload" as="image" href="images/navbar/homeup.jpg">
<link rel="preload" as="image" href="images/navbar/instockup.jpg">
<link rel="preload" as="image" href="images/navbar/customup.jpg">
<link rel="preload" as="image" href="images/navbar/adjustableup.jpg">
<link rel="preload" as="image" href="images/navbar/portableup.jpg">
<link rel="preload" as="image" href="images/navbar/orbitup.jpg">
<link rel="preload" as="image" href="images/navbar/shelvingup.jpg">
<link rel="preload" as="image" href="background.jpg">
<link rel="preload" as="image" href="images/middleback.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/In-Stock.jpg">
<link rel="preload" as="image" href="images/shortVertSepIS.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/Custom.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/Adjustable.jpg">
<link rel="preload" as="image" href="sneezegaurd/images/homepage_images/Portables.jpg">
<link rel="preload" as="image" href="img/social/new_img_bestprice.jpg">
<link rel="preload" as="image" href="images/backBottom.jpg">
<link rel="preload" as="image" href="images/comstr.jpg">
<link rel="preload" as="image" href="images/nsflogo.jpg">









<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="ADM Sneezeguards have dozens of line features styles in time in-stock in California and ready to ship in business days.">
<meta name="keywords" content="tempered glass for Desktop, glass panels models to choose, custom line Glass, Snneze Guard in-stock">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>



<link rel="alternate" href="https://www.sneezeguard.com/" hreflang="en-US" />

<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />






<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>





<?
if (!$detect->isMobile())
{
?>

<?
}
else{
?>
<td id="ex1" align=center width="190" valign="top">

<style>
.form_white h2 {
    color: black;
    font-size: 31px;
}
.contentss{
	font-size:22px;
}

p {
    font-family: "Times New Roman", Times, serif;
    font-size: 24px;
    /* padding-left: 35px; */
	font-size: 20px !important;
}

.form_white td {
    background: #FFFFFF !important;
    color: #000000;
    font-size: 22px;
}
.main img{
    width: 736px;
}
</style>
<?
}
?>


<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 
<tr>
<td colspan="3" style="background:url(img/TopBak.jpg) !important ; height:26px; border:1px solid #ccc;">&nbsp;</td>
</tr>
<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tbody><tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td class="pageHeading"><h2>Shipping &amp; Returns</h2></td>
          </tr>
        </tbody></table></td>
      </tr>
      <tr>
        <td><img alt="sneeze guard" title="sneeze guard" src="images/pixel_trans.gif" border="0" alt="" width="100%" height="10"></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tbody><tr>
            <td class="main">
Transit Time:<p>

</p><div align="center"><img alt="sneeze guard" title="sneeze guard" src="images/transit.jpg"></div><p>

Except where noted, most orders can be shipped on the day following the day which the order was received. We currently ship via FedEx. You will be charged shipping based upon the weight of the products ordered including their packaging.</p><p>

 Our return policy: Products purchased on our web site may be returned within 30 days of receipt of purchase for a refund or exchange. The products must be unused and in original sealed package and un-opened.</p>
 
 <p>Important Exception: Any item or part of any item that has been modified/altered or customized will be considered non-returnable.</p>
 
 <p>Glass orders: All orders with glass are considered custom and are non-returnable item.</p>
 
 <p>Portable orders: Are considered a custom item which are non-returnable. (Due to the fact that they can be used for an event for one time only.)</p><p>

 Units that involve a custom depth measurements (ES29, ES82, & ED20) are non-returnable, as they are built specifically to your specifications.</p><p>

 Refunds will be issued for the amount of the product less a 35% restocking fee. Shipping is NOT refundable.</p><p>

 Please call us prior to returning any item for a return authorization (RMA) number. 1-800-690-0002.  An RMA number may be issued for the return of merchandise.  All returned merchandise must be in resalable condition for a credit to be issued.  Credits may take up to 2-3 weeks to process.</p><p>

REFUNDS will be posted within 10 business days of the date that we receive the returned product. Your credit card company may take up to 30 days to post the credit to your statement.  We will post the credit in the time period we promised, provided our return instructions have been followed.</p><p>

IF YOU RECEIVED FREE SHIPPING and you are returning a non-defective product, you will not receive a full refund. We will credit the price of the item returned less a 20% restocking charge and less the actual shipping costs.</p><p>
</p></td>
          </tr>
        </tbody></table></td>
      </tr>
      <tr>
        <td><img alt="sneeze guard" title="sneeze guard" src="images/pixel_trans.gif" border="0" alt="" width="100%" height="10"></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          <tbody><tr class="infoBoxContents">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tbody><tr>
                <td width="10"><img alt="sneeze guard" title="sneeze guard" src="images/pixel_trans.gif" border="0" alt="" width="10" height="1"></td>
                <td align="right"></td>
                <td width="10"><img alt="sneeze guard" title="sneeze guard" src="images/pixel_trans.gif" border="0" alt="" width="10" height="1"></td>
              </tr>
            </tbody></table></td>
          </tr>
        </tbody></table></td>
      </tr>
    </tbody></table></td>
<!-- body_text_eof //-->
  </tr>
</tbody></table>
</td></tr></table></td></tr></table></div>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
